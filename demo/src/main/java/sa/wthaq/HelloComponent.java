package sa.wthaq;

import org.springframework.stereotype.Component;

@Component
public class HelloComponent {
    public void go() {
        System.out.println("Inside filter's component");
    }
}