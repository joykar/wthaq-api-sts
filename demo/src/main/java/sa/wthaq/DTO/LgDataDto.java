package sa.wthaq.DTO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("deprecation")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
public class LgDataDto {
	
	String lg_type;
	String terms_conditions;
	String contract_value;
	String bank_name;
	String bank_code;
	String lg_startDate;
	String lg_endDate;
	String project_id;
	String project_name;
	String zakat_start_date;
	String zakat_end_date;
	String purposeOfBond;
	String ibanNo;
	String byan_no;
	String active_status;
	String lg_issuedBy;
	String lg_createBy;
	String terms_condition_message;
	String response_code;
	String lg_validity_date;
	String agree_tag;
	
	
	
	//for reponse added
	String lgAmount;
	String lgTypeName;
	String beneficiaryName;
	String projectName;
	String bankName;
	String lgStatus;
	String reqDate;
	String currencyCode;
	String currency;
	String requestId;
	
	
	
	
	
	
	
	public String getLg_validity_date() {
		return lg_validity_date;
	}
	public void setLg_validity_date(String lg_validity_date) {
		this.lg_validity_date = lg_validity_date;
	}
	public String getLg_type() {
		return lg_type;
	}
	public void setLg_type(String lg_type) {
		this.lg_type = lg_type;
	}
	public String getTerms_conditions() {
		return terms_conditions;
	}
	public void setTerms_conditions(String terms_conditions) {
		this.terms_conditions = terms_conditions;
	}
	public String getContract_value() {
		return contract_value;
	}
	public void setContract_value(String contract_value) {
		this.contract_value = contract_value;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public String getBank_code() {
		return bank_code;
	}
	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}
	public String getLg_startDate() {
		return lg_startDate;
	}
	public void setLg_startDate(String lg_startDate) {
		this.lg_startDate = lg_startDate;
	}
	public String getLg_endDate() {
		return lg_endDate;
	}
	public void setLg_endDate(String lg_endDate) {
		this.lg_endDate = lg_endDate;
	}
	public String getProject_id() {
		return project_id;
	}
	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public String getZakat_start_date() {
		return zakat_start_date;
	}
	public void setZakat_start_date(String zakat_start_date) {
		this.zakat_start_date = zakat_start_date;
	}
	public String getZakat_end_date() {
		return zakat_end_date;
	}
	public void setZakat_end_date(String zakat_end_date) {
		this.zakat_end_date = zakat_end_date;
	}
	public String getPurposeOfBond() {
		return purposeOfBond;
	}
	public void setPurposeOfBond(String purposeOfBond) {
		this.purposeOfBond = purposeOfBond;
	}
	public String getIbanNo() {
		return ibanNo;
	}
	public void setIbanNo(String ibanNo) {
		this.ibanNo = ibanNo;
	}
	public String getByan_no() {
		return byan_no;
	}
	public void setByan_no(String byan_no) {
		this.byan_no = byan_no;
	}
	public String getActive_status() {
		return active_status;
	}
	public void setActive_status(String active_status) {
		this.active_status = active_status;
	}
	public String getLg_issuedBy() {
		return lg_issuedBy;
	}
	public void setLg_issuedBy(String lg_issuedBy) {
		this.lg_issuedBy = lg_issuedBy;
	}
	public String getLg_createBy() {
		return lg_createBy;
	}
	public void setLg_createBy(String lg_createBy) {
		this.lg_createBy = lg_createBy;
	}
	public String getLgAmount() {
		return lgAmount;
	}
	public void setLgAmount(String lgAmount) {
		this.lgAmount = lgAmount;
	}
	public String getLgTypeName() {
		return lgTypeName;
	}
	public void setLgTypeName(String lgTypeName) {
		this.lgTypeName = lgTypeName;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getLgStatus() {
		return lgStatus;
	}
	public void setLgStatus(String lgStatus) {
		this.lgStatus = lgStatus;
	}
	public String getReqDate() {
		return reqDate;
	}
	public void setReqDate(String reqDate) {
		this.reqDate = reqDate;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getTerms_condition_message() {
		return terms_condition_message;
	}
	public void setTerms_condition_message(String terms_condition_message) {
		this.terms_condition_message = terms_condition_message;
	}
	public String getResponse_code() {
		return response_code;
	}
	public void setResponse_code(String response_code) {
		this.response_code = response_code;
	}
	
	public String getAgree_tag() {
		return agree_tag;
	}
	public void setAgree_tag(String agree_tag) {
		this.agree_tag = agree_tag;
	}
	
	

}
