package sa.wthaq.DTO;

 
 
public interface EntityModelPrivate {
	
	public String getCr_no();  
	public String getEntity_name();
	public String getAddress();
    public String getRep_name();
    public String getRep_mobile_no();
	public String getRep_email();
	public String getNational_id();

	public String getCr_type(); 
	public String getCr_status(); 
	public String getCr_issue_date(); 
	public String getCr_expiry_date(); 
	 
	public String getRep_national_id(); 
	public String getComm_reg_doc_name(); 
	public String getComm_reg_doc_path(); 
	public String getAuth_lettter_doc_name(); 
	public String getAuth_lettter_doc_path(); 
	public String getLeg_agree_doc_name(); 
	public String getLeg_agree_doc_path(); 
	public String getRep_dob(); 

}
