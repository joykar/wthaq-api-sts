package sa.wthaq.DTO;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@SuppressWarnings("deprecation")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LgListWithChangesAcceptSupplier {
	
	private String lg_type	;
	private String terms_condition_type	;
	private String contract_value	;
	private String bank_code;	
	private String bank_name;	
	private String lg_start_date;	
	private String lg_end_date	;
	private String project_id	;
	private String project_name;	
	private String 	zakat_start_date;
	private String zakat_end_date;	
	private String request_date	;
	private String purpose_of_bond	;
	private String iban_no	;
	private String byan_no;	
	private String active_status;	
	private String lg_issued_by	;
	private String create_by	;
	private String create_on;	
	private String supplier_name;	
	private String request_status_flag	;
	private String currency	;
	private String currency_code;
	@Id
	private String request_id;
	public String getLg_type() {
		return lg_type;
	}
	public void setLg_type(String lg_type) {
		this.lg_type = lg_type;
	}
	public String getTerms_condition_type() {
		return terms_condition_type;
	}
	public void setTerms_condition_type(String terms_condition_type) {
		this.terms_condition_type = terms_condition_type;
	}
	public String getContract_value() {
		return contract_value;
	}
	public void setContract_value(String contract_value) {
		this.contract_value = contract_value;
	}
	public String getBank_code() {
		return bank_code;
	}
	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public String getLg_start_date() {
		return lg_start_date;
	}
	public void setLg_start_date(String lg_start_date) {
		this.lg_start_date = lg_start_date;
	}
	public String getLg_end_date() {
		return lg_end_date;
	}
	public void setLg_end_date(String lg_end_date) {
		this.lg_end_date = lg_end_date;
	}
	public String getProject_id() {
		return project_id;
	}
	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public String getZakat_start_date() {
		return zakat_start_date;
	}
	public void setZakat_start_date(String zakat_start_date) {
		this.zakat_start_date = zakat_start_date;
	}
	public String getZakat_end_date() {
		return zakat_end_date;
	}
	public void setZakat_end_date(String zakat_end_date) {
		this.zakat_end_date = zakat_end_date;
	}
	public String getRequest_date() {
		return request_date;
	}
	public void setRequest_date(String request_date) {
		this.request_date = request_date;
	}
	public String getPurpose_of_bond() {
		return purpose_of_bond;
	}
	public void setPurpose_of_bond(String purpose_of_bond) {
		this.purpose_of_bond = purpose_of_bond;
	}
	public String getIban_no() {
		return iban_no;
	}
	public void setIban_no(String iban_no) {
		this.iban_no = iban_no;
	}
	public String getByan_no() {
		return byan_no;
	}
	public void setByan_no(String byan_no) {
		this.byan_no = byan_no;
	}
	public String getActive_status() {
		return active_status;
	}
	public void setActive_status(String active_status) {
		this.active_status = active_status;
	}
	public String getLg_issued_by() {
		return lg_issued_by;
	}
	public void setLg_issued_by(String lg_issued_by) {
		this.lg_issued_by = lg_issued_by;
	}
	public String getCreate_by() {
		return create_by;
	}
	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}
	public String getCreate_on() {
		return create_on;
	}
	public void setCreate_on(String create_on) {
		this.create_on = create_on;
	}
	public String getSupplier_name() {
		return supplier_name;
	}
	public void setSupplier_name(String supplier_name) {
		this.supplier_name = supplier_name;
	}
	public String getRequest_status_flag() {
		return request_status_flag;
	}
	public void setRequest_status_flag(String request_status_flag) {
		this.request_status_flag = request_status_flag;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCurrency_code() {
		return currency_code;
	}
	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}
	public String getRequest_id() {
		return request_id;
	}
	public void setRequest_id(String request_id) {
		this.request_id = request_id;
	}
	
	
	
	
	
	

}
