package sa.wthaq.DTO;

public class MciRequestData {
	
	private String dob;
	private int cr_no;
	private String NIN;
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public int getCr_no() {
		return cr_no;
	}
	public void setCr_no(int cr_no) {
		this.cr_no = cr_no;
	}
	public String getNIN() {
		return NIN;
	}
	public void setNIN(String nIN) {
		NIN = nIN;
	}
	
	
	

}
