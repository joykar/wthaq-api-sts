package sa.wthaq.DTO;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("deprecation")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
@Entity
public class EntityModel {
	@Id
	private int user_id;
	//private String role_Name;
	private String role_id;
	private String login_id;
	private String user_name;
	private String user_status;
	private String uniq_no;
	private int entity_id;
	private String contact;
	private String email;
	private String reg_date;
	//private String ben_contact;
	//private String ben_email;
	//private String ben_reg_date;
	//private String beneficiary_id;
	//private String beneficiary_UID;
	
	
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	
	public String getUser_status() {
		return user_status;
	}
	public void setUser_status(String user_status) {
		this.user_status = user_status;
	}
	/*
	 * public String getCrn_no() { return cr_no; } public void setCrn_no(String
	 * crn_no) { this.cr_no = crn_no; } public String getSupplier_id() { return
	 * supplier_id; } public void setSupplier_id(String supplier_id) {
	 * this.supplier_id = supplier_id; } public String getBeneficiary_id() { return
	 * beneficiary_id; } public void setBeneficiary_id(String beneficiary_id) {
	 * this.beneficiary_id = beneficiary_id; }
	 */
	
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getRole_id() {
		return role_id;
	}
	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}
	
	
	public String getUniq_no() {
		return uniq_no;
	}
	public void setUniq_no(String uniq_no) {
		this.uniq_no = uniq_no;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public int getEntity_id() {
		return entity_id;
	}
	public void setEntity_id(int entity_id) {
		this.entity_id = entity_id;
	}

	/*
	 * public String getSup_contact() { return sup_contact; } public void
	 * setSup_contact(String sup_contact) { this.sup_contact = sup_contact; }
	 */
	/*
	 * public String getSup_email() { return sup_email; } public void
	 * setSup_email(String sup_email) { this.sup_email = sup_email; } public String
	 * getSup_reg_date() { return sup_reg_date; } public void setSup_reg_date(String
	 * sup_reg_date) { this.sup_reg_date = sup_reg_date; } public String
	 * getBen_contact() { return ben_contact; } public void setBen_contact(String
	 * ben_contact) { this.ben_contact = ben_contact; } public String getBen_email()
	 * { return ben_email; } public void setBen_email(String ben_email) {
	 * this.ben_email = ben_email; } public String getBen_reg_date() { return
	 * ben_reg_date; } public void setBen_reg_date(String ben_reg_date) {
	 * this.ben_reg_date = ben_reg_date; } public String getBeneficiary_UID() {
	 * return beneficiary_UID; } public void setBeneficiary_UID(String
	 * beneficiary_UID) { this.beneficiary_UID = beneficiary_UID; }
	 */
	
	
	
	
	

}
