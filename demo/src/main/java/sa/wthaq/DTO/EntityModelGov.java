package sa.wthaq.DTO;

 
 
public interface EntityModelGov {
	
	public String getBeneficiary_name();  
	public String getAddress();
    public String getBeneficiary_UID();
    public String getRep_email();
	public String getRep_name();
	public String getRep_mobile_no();
	public String getNational_id();

	 
	public String getBeneficiary_reg_date();
	public String getBeneficiary_email();
	public String getNational_id_doc_name();
	public String getNational_id_doc_path();
	public String getLeg_auth_doc_name();
	public String getLeg_auth_doc_path();
	public String getLeg_agree_doc_name();
	public String getLeg_agree_doc_path();
}
