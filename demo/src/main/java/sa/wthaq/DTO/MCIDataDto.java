package sa.wthaq.DTO;

public class MCIDataDto {
	
	private int cr_no;
	private String cr_type;
	private String cr_status;
	private String address;
	private String city;
	private String entity_name;
	
	
	private String cr_issue_dt;
	private String cr_expiry_dt;
	
	private String fname;
	private String dob;
	private String mob_no;
	private String NIN;
	public int getCr_no() {
		return cr_no;
	}
	public void setCr_no(int cr_no) {
		this.cr_no = cr_no;
	}
	public String getEntity_name() {
		return entity_name;
	}
	public void setEntity_name(String entity_name) {
		this.entity_name = entity_name;
	}
	public String getCr_type() {
		return cr_type;
	}
	public void setCr_type(String cr_type) {
		this.cr_type = cr_type;
	}
	public String getCr_status() {
		return cr_status;
	}
	public void setCr_status(String cr_status) {
		this.cr_status = cr_status;
	}
	public String getCr_issue_dt() {
		return cr_issue_dt;
	}
	public void setCr_issue_dt(String cr_issue_dt) {
		this.cr_issue_dt = cr_issue_dt;
	}
	public String getCr_expiry_dt() {
		return cr_expiry_dt;
	}
	public void setCr_expiry_dt(String cr_expiry_dt) {
		this.cr_expiry_dt = cr_expiry_dt;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getMob_no() {
		return mob_no;
	}
	public void setMob_no(String mob_no) {
		this.mob_no = mob_no;
	}
	public String getNIN() {
		return NIN;
	}
	public void setNIN(String nIN) {
		NIN = nIN;
	}
	
	
	
	
	
	

}
