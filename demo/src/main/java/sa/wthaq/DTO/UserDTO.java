package sa.wthaq.DTO;

 

public class UserDTO {

String id;
String name;
String officialemail;

String supplier_id;
String supplier_name;
String contact_info;
String login_id;
String national_id;   
String national_id_doc_name;  
String rep_email;
String rep_mobile_no;
String comm_reg_doc_name;
String auth_lettter_doc_name;  
String leg_agree_doc_name;
 
String leg_auth_doc_name ; 
String user_name;
String roleType;
String roleName;

 



public String getUser_name() {
	return user_name;
}
public void setUser_name(String user_name) {
	this.user_name = user_name;
}
private long supplierId;

 
private String agreeTag;

 private String authLettterDocName;

 private String authLettterDocPath;

 private String commRegDocName;

 private String commRegDocPath;

 private String contactInfo;

private String country;

 
 

 private String crnNo;

 

 private String legAgreeDocName;

 private String legAgreeDocPath;

 private String loginId;
 

 

 private String nationalIdDocName;

 private String nationalId;
	
 private String nationalIdDocPath;

private String passwd;

 private String pinCode;

 private String repEmail;

 private String repId;

 private String repMobileNo;

 private String repName;

 private String repNameAr;

 private String state;

 private String supplierAddr1;

 private String supplierAddr1Ar;

 private String supplierAddr2;

 private String supplierAddr2Ar;

 private String supplierName;

 private String supplierNameAr;

 private String supplierStatusFlag;

 
 









public String getOfficialemail() {
	return officialemail;
}
public void setOfficialemail(String officialemail) {
	this.officialemail = officialemail;
}
public String getSupplier_id() {
	return supplier_id;
}
public void setSupplier_id(String supplier_id) {
	this.supplier_id = supplier_id;
}
public String getSupplier_name() {
	return supplier_name;
}
public void setSupplier_name(String supplier_name) {
	this.supplier_name = supplier_name;
}
public String getContact_info() {
	return contact_info;
}
public void setContact_info(String contact_info) {
	this.contact_info = contact_info;
}
public String getLogin_id() {
	return login_id;
}
public void setLogin_id(String login_id) {
	this.login_id = login_id;
}
public String getNational_id() {
	return national_id;
}
public void setNational_id(String national_id) {
	this.national_id = national_id;
}
public String getNational_id_doc_name() {
	return national_id_doc_name;
}
public void setNational_id_doc_name(String national_id_doc_name) {
	this.national_id_doc_name = national_id_doc_name;
}
public String getRep_email() {
	return rep_email;
}
public void setRep_email(String rep_email) {
	this.rep_email = rep_email;
}
public String getRep_mobile_no() {
	return rep_mobile_no;
}
public void setRep_mobile_no(String rep_mobile_no) {
	this.rep_mobile_no = rep_mobile_no;
}
public String getComm_reg_doc_name() {
	return comm_reg_doc_name;
}
public void setComm_reg_doc_name(String comm_reg_doc_name) {
	this.comm_reg_doc_name = comm_reg_doc_name;
}
public String getAuth_lettter_doc_name() {
	return auth_lettter_doc_name;
}
public void setAuth_lettter_doc_name(String auth_lettter_doc_name) {
	this.auth_lettter_doc_name = auth_lettter_doc_name;
}
public String getLeg_agree_doc_name() {
	return leg_agree_doc_name;
}
public void setLeg_agree_doc_name(String leg_agree_doc_name) {
	this.leg_agree_doc_name = leg_agree_doc_name;
}
 
public String getLeg_auth_doc_name() {
	return leg_auth_doc_name;
}
public void setLeg_auth_doc_name(String leg_auth_doc_name) {
	this.leg_auth_doc_name = leg_auth_doc_name;
}
public long getSupplierId() {
	return supplierId;
}
public void setSupplierId(long supplierId) {
	this.supplierId = supplierId;
}
public String getAgreeTag() {
	return agreeTag;
}
public void setAgreeTag(String agreeTag) {
	this.agreeTag = agreeTag;
}
public String getAuthLettterDocName() {
	return authLettterDocName;
}
public void setAuthLettterDocName(String authLettterDocName) {
	this.authLettterDocName = authLettterDocName;
}
public String getAuthLettterDocPath() {
	return authLettterDocPath;
}
public void setAuthLettterDocPath(String authLettterDocPath) {
	this.authLettterDocPath = authLettterDocPath;
}
public String getCommRegDocName() {
	return commRegDocName;
}
public void setCommRegDocName(String commRegDocName) {
	this.commRegDocName = commRegDocName;
}
public String getCommRegDocPath() {
	return commRegDocPath;
}
public void setCommRegDocPath(String commRegDocPath) {
	this.commRegDocPath = commRegDocPath;
}
public String getContactInfo() {
	return contactInfo;
}
public void setContactInfo(String contactInfo) {
	this.contactInfo = contactInfo;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getCrnNo() {
	return crnNo;
}
public void setCrnNo(String crnNo) {
	this.crnNo = crnNo;
}
public String getLegAgreeDocName() {
	return legAgreeDocName;
}
public void setLegAgreeDocName(String legAgreeDocName) {
	this.legAgreeDocName = legAgreeDocName;
}
public String getLegAgreeDocPath() {
	return legAgreeDocPath;
}
public void setLegAgreeDocPath(String legAgreeDocPath) {
	this.legAgreeDocPath = legAgreeDocPath;
}
public String getLoginId() {
	return loginId;
}
public void setLoginId(String loginId) {
	this.loginId = loginId;
}
public String getNationalIdDocName() {
	return nationalIdDocName;
}
public void setNationalIdDocName(String nationalIdDocName) {
	this.nationalIdDocName = nationalIdDocName;
}
public String getNationalId() {
	return nationalId;
}
public void setNationalId(String nationalId) {
	this.nationalId = nationalId;
}
public String getNationalIdDocPath() {
	return nationalIdDocPath;
}
public void setNationalIdDocPath(String nationalIdDocPath) {
	this.nationalIdDocPath = nationalIdDocPath;
}
public String getPasswd() {
	return passwd;
}
public void setPasswd(String passwd) {
	this.passwd = passwd;
}
public String getPinCode() {
	return pinCode;
}
public void setPinCode(String pinCode) {
	this.pinCode = pinCode;
}
public String getRepEmail() {
	return repEmail;
}
public void setRepEmail(String repEmail) {
	this.repEmail = repEmail;
}
public String getRepId() {
	return repId;
}
public void setRepId(String repId) {
	this.repId = repId;
}
public String getRepMobileNo() {
	return repMobileNo;
}
public void setRepMobileNo(String repMobileNo) {
	this.repMobileNo = repMobileNo;
}
public String getRepName() {
	return repName;
}
public void setRepName(String repName) {
	this.repName = repName;
}
public String getRepNameAr() {
	return repNameAr;
}
public void setRepNameAr(String repNameAr) {
	this.repNameAr = repNameAr;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public String getSupplierAddr1() {
	return supplierAddr1;
}
public void setSupplierAddr1(String supplierAddr1) {
	this.supplierAddr1 = supplierAddr1;
}
public String getSupplierAddr1Ar() {
	return supplierAddr1Ar;
}
public void setSupplierAddr1Ar(String supplierAddr1Ar) {
	this.supplierAddr1Ar = supplierAddr1Ar;
}
public String getSupplierAddr2() {
	return supplierAddr2;
}
public void setSupplierAddr2(String supplierAddr2) {
	this.supplierAddr2 = supplierAddr2;
}
public String getSupplierAddr2Ar() {
	return supplierAddr2Ar;
}
public void setSupplierAddr2Ar(String supplierAddr2Ar) {
	this.supplierAddr2Ar = supplierAddr2Ar;
}
public String getSupplierName() {
	return supplierName;
}
public void setSupplierName(String supplierName) {
	this.supplierName = supplierName;
}
public String getSupplierNameAr() {
	return supplierNameAr;
}
public void setSupplierNameAr(String supplierNameAr) {
	this.supplierNameAr = supplierNameAr;
}
public String getSupplierStatusFlag() {
	return supplierStatusFlag;
}
public void setSupplierStatusFlag(String supplierStatusFlag) {
	this.supplierStatusFlag = supplierStatusFlag;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getOfficeemail() {
	return officialemail;
}
public void setOfficeemail(String officeemail) {
	this.officialemail = officeemail;
}
public String getRoleType() {
	return roleType;
}
public void setRoleType(String roleType) {
	this.roleType = roleType;
}
public String getRoleName() {
	return roleName;
}
public void setRoleName(String roleName) {
	this.roleName = roleName;
}
	





}
