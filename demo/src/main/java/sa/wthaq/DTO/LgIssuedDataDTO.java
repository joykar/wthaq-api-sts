package sa.wthaq.DTO;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@SuppressWarnings("deprecation")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) 
public class LgIssuedDataDTO {
	
	@Id
	private int request_id;
	private String request_date;
	private String lG_amount;	
	private String lG_type;
	private String beneficiary_name;
	private String project_name;
	private String bank_name;	
	private String status;
	private String start_date;
	private String end_date;
	private String currency;
	private String currency_name;
	private String termsNConditions;
	private String role_id;
	private String lg_no;
	private String create_on;
	
	
	
	public int getRequest_id() {
		return request_id;
	}
	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}
	public String getRequest_date() {
		return request_date;
	}
	public void setRequest_date(String request_date) {
		this.request_date = request_date;
	}
	public String getlG_amount() {
		return lG_amount;
	}
	public void setlG_amount(String lG_amount) {
		this.lG_amount = lG_amount;
	}
	public String getlG_type() {
		return lG_type;
	}
	public void setlG_type(String lG_type) {
		this.lG_type = lG_type;
	}
	public String getBeneficiary_name() {
		return beneficiary_name;
	}
	public void setBeneficiary_name(String beneficiary_name) {
		this.beneficiary_name = beneficiary_name;
	}
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getTermsNConditions() {
		return termsNConditions;
	}
	public void setTermsNConditions(String termsNConditions) {
		this.termsNConditions = termsNConditions;
	}
	public String getRole_id() {
		return role_id;
	}
	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}
	public String getLg_no() {
		return lg_no;
	}
	public void setLg_no(String lg_no) {
		this.lg_no = lg_no;
	}
	public String getCurrency_name() {
		return currency_name;
	}
	public void setCurrency_name(String currency_name) {
		this.currency_name = currency_name;
	}
	public String getCreate_on() {
		return create_on;
	}
	public void setCreate_on(String create_on) {
		this.create_on = create_on;
	}
	
	
	

}
