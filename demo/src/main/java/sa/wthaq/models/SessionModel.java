package sa.wthaq.models;

public class SessionModel {

	
String loginId;       
String	sessionId;
String 	token;
String	sessionStartTime;
String	lastAccessTime;
String	remoteIp;       
String	remoteHost;   
String	remotePort ; 
String	remoteUser;    
String	remoteAgent;






public String getLoginId() {
	return loginId;
}
public void setLoginId(String loginId) {
	this.loginId = loginId;
}
public String getSessionId() {
	return sessionId;
}
public void setSessionId(String sessionId) {
	this.sessionId = sessionId;
}
public String getToken() {
	return token;
}
public void setToken(String token) {
	this.token = token;
}
public String getSessionStartTime() {
	return sessionStartTime;
}
public void setSessionStartTime(String sessionStartTime) {
	this.sessionStartTime = sessionStartTime;
}
public String getLastAccessTime() {
	return lastAccessTime;
}
public void setLastAccessTime(String lastAccessTime) {
	this.lastAccessTime = lastAccessTime;
}
public String getRemoteIp() {
	return remoteIp;
}
public void setRemoteIp(String remoteIp) {
	this.remoteIp = remoteIp;
}
public String getRemoteHost() {
	return remoteHost;
}
public void setRemoteHost(String remoteHost) {
	this.remoteHost = remoteHost;
}
public String getRemotePort() {
	return remotePort;
}
public void setRemotePort(String remotePort) {
	this.remotePort = remotePort;
}
public String getRemoteUser() {
	return remoteUser;
}
public void setRemoteUser(String remoteUser) {
	this.remoteUser = remoteUser;
}
public String getRemoteAgent() {
	return remoteAgent;
}
public void setRemoteAgent(String remoteAgent) {
	this.remoteAgent = remoteAgent;
}
	
	
	
	
}
