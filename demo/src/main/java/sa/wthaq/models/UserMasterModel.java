package sa.wthaq.models;

 
 

public class UserMasterModel extends ModelCommProp {

	

	String loginId;
	Long userId;
	String password;
	RoleMasterModel  roleId;
	String  userName;
	String  userNameAr;
	String  userStatus;
	String  language;
	
	
	
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public RoleMasterModel getRoleId() {
		return roleId;
	}
	public void setRoleId(RoleMasterModel roleId) {
		this.roleId = roleId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserNameAr() {
		return userNameAr;
	}
	public void setUserNameAr(String userNameAr) {
		this.userNameAr = userNameAr;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	
	 
	
}
