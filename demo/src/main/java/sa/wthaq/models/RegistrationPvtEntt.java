package sa.wthaq.models;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

public class RegistrationPvtEntt {

	
	
	String userId;  //
	String password;
	String otp;
	String mobileNo;
	String commRegNo;
	String EntityName;
	String ContactInformation;
	String Address1;
	String Address2;
	String State;
	String PINCode;
	String Country;
	String IBANNumber;
	String BankName;
	String RepresentativeName;
	String RepresentativeID;
	String OfficeEMail;
	String OfficeMobileNumber;
	String name;
	String userName;//
	
	String crFile;
	String rniFile;
	String lalFile;
	String laglFile;
	
	String crFilePath;
	String rniFilePath;
	String lalFilePath;
	String laglFilePath;
	
	MultipartFile crFileContent; 
	MultipartFile rniFileContent;
	MultipartFile lalFileContent;
	MultipartFile laglFileContent;
	
	  
	// String Name;//
	 String Email;//
	 String NationalId;//
	 String Mobile;//
	 String commRegNoMCI;
	
	
	
	

	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getNationalId() {
		return NationalId;
	}
	public void setNationalId(String nationalId) {
		NationalId = nationalId;
	}
	public String getMobile() {
		return Mobile;
	}
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	public String getCommRegNoMCI() {
		return commRegNoMCI;
	}
	public void setCommRegNoMCI(String commRegNoMCI) {
		this.commRegNoMCI = commRegNoMCI;
	}
	public MultipartFile getCrFileContent() {
	return crFileContent;
}
public void setCrFileContent(MultipartFile crFileContent) {
	this.crFileContent = crFileContent;
}
public MultipartFile getRniFileContent() {
	return rniFileContent;
}
public void setRniFileContent(MultipartFile rniFileContent) {
	this.rniFileContent = rniFileContent;
}
public MultipartFile getLalFileContent() {
	return lalFileContent;
}
public void setLalFileContent(MultipartFile lalFileContent) {
	this.lalFileContent = lalFileContent;
}
public MultipartFile getLaglFileContent() {
	return laglFileContent;
}
public void setLaglFileContent(MultipartFile laglFileContent) {
	this.laglFileContent = laglFileContent;
}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCrFilePath() {
		return crFilePath;
	}
	public void setCrFilePath(String crFilePath) {
		this.crFilePath = crFilePath;
	}
	public String getRniFilePath() {
		return rniFilePath;
	}
	public void setRniFilePath(String rniFilePath) {
		this.rniFilePath = rniFilePath;
	}
	public String getLalFilePath() {
		return lalFilePath;
	}
	public void setLalFilePath(String lalFilePath) {
		this.lalFilePath = lalFilePath;
	}
	public String getLaglFilePath() {
		return laglFilePath;
	}
	public void setLaglFilePath(String laglFilePath) {
		this.laglFilePath = laglFilePath;
	}
	
	
	public String getCrFile() {
		return crFile;
	}
	public void setCrFile(String crFile) {
		this.crFile = crFile;
	}
	public String getRniFile() {
		return rniFile;
	}
	public void setRniFile(String rniFile) {
		this.rniFile = rniFile;
	}
	public String getLalFile() {
		return lalFile;
	}
	public void setLalFile(String lalFile) {
		this.lalFile = lalFile;
	}
	public String getLaglFile() {
		return laglFile;
	}
	public void setLaglFile(String laglFile) {
		this.laglFile = laglFile;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getCommRegNo() {
		return commRegNo;
	}
	public void setCommRegNo(String commRegNo) {
		this.commRegNo = commRegNo;
	}
	public String getEntityName() {
		return EntityName;
	}
	public void setEntityName(String entityName) {
		EntityName = entityName;
	}
	public String getContactInformation() {
		return ContactInformation;
	}
	public void setContactInformation(String contactInformation) {
		ContactInformation = contactInformation;
	}
	public String getAddress1() {
		return Address1;
	}
	public void setAddress1(String address1) {
		Address1 = address1;
	}
	public String getAddress2() {
		return Address2;
	}
	public void setAddress2(String address2) {
		Address2 = address2;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getPINCode() {
		return PINCode;
	}
	public void setPINCode(String pINCode) {
		PINCode = pINCode;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getIBANNumber() {
		return IBANNumber;
	}
	public void setIBANNumber(String iBANNumber) {
		IBANNumber = iBANNumber;
	}
	public String getBankName() {
		return BankName;
	}
	public void setBankName(String bankName) {
		BankName = bankName;
	}
	public String getRepresentativeName() {
		return RepresentativeName;
	}
	public void setRepresentativeName(String representativeName) {
		RepresentativeName = representativeName;
	}
	public String getRepresentativeID() {
		return RepresentativeID;
	}
	public void setRepresentativeID(String representativeID) {
		RepresentativeID = representativeID;
	}
	public String getOfficeEMail() {
		return OfficeEMail;
	}
	public void setOfficeEMail(String officeEMail) {
		OfficeEMail = officeEMail;
	}
	public String getOfficeMobileNumber() {
		return OfficeMobileNumber;
	}
	public void setOfficeMobileNumber(String officeMobileNumber) {
		OfficeMobileNumber = officeMobileNumber;
	}
	 
	
	
	
}
