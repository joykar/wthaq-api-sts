package sa.wthaq.models;

import java.math.BigDecimal;

 
public class PaymentModel {

	 
			 /* should come from CMS*/
			BigDecimal  amount;
			String currency;
			String language;
			String customerEmail;
			String orderDescription;     		String loginId;
     	    String requestId;
     	    String returnUrl;
     	   String localTime;
     	    
     	    
     	    /* will be calculated and sent back */
     	    
     	  String   command ;
     	  String   access_code ;
     	  String   merchant_identifier ;
     	  String   merchant_reference ;
      	  String   signature ;
       	  String   SHARequestPhrase;
     	  String   SHAType;
     	  
     	 int   paymentID;
     	  
     	  /* will come from PayFort after payment operation (irrespective of success or failure)   */
     	  
     	  String responseMessage;
     	  String status;
     	  String responseCode;
     	  
     	  
     	  
     	  
     	  
     	  
     public int getPaymentID() {
			return paymentID;
		}
		public void setPaymentID(int paymentID) {
			this.paymentID = paymentID;
		}
		public String getLocalTime() {
			return localTime;
		}
		public void setLocalTime(String localTime) {
			this.localTime = localTime;
		}
		public String getResponseMessage() {
			return responseMessage;
		}
		public void setResponseMessage(String responseMessage) {
			this.responseMessage = responseMessage;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getResponseCode() {
			return responseCode;
		}
		public void setResponseCode(String responseCode) {
			this.responseCode = responseCode;
		}
		public String getSHARequestPhrase() {
				return SHARequestPhrase;
			}
			public void setSHARequestPhrase(String sHARequestPhrase) {
				SHARequestPhrase = sHARequestPhrase;
			}
			public String getSHAType() {
				return SHAType;
			}
			public void setSHAType(String sHAType) {
				SHAType = sHAType;
			}
	    	public String getOrderDescription() {
			return orderDescription;
		    }
	    	public void setOrderDescription(String orderDescription) {
			this.orderDescription = orderDescription;
	    	}
			public String getReturnUrl() {
				return returnUrl;
			}
			public void setReturnUrl(String returnUrl) {
				this.returnUrl = returnUrl;
			}
			public String getCommand() {
				return command;
			}
			public void setCommand(String command) {
				this.command = command;
			}
			public String getAccess_code() {
				return access_code;
			}
			public void setAccess_code(String access_code) {
				this.access_code = access_code;
			}
			public String getMerchant_identifier() {
				return merchant_identifier;
			}
			public void setMerchant_identifier(String merchant_identifier) {
				this.merchant_identifier = merchant_identifier;
			}
			public String getMerchant_reference() {
				return merchant_reference;
			}
			public void setMerchant_reference(String merchant_reference) {
				this.merchant_reference = merchant_reference;
			}
			 
			public String getSignature() {
				return signature;
			}
			public void setSignature(String signature) {
				this.signature = signature;
			}
		 
		 
	 
     	    
			public BigDecimal getAmount() {
				return amount;
			}
			public void setAmount(BigDecimal amount) {
				this.amount = amount;
			}
			public String getCurrency() {
				return currency;
			}
			public void setCurrency(String currency) {
				this.currency = currency;
			}
			public String getLanguage() {
				return language;
			}
			public void setLanguage(String language) {
				this.language = language;
			}
			public String getCustomerEmail() {
				return customerEmail;
			}
			public void setCustomerEmail(String customerEmail) {
				this.customerEmail = customerEmail;
			}
			 
			public String getLoginId() {
				return loginId;
			}
			public void setLoginId(String loginId) {
				this.loginId = loginId;
			}
			public String getRequestId() {
				return requestId;
			}
			public void setRequestId(String requestId) {
				this.requestId = requestId;
			}
     	    
	
	
	
}
