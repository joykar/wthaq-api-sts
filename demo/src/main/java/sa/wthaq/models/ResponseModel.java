package sa.wthaq.models;

public class ResponseModel {
int responseCode;
String responseMessage;
Object userObject;
String jwtToken;
int logout; 



public String getJwtToken() {
	return jwtToken;
}
public void setJwtToken(String jwtToken) {
	this.jwtToken = jwtToken;
}
public int getLogout() {
	return logout;
}
public void setLogout(int logout) {
	this.logout = logout;
}
public Object getUserObject() {
	return userObject;
}
public void setUserObject(Object userObject) {
	this.userObject = userObject;
}
public int getResponseCode() {
	return responseCode;
}
public void setResponseCode(int success) {
	this.responseCode = success;
}
public String getResponseMessage() {
	return responseMessage;
}
public void setResponseMessage(String responseMessage) {
	this.responseMessage = responseMessage;
}

}
