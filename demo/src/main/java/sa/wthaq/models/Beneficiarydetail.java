package sa.wthaq.models;

import org.springframework.web.multipart.MultipartFile;

public class Beneficiarydetail {

	private String entityname;
	private String entityUID;
	private String Name;
	private String entityNationalID;
	private String entityMobNo;
	private String entityEmail;
	private String address1;
	private String address2;
	private String city;
	private String country;
	private String entityTnC;
	private String userId;
	private MultipartFile laglFile;
	private MultipartFile lalFile;
	private MultipartFile rniFile;
	
	public String getEntityname() {
		return entityname;
	}
	public void setEntityname(String entityname) {
		this.entityname = entityname;
	}
	public String getEntityUID() {
		return entityUID;
	}
	public void setEntityUID(String entityUID) {
		this.entityUID = entityUID;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getEntityNationalID() {
		return entityNationalID;
	}
	public void setEntityNationalID(String entityNationalID) {
		this.entityNationalID = entityNationalID;
	}
	public String getEntityMobNo() {
		return entityMobNo;
	}
	public void setEntityMobNo(String entityMobNo) {
		this.entityMobNo = entityMobNo;
	}
	public String getEntityEmail() {
		return entityEmail;
	}
	public void setEntityEmail(String entityEmail) {
		this.entityEmail = entityEmail;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public MultipartFile getLaglFile() {
		return laglFile;
	}
	public void setLaglFile(MultipartFile laglFile) {
		this.laglFile = laglFile;
	}
	public MultipartFile getLalFile() {
		return lalFile;
	}
	public void setLalFile(MultipartFile lalFile) {
		this.lalFile = lalFile;
	}
	public MultipartFile getRniFile() {
		return rniFile;
	}
	public void setRniFile(MultipartFile rniFile) {
		this.rniFile = rniFile;
	}
	public String getEntityTnC() {
		return entityTnC;
	}
	public void setEntityTnC(String entityTnC) {
		this.entityTnC = entityTnC;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
