package sa.wthaq.models;

public class TabadulUser {

    String name;
    String username;
    String email;
    String mobileNo;
    String nationalId;
    String password;
    
    String addr1;
    String addr2;        
    String ibanNo;
    


    
    public String getPassword() {
            return password;
    }
    public void setPassword(String password) {
            this.password = password;
    }
    public String getName() {
            return name;
    }
    public void setName(String name) {
            this.name = name;
    }
    public String getUsername() {
            return username;
    }
    public void setUsername(String username) {
            this.username = username;
    }
    public String getEmail() {
            return email;
    }
    public void setEmail(String email) {
            this.email = email;
    }
    public String getMobileNo() {
            return mobileNo;
    }
    public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
    }
    public String getNationalId() {
            return nationalId;
    }
    public void setNationalId(String nationalId) {
            this.nationalId = nationalId;
    }
    
    
    
}


