package sa.wthaq.models;

public class RoleMasterModel extends ModelCommProp {
	
	String roleId;        
	String roleDesc;     
	String  roleDescAr;
	
 	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleDesc() {
		return roleDesc;
	}
	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}
	public String getRoleDescAr() {
		return roleDescAr;
	}
	public void setRoleDescAr(String roleDescAr) {
		this.roleDescAr = roleDescAr;
	}
  
}
