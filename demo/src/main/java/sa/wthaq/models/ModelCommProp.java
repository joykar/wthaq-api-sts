package sa.wthaq.models;

 

 
public class ModelCommProp {

	private java.sql.Timestamp createOn;  
	private String createBy;              
	private java.sql.Timestamp modifiedOn;    
	private String  modifiedBy;
	
	
	
	public java.sql.Timestamp getCreateOn() {
		return createOn;
	}
	public void setCreateOn(java.sql.Timestamp createOn) {
		this.createOn = createOn;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public java.sql.Timestamp getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(java.sql.Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	
	
	
}
