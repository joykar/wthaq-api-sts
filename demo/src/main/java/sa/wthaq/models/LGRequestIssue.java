package sa.wthaq.models;

public class LGRequestIssue {

	private String LGType;
	private String writeUpType;
	private String bankCode;
	private String IBAN;
	private String CR;
	private String Amount;
	private String amountInNumber;
	private String currency;
	private String LGStartDate;
	private String LGEndDate;
	private String projectName;
	private String projectNumber;
	private String ZakatPeriodStartDate;
	private String ZakatPeriodEndDate;
	private String puroseOfBond;
	private String bayanNuymber;
	private String checkBox;
	private String userId;
	private String beneficiaryId;
	private String termsNconditionsMessage;
	private String privateEntityCheckBox;
	private String ninNumber;
	private String uidNumber;
	
	private int percentageOfContractValue;
	private String beneficiary_UID;
	
	public String getLGType() {
		return LGType;
	}
	public void setLGType(String lGType) {
		LGType = lGType;
	}
	public String getWriteUpType() {
		return writeUpType;
	}
	public void setWriteUpType(String writeUpType) {
		this.writeUpType = writeUpType;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getIBAN() {
		return IBAN;
	}
	public void setIBAN(String iBAN) {
		IBAN = iBAN;
	}
	public String getCR() {
		return CR;
	}
	public void setCR(String cR) {
		CR = cR;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getAmountInNumber() {
		return amountInNumber;
	}
	public void setAmountInNumber(String amountInNumber) {
		this.amountInNumber = amountInNumber;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getLGStartDate() {
		return LGStartDate;
	}
	public void setLGStartDate(String lGStartDate) {
		LGStartDate = lGStartDate;
	}
	public String getLGEndDate() {
		return LGEndDate;
	}
	public void setLGEndDate(String lGEndDate) {
		LGEndDate = lGEndDate;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectNumber() {
		return projectNumber;
	}
	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}
	public String getZakatPeriodStartDate() {
		return ZakatPeriodStartDate;
	}
	public void setZakatPeriodStartDate(String zakatPeriodStartDate) {
		ZakatPeriodStartDate = zakatPeriodStartDate;
	}
	public String getZakatPeriodEndDate() {
		return ZakatPeriodEndDate;
	}
	public void setZakatPeriodEndDate(String zakatPeriodEndDate) {
		ZakatPeriodEndDate = zakatPeriodEndDate;
	}
	public String getPuroseOfBond() {
		return puroseOfBond;
	}
	public void setPuroseOfBond(String puroseOfBond) {
		this.puroseOfBond = puroseOfBond;
	}
	public String getBayanNuymber() {
		return bayanNuymber;
	}
	public void setBayanNuymber(String bayanNuymber) {
		this.bayanNuymber = bayanNuymber;
	}
	public String getCheckBox() {
		return checkBox;
	}
	public void setCheckBox(String checkBox) {
		this.checkBox = checkBox;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
//	@Override
//	public String toString() {
//		return "LGRequestIssue [LGType=" + LGType + ", writeUpType=" + writeUpType + ", bankName=" + bankCode
//				+ ", IBAN=" + IBAN + ", CR=" + CR + ", Amount=" + Amount + ", amountInNumber=" + amountInNumber
//				+ ", currency=" + currency + ", LGStartDate=" + LGStartDate + ", LGEndDate=" + LGEndDate
//				+ ", projectName=" + projectName + ", projectNumber=" + projectNumber + ", ZakatPeriodStartDate="
//				+ ZakatPeriodStartDate + ", ZakatPeriodEndDate=" + ZakatPeriodEndDate + ", puroseOfBond=" + puroseOfBond
//				+ ", bayanNuymber=" + bayanNuymber + ", checkBox=" + checkBox + ", userId=" + userId + "]";
//	}
//	
	public String getBeneficiaryId() {
		return beneficiaryId;
	}
	public void setBeneficiaryId(String beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}
	public String getTermsNconditionsMessage() {
		return termsNconditionsMessage;
	}
	public void setTermsNconditionsMessage(String termsNconditionsMessage) {
		this.termsNconditionsMessage = termsNconditionsMessage;
	}
	public String getPrivateEntityCheckBox() {
		return privateEntityCheckBox;
	}
	public void setPrivateEntityCheckBox(String privateEntityCheckBox) {
		this.privateEntityCheckBox = privateEntityCheckBox;
	}
	public int getPercentageOfContractValue() {
		return percentageOfContractValue;
	}
	public void setPercentageOfContractValue(int percentageOfContractValue) {
		this.percentageOfContractValue = percentageOfContractValue;
	}
	public String getBeneficiary_UID() {
		return beneficiary_UID;
	}
	public void setBeneficiary_UID(String beneficiary_UID) {
		this.beneficiary_UID = beneficiary_UID;
	}
	public String getNinNumber() {
		return ninNumber;
	}
	public void setNinNumber(String ninNumber) {
		this.ninNumber = ninNumber;
	}
	public String getUidNumber() {
		return uidNumber;
	}
	public void setUidNumber(String uidNumber) {
		this.uidNumber = uidNumber;
	}
	
	
	
}
