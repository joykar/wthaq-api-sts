package sa.wthaq.Utilities;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import sa.wthaq.Entities.TUsermaster;
import sa.wthaq.Entities.TUsermaster2;
import sa.wthaq.models.PaymentModel;
import sa.wthaq.models.SessionModel;

public class JWTUtils {

	private final static String SECRET_KEY="dfbdsalkbfdaslhi45737h(%^%*&*(";
	
	
	public static String createJWT(String id, String issuer, String subject, long ttlMillis, Map<String, Object> payloads) {
		  
	    //The JWT signature algorithm we will be using to sign the token
	    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
	    Date date = new Date();
	    long t = date.getTime();
	    Date expirationTime = new Date(/*t + 5000l*/); // set 5 seconds
	   // long nowMillis = System.currentTimeMillis();
	    //Date now = new Date(nowMillis);

	    Date now = new Date( );
	    Calendar cal = Calendar.getInstance(); // creates calendar
	    cal.setTime(new Date()); // sets calendar time/date
	    cal.add(Calendar.HOUR_OF_DAY, 2); // adds one hour
	    expirationTime=  cal.getTime();
	    
	    
	    
	    //We will sign our JWT with our ApiKey secret
	    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

	    
		//Let's set the JWT Claims
	    JwtBuilder builder = Jwts.builder()
	    		.setId(id)
	            .setIssuedAt(now)
	            .setSubject(subject)
	            .setIssuer(issuer)
	            .addClaims(payloads)
	            .signWith(signatureAlgorithm, signingKey)
	            .setExpiration(expirationTime);
	  
	    //if it has been specified, let's add the expiration
	   /* if (ttlMillis > 0) {
	        long expMillis = nowMillis + ttlMillis;
	        Date exp = new Date(expMillis);
	        builder.setExpiration(exp);
	    }  */
	  
	    //Builds the JWT and serializes it to a compact, URL-safe string
	    return builder.compact();
	}
	
	
	
	
	public static Claims decodeJWT(String jwt) {
	    //This line will throw an exception if it is not a signed JWS (as expected)
	  try {
		Claims claims = Jwts.parser()
		            .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
		            .parseClaimsJws(jwt).getBody();
		    return claims;
	} catch (ExpiredJwtException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	} catch (UnsupportedJwtException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	} catch (MalformedJwtException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	} catch (SignatureException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	} catch (IllegalArgumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	}
	}
	
	
	public static final String generateSessId(String loginId, String password, String username) {
		
		String sessid="";
	try {
		
		
	String ui=	UUID.randomUUID().toString();
 String stringToEncrypt= (new Random())+""+loginId+""+(new Random())+ password+(new Random())+ username+(new Random()) ;
	MessageDigest messageDigest;
		messageDigest = MessageDigest.getInstance("SHA-256");
		
	messageDigest.update(stringToEncrypt.getBytes());
	 byte[] digest  = messageDigest.digest();
		
	
	//Converting the byte array in to HexString format
    StringBuffer hexString = new StringBuffer();
    
    for (int i = 0;i<digest.length;i++) {
       hexString.append(Integer.toHexString(0xFF & digest[i]));
    }
		sessid=ui+"$"+hexString.toString();
		return sessid;	
		
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return sessid;	
		
	} 
	
	
	
	
	public static String getClientIp(HttpServletRequest request) {

        String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }

        return remoteAddr;
    }
	
	
	
	public static SessionModel createJWTToken(HttpServletRequest req, TUsermaster2 um) {

		Map<String, Object> payloads = new HashMap<String, Object>();
 		//Date sstime=new Date(System.currentTimeMillis());
 		long sstime=System.currentTimeMillis();
		String sessid=JWTUtils.generateSessId(um.getLoginId(), um.getPassword(), um.getUserName());
		String remoteip=JWTUtils.getClientIp(req);
		String userAgent=req.getHeader("User-Agent")==null?(""+new Random()+System.currentTimeMillis()):req.getHeader("User-Agent");
		String ru=req.getRemoteUser()==null?"-":req.getRemoteUser();
		
		
 		payloads.put("uid", um.getLoginId());
		payloads.put("starttime",sstime );
		payloads.put("sessid", sessid); 
		payloads.put("remote-ip", remoteip);
		payloads.put("remote-host", req.getRemoteHost());
		payloads.put("remote-port", req.getRemotePort());
		payloads.put("remote-user", ru);
		payloads.put("remote-agent", userAgent);
		
		System.out.println("sstime: "+sstime);
		System.out.println("uid: "+ um.getLoginId());
		System.out.println("sessid: "+sessid);
		System.out.println("remote-ip: "+remoteip);
		System.out.println("remote-host: "+req.getRemoteHost());
		System.out.println("remote-port: "+req.getRemotePort());
		System.out.println("remote-user: "+ru);
		System.out.println("remote-agent: "+userAgent);
		
		
		String jwtToken =  JWTUtils.createJWT(um.getLoginId(), "WTHAQ", "Authentication and Session Control Tag", 10000,payloads );
		System.out.println("jwtToken: "+jwtToken);
	
		
		SessionModel sm=new SessionModel();
		sm.setLoginId(um.getLoginId());
		sm.setRemoteAgent(userAgent);
		sm.setRemoteHost(req.getRemoteHost());
		sm.setRemoteIp(remoteip);
		sm.setRemotePort(""+req.getRemotePort());
		sm.setRemoteUser(ru);
		sm.setSessionId(sessid);
		sm.setToken(jwtToken); 
		
		
		
		return sm;
	
	}
	
	
	
	public static String createDGtalSignatureHashFromString(String d, String algo) {

		String uniqueHash = "";
 
			 
			  
			  try {
				uniqueHash=JWTUtils.toHexString(JWTUtils.getSHA(d, algo));
				  
				return uniqueHash;
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		  return uniqueHash;
	}

	
	
	public static byte[] getSHA(String input, String algo) throws NoSuchAlgorithmException 
    {  
        // Static getInstance method is called with hashing SHA  
        MessageDigest md = MessageDigest.getInstance(algo);  
  
        // digest() method called  
        // to calculate message digest of an input  
        // and return array of byte 
        return md.digest(input.getBytes(StandardCharsets.UTF_8));  
    } 
    
    public static String toHexString(byte[] hash) 
    { 
        // Convert byte array into signum representation  
        BigInteger number = new BigInteger(1, hash);  
  
        // Convert message digest into hex value  
        StringBuilder hexString = new StringBuilder(number.toString(16));  
  
        // Pad with leading zeros 
        while (hexString.length() < 32)  
        {  
            hexString.insert(0, '0');  
        }  
  
        return hexString.toString();  
    } 
  
	
	
	
	
	
	
	
	public static String createSignatureStringBySignaturePattern(PaymentModel paymentModel) {

		/*
		 * 
		 * https://docs.payfort.com/docs/api/build/index.html?php#signature-pattern
		 * 
		 * Signature Pattern The below steps describe the signature pattern:
		 * 
		 * Sort all PayFort requests parameters (both mandatory and optional) in an
		 * ascending alphabetical order based on the parameters names.
		 * 
		 * Concatenate the parameter name with the value separated by ’=’
		 * (param_name=param_value).
		 * 
		 * Concatenate all the parameters directly without any separator.
		 * (param_name1=param_value1param_name2=param_value2).
		 * 
		 * Add the Merchant’s Passphrase at the beginning and end of the parameters
		 * string. (REQUESTPHRASEparam_name1=param_value1param_name2=param_value2RE
		 * QUESTPHRASE).
		 * 
		 * Use the SHA function to generate the SHA value of the resulted string
		 * depending on the type of SHA selected by the Merchant.
		 * 
		 * 
		 * 
		 */

		String sigPatternString = 
				paymentModel.getSHARequestPhrase() + 
				"access_code=" + paymentModel.getAccess_code()
				+ "amount=" + paymentModel.getAmount() 
				+ "command=" + paymentModel.getCommand() 
				+ "currency="+ paymentModel.getCurrency() 
				+ "customer_email=" + paymentModel.getCustomerEmail() 
				+ "language="+ paymentModel.getLanguage() 
				+ "merchant_identifier=" + paymentModel.getMerchant_identifier()
				+ "merchant_reference=" + paymentModel.getMerchant_reference() 
				+ "order_description="+ paymentModel.getOrderDescription() 
				+ "return_url=" + paymentModel.getReturnUrl()
				+ paymentModel.getSHARequestPhrase();

		return sigPatternString;

	}
	 
	
	 
    
     
	
	
	
	
	 

	
}
