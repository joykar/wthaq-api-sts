
package sa.wthaq.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProfileAdmin complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProfileAdmin"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rep_Id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="rep_email" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="rep_mobile_No" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="rep_msg" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="rep_name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="rep_name_ar" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="rep_status" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="user_Id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfileAdmin", namespace = "http://request.wthaq.sa", propOrder = {
    "repId",
    "repEmail",
    "repMobileNo",
    "repMsg",
    "repName",
    "repNameAr",
    "repStatus",
    "userId"
})
public class ProfileAdmin {

    @XmlElement(name = "rep_Id", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String repId;
    @XmlElement(name = "rep_email", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String repEmail;
    @XmlElement(name = "rep_mobile_No", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String repMobileNo;
    @XmlElement(name = "rep_msg", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String repMsg;
    @XmlElement(name = "rep_name", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String repName;
    @XmlElement(name = "rep_name_ar", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String repNameAr;
    @XmlElement(name = "rep_status", namespace = "http://request.wthaq.sa", required = true, type = Boolean.class, nillable = true)
    protected Boolean repStatus;
    @XmlElement(name = "user_Id", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String userId;

    /**
     * Gets the value of the repId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepId() {
        return repId;
    }

    /**
     * Sets the value of the repId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepId(String value) {
        this.repId = value;
    }

    /**
     * Gets the value of the repEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepEmail() {
        return repEmail;
    }

    /**
     * Sets the value of the repEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepEmail(String value) {
        this.repEmail = value;
    }

    /**
     * Gets the value of the repMobileNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepMobileNo() {
        return repMobileNo;
    }

    /**
     * Sets the value of the repMobileNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepMobileNo(String value) {
        this.repMobileNo = value;
    }

    /**
     * Gets the value of the repMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepMsg() {
        return repMsg;
    }

    /**
     * Sets the value of the repMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepMsg(String value) {
        this.repMsg = value;
    }

    /**
     * Gets the value of the repName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepName() {
        return repName;
    }

    /**
     * Sets the value of the repName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepName(String value) {
        this.repName = value;
    }

    /**
     * Gets the value of the repNameAr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepNameAr() {
        return repNameAr;
    }

    /**
     * Sets the value of the repNameAr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepNameAr(String value) {
        this.repNameAr = value;
    }

    /**
     * Gets the value of the repStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRepStatus() {
        return repStatus;
    }

    /**
     * Sets the value of the repStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRepStatus(Boolean value) {
        this.repStatus = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

}
