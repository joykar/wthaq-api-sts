
package sa.wthaq.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LgReturnInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LgReturnInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="beneficiary_Info" type="{http://request.wthaq.sa}Beneficiary_Info"/&gt;
 *         &lt;element name="conditions" type="{http://request.wthaq.sa}Special_Conditions"/&gt;
 *         &lt;element name="info" type="{http://request.wthaq.sa}LG_Info"/&gt;
 *         &lt;element name="request_Id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="supplier_Info" type="{http://request.wthaq.sa}Supplier_Info"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LgReturnInfo", namespace = "http://request.wthaq.sa", propOrder = {
    "beneficiaryInfo",
    "conditions",
    "info",
    "requestId",
    "supplierInfo"
})
public class LgReturnInfo {

    @XmlElement(name = "beneficiary_Info", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected BeneficiaryInfo beneficiaryInfo;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected SpecialConditions conditions;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected LGInfo info;
    @XmlElement(name = "request_Id", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String requestId;
    @XmlElement(name = "supplier_Info", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected SupplierInfo supplierInfo;

    /**
     * Gets the value of the beneficiaryInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BeneficiaryInfo }
     *     
     */
    public BeneficiaryInfo getBeneficiaryInfo() {
        return beneficiaryInfo;
    }

    /**
     * Sets the value of the beneficiaryInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BeneficiaryInfo }
     *     
     */
    public void setBeneficiaryInfo(BeneficiaryInfo value) {
        this.beneficiaryInfo = value;
    }

    /**
     * Gets the value of the conditions property.
     * 
     * @return
     *     possible object is
     *     {@link SpecialConditions }
     *     
     */
    public SpecialConditions getConditions() {
        return conditions;
    }

    /**
     * Sets the value of the conditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecialConditions }
     *     
     */
    public void setConditions(SpecialConditions value) {
        this.conditions = value;
    }

    /**
     * Gets the value of the info property.
     * 
     * @return
     *     possible object is
     *     {@link LGInfo }
     *     
     */
    public LGInfo getInfo() {
        return info;
    }

    /**
     * Sets the value of the info property.
     * 
     * @param value
     *     allowed object is
     *     {@link LGInfo }
     *     
     */
    public void setInfo(LGInfo value) {
        this.info = value;
    }

    /**
     * Gets the value of the requestId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Gets the value of the supplierInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SupplierInfo }
     *     
     */
    public SupplierInfo getSupplierInfo() {
        return supplierInfo;
    }

    /**
     * Sets the value of the supplierInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplierInfo }
     *     
     */
    public void setSupplierInfo(SupplierInfo value) {
        this.supplierInfo = value;
    }

}
