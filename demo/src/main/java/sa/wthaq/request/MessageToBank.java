
package sa.wthaq.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for MessageToBank complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MessageToBank"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bankMessage_Id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="request_id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="status_code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="timeData" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageToBank", namespace = "http://request.wthaq.sa", propOrder = {
    "bankMessageId",
    "requestId",
    "statusCode",
    "timeData"
})
public class MessageToBank {

    @XmlElement(name = "bankMessage_Id", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String bankMessageId;
    @XmlElement(name = "request_id", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String requestId;
    @XmlElement(name = "status_code", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String statusCode;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeData;

    /**
     * Gets the value of the bankMessageId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankMessageId() {
        return bankMessageId;
    }

    /**
     * Sets the value of the bankMessageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankMessageId(String value) {
        this.bankMessageId = value;
    }

    /**
     * Gets the value of the requestId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the timeData property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeData() {
        return timeData;
    }

    /**
     * Sets the value of the timeData property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeData(XMLGregorianCalendar value) {
        this.timeData = value;
    }

}
