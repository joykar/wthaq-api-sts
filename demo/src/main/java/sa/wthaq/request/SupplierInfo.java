
package sa.wthaq.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Supplier_Info complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Supplier_Info"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PObox" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="applicantCR" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="applicantIBAN" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="contactNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="emailAddress" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="faxNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="postalcode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="supplierName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="supplierNameAr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Supplier_Info", namespace = "http://request.wthaq.sa", propOrder = {
    "pObox",
    "address",
    "applicantCR",
    "applicantIBAN",
    "contactNumber",
    "emailAddress",
    "faxNumber",
    "postalcode",
    "supplierName",
    "supplierNameAr"
})
public class SupplierInfo {

    @XmlElement(name = "PObox", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String pObox;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String address;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String applicantCR;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String applicantIBAN;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String contactNumber;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String emailAddress;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String faxNumber;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String postalcode;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String supplierName;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String supplierNameAr;

    /**
     * Gets the value of the pObox property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPObox() {
        return pObox;
    }

    /**
     * Sets the value of the pObox property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPObox(String value) {
        this.pObox = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * Gets the value of the applicantCR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantCR() {
        return applicantCR;
    }

    /**
     * Sets the value of the applicantCR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantCR(String value) {
        this.applicantCR = value;
    }

    /**
     * Gets the value of the applicantIBAN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantIBAN() {
        return applicantIBAN;
    }

    /**
     * Sets the value of the applicantIBAN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantIBAN(String value) {
        this.applicantIBAN = value;
    }

    /**
     * Gets the value of the contactNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactNumber() {
        return contactNumber;
    }

    /**
     * Sets the value of the contactNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactNumber(String value) {
        this.contactNumber = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the faxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * Sets the value of the faxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxNumber(String value) {
        this.faxNumber = value;
    }

    /**
     * Gets the value of the postalcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalcode() {
        return postalcode;
    }

    /**
     * Sets the value of the postalcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalcode(String value) {
        this.postalcode = value;
    }

    /**
     * Gets the value of the supplierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * Sets the value of the supplierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierName(String value) {
        this.supplierName = value;
    }

    /**
     * Gets the value of the supplierNameAr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierNameAr() {
        return supplierNameAr;
    }

    /**
     * Sets the value of the supplierNameAr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierNameAr(String value) {
        this.supplierNameAr = value;
    }

}
