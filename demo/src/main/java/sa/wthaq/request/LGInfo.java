
package sa.wthaq.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for LG_Info complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LG_Info"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LGNumber" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LGType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LGValidity" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="amountText" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="byanNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="hijriDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="percentageOfContract_value" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="projectName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="projectNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="purposeOfBbond" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="zakatPeriodEndDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="zakatPeriodStart" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LG_Info", namespace = "http://request.wthaq.sa", propOrder = {
    "lgNumber",
    "lgType",
    "lgValidity",
    "amount",
    "amountText",
    "byanNumber",
    "currency",
    "hijriDate",
    "percentageOfContractValue",
    "projectName",
    "projectNumber",
    "purposeOfBbond",
    "transactionId",
    "zakatPeriodEndDate",
    "zakatPeriodStart"
})
public class LGInfo {

    @XmlElement(name = "LGNumber", namespace = "http://request.wthaq.sa")
    protected int lgNumber;
    @XmlElement(name = "LGType", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String lgType;
    @XmlElement(name = "LGValidity", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar lgValidity;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, type = Long.class, nillable = true)
    protected Long amount;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String amountText;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String byanNumber;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String currency;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String hijriDate;
    @XmlElement(name = "percentageOfContract_value", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String percentageOfContractValue;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String projectName;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String projectNumber;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String purposeOfBbond;
    @XmlElement(namespace = "http://request.wthaq.sa")
    protected int transactionId;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar zakatPeriodEndDate;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar zakatPeriodStart;

    /**
     * Gets the value of the lgNumber property.
     * 
     */
    public int getLGNumber() {
        return lgNumber;
    }

    /**
     * Sets the value of the lgNumber property.
     * 
     */
    public void setLGNumber(int value) {
        this.lgNumber = value;
    }

    /**
     * Gets the value of the lgType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLGType() {
        return lgType;
    }

    /**
     * Sets the value of the lgType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLGType(String value) {
        this.lgType = value;
    }

    /**
     * Gets the value of the lgValidity property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLGValidity() {
        return lgValidity;
    }

    /**
     * Sets the value of the lgValidity property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLGValidity(XMLGregorianCalendar value) {
        this.lgValidity = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAmount(Long value) {
        this.amount = value;
    }

    /**
     * Gets the value of the amountText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmountText() {
        return amountText;
    }

    /**
     * Sets the value of the amountText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmountText(String value) {
        this.amountText = value;
    }

    /**
     * Gets the value of the byanNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getByanNumber() {
        return byanNumber;
    }

    /**
     * Sets the value of the byanNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setByanNumber(String value) {
        this.byanNumber = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the hijriDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHijriDate() {
        return hijriDate;
    }

    /**
     * Sets the value of the hijriDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHijriDate(String value) {
        this.hijriDate = value;
    }

    /**
     * Gets the value of the percentageOfContractValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPercentageOfContractValue() {
        return percentageOfContractValue;
    }

    /**
     * Sets the value of the percentageOfContractValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPercentageOfContractValue(String value) {
        this.percentageOfContractValue = value;
    }

    /**
     * Gets the value of the projectName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * Sets the value of the projectName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProjectName(String value) {
        this.projectName = value;
    }

    /**
     * Gets the value of the projectNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProjectNumber() {
        return projectNumber;
    }

    /**
     * Sets the value of the projectNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProjectNumber(String value) {
        this.projectNumber = value;
    }

    /**
     * Gets the value of the purposeOfBbond property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurposeOfBbond() {
        return purposeOfBbond;
    }

    /**
     * Sets the value of the purposeOfBbond property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurposeOfBbond(String value) {
        this.purposeOfBbond = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     */
    public int getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     */
    public void setTransactionId(int value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the zakatPeriodEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getZakatPeriodEndDate() {
        return zakatPeriodEndDate;
    }

    /**
     * Sets the value of the zakatPeriodEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setZakatPeriodEndDate(XMLGregorianCalendar value) {
        this.zakatPeriodEndDate = value;
    }

    /**
     * Gets the value of the zakatPeriodStart property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getZakatPeriodStart() {
        return zakatPeriodStart;
    }

    /**
     * Sets the value of the zakatPeriodStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setZakatPeriodStart(XMLGregorianCalendar value) {
        this.zakatPeriodStart = value;
    }

}
