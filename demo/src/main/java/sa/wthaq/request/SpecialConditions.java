
package sa.wthaq.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Special_Conditions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Special_Conditions"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PObox" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="actualApplicantarName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="actualApplicantenName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="assignableLG" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="autoRenewalLG" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="contactNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="emailAddress" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="faxNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="openEndedLG" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="postalCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="transferableLG" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Special_Conditions", namespace = "http://request.wthaq.sa", propOrder = {
    "pObox",
    "actualApplicantarName",
    "actualApplicantenName",
    "address",
    "assignableLG",
    "autoRenewalLG",
    "contactNumber",
    "emailAddress",
    "faxNumber",
    "openEndedLG",
    "postalCode",
    "transferableLG"
})
public class SpecialConditions {

    @XmlElement(name = "PObox", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String pObox;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String actualApplicantarName;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String actualApplicantenName;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String address;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, type = Boolean.class, nillable = true)
    protected Boolean assignableLG;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, type = Boolean.class, nillable = true)
    protected Boolean autoRenewalLG;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String contactNumber;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String emailAddress;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String faxNumber;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, type = Boolean.class, nillable = true)
    protected Boolean openEndedLG;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String postalCode;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, type = Boolean.class, nillable = true)
    protected Boolean transferableLG;

    /**
     * Gets the value of the pObox property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPObox() {
        return pObox;
    }

    /**
     * Sets the value of the pObox property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPObox(String value) {
        this.pObox = value;
    }

    /**
     * Gets the value of the actualApplicantarName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActualApplicantarName() {
        return actualApplicantarName;
    }

    /**
     * Sets the value of the actualApplicantarName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActualApplicantarName(String value) {
        this.actualApplicantarName = value;
    }

    /**
     * Gets the value of the actualApplicantenName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActualApplicantenName() {
        return actualApplicantenName;
    }

    /**
     * Sets the value of the actualApplicantenName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActualApplicantenName(String value) {
        this.actualApplicantenName = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * Gets the value of the assignableLG property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAssignableLG() {
        return assignableLG;
    }

    /**
     * Sets the value of the assignableLG property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAssignableLG(Boolean value) {
        this.assignableLG = value;
    }

    /**
     * Gets the value of the autoRenewalLG property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoRenewalLG() {
        return autoRenewalLG;
    }

    /**
     * Sets the value of the autoRenewalLG property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoRenewalLG(Boolean value) {
        this.autoRenewalLG = value;
    }

    /**
     * Gets the value of the contactNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactNumber() {
        return contactNumber;
    }

    /**
     * Sets the value of the contactNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactNumber(String value) {
        this.contactNumber = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the faxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * Sets the value of the faxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxNumber(String value) {
        this.faxNumber = value;
    }

    /**
     * Gets the value of the openEndedLG property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOpenEndedLG() {
        return openEndedLG;
    }

    /**
     * Sets the value of the openEndedLG property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpenEndedLG(Boolean value) {
        this.openEndedLG = value;
    }

    /**
     * Gets the value of the postalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets the value of the postalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalCode(String value) {
        this.postalCode = value;
    }

    /**
     * Gets the value of the transferableLG property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTransferableLG() {
        return transferableLG;
    }

    /**
     * Sets the value of the transferableLG property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTransferableLG(Boolean value) {
        this.transferableLG = value;
    }

}
