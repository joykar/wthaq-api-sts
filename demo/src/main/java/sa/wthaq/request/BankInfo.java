
package sa.wthaq.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BankInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bank_code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="branch_name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="iban_no" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="msg" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankInfo", namespace = "http://request.wthaq.sa", propOrder = {
    "bankCode",
    "branchName",
    "ibanNo",
    "msg",
    "userId"
})
public class BankInfo {

    @XmlElement(name = "bank_code", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String bankCode;
    @XmlElement(name = "branch_name", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String branchName;
    @XmlElement(name = "iban_no", namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String ibanNo;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String msg;
    @XmlElement(namespace = "http://request.wthaq.sa", required = true, nillable = true)
    protected String userId;

    /**
     * Gets the value of the bankCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * Sets the value of the bankCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankCode(String value) {
        this.bankCode = value;
    }

    /**
     * Gets the value of the branchName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * Sets the value of the branchName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranchName(String value) {
        this.branchName = value;
    }

    /**
     * Gets the value of the ibanNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIbanNo() {
        return ibanNo;
    }

    /**
     * Sets the value of the ibanNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbanNo(String value) {
        this.ibanNo = value;
    }

    /**
     * Gets the value of the msg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsg() {
        return msg;
    }

    /**
     * Sets the value of the msg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsg(String value) {
        this.msg = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

}
