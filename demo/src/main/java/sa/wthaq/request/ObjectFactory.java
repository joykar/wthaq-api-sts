
package sa.wthaq.request;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the sa.wthaq.request package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: sa.wthaq.request
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MessageFromBank }
     * 
     */
    public MessageFromBank createMessageFromBank() {
        return new MessageFromBank();
    }

    /**
     * Create an instance of {@link BankInfo }
     * 
     */
    public BankInfo createBankInfo() {
        return new BankInfo();
    }

    /**
     * Create an instance of {@link ProfileAdmin }
     * 
     */
    public ProfileAdmin createProfileAdmin() {
        return new ProfileAdmin();
    }

    /**
     * Create an instance of {@link UserDetail }
     * 
     */
    public UserDetail createUserDetail() {
        return new UserDetail();
    }

    /**
     * Create an instance of {@link SupplierInfo }
     * 
     */
    public SupplierInfo createSupplierInfo() {
        return new SupplierInfo();
    }

    /**
     * Create an instance of {@link BeneficiaryInfo }
     * 
     */
    public BeneficiaryInfo createBeneficiaryInfo() {
        return new BeneficiaryInfo();
    }

    /**
     * Create an instance of {@link LGInfo }
     * 
     */
    public LGInfo createLGInfo() {
        return new LGInfo();
    }

    /**
     * Create an instance of {@link SpecialConditions }
     * 
     */
    public SpecialConditions createSpecialConditions() {
        return new SpecialConditions();
    }

    /**
     * Create an instance of {@link MessageToBank }
     * 
     */
    public MessageToBank createMessageToBank() {
        return new MessageToBank();
    }

    /**
     * Create an instance of {@link LgReturnInfo }
     * 
     */
    public LgReturnInfo createLgReturnInfo() {
        return new LgReturnInfo();
    }

}
