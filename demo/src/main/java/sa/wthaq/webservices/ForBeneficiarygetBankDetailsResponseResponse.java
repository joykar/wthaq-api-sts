
package sa.wthaq.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import sa.wthaq.request.BankInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ForBeneficiarygetBankDetailsResponseReturn" type="{http://request.wthaq.sa}BankInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "forBeneficiarygetBankDetailsResponseReturn"
})
@XmlRootElement(name = "ForBeneficiarygetBankDetailsResponseResponse", namespace = "http://webservices.wthaq.sa")
public class ForBeneficiarygetBankDetailsResponseResponse {

    @XmlElement(name = "ForBeneficiarygetBankDetailsResponseReturn", namespace = "http://webservices.wthaq.sa", required = true)
    protected BankInfo forBeneficiarygetBankDetailsResponseReturn;

    /**
     * Gets the value of the forBeneficiarygetBankDetailsResponseReturn property.
     * 
     * @return
     *     possible object is
     *     {@link BankInfo }
     *     
     */
    public BankInfo getForBeneficiarygetBankDetailsResponseReturn() {
        return forBeneficiarygetBankDetailsResponseReturn;
    }

    /**
     * Sets the value of the forBeneficiarygetBankDetailsResponseReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankInfo }
     *     
     */
    public void setForBeneficiarygetBankDetailsResponseReturn(BankInfo value) {
        this.forBeneficiarygetBankDetailsResponseReturn = value;
    }

}
