
package sa.wthaq.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="supplierName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ibanNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="crNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "supplierName",
    "ibanNo",
    "crNo",
    "userId"
})
@XmlRootElement(name = "ForSupplierverifyIbanDetail", namespace = "http://webservices.wthaq.sa")
public class ForSupplierverifyIbanDetail {

    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected String supplierName;
    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected String ibanNo;
    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected String crNo;
    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected String userId;

    /**
     * Gets the value of the supplierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * Sets the value of the supplierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierName(String value) {
        this.supplierName = value;
    }

    /**
     * Gets the value of the ibanNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIbanNo() {
        return ibanNo;
    }

    /**
     * Sets the value of the ibanNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbanNo(String value) {
        this.ibanNo = value;
    }

    /**
     * Gets the value of the crNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrNo() {
        return crNo;
    }

    /**
     * Sets the value of the crNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrNo(String value) {
        this.crNo = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

}
