
package sa.wthaq.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import sa.wthaq.request.UserDetail;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="getRequestDataReturn" type="{http://request.wthaq.sa}UserDetail"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getRequestDataReturn"
})
@XmlRootElement(name = "getRequestDataResponse", namespace = "http://webservices.wthaq.sa")
public class GetRequestDataResponse {

    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected UserDetail getRequestDataReturn;

    /**
     * Gets the value of the getRequestDataReturn property.
     * 
     * @return
     *     possible object is
     *     {@link UserDetail }
     *     
     */
    public UserDetail getGetRequestDataReturn() {
        return getRequestDataReturn;
    }

    /**
     * Sets the value of the getRequestDataReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDetail }
     *     
     */
    public void setGetRequestDataReturn(UserDetail value) {
        this.getRequestDataReturn = value;
    }

}
