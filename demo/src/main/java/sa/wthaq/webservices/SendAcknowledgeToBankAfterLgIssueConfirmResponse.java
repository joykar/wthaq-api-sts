
package sa.wthaq.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import sa.wthaq.request.MessageFromBank;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sendAcknowledgeToBankAfterLgIssueConfirmReturn" type="{http://request.wthaq.sa}MessageFromBank"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sendAcknowledgeToBankAfterLgIssueConfirmReturn"
})
@XmlRootElement(name = "sendAcknowledgeToBankAfterLgIssueConfirmResponse", namespace = "http://webservices.wthaq.sa")
public class SendAcknowledgeToBankAfterLgIssueConfirmResponse {

    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected MessageFromBank sendAcknowledgeToBankAfterLgIssueConfirmReturn;

    /**
     * Gets the value of the sendAcknowledgeToBankAfterLgIssueConfirmReturn property.
     * 
     * @return
     *     possible object is
     *     {@link MessageFromBank }
     *     
     */
    public MessageFromBank getSendAcknowledgeToBankAfterLgIssueConfirmReturn() {
        return sendAcknowledgeToBankAfterLgIssueConfirmReturn;
    }

    /**
     * Sets the value of the sendAcknowledgeToBankAfterLgIssueConfirmReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageFromBank }
     *     
     */
    public void setSendAcknowledgeToBankAfterLgIssueConfirmReturn(MessageFromBank value) {
        this.sendAcknowledgeToBankAfterLgIssueConfirmReturn = value;
    }

}
