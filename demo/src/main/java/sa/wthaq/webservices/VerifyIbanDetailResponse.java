
package sa.wthaq.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="verifyIbanDetailReturn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "verifyIbanDetailReturn"
})
@XmlRootElement(name = "verifyIbanDetailResponse", namespace = "http://webservices.wthaq.sa")
public class VerifyIbanDetailResponse {

    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected String verifyIbanDetailReturn;

    /**
     * Gets the value of the verifyIbanDetailReturn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerifyIbanDetailReturn() {
        return verifyIbanDetailReturn;
    }

    /**
     * Sets the value of the verifyIbanDetailReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerifyIbanDetailReturn(String value) {
        this.verifyIbanDetailReturn = value;
    }

}
