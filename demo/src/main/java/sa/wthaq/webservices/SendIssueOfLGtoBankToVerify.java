
package sa.wthaq.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import sa.wthaq.request.BeneficiaryInfo;
import sa.wthaq.request.LGInfo;
import sa.wthaq.request.SpecialConditions;
import sa.wthaq.request.SupplierInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="request_Id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="info" type="{http://request.wthaq.sa}Supplier_Info"/&gt;
 *         &lt;element name="info2" type="{http://request.wthaq.sa}Beneficiary_Info"/&gt;
 *         &lt;element name="lg_Info" type="{http://request.wthaq.sa}LG_Info"/&gt;
 *         &lt;element name="conditions" type="{http://request.wthaq.sa}Special_Conditions"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestId",
    "info",
    "info2",
    "lgInfo",
    "conditions"
})
@XmlRootElement(name = "sendIssueOfLGtoBankToVerify", namespace = "http://webservices.wthaq.sa")
public class SendIssueOfLGtoBankToVerify {

    @XmlElement(name = "request_Id", namespace = "http://webservices.wthaq.sa", required = true)
    protected String requestId;
    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected SupplierInfo info;
    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected BeneficiaryInfo info2;
    @XmlElement(name = "lg_Info", namespace = "http://webservices.wthaq.sa", required = true)
    protected LGInfo lgInfo;
    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected SpecialConditions conditions;

    /**
     * Gets the value of the requestId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Gets the value of the info property.
     * 
     * @return
     *     possible object is
     *     {@link SupplierInfo }
     *     
     */
    public SupplierInfo getInfo() {
        return info;
    }

    /**
     * Sets the value of the info property.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplierInfo }
     *     
     */
    public void setInfo(SupplierInfo value) {
        this.info = value;
    }

    /**
     * Gets the value of the info2 property.
     * 
     * @return
     *     possible object is
     *     {@link BeneficiaryInfo }
     *     
     */
    public BeneficiaryInfo getInfo2() {
        return info2;
    }

    /**
     * Sets the value of the info2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BeneficiaryInfo }
     *     
     */
    public void setInfo2(BeneficiaryInfo value) {
        this.info2 = value;
    }

    /**
     * Gets the value of the lgInfo property.
     * 
     * @return
     *     possible object is
     *     {@link LGInfo }
     *     
     */
    public LGInfo getLgInfo() {
        return lgInfo;
    }

    /**
     * Sets the value of the lgInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link LGInfo }
     *     
     */
    public void setLgInfo(LGInfo value) {
        this.lgInfo = value;
    }

    /**
     * Gets the value of the conditions property.
     * 
     * @return
     *     possible object is
     *     {@link SpecialConditions }
     *     
     */
    public SpecialConditions getConditions() {
        return conditions;
    }

    /**
     * Sets the value of the conditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecialConditions }
     *     
     */
    public void setConditions(SpecialConditions value) {
        this.conditions = value;
    }

}
