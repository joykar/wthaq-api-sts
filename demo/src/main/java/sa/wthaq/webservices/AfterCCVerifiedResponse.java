
package sa.wthaq.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import sa.wthaq.request.ProfileAdmin;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="afterCCVerifiedReturn" type="{http://request.wthaq.sa}ProfileAdmin"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "afterCCVerifiedReturn"
})
@XmlRootElement(name = "afterCCVerifiedResponse", namespace = "http://webservices.wthaq.sa")
public class AfterCCVerifiedResponse {

    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected ProfileAdmin afterCCVerifiedReturn;

    /**
     * Gets the value of the afterCCVerifiedReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ProfileAdmin }
     *     
     */
    public ProfileAdmin getAfterCCVerifiedReturn() {
        return afterCCVerifiedReturn;
    }

    /**
     * Sets the value of the afterCCVerifiedReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProfileAdmin }
     *     
     */
    public void setAfterCCVerifiedReturn(ProfileAdmin value) {
        this.afterCCVerifiedReturn = value;
    }

}
