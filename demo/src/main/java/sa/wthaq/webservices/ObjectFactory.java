
package sa.wthaq.webservices;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the sa.wthaq.webservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: sa.wthaq.webservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAcknowledgeFromBankAfterLgIssueRequest }
     * 
     */
    public GetAcknowledgeFromBankAfterLgIssueRequest createGetAcknowledgeFromBankAfterLgIssueRequest() {
        return new GetAcknowledgeFromBankAfterLgIssueRequest();
    }

    /**
     * Create an instance of {@link GetAcknowledgeFromBankAfterLgIssueRequestResponse }
     * 
     */
    public GetAcknowledgeFromBankAfterLgIssueRequestResponse createGetAcknowledgeFromBankAfterLgIssueRequestResponse() {
        return new GetAcknowledgeFromBankAfterLgIssueRequestResponse();
    }

    /**
     * Create an instance of {@link ForSuppliergetBankDetailsResponse }
     * 
     */
    public ForSuppliergetBankDetailsResponse createForSuppliergetBankDetailsResponse() {
        return new ForSuppliergetBankDetailsResponse();
    }

    /**
     * Create an instance of {@link ForSuppliergetBankDetailsResponseResponse }
     * 
     */
    public ForSuppliergetBankDetailsResponseResponse createForSuppliergetBankDetailsResponseResponse() {
        return new ForSuppliergetBankDetailsResponseResponse();
    }

    /**
     * Create an instance of {@link ForBeneficiarygetBankDetailsResponse }
     * 
     */
    public ForBeneficiarygetBankDetailsResponse createForBeneficiarygetBankDetailsResponse() {
        return new ForBeneficiarygetBankDetailsResponse();
    }

    /**
     * Create an instance of {@link ForBeneficiarygetBankDetailsResponseResponse }
     * 
     */
    public ForBeneficiarygetBankDetailsResponseResponse createForBeneficiarygetBankDetailsResponseResponse() {
        return new ForBeneficiarygetBankDetailsResponseResponse();
    }

    /**
     * Create an instance of {@link SendAcknowledgeToBankAfterLgIssueConfirm }
     * 
     */
    public SendAcknowledgeToBankAfterLgIssueConfirm createSendAcknowledgeToBankAfterLgIssueConfirm() {
        return new SendAcknowledgeToBankAfterLgIssueConfirm();
    }

    /**
     * Create an instance of {@link SendAcknowledgeToBankAfterLgIssueConfirmResponse }
     * 
     */
    public SendAcknowledgeToBankAfterLgIssueConfirmResponse createSendAcknowledgeToBankAfterLgIssueConfirmResponse() {
        return new SendAcknowledgeToBankAfterLgIssueConfirmResponse();
    }

    /**
     * Create an instance of {@link AfterCCVerified }
     * 
     */
    public AfterCCVerified createAfterCCVerified() {
        return new AfterCCVerified();
    }

    /**
     * Create an instance of {@link AfterCCVerifiedResponse }
     * 
     */
    public AfterCCVerifiedResponse createAfterCCVerifiedResponse() {
        return new AfterCCVerifiedResponse();
    }

    /**
     * Create an instance of {@link VerifyIbanDetail }
     * 
     */
    public VerifyIbanDetail createVerifyIbanDetail() {
        return new VerifyIbanDetail();
    }

    /**
     * Create an instance of {@link VerifyIbanDetailResponse }
     * 
     */
    public VerifyIbanDetailResponse createVerifyIbanDetailResponse() {
        return new VerifyIbanDetailResponse();
    }

    /**
     * Create an instance of {@link GetRequestData }
     * 
     */
    public GetRequestData createGetRequestData() {
        return new GetRequestData();
    }

    /**
     * Create an instance of {@link GetRequestDataResponse }
     * 
     */
    public GetRequestDataResponse createGetRequestDataResponse() {
        return new GetRequestDataResponse();
    }

    /**
     * Create an instance of {@link GetBankDetailsFromReq }
     * 
     */
    public GetBankDetailsFromReq createGetBankDetailsFromReq() {
        return new GetBankDetailsFromReq();
    }

    /**
     * Create an instance of {@link GetBankDetailsFromReqResponse }
     * 
     */
    public GetBankDetailsFromReqResponse createGetBankDetailsFromReqResponse() {
        return new GetBankDetailsFromReqResponse();
    }

    /**
     * Create an instance of {@link VerificationAdminDetails }
     * 
     */
    public VerificationAdminDetails createVerificationAdminDetails() {
        return new VerificationAdminDetails();
    }

    /**
     * Create an instance of {@link VerificationAdminDetailsResponse }
     * 
     */
    public VerificationAdminDetailsResponse createVerificationAdminDetailsResponse() {
        return new VerificationAdminDetailsResponse();
    }

    /**
     * Create an instance of {@link SendIssueOfLGtoBankToVerify }
     * 
     */
    public SendIssueOfLGtoBankToVerify createSendIssueOfLGtoBankToVerify() {
        return new SendIssueOfLGtoBankToVerify();
    }

    /**
     * Create an instance of {@link SendIssueOfLGtoBankToVerifyResponse }
     * 
     */
    public SendIssueOfLGtoBankToVerifyResponse createSendIssueOfLGtoBankToVerifyResponse() {
        return new SendIssueOfLGtoBankToVerifyResponse();
    }

    /**
     * Create an instance of {@link SendAcknowledgeToBank }
     * 
     */
    public SendAcknowledgeToBank createSendAcknowledgeToBank() {
        return new SendAcknowledgeToBank();
    }

    /**
     * Create an instance of {@link SendAcknowledgeToBankResponse }
     * 
     */
    public SendAcknowledgeToBankResponse createSendAcknowledgeToBankResponse() {
        return new SendAcknowledgeToBankResponse();
    }

    /**
     * Create an instance of {@link GetAcknowledgeFromBank }
     * 
     */
    public GetAcknowledgeFromBank createGetAcknowledgeFromBank() {
        return new GetAcknowledgeFromBank();
    }

    /**
     * Create an instance of {@link GetAcknowledgeFromBankResponse }
     * 
     */
    public GetAcknowledgeFromBankResponse createGetAcknowledgeFromBankResponse() {
        return new GetAcknowledgeFromBankResponse();
    }

    /**
     * Create an instance of {@link ForSupplierverifyIbanDetail }
     * 
     */
    public ForSupplierverifyIbanDetail createForSupplierverifyIbanDetail() {
        return new ForSupplierverifyIbanDetail();
    }

    /**
     * Create an instance of {@link ForSupplierverifyIbanDetailResponse }
     * 
     */
    public ForSupplierverifyIbanDetailResponse createForSupplierverifyIbanDetailResponse() {
        return new ForSupplierverifyIbanDetailResponse();
    }

    /**
     * Create an instance of {@link GetLgDetailsFromBankAfterVerify }
     * 
     */
    public GetLgDetailsFromBankAfterVerify createGetLgDetailsFromBankAfterVerify() {
        return new GetLgDetailsFromBankAfterVerify();
    }

    /**
     * Create an instance of {@link GetLgDetailsFromBankAfterVerifyResponse }
     * 
     */
    public GetLgDetailsFromBankAfterVerifyResponse createGetLgDetailsFromBankAfterVerifyResponse() {
        return new GetLgDetailsFromBankAfterVerifyResponse();
    }

    /**
     * Create an instance of {@link ForBeneficiaryverifyIbanDetail }
     * 
     */
    public ForBeneficiaryverifyIbanDetail createForBeneficiaryverifyIbanDetail() {
        return new ForBeneficiaryverifyIbanDetail();
    }

    /**
     * Create an instance of {@link ForBeneficiaryverifyIbanDetailResponse }
     * 
     */
    public ForBeneficiaryverifyIbanDetailResponse createForBeneficiaryverifyIbanDetailResponse() {
        return new ForBeneficiaryverifyIbanDetailResponse();
    }

}
