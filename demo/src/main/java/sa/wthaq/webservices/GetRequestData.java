
package sa.wthaq.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="crn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="mno" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "crn",
    "mno"
})
@XmlRootElement(name = "getRequestData", namespace = "http://webservices.wthaq.sa")
public class GetRequestData {

    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected String crn;
    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected String mno;

    /**
     * Gets the value of the crn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrn() {
        return crn;
    }

    /**
     * Sets the value of the crn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrn(String value) {
        this.crn = value;
    }

    /**
     * Gets the value of the mno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMno() {
        return mno;
    }

    /**
     * Sets the value of the mno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMno(String value) {
        this.mno = value;
    }

}
