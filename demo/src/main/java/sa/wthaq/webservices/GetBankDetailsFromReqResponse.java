
package sa.wthaq.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import sa.wthaq.request.MessageFromBank;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="getBankDetailsFromReqReturn" type="{http://request.wthaq.sa}MessageFromBank"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getBankDetailsFromReqReturn"
})
@XmlRootElement(name = "getBankDetailsFromReqResponse", namespace = "http://webservices.wthaq.sa")
public class GetBankDetailsFromReqResponse {

    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected MessageFromBank getBankDetailsFromReqReturn;

    /**
     * Gets the value of the getBankDetailsFromReqReturn property.
     * 
     * @return
     *     possible object is
     *     {@link MessageFromBank }
     *     
     */
    public MessageFromBank getGetBankDetailsFromReqReturn() {
        return getBankDetailsFromReqReturn;
    }

    /**
     * Sets the value of the getBankDetailsFromReqReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageFromBank }
     *     
     */
    public void setGetBankDetailsFromReqReturn(MessageFromBank value) {
        this.getBankDetailsFromReqReturn = value;
    }

}
