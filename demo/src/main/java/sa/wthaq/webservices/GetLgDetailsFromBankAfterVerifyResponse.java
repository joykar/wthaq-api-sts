
package sa.wthaq.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import sa.wthaq.request.LgReturnInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="getLgDetailsFromBankAfterVerifyReturn" type="{http://request.wthaq.sa}LgReturnInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getLgDetailsFromBankAfterVerifyReturn"
})
@XmlRootElement(name = "getLgDetailsFromBankAfterVerifyResponse", namespace = "http://webservices.wthaq.sa")
public class GetLgDetailsFromBankAfterVerifyResponse {

    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected LgReturnInfo getLgDetailsFromBankAfterVerifyReturn;

    /**
     * Gets the value of the getLgDetailsFromBankAfterVerifyReturn property.
     * 
     * @return
     *     possible object is
     *     {@link LgReturnInfo }
     *     
     */
    public LgReturnInfo getGetLgDetailsFromBankAfterVerifyReturn() {
        return getLgDetailsFromBankAfterVerifyReturn;
    }

    /**
     * Sets the value of the getLgDetailsFromBankAfterVerifyReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link LgReturnInfo }
     *     
     */
    public void setGetLgDetailsFromBankAfterVerifyReturn(LgReturnInfo value) {
        this.getLgDetailsFromBankAfterVerifyReturn = value;
    }

}
