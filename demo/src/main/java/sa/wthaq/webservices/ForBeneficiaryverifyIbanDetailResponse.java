
package sa.wthaq.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ForBeneficiaryverifyIbanDetailReturn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "forBeneficiaryverifyIbanDetailReturn"
})
@XmlRootElement(name = "ForBeneficiaryverifyIbanDetailResponse", namespace = "http://webservices.wthaq.sa")
public class ForBeneficiaryverifyIbanDetailResponse {

    @XmlElement(name = "ForBeneficiaryverifyIbanDetailReturn", namespace = "http://webservices.wthaq.sa", required = true)
    protected String forBeneficiaryverifyIbanDetailReturn;

    /**
     * Gets the value of the forBeneficiaryverifyIbanDetailReturn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForBeneficiaryverifyIbanDetailReturn() {
        return forBeneficiaryverifyIbanDetailReturn;
    }

    /**
     * Sets the value of the forBeneficiaryverifyIbanDetailReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForBeneficiaryverifyIbanDetailReturn(String value) {
        this.forBeneficiaryverifyIbanDetailReturn = value;
    }

}
