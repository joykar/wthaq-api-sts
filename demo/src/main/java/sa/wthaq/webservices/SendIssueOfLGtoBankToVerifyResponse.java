
package sa.wthaq.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sendIssueOfLGtoBankToVerifyReturn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sendIssueOfLGtoBankToVerifyReturn"
})
@XmlRootElement(name = "sendIssueOfLGtoBankToVerifyResponse", namespace = "http://webservices.wthaq.sa")
public class SendIssueOfLGtoBankToVerifyResponse {

    @XmlElement(namespace = "http://webservices.wthaq.sa", required = true)
    protected String sendIssueOfLGtoBankToVerifyReturn;

    /**
     * Gets the value of the sendIssueOfLGtoBankToVerifyReturn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendIssueOfLGtoBankToVerifyReturn() {
        return sendIssueOfLGtoBankToVerifyReturn;
    }

    /**
     * Sets the value of the sendIssueOfLGtoBankToVerifyReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendIssueOfLGtoBankToVerifyReturn(String value) {
        this.sendIssueOfLGtoBankToVerifyReturn = value;
    }

}
