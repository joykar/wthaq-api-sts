package sa.wthaq.Communication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
 
import com.sun.mail.smtp.SMTPTransport;

import sa.wthaq.controller.TabadulController;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;

@Configuration
@PropertySource(value="classpath:application.properties")
@EntityScan(basePackages ="sa.wthaq")
public class SendEmail1 {
	
	private static final Logger logger = LoggerFactory.getLogger(SendEmail.class);
	
	
	@Autowired
	private static Environment env;
	
    // for example, smtp.mailgun.org
	 @Value("${GMAIL_SMTP_SERVER}")
    private     String SMTP_SERVER;// = env.getProperty("GMAIL_SMTP_SERVER");
	 @Value("${GMAIL_USERNAME}")
    private     String USERNAME;// =env.getProperty("GMAIL_USERNAME");
	 @Value("${GMAIL_PASSWORD}")
    private     String PASSWORD;// = env.getProperty("GMAIL_PASSWORD");
	 @Value("${GMAIL_EMAIL_FROM}")
    private     String EMAIL_FROM;// = env.getProperty("GMAIL_EMAIL_FROM");
    private static String EMAIL_TO = "";
    private static   String EMAIL_TO_CC = "";

    private static String EMAIL_SUBJECT = "";
    private static String EMAIL_TEXT = "";
     @Value("${GMAIL_SMTP_PORT}")
    private   String EMAIL_PORT;//=env.getProperty("GMAIL_SMTP_PORT");
     @Value("${GMAIL_EMAIL_REGISTRATION_FIRST_PHASE_SUBJECT}")
     private   String EMAIL_SUBJECT_REGSITRATION_FIRST_PHASE;
     @Value("${GMAIL_EMAIL_REGISTRATION_FIRST_PHASE_BODY}")
     private   String EMAIL_TEXT_REGSITRATION_FIRST_PHASE;
    
    public   void SendRegistrationFirstPhaseEmail(String toEmail, String companyName, String loginid, String pass) {
    	EMAIL_SUBJECT ="Registration Request Received | WETHAQ"; //EMAIL_SUBJECT_REGSITRATION_FIRST_PHASE;
    	EMAIL_TO = toEmail;
    	EMAIL_TEXT = "Greetings ENTT, <p> We have received your request for registration on WETHAQ.</p><p>Your login id : <h3>loginid</h3> <br>Your password: <h3>pass-word</h3> </p><p>Your documents are currently under verification.</p><p>You will be notified by the communication channels you provided after verification is complete.</p><br><br><br><br><br><p>- WETHAQ team</p>"; //EMAIL_TEXT_REGSITRATION_FIRST_PHASE;
    	EMAIL_TEXT=EMAIL_TEXT.replace("ENTT", companyName);
    	EMAIL_TEXT=EMAIL_TEXT.replace("loginid", loginid);
    	EMAIL_TEXT=EMAIL_TEXT.replace("pass-word", pass);
    	
    	
    /*	Properties prop = System.getProperties();
        prop.put("mail.smtp.auth", "true");
*/
        
        Properties prop = new Properties();
		prop.put("mail.smtp.host","smtp.gmail.com"/*SMTP_SERVER*/);
		///prop.put("mail.smtp.starttls.enable","true");
        prop.put("mail.smtp.port",587 /*EMAIL_PORT*/);
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS
        prop.put("mail.smtp.ssl.trust", "*");
        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("Bdevtestmailservice@gmail.com","Asdfg@123"/*USERNAME, PASSWORD*/);
                    }
                });

         
         
        
      /*  Session session = Session.getInstance(prop, null);
        Message msg = new MimeMessage(session);
*/
        try {

        	Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("Bdevtestmailservice@gmail.com"/*EMAIL_FROM*/));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(toEmail/*EMAIL_TO*/)
            );
            message.setSubject(EMAIL_SUBJECT);
          //  message.setText(EMAIL_TEXT);;
            logger.info("Failed to send email: pass: "+pass+", getName: " +companyName+"getUserId: " +loginid);
            logger.info("EMAIL_TEXT: "+EMAIL_TEXT);
            message.setDataHandler(new DataHandler(new HTMLDataSource(EMAIL_TEXT)));

            
			//SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
			//t.connect();
			 Transport.send(message, message.getAllRecipients());
            
			 
			 //t.close();
         //   Transport.send(message);

            System.out.println("Done");
        	
        	
        	
        	
        	
        	
        	
        	
        	
        	
        	
        	
        	
        	
           /* msg.setFrom(new InternetAddress(EMAIL_FROM));

            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(EMAIL_TO, false));

            msg.setSubject(EMAIL_SUBJECT);*/
			
			// TEXT email
            //msg.setText(EMAIL_TEXT);

			// HTML email
          /*  msg.setDataHandler(new DataHandler(new HTMLDataSource(EMAIL_TEXT)));

            
			SMTPTransport t = (SMTPTransport) session.getTransport("smtp");*/
			
			// connect
        /*    t.connect(SMTP_SERVER, USERNAME, PASSWORD);
			
			// send
            t.sendMessage(msg, msg.getAllRecipients());

            System.out.println("Response: " + t.getLastServerResponse());

            t.close();*/
        	
        	
        	
        	
            logger.info("Email sent successfully to "+companyName+", email:  "+toEmail);
        } catch (MessagingException e) {
            e.printStackTrace();
            /* fall back silently */
            logger.info("Email sent failed "+companyName+", email:  "+toEmail);
        }

    }

    static class HTMLDataSource implements DataSource {

        private String html;

        public HTMLDataSource(String htmlString) {
            html = htmlString;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            if (html == null) throw new IOException("html message is null!");
            return new ByteArrayInputStream(html.getBytes());
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            throw new IOException("This DataHandler cannot write HTML");
        }

        @Override
        public String getContentType() {
            return "text/html";
        }

        @Override
        public String getName() {
            return "HTMLDataSource";
        }
    }
}