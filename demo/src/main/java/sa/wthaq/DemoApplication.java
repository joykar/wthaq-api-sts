package sa.wthaq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

//import springfox.documentation.swagger2.annotations.EnableSwagger2;

@PropertySource(value="classpath:application.properties")
@SpringBootApplication(scanBasePackages= {"sa.wthaq.*"}/*,exclude={DataSourceAutoConfiguration.class}*/)
//@ComponentScan("com.wethaq.*") /*(basePackageClasses = TabadulController.class) */
@EnableConfigurationProperties({
    FileStorageProperties.class
})
@EnableJpaRepositories //(basePackages="sa.wthaq.JPA.Repository", entityManagerFactoryRef="entityManagerFactory") /*("com.wethaq.elog.JPA.Repository")*/
@EntityScan(basePackages ="sa.wthaq.Entities")
@EnableTransactionManagement
@EnableJpaAuditing
@EnableSwagger2
public class DemoApplication {

	public static void main(String[] args) throws JSONException {
		SpringApplication.run(DemoApplication.class, args);
	}

}
