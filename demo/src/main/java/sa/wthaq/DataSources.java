package sa.wthaq;
 
	import java.beans.PropertyVetoException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.servlet.MultipartConfigElement;
import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
	import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
	import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
	import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
	import org.springframework.boot.context.properties.ConfigurationProperties;
	import org.springframework.boot.jdbc.DataSourceBuilder;
	import org.springframework.context.annotation.Bean;
	import org.springframework.context.annotation.Configuration;
	import org.springframework.context.annotation.Primary;
	import org.springframework.context.annotation.Profile;
	import org.springframework.context.annotation.PropertySource;
	import org.springframework.core.env.Environment;
	import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
	import org.springframework.http.HttpMethod;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
/*import org.springframework.security.authentication.AuthenticationManager;
	import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
	import org.springframework.security.config.annotation.web.builders.HttpSecurity;
	import org.springframework.security.config.annotation.web.builders.WebSecurity;
	import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
	import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
	import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
	import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
	import org.springframework.security.core.userdetails.User;
	import org.springframework.security.core.userdetails.UserDetails;
	import org.springframework.security.core.userdetails.UserDetailsService;
	import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
	import org.springframework.security.crypto.password.PasswordEncoder;
	import org.springframework.security.provisioning.InMemoryUserDetailsManager;
	import org.springframework.security.web.AuthenticationEntryPoint;*/
	import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

import com.zaxxer.hikari.HikariDataSource;

	import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
	
	
	@Configuration
	//@Component
	@PropertySource(value="classpath:application.properties")
	//@EnableJpaRepositories("com.wethaq.elog.JPA.Repository")
	@EnableTransactionManagement
	@EntityScan(basePackages ="sa.wthaq.Entities")
	public class DataSources  {
	 
		/*  @Value("${spring.datasource.driver.class}")
		    private String DB_DRIVER;

		    @Value("${spring.datasource.password}")
		    private String DB_PASSWORD;

		    @Value("${spring.datasource.url}")
		    private String DB_URL;

		    @Value("${spring.datasource.username}")
		    private String DB_USERNAME;

		    @Value("${hibernate.dialect}")
		    private String HIBERNATE_DIALECT;

		    @Value("${spring.jpa.show-sql}")
		    private String HIBERNATE_SHOW_SQL;

		    @Value("${spring.jpa.hibernate.ddl-auto}")
		    private String HIBERNATE_HBM2DDL_AUTO;

		    @Value("${entitymanager.packagesToScan}")
		    private String ENTITYMANAGER_PACKAGES_TO_SCAN;
		*/
		
		
		
		
		
	    
		@Autowired
		private Environment env;
		
		@Autowired
		private DataSourceProperties dataSourceProperties;
	    
		 @Primary
		@Bean/*(name="elgDS")*/
		public DataSource customDataSource() {

		    DriverManagerDataSource dataSource = new DriverManagerDataSource();
		    dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
		    dataSource.setUrl(env.getProperty("spring.datasource.url"));
		    dataSource.setUsername(env.getProperty("spring.datasource.username"));
		    dataSource.setPassword(env.getProperty("spring.datasource.password"));

		    return dataSource;

		}
	     
		 @Bean
		    public JdbcTemplate jdbcTemplate()
		    {
		        return new JdbcTemplate(customDataSource());
		    }


		/*@Bean(name="elgDSP")
	    @ConfigurationProperties("spring.datasource")
	    public DataSourceProperties dataSourceProperties() {
	        return new DataSourceProperties();
	    }*/

	    /* @Bean(name="elgHikariDS")
	    @ConfigurationProperties("app.datasource")
	    public HikariDataSource dataSource(DataSourceProperties properties) {
	        return properties.initializeDataSourceBuilder().type(HikariDataSource.class)
	                .build();
	    } */
		
	     
	     /*@Primary
	       @Bean  (name = "entityManagerFactorySession")
	     public LocalSessionFactoryBean sessionFactory() {
	         LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	         sessionFactory.setDataSource(customDataSource());
	         sessionFactory.setPackagesToScan("sa.wthaq.Entities");
	         Properties hibernateProperties = new Properties();
	         hibernateProperties.put("hibernate.dialect", HIBERNATE_DIALECT);
	         hibernateProperties.put("hibernate.show_sql", HIBERNATE_SHOW_SQL);
	         hibernateProperties.put("hibernate.hbm2ddl.auto", HIBERNATE_HBM2DDL_AUTO);
	         sessionFactory.setHibernateProperties(hibernateProperties);
	         return sessionFactory;
	     }  */ 
	     
	      @Bean(name = "entityManagerFactory")
	     public EntityManagerFactory entityManagerFactory() throws PropertyVetoException {
	         HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
	         vendorAdapter.setGenerateDdl(true);

	         LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
	         factory.setJpaVendorAdapter(vendorAdapter);
	         factory.setPackagesToScan(/*ENTITYMANAGER_PACKAGES_TO_SCAN*/ "sa.wthaq.Entities");
	         factory.setDataSource(customDataSource());
	         factory.afterPropertiesSet();

	         return factory.getObject();
	     } 
	     
	     
	     
	     /*@Bean
	     public HibernateTransactionManager transactionManager() {
	         HibernateTransactionManager txManager = new HibernateTransactionManager();
	         txManager.setSessionFactory(sessionFactory().getObject());
	         return txManager;
	     }*/
	     
	     
	      @Bean
	     public HibernateTransactionManager transactionManager(/*SessionFactory sf*/) {
	         try {
				return new HibernateTransactionManager(/*sf*/entityManagerFactory().unwrap(SessionFactory.class));
			} catch (PropertyVetoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
	     } 
	     
	     
	      /* @SuppressWarnings("deprecation")
		@Bean
	      public HibernateJpaSessionFactoryBean sessionFactory(EntityManagerFactory emf) {
	          HibernateJpaSessionFactoryBean factory = new HibernateJpaSessionFactoryBean();
	          try {
				factory.setEntityManagerFactory(entityManagerFactory());
			} catch (PropertyVetoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	          return factory;
	      } */
	     
	      
	      //@Autowired
	     // private EntityManagerFactory entityManagerFactory;

	      /*@Bean
	      public SessionFactory getSessionFactory() {
	           return entityManagerFactory.unwrap(SessionFactory.class);
	      }*/
	      
	     
	  /*   @Autowired
	 	private EntityManagerFactory entityManagerFactory;

	 	@Bean
	 	public SessionFactory getSessionFactory() {
	 	    if (entityManagerFactory.unwrap(SessionFactory.class) == null) {
	 	        throw new NullPointerException("factory is not a hibernate factory");
	 	    }
	 	    return entityManagerFactory.unwrap(SessionFactory.class);
	 	} 
*/
	     /* @Bean
	      public MultipartConfigElement multipartConfigElement() {
	          return new MultipartConfigElement("");
	      }
	     
	     
	      @Bean(name = "multipartResolver")
	      public CommonsMultipartResolver multipartResolver() {
	          CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
	          multipartResolver.setMaxUploadSize(10485760);
	          return multipartResolver;
	      }
	     */
	     
	      @Bean
	      public StandardServletMultipartResolver multipartResolver() {
	         return new StandardServletMultipartResolver();
	      }
	     
	     
	   /* @Bean
	    public PasswordEncoder passwordEncoder() {
	        return new BCryptPasswordEncoder();
	    }*/
	    
	    
	     
	    
	}
