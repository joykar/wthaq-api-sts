package sa.wthaq.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import org.springframework.beans.factory.annotation.Autowired;

import sa.wthaq.DTO.EntityModel;
import sa.wthaq.DTO.LgDataDto;
import sa.wthaq.DTO.LgIssuedDataDTO;
import sa.wthaq.DTO.LgListWithChangesAcceptSupplier;
import sa.wthaq.DTO.LgPendingListDTO;
import sa.wthaq.Entities.TSupplierdetail;
import sa.wthaq.Entities.TUsermaster;
import sa.wthaq.JPA.Repository.SupplierMasterRepository;
import sa.wthaq.models.LoginModel;
import sa.wthaq.models.RegistrationPvtEntt;
import sa.wthaq.models.ResponseModel;
import sa.wthaq.models.TabadulUser;

public interface ElogDao {
 
 	 public  Boolean checkIfCRNOK(String crnNo);
 	public TSupplierdetail findSupplierCRNWise(String crnNo);
 	public List<TSupplierdetail> findSuppliers();
 	public List<Object[]> findSuppliersIdAndName();
	public TUsermaster getLoginDetails(LoginModel lm); 
	public List<TSupplierdetail> findSuppliersDetailsWithoutMapped();
	public ResponseModel registerSupplier(RegistrationPvtEntt re);
	public ResponseModel registerActors(RegistrationPvtEntt re);
	public boolean checkValidLoginId(String tempLoginId);
	public List<TUsermaster> findUserByLoginIdAndPass(String lid, String pass);
	ResponseModel profileUpdateActors(RegistrationPvtEntt re);
	public List<?> getLGDetailsPendingForBeneficiary(Long login);
	public List<?> getLGDetailsPendingForSupplier(Long login);
	public List<EntityModel> getUnverifiedEntityList();
	public List<LgIssuedDataDTO> getAllLGApprovedDetailsForBeneficiary(String loginId);
	
	public List<LgIssuedDataDTO> getAllLGApprovedDetailsForSupplier(String loginId);
	
	public List<LgListWithChangesAcceptSupplier> getAllLGChangedTnCForSupplierList(String loginId);
	
	
	/*   JK   */
	public ResponseModel registerAdmin(TabadulUser ur);
	
	public List<EntityModel> getBeneficiaryUnverifiedEntityList();
	
	public List<EntityModel> getSupplierUnverifiedEntityList();

	
	
	/*   JK  */
}
