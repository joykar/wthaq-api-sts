package sa.wthaq.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sa.wthaq.Constants;
import sa.wthaq.DTO.EntityModel;
import sa.wthaq.DTO.LgDataDto;
import sa.wthaq.DTO.LgIssuedDataDTO;
import sa.wthaq.DTO.LgListWithChangesAcceptSupplier;
import sa.wthaq.DTO.LgPendingListDTO;
import sa.wthaq.Entities.TRolemaster;
import sa.wthaq.Entities.TSupplierdetail;
import sa.wthaq.Entities.TUsermaster;
import sa.wthaq.JPA.Repository.LinkUserMasterRepository;
import sa.wthaq.JPA.Repository.RoleMasterRepository;
import sa.wthaq.JPA.Repository.SupplierMasterRepository;
import sa.wthaq.JPA.Repository.UserMasterRepository;
import sa.wthaq.controller.TabadulController;
import sa.wthaq.models.LoginModel;
import sa.wthaq.models.RegistrationPvtEntt;
import sa.wthaq.models.ResponseModel;
import sa.wthaq.models.TabadulUser;


@Service("DAOService")
public class ElogDaoImpl  extends JdbcDaoSupport implements ElogDao{

	private static final Logger logger = LoggerFactory.getLogger(ElogDaoImpl.class);
	
	@Autowired
    private Environment env;
	
	@PersistenceContext
	private EntityManager entityManager;


	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;
	
	
	@Autowired
    private SupplierMasterRepository s_repository;
	
	
	@Autowired
    private RoleMasterRepository roleRepository;
	/*@Autowired
    private LinkUserMasterRepository linkRepository;*/
	
	@Autowired
    private UserMasterRepository userMasterRepository;
	 
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private DataSource customDataSource;
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	 /*@Autowired
	public setJdbcTemplate() {
		jdbcTemplate = new JdbcTemplate(customDataSource);
	} */
	
	 @Autowired
	 public void setJT(JdbcTemplate jdbcTemplate) {
	      setJdbcTemplate(jdbcTemplate);
	 }
	
	
	
	
	
	
	
	
	@Override
	 public  Boolean checkIfCRNOK(String crnNo) {
 		
javax.persistence.Query query = entityManager.createNativeQuery("SELECT count(cr_no) FROM T_SupplierDetails as em " +
	                "WHERE em.cr_no = ?", TSupplierdetail.class);
	        query.setParameter(1, crnNo);
	        if(query.getResultList().size()>0) {
	           return true;	        	
	        }
	        else {
	            return false;
	        }
	    
	 }



	@Override
	public TSupplierdetail findSupplierCRNWise(String crnNo) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<TSupplierdetail> findSuppliers() {
		// TODO Auto-generated method stub
		return s_repository.findAll();
	}



	@Override
	public List<Object[]> findSuppliersIdAndName() {
		// TODO Auto-generated method stub
		 
		javax.persistence.Query query = entityManager.createNativeQuery("SELECT em.supplier_id, em.supplier_name FROM T_SupplierDetails as em " +
                " ");
        
		List<Object[]>  suppliersIdAndName=query.getResultList(); 
         
         return suppliersIdAndName;
         
	}

	
	@Override
 @Transactional
	public ResponseModel registerSupplier(RegistrationPvtEntt re) {
		// TODO Auto-generated method stub
	 	try {
			
			
			//SessionFactory sessionF = (SessionFactory) entityManagerFactory.unwrap(SessionFactory.class);//.openSession();
	        
	       // Session sess=sessionF.openSession();
	        
			
			//  transaction = session.beginTransaction();
	     //   transaction.begin();

	        TSupplierdetail sd=new TSupplierdetail();
	        sd.setContactInfo(""+re.getMobileNo());
	        sd.setCrnNo(new BigDecimal(re.getCommRegNo()));
	        sd.setLegAgreeDocName(""+re.getCrFile());
	        sd.setLegAgreeDocPath(""+re.getCrFilePath());
	        sd.setLoginId(""+re.getUserId());
	        sd.setNationalIdDocName(""+re.getLaglFile());
	        sd.setNationalIdDocPath(""+re.getLaglFilePath());
	        sd.setPasswd(""+re.getPassword());
	        sd.setPinCode(""+re.getPINCode());
	        sd.setRepEmail("");
	        sd.setRepId(""+re.getRepresentativeID());
	        sd.setRepMobileNo("");
	        sd.setRepName(""+re.getRepresentativeName());
	        sd.setRepNameAr("");
	        sd.setState("");
	        sd.setSupplierAddr1(""+re.getAddress1());
	        sd.setSupplierAddr2( ""+re.getAddress2());
	        sd.setSupplierAddr1Ar("");
	        sd.setSupplierAddr2Ar("");
	        sd.setSupplierName(""+re.getName());
	        sd.setSupplierStatusFlag("INACTIVE");
	        sd.setCommRegDocName(""+re.getRniFile());
	        sd.setAuthLettterDocName(""+re.getLalFile());
	        sd.setCommRegDocPath(""+re.getRniFilePath()); 
	       sd.setAuthLettterDocPath(""+re.getLalFilePath()); 
	       sd.setNationalId(new BigDecimal(re.getNationalId()));
		 
		 
			      
	      

	       // transaction.commit();
			
	       TSupplierdetail tsd=   s_repository.save(sd);
			 	          // sess.persist(sd);//.save(sd);//sd.getSupplierId();
			 	         
			 	         s_repository.flush();
			 	        int sid =(int) tsd.getSupplierId();
			 	           //  session.flush();
			 	        logger.info("sid: "+sid);
	      

	        
	        List<TRolemaster> trm=  roleRepository.findByRoleId(1);
	        
	        logger.info("trm: "+trm.get(0).getRoleDesc());
	        
	     /*   T_LinkUser lu=new T_LinkUser();
	        lu.setTSupplierDetail(tsd);**/
	       // T_LinkUserPK  lupk=new T_LinkUserPK ();
	       // lupk.setRoleId(trm.get(0).getRoleId());
	       /// lu/*.setId(lupk);*/.setRoleId(trm.get(0).getRoleId());
	      ///  T_LinkUser tlu=   linkRepository.save(lu);
	      ///  linkRepository.flush();
	       /// int uid = tlu/*.getId()*/.getUserId();
	      ///  logger.info("uid: "+uid);
	        
	       //  sess.persist(lu);//sd.getSupplierId();
	       // sessionF.openSession().flush();
	       //  int uid = lu.getUserId();
	         
	         
	       //  logger.info("uid: "+uid);
	        /*
	        
	        javax.persistence.Query query =     entityManager.createNativeQuery("INSERT INTO T_LinkUser" + 
					"	(" + 
					"	user_id," + 
					"	role_id," + 
					"	supplier_id," + 
					"	beneficiary_id," + 
					"	create_on," + 
					"	create_by," + 
					"	modified_on," + 
					"	modified_by" + 
					"	delete_flag" + 
					"	)" + 
					"VALUES " + 
					"	(" + 
					"	:user_id," + 
					"   :role_id," + 
					"	:supplier_id," + 
					"	:beneficiary_id," + 
					"	:create_on," + 
					"	:create_by," + 
					"	:modified_on," + 
					"	:modified_by" + 
					"	:delete_flag" + 
					"	)" + 
					"");
			
			query.setParameter("role_id", 1);
			query.setParameter("supplier_id", ""+sid);
			query.setParameter("beneficiary_id", null);
			query.setParameter("create_by", "SYSTEM");
			query.setParameter("modified_by", "");
			 
			
			
			BigInteger uiid = (BigInteger) query.getSingleResult();
			long uid = uiid.longValue();
			*/
			
			
	        TUsermaster um = new TUsermaster();
			um.setLoginId(re.getUserId());
			um.setPassword(""+re.getPassword());
			//T_UserMasterPK tupk =new T_UserMasterPK();
			//tupk.setRoleId(trm.get(0).getRoleId());
			 
			um.setRoleId((int)trm.get(0).getRoleId());
			um.setUserStatus(""+env.getProperty("CC_PENDING"));
			um.setUserName(re.getUserName());
			um.setLanguage("EN");
			
			TUsermaster tum=	userMasterRepository.save(um);
			userMasterRepository.flush();
			int umid = tum/*.getId()*/.getUserId();
			
			//  sess.persist(um); 
			//  int umid = um.getUserId();
			
			  logger.info("umid: "+umid);
	        /*javax.persistence.Query query =    	entityManager.createNativeQuery("INSERT INTO  T_UserMaster" + 
					"	(" + 
					"	login_id," + 
					 "	user_id," +  
					"	password," + 
					"	role_id," + 
					"	user_name," + 
					"	user_name_ar," + 
					"	user_status," + 
					"	language," + 
					"	create_on," + 
					"	create_by," + 
					"	modified_on," + 
					"	modified_by" + 
					"	delete_flag" + 
					"	)" + 
					"VALUES " + 
					"	(" + 
					"	:login_id," + 
				 	"	:user_id," + 
					"	:password," + 
					"	:role_id," + 
					"	:user_name," + 
					"	:user_name_ar," + 
					"	:user_status," + 
					"	:language," + 
					"	:create_on," + 
					"	:create_by," + 
					"	:modified_on," + 
					"	:modified_by" + 
					"	:delete_flag" + 
					"	)" + 
					"");
			
			query.setParameter("login_id", re.getUserId());
			query.setParameter("password", ""+re.getPassword());
			query.setParameter("user_id", ""+uid.getUserId());
			query.setParameter("role_id", 1);
			query.setParameter("user_name", re.getUserName());
			query.setParameter("user_name_ar", "");
			query.setParameter("user_status", ""+env.getProperty("CC_PENDING"));
			query.setParameter("language", "");
			 
			query.setParameter("create_by", "SYSTEM");
			query.setParameter("modified_by", "");
			 
			*/
			 
			
			
			
			
			
//	List<Object[]>  suppliersIdAndName=query.getResultList(); 
			ResponseModel rm =new ResponseModel();
			rm.setResponseMessage(Constants.REGISTRATION_SUCCESS_PENDING_VERIFICATION);
			rm.setResponseCode(Constants.SUCCESS);
			 return rm;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			 
			
			e.printStackTrace();
			
		}
		
		ResponseModel rm =new ResponseModel();
		rm.setResponseMessage(Constants.REGISTRATION_ERROR);
		rm.setResponseCode(Constants.ERROR);
		return rm;
	}
	

	@Override
 @Transactional(rollbackFor = Exception.class)
	public ResponseModel registerActors(RegistrationPvtEntt re) {
  
		String INSERT_SUPPLIER =   " INSERT INTO T_SupplierDetails" + 
				"	(" + 
				/*"	supplier_id," + */
				"	supplier_name," + 
				"	supplier_name_ar," + 
				"	cr_no," + 
				"	contact_info," + 
				"	supplier_addr1," + 
				"	supplier_addr1_ar," + 
				"	supplier_addr2," + 
				"	supplier_addr2_ar," + 
				"	state," + 
				"	country," + 
				"	pin_code," + 
				"	rep_id," + 
				"	rep_name," + 
				"	rep_name_ar," + 
				"	rep_email," + 
				"	rep_mobile_no," + 
				"	comm_reg_doc_name," + 
				"	comm_reg_doc_path," + 
				"	national_id_doc_name," + 
				"	national_id_doc_path," + 
				"	auth_lettter_doc_name," + 
				"	auth_lettter_doc_path," + 
				"	leg_agree_doc_name," + 
				"	leg_agree_doc_path," + 
				"	agree_tag," + 
				"	supplier_status_flag," + 
				"	login_id," + 
				"	passwd" + 
			/*	"	create_on," + 
				"	create_by," + 
				"	modified_on," + 
				"	modified_by," + 
				"	delete_flag" + */
				"	)" + 
				"VALUES " + 
				"	(" + 
			/*	"	:supplier_id," + */
				"	:supplier_name," + 
				"	:supplier_name_ar," + 
				"	:crn_no," + 
				"	:contact_info," + 
				"	:supplier_addr1," + 
				"	:supplier_addr1_ar," + 
				"	:supplier_addr2," + 
				"	:supplier_addr2_ar," + 
				"	:state," + 
				"	:country," + 
				"	:pin_code," + 
				"	:rep_id," + 
				"	:rep_name," + 
				"	:rep_name_ar," + 
				"	:rep_email," + 
				"	:rep_mobile_no," + 
				"	:comm_reg_doc_name," + 
				"	:comm_reg_doc_path," + 
				"	:national_id_doc_name," + 
				"	:national_id_doc_path," + 
				"	:auth_lettter_doc_name," + 
				"	:auth_lettter_doc_path," + 
				"	:leg_agree_doc_name," + 
				"	:leg_agree_doc_path," + 
				"	:agree_tag," + 
				"	:supplier_status_flag," + 
				"	:login_id," + 
				"	:passwd" + 
				/*"	:create_on," + 
				"	:create_by," + 
				"	:modified_on," + 
				"	:modified_by," + 
				"	:delete_flag" + */
				"	)" + 
				"";
		 
		
	String INSERT_LINK=	"INSERT INTO T_LinkUser" + 
		"	(" + 
		/*"	user_id," + */
		"	role_id," + 
		"	supplier_id," + 
		"	beneficiary_id" + 
		/*"	create_on," + 
		"	create_by," + 
		"	modified_on," + 
		"	modified_by" + 
		"	delete_flag" + */
		"	)" + 
		"VALUES " + 
		"	(" + 
	/*	"	:user_id," + */
		"   :role_id," + 
		"	:supplier_id," + 
		"	:beneficiary_id" + 
		/*"	:create_on," + 
		"	:create_by," + 
		"	:modified_on," + 
		"	:modified_by" + 
		"	:delete_flag" + */
		"	)" + 
		"";
		
String INSERT_USER=	"INSERT INTO  T_UserMaster" + 
	"	(" + 
	"	login_id," + 
	 "	user_id," +  
	"	password," + 
	"	role_id," + 
	"	user_name," + 
	"	user_name_ar," + 
	"	user_status," + 
	"	language" + 
	/*"	create_on," + 
	"	create_by," + 
	"	modified_on," + 
	"	modified_by" + 
	"	delete_flag" + */
	"	)" + 
	"VALUES " + 
	"	(" + 
	"	:login_id," + 
 	"	:user_id," + 
	"	:password," + 
	"	:role_id," + 
	"	:user_name," + 
	"	:user_name_ar," + 
	"	:user_status," + 
	"	:language" + 
	/*"	:create_on," + 
	"	:create_by," + 
	"	:modified_on," + 
	"	:modified_by" + 
	"	:delete_flag" + */
	"	)" + 
	"";
		





try {
	/*  insert supplier   */
	KeyHolder holder = new GeneratedKeyHolder();
	SqlParameterSource parameters = new MapSqlParameterSource()
			
	.addValue("supplier_name", ""+re.getEntityName())
	.addValue("supplier_name_ar", "")
	.addValue("crn_no", ""+re.getCommRegNo())
	.addValue("contact_info", ""+re.getOfficeMobileNumber())
	.addValue("supplier_addr1", ""+re.getAddress1())
	.addValue("supplier_addr1_ar", "")
	.addValue("supplier_addr2", ""+re.getAddress2())
	.addValue("supplier_addr2_ar", "")
	.addValue("state", ""+re.getState())
	.addValue("country", ""+re.getCountry())
	.addValue("pin_code", ""+re.getPINCode())
	.addValue("rep_id", ""+re.getRepresentativeID())
	.addValue("rep_name", ""+re.getName())
	.addValue("rep_name_ar", "")
	.addValue("rep_email", re.getEmail())
	.addValue("rep_mobile_no", re.getMobile())
	.addValue("comm_reg_doc_name", ""+re.getRniFile())
	.addValue("comm_reg_doc_path", ""+re.getRniFilePath())
	.addValue("national_id_doc_name", ""+re.getLaglFile())
	.addValue("national_id_doc_path", ""+re.getLaglFilePath())
	.addValue("auth_lettter_doc_name", ""+re.getLalFile())
	.addValue("auth_lettter_doc_path", ""+re.getLalFilePath())
	.addValue("leg_agree_doc_name", ""+re.getCrFile())
	.addValue("leg_agree_doc_path", ""+re.getCrFilePath())
	.addValue("agree_tag", "")
	.addValue("supplier_status_flag", env.getProperty("CC_PENDING"))
	.addValue("login_id", ""+re.getUserId())
	.addValue("passwd", ""+re.getPassword())
	.addValue("national_id", ""+re.getNationalId())
	
	/*.addValue("create_by", "SYSTEM")
	.addValue("modified_by", "")*/;
	
	
	namedParameterJdbcTemplate.update(INSERT_SUPPLIER, parameters, holder);
	
	int supplierId=holder.getKey().intValue();
	
	logger.info("Named Param:supplierId "+supplierId);
	 
	
	
	
	/*  insert link   */
	 /** holder = new GeneratedKeyHolder();
	  parameters = new MapSqlParameterSource()
	.addValue("role_id", 1) 
	  .addValue("supplier_id", ""+supplierId) 
	  .addValue("beneficiary_id", null) **/
	/*  .addValue("create_by", "SYSTEM") 
	  .addValue("modified_by", "")*/;
	 
	/**namedParameterJdbcTemplate.update(INSERT_LINK, parameters, holder);
	
	
	int linkUId=(int) holder.getKeys().get("user_id");
	int linkRId=(int) holder.getKeys().get("role_id");
	
	logger.info("Named Param:linkUId "+linkUId+", linkRId: "+linkRId);
	 **/
	 
	
	
	
	/*  insert user   */
	holder = new GeneratedKeyHolder();
	parameters = new MapSqlParameterSource()
	.addValue("login_id", re.getUserId()) 
	.addValue("password", ""+re.getPassword()) 
	.addValue("user_id", ""+/*linkUId*/supplierId) 
	.addValue("role_id", 1) 
	.addValue("user_name", re.getUserName()) 
	.addValue("user_name_ar", "") 
	.addValue("user_status", Constants.ACTIVE) 
	.addValue("language", "EN") 
	 
	/*.addValue("create_by", "SYSTEM") 
	.addValue("modified_by", "")*/;
	
	namedParameterJdbcTemplate.update(INSERT_USER, parameters, holder);
	//int linkId=holder.getKey().intValue();
	//logger.info("Named Param:linkId "+holder.getKey().intValue());
	
	
	
	
	
	ResponseModel rm =new ResponseModel();
	rm.setResponseMessage(Constants.REGISTRATION_SUCCESS_PENDING_VERIFICATION);
	rm.setResponseCode(Constants.SUCCESS);
	 return rm;
} catch (InvalidDataAccessApiUsageException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (DataAccessException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}



ResponseModel rm =new ResponseModel();
rm.setResponseMessage(Constants.REGISTRATION_ERROR);
rm.setResponseCode(Constants.ERROR);
return rm;


	} 
 
 
 

@Override
public TUsermaster getLoginDetails(LoginModel lm) {
	// TODO Auto-generated method stub
	 
	
	try {
		javax.persistence.Query query = entityManager.createNativeQuery(" Select user_id, user_name, role_id, user_status From T_UserMaster Where upper(login_id) = upper(:login_id) " + 
				"");
		query.setParameter("login_id", ""+lm.getUserId());
		
		TUsermaster um=(TUsermaster) query.getSingleResult();
		
		
		return um;
	} catch (javax.persistence.NoResultException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	}
	catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	}
 	
	 
}







@Override
public boolean checkValidLoginId(String tempLoginId) {
	// TODO Auto-generated method stub
	System.out.println("tempLoginId"+tempLoginId);
	
	try {
		javax.persistence.Query query = entityManager.createNativeQuery(" Select login_id From T_UserMaster Where upper(login_id) = upper(:login_id) " + 
				"");
		query.setParameter("login_id", ""+tempLoginId);
	 	String um=(String) query.getSingleResult();
	 	logger.info("um: "+um);
	 	System.out.println("um"+um);
		if(um==null) {
		 	return true;
		 }
		else {
			return false;
	 	}
 	} catch (javax.persistence.NoResultException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return true;
	}
	catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return true;
	}
	 
 
}







@Override
public List<TUsermaster> findUserByLoginIdAndPass(String lid, String pass) {
	return null;
	// TODO Auto-generated method stub
	 
}







@Override
public ResponseModel profileUpdateActors(RegistrationPvtEntt re) {
	// TODO Auto-generated method stub
	

	String INSERT_SUPPLIER =   " UPDATE T_SupplierDetails" + 
			" SET	" + 
			/*"	supplier_id," + */
			"	supplier_name," + 
			"	supplier_name_ar," + 
			"	cr_no," + 
			"	contact_info," + 
			"	supplier_addr1," + 
			"	supplier_addr1_ar," + 
			"	supplier_addr2," + 
			"	supplier_addr2_ar," + 
			"	state," + 
			"	country," + 
			"	pin_code," + 
			"	rep_id," + 
			"	rep_name," + 
			"	rep_name_ar," + 
			"	rep_email," + 
			"	rep_mobile_no," + 
			"	comm_reg_doc_name," + 
			"	comm_reg_doc_path," + 
			"	national_id_doc_name," + 
			"	national_id_doc_path," + 
			"	auth_lettter_doc_name," + 
			"	auth_lettter_doc_path," + 
			"	leg_agree_doc_name," + 
			"	leg_agree_doc_path," + 
			"	agree_tag," + 
			/*"	supplier_status_flag," + */
		/*	"	login_id," + */
		/*	"	passwd," + */
		/*	"	create_on," + 
			"	create_by," + 
			"	modified_on," + */ 
			"	modified_by" + 
/*			"	delete_flag" + */
			"	" + 
			"VALUES " + 
			"	" + 
		/*	"	:supplier_id," + */
			"	:supplier_name," + 
			"	:supplier_name_ar," + 
			"	:crn_no," + 
			"	:contact_info," + 
			"	:supplier_addr1," + 
			"	:supplier_addr1_ar," + 
			"	:supplier_addr2," + 
			"	:supplier_addr2_ar," + 
			"	:state," + 
			"	:country," + 
			"	:pin_code," + 
			"	:rep_id," + 
			"	:rep_name," + 
			"	:rep_name_ar," + 
			"	:rep_email," + 
			"	:rep_mobile_no," + 
			"	:comm_reg_doc_name," + 
			"	:comm_reg_doc_path," + 
			"	:national_id_doc_name," + 
			"	:national_id_doc_path," + 
			"	:auth_lettter_doc_name," + 
			"	:auth_lettter_doc_path," + 
			"	:leg_agree_doc_name," + 
			"	:leg_agree_doc_path," + 
			"	:agree_tag," + 
			/*"	:supplier_status_flag," + */
		/*	"	:login_id," + */
		/*	"	:passwd" + */
			/*"	:create_on," + 
			"	:create_by," + 
			"	:modified_on," + */ 
			"	:modified_by" + 
/*			"	:delete_flag" + */
			"	 WHERE login_id= :supplier_id" + 
			"";
	 
	 
	 
	
	String INSERT_SUPPLIER1="UPDATE T_SupplierDetails " + 
			"SET "
			/*+ "supplier_id = @supplier_id,"*/ + 
			/*"	supplier_name = :supplier_name," + */
			/*"	supplier_name_ar = :supplier_name_ar," + */
		/*	"	crn_no = :crn_no," + */
			"	contact_info = :contact_info," + 
			"	supplier_addr1 = :supplier_addr1," + 
			"	supplier_addr1_ar = :supplier_addr1_ar," + 
			"	supplier_addr2 = :supplier_addr2," + 
			"	supplier_addr2_ar = :supplier_addr2_ar," + 
			"	state = :state," + 
			"	country = :country," + 
			"	pin_code = :pin_code," + 
			"	rep_id = :rep_id," + 
			"	rep_name = :rep_name," + 
			"	rep_name_ar = :rep_name_ar," + 
			"	rep_email = :rep_email," + 
			"	rep_mobile_no = :rep_mobile_no," + 
			"	comm_reg_doc_name = :comm_reg_doc_name," + 
			"	comm_reg_doc_path = :comm_reg_doc_path," + 
			"	national_id_doc_name = :national_id_doc_name," + 
			"	national_id_doc_path = :national_id_doc_path," + 
			"	auth_lettter_doc_name = :auth_lettter_doc_name," + 
			"	auth_lettter_doc_path = :auth_lettter_doc_path," + 
			"	leg_agree_doc_name = :leg_agree_doc_name," + 
			"	leg_agree_doc_path = :leg_agree_doc_path," + 
			/*"	agree_tag = :agree_tag," + */
			/*"	supplier_status_flag = :supplier_status_flag," + 
			"	login_id = :login_id," + 
			"	passwd = :passwd," + 
			"	delete_flag = :delete_flag," + 
			"	create_on = :create_on," + 
			"	create_by = :create_by," + 
			"	modified_on = :modified_on," + */
			"	modified_by = :modified_by," + 
			"	national_id = :national_id" + 
			"	WHERE login_id= :supplier_id" + 
			"";



try {
/*  update supplier   */
 
SqlParameterSource parameters = new MapSqlParameterSource()
/*.addValue("supplier_name", ""+re.getEntityName())*/
/*.addValue("supplier_name_ar", "")*/
/*.addValue("crn_no", ""+re.getCommRegNo())*/
.addValue("contact_info", ""+re.getOfficeEMail())
.addValue("supplier_addr1", ""+re.getAddress1())
.addValue("supplier_addr1_ar", "")
.addValue("supplier_addr2", ""+re.getAddress2())
.addValue("supplier_addr2_ar", "")
.addValue("state", ""+re.getState())
.addValue("country", ""+re.getCountry())
.addValue("pin_code", ""+re.getPINCode())
.addValue("rep_id", ""+re.getRepresentativeID())
.addValue("rep_name", ""+re.getName())
.addValue("rep_name_ar", "")
.addValue("rep_email", re.getEmail())
.addValue("rep_mobile_no", re.getMobileNo())
.addValue("comm_reg_doc_name", ""+re.getRniFile())
.addValue("comm_reg_doc_path", ""+re.getRniFilePath())
.addValue("national_id_doc_name", ""+re.getLaglFile())
.addValue("national_id_doc_path", ""+re.getLaglFilePath())
.addValue("auth_lettter_doc_name", ""+re.getLalFile())
.addValue("auth_lettter_doc_path", ""+re.getLalFilePath())
.addValue("leg_agree_doc_name", ""+re.getCrFile())
.addValue("leg_agree_doc_path", ""+re.getCrFilePath())
/*.addValue("agree_tag", "")*/
/*.addValue("supplier_status_flag", "INACTIVE")*/
/*.addValue("login_id", ""+re.getUserId())*/
/*.addValue("passwd", ""+re.getPassword())*/
.addValue("national_id", ""+re.getNationalId())
.addValue("supplier_id", ""+re.getUserId())
.addValue("modified_by", ""+re.getUserId());


namedParameterJdbcTemplate.update(INSERT_SUPPLIER1, parameters);
 
 
 
 
ResponseModel rm =new ResponseModel();
rm.setResponseMessage(Constants.PROFILE_SETTINGS_UPDATED_SUCCESS);
rm.setResponseCode(Constants.SUCCESS);
return rm;
} catch (InvalidDataAccessApiUsageException e) {
// TODO Auto-generated catch block
e.printStackTrace();
} catch (DataAccessException e) {
// TODO Auto-generated catch block
e.printStackTrace();
}



ResponseModel rm =new ResponseModel();
rm.setResponseMessage(Constants.PROFILE_SETTINGS_UPDATED_ERROR);
rm.setResponseCode(Constants.ERROR);
return rm;


	
 
}








@Override
public List<TSupplierdetail> findSuppliersDetailsWithoutMapped() {
	javax.persistence.Query query = entityManager.createNativeQuery("SELECT em.supplier_id,em.supplier_name," + 
			"	em.supplier_name_ar," + 
			"	em.cr_no," + 
			"	em.contact_info," + 
			"	em.supplier_addr1," + 
			"	em.supplier_addr1_ar," + 
			"	em.supplier_addr2," + 
			"	em.supplier_addr2_ar," + 
			"	em.state," + 
			"	em.country," + 
			"	em.pin_code," + 
			"	em.rep_id," + 
			"	em.rep_name," + 
			"	em.rep_name_ar," + 
			"	em.rep_email," + 
			"	em.rep_mobile_no," + 
			"	em.national_id," + 
			"	em.comm_reg_doc_name," + 
			"	em.comm_reg_doc_path," + 
			"	em.national_id_doc_name," + 
			"	em.national_id_doc_path," + 
			"	em.auth_lettter_doc_name," + 
			"	em.auth_lettter_doc_path," + 
			"	em.leg_agree_doc_name," + 
			"	em.leg_agree_doc_path," + 
			"	em.agree_tag," + 
			"	em.supplier_status_flag," + 
			"	em.login_id," + 
			"	em.passwd," + 
			"	um.user_id,um.password,um.user_name,um.user_status,um.language,um.delete_flag FROM T_SupplierDetails em  "+
			 " INNER JOIN T_UserMaster um on em.login_id = um.login_id"+
			 " INNER JOIN T_RoleMaster rm on rm.role_id = um.role_id"+" ");
    //query.setParameter(1, crnNo);
	
	List<TSupplierdetail> um=(List<TSupplierdetail>) query.getResultList();
    if(query.getResultList().size()>0) {
       return um;	        	
    }
    else {
        return um;
    }
} 
@Override
	public List<LgPendingListDTO> getLGDetailsPendingForSupplier(Long login){
		javax.persistence.Query query = entityManager.createNativeQuery("select b.lg_type ,a.request_id,CONVERT(VARCHAR(10),d.request_date,103) request_date," + 
				"a.project_name,  " + 
				"CASE " + 
				"WHEN a.role_id = '"+env.getProperty("ROLE_SUPP_ID")+"' THEN" + 
				"    (select s.supplier_name from t_supplierdetails s" + 
				"	 where s.supplier_id = a.supplier_id) " + 
				"WHEN a.role_id = '"+env.getProperty("ROLE_BENF_ID")+"' THEN" + 
				"    (select s.beneficiary_name from t_beneficiarydetails s" + 
				"	 where s.beneficiary_id = a.beneficiary_id) END AS beneficiary_name," + 
				"a.terms_condition_type,a.contract_value,a.bank_code,c.bank_name,  " + 
				"				   CONVERT(VARCHAR(10), a.lg_start_date, 103)  lg_start_date,       " + 
				"					   CONVERT(VARCHAR(10), a.lg_end_date, 103)  lg_end_date,       " + 
				"					   a.project_id,       " + 
				"					   CONVERT(VARCHAR(10), a.zakat_start_date, 103)  zakat_start_date,   " + 
				"					   CONVERT(VARCHAR(10), a.zakat_start_date, 103)  zakat_end_date,     " + 
				"					   a.purpose_of_bond ,a.iban_no,a.byan_no,d.request_status_flag,d.currency_code	,g.currency,a.terms_condition_message, " + 
				"					   a.lg_issued_by,a.create_by,a.create_on, " + 
				"							 CASE  " + 
				"							WHEN d.request_status_flag = '"+Constants.PENDING+"' THEN '"+Constants.PENDING_CODE+"'  " + 
				"								WHEN d.request_status_flag = '"+Constants.NS_CHANGE+"' THEN '"+Constants.NS_CHANGE_CODE+"' " + 
				"								WHEN d.request_status_flag = '"+Constants.NS_ACCEPT+"' THEN '"+Constants.NS_ACCEPT_CODE+"' " + 
				"								WHEN d.request_status_flag = '"+Constants.NS_PENDING+"' THEN '"+Constants.NS_PENDING_CODE+"'" + 
				"								WHEN d.request_status_flag = '"+Constants.CANCELLED+"' THEN '"+Constants.CANCELLED_CODE+"'" + 
				"								ELSE 'ERROR'" + 
				"								END AS response_code  ,CONVERT(VARCHAR(10), a.lg_validity_date, 103)  lg_validity_date,a.agree_tag " + 
				"										FROM T_LgrequestIssue a       " + 
				"										INNER JOIN T_LgTypeMaster b ON a.lg_type_id = b.lg_type_id       " + 
				"										INNER JOIN T_BankMaster c   ON c.bank_code = a.bank_code       " + 
				"										INNER JOIN T_servicerequest d ON d.request_id = a.request_id  AND  d.login_id = a.login_id      " + 
				"										AND  d.request_status_flag IN ('PENDING','NS_CHANGE','NS_ACCEPT','NS_PENDING','CANCELLED') " + 
				"										INNER JOIN T_CurrencyTypeMaster g ON g.currency_code = d.currency_code  " + 
				"									WHERE (a.beneficiary_id = :loginID  OR a.supplier_id = :loginID ) " + 
				"										AND   ISNULL(a.delete_flag,'N') <> 'Y'        " + 
				"										 " + 
				"										ORDER BY d.request_date DESC",LgPendingListDTO.class);
		query.setParameter("loginID", login);
		
		List<LgPendingListDTO> lis = query.getResultList();
		/*if(query.getResultList().size() > 0) {*/
			return lis;
		/*
		 * }else { System.out.println("No data"); return null; }
		 */
	}
	
	@Override
	public List<Object[]> getLGDetailsPendingForBeneficiary(Long login){
		javax.persistence.Query query = entityManager.createNativeQuery("select b.lg_type ,a.request_id,CONVERT(VARCHAR(10),d.request_date,103) request_date," + 
				"a.project_name,  " + 
				"CASE " + 
				"WHEN a.role_id = '"+env.getProperty("ROLE_SUPP_ID")+"' THEN" + 
				"    (select s.supplier_name from t_supplierdetails s" + 
				"	 where s.supplier_id = a.supplier_id) " + 
				"WHEN a.role_id = '"+env.getProperty("ROLE_BENF_ID")+"' THEN" + 
				"    (select s.beneficiary_name from t_beneficiarydetails s" + 
				"	 where s.beneficiary_id = a.beneficiary_id) END AS beneficiaryName," + 
				"a.terms_condition_type,a.contract_value,a.bank_code,c.bank_name,  " + 
				"				   CONVERT(VARCHAR(10), a.lg_start_date, 103)  lg_start_date,       " + 
				"					   CONVERT(VARCHAR(10), a.lg_end_date, 103)  lg_end_date,       " + 
				"					   a.project_id,       " + 
				"					   CONVERT(VARCHAR(10), a.zakat_start_date, 103)  zakat_start_date,   " + 
				"					   CONVERT(VARCHAR(10), a.zakat_start_date, 103)  zakat_end_date,     " + 
				"					   a.purpose_of_bond ,a.iban_no,a.byan_no,d.request_status_flag,d.currency_code	,g.currency AS currency,a.terms_condition_message, " + 
				"					   a.lg_issued_by,a.create_by,a.create_on, " + 
				"							 CASE  " + 
				"							WHEN d.request_status_flag = '"+Constants.PENDING+"' THEN '"+Constants.PENDING_CODE+"'  " + 
				"								WHEN d.request_status_flag = '"+Constants.NS_CHANGE+"' THEN '"+Constants.NS_CHANGE_CODE+"' " + 
				"								WHEN d.request_status_flag = '"+Constants.NS_ACCEPT+"' THEN '"+Constants.NS_ACCEPT_CODE+"' " + 
				"								WHEN d.request_status_flag = '"+Constants.NS_PENDING+"' THEN '"+Constants.NS_PENDING_CODE+"' " + 
				"								WHEN d.request_status_flag = '"+Constants.CANCELLED+"' THEN '"+Constants.CANCELLED_CODE+"' " + 
				"								ELSE 'ERROR'" + 
				"								END AS response_code  ,CONVERT(VARCHAR(10), a.lg_validity_date, 103)  lg_validity_date,a.agree_tag " + 
				"										FROM T_LgrequestIssue a       " + 
				"										INNER JOIN T_LgTypeMaster b ON a.lg_type_id = b.lg_type_id       " + 
				"										INNER JOIN T_BankMaster c   ON c.bank_code = a.bank_code       " + 
				"										INNER JOIN T_servicerequest d ON d.request_id = a.request_id  AND  d.login_id = a.login_id      " + 
				"										AND  d.request_status_flag IN ('"+Constants.PENDING+"','"+Constants.NS_CHANGE+"','"+Constants.NS_ACCEPT+"','"+Constants.NS_PENDING+"','"+Constants.CANCELLED+"') " + 
				"										INNER JOIN T_CurrencyTypeMaster g ON g.currency_code = d.currency_code  " + 
				"									WHERE (a.beneficiary_id = :loginID  OR a.supplier_id = :loginID )       " + 
				"										AND   ISNULL(a.delete_flag,'N') <> 'Y'        " + 
				"										 " + 
				"										ORDER BY d.request_date DESC");
		query.setParameter("loginID", login);
				
				List<Object[]> lis = query.getResultList();
				if(query.getResultList().size() > 0) {
					return lis;
				}else {
					System.out.println("No data");
					return null;
				}
			}
	
	/*   JK       */
	
	 @Override
     @Transactional(rollbackFor = Exception.class)
     public ResponseModel registerAdmin(TabadulUser ur) {
             // TODO Auto-generated method stub
             
             ResponseModel rm=new ResponseModel();
             
             
              
             String INSERT_ADMIN =   "  INSERT INTO T_AdminRegistration" + 
                             "" + 
                             "  ( admin_name   ," + 
                             "    admin_mobile ," + 
                             "    admin_email  ," + 
                             "    national_id  ," + 
                             "    login_id     ," + 
                             "    admin_status_flag" + 
                             "  )" + 
                             "  VALUES ( :name           ," + 
                             "        :mobile         ," + 
                             "        :email          ," + 
                             "        :national_id   ," + 
                             "        :user_name      ," + 
                             "        '"+Constants.ACTIVE+"' );" + 
                             "" + 
                             "";
              
             
     String INSERT_USERMASTER=        " INSERT INTO t_UserMaster" + 
                     "  (login_id ," + 
                     "  user_id  ," + 
                     "  password ," + 
                     "  role_id  ," + 
                     "  user_name ," + 
                     "  user_status ," + 
                     "  language    )" + 
                     "  VALUES ( :user_id   ," + 
                     "           :admin_id  ," + 
                     "                    :passwd    ," + 
                     "                    :role          ," + 
                     "                    :user_name ," + 
                     "                  '"+Constants.ACTIVE+"' ," + 
                     "                  'EN')" + 
                     "" + 
             "";
             


try {
     
     
      
     //get role for admin
     List<TRolemaster> rmList=roleRepository.findByRoleDesc(env.getProperty("ROLE_ADMIN"));
     
     if(rmList.size()<1) {
             rm.setResponseCode(Constants.ERROR);
             rm.setResponseMessage(Constants.REGISTRATION_ERROR+" The specified role does not exists!");
             return rm;
     }
     
     
     int roleId=new Long(rmList.get(0).getRoleId()).intValue();
     
     /*  insert admin   */
     KeyHolder holder = new GeneratedKeyHolder();
     SqlParameterSource parameters = new MapSqlParameterSource()
     .addValue("name", ""+ur.getName())
     .addValue("mobile", ""+ur.getMobileNo())
     .addValue("email", ""+ur.getEmail())
     .addValue("national_id", ""+ur.getNationalId())
     .addValue("user_name", ""+ur.getUsername())
      ;
     
     
     namedParameterJdbcTemplate.update(INSERT_ADMIN, parameters, holder);
     
     int adminId=holder.getKey().intValue();
     
     logger.info("Named Param:adminId "+adminId);
      
      
     
     /*  insert user   */
      
     parameters = new MapSqlParameterSource()
     .addValue("user_id", ur.getUsername()) 
     .addValue("admin_id", ""+adminId) 
     .addValue("passwd", ""+ur.getPassword()) 
     .addValue("role", roleId) 
     .addValue("user_name", ur.getName()) 
     .addValue("user_name_ar", "") 
     .addValue("user_status", ""+Constants.ACTIVE) 
     .addValue("language", "EN") ;
      

     int pk=namedParameterJdbcTemplate.update(INSERT_USERMASTER, parameters);

      
     rm.setResponseCode(Constants.SUCCESS);
      return rm;
             } /*
                      * catch (InvalidDataAccessApiUsageException e) { // TODO Auto-generated catch
                      * block e.printStackTrace(); } catch (DataAccessException e) { // TODO
                      * Auto-generated catch block e.printStackTrace(); }
                      */
catch (Exception e) {
     // TODO Auto-generated catch block
     e.printStackTrace();
}

rm.setResponseCode(Constants.ERROR);
return rm;


     }


	
	
	
	/*   JK   */
	 
	 
	 
	 
	 
	 
	 
	 @Override
	 public List<EntityModel> getUnverifiedEntityList() {
		    List<EntityModel> groupList = new ArrayList<EntityModel>();
		    //Session session = ((SessionFactory) entityManager).getCurrentSession();
		/*
		 * javax.persistence.Query query = entityManager.
		 * createNativeQuery(" select  CASE a.role_id WHEN 1 THEN 'Private Entity'" +
		 * "                     WHEN 2 THEN 'Goverment Entity'" +
		 * "		  ELSE  'Tabadul' end AS role,a.user_id,a.login_id,a.user_name,a.user_status,b.cr_no,b.supplier_id,b.contact_info AS sup_contact,b.rep_email AS sup_email,b.create_on AS sup_reg_date "
		 * +
		 * "		  ,c.contact_info AS ben_contact,c.beneficiary_email AS ben_email,c.create_on AS ben_reg_date,c.beneficiary_id,c.beneficiary_UID "
		 * + "  from t_usermaster a" +
		 * "  LEFT JOIN t_supplierdetails b ON b.login_id = a.login_id" +
		 * "  LEFT JOIN t_beneficiarydetails c ON c.login_id = a.login_id" +
		 * "  where upper(a.user_status) in ('CC_PENDING','G_CC_PENDING')" +
		 * "  order by a.role_id",EntityModel.class); //query.setParameter("id", id);
		 * groupList = query.getResultList();
		 */
		    return groupList;
		}
	 
	 








	@Override
	public List<LgIssuedDataDTO> getAllLGApprovedDetailsForBeneficiary(String loginId) {
		List<LgIssuedDataDTO> groupList;
	    //Session session = ((SessionFactory) entityManager).getCurrentSession();
	    javax.persistence.Query query = entityManager.createNativeQuery(" SELECT l.request_id,l.lg_issue_date AS request_date,l.lg_value AS lG_amount,l.lg_type_id AS lg_type,b.beneficiary_name AS beneficiary_name,l.project_name AS project_name," + 
	    		"  m.bank_name AS bank_name,l.active_status AS status,l.role_id,l.lg_no,l.lg_issue_date AS start_date,l.lg_validity_date AS end_date,l.currency_code AS currency,c.currency AS currency_name,l.terms_condition_message AS termsNConditions,l.create_on " + 
	    		"FROM T_LG l " + 
	    		"INNER JOIN T_LGDetails d ON l.case_id = d.case_id " + 
	    		"INNER JOIN T_BeneficiaryDetails b ON b.beneficiary_id = d.beneficiary_id " + 
	    		"INNER JOIN T_BankMaster m ON m.bank_code = d.bank_code " + 
	    		"INNER JOIN T_CurrencyTypeMaster c ON c.currency_code = l.currency_code " + 
	    		"INNER JOIN T_UserMaster u ON d.login_id = u.login_id " + 
	    		"WHERE d.login_id = :loginId " + 
	    		"AND   l.role_id = '"+env.getProperty("ROLE_BENF_ID")+"'" + 
	    		"UNION " + 
	    		"SELECT " + 
	    		"l.request_id,l.lg_issue_date AS request_date,l.lg_value AS lG_amount,l.lg_type_id AS lg_type,b.supplier_name AS supplier_name,l.project_name AS project_name, " + 
	    		"  m.bank_name AS bank_name,l.active_status AS status,l.role_id,l.lg_no,l.lg_issue_date AS start_date,l.lg_validity_date AS end_date,l.currency_code AS currency,c.currency AS currency_name,l.terms_condition_message AS termsNConditions,l.create_on " + 
	    	
	    		"FROM T_LG l " + 
	    		"INNER JOIN T_LGDetails d ON l.case_id = d.case_id " + 
	    		"INNER JOIN T_supplierDetails b ON b.supplier_id = d.supplier_id " + 
	    		"INNER JOIN T_BankMaster m ON m.bank_code = d.bank_code " + 
	    		"INNER JOIN T_CurrencyTypeMaster c ON c.currency_code = l.currency_code " + 
	    		"INNER JOIN T_UserMaster u ON d.login_id = u.login_id " + 
	    		"WHERE d.login_id = :loginId " + 
	    		"AND   l.role_id = '"+env.getProperty("ROLE_SUPP_ID")+"' order by l.create_on desc" ,LgIssuedDataDTO.class);
	    //query.setParameter("id", id);
	    query.setParameter("loginId", loginId);
	    groupList = query.getResultList();
	    return groupList;
	}








	@Override
	public List<LgIssuedDataDTO> getAllLGApprovedDetailsForSupplier(String loginId) {
		List<LgIssuedDataDTO> groupList;
	    //Session session = ((SessionFactory) entityManager).getCurrentSession();
	    javax.persistence.Query query = entityManager.createNativeQuery("SELECT                      d.request_id,   l.role_id, " + 
	    		"	    			 		l.lg_issue_date AS request_date,    " + 
	    		"	    			 		d.lg_amount AS lG_amount,   " + 
	    		"	    			 		l.lg_type_id AS lG_type,   " + 
	    		"	    			 		b.beneficiary_name AS beneficiary_name,   " + 
	    		"							l.project_name AS project_name,   " + 
	    		"	    			 		m.bank_name AS bank_name,   " + 
	    		"	    			 		l.active_status AS status," + 
	    		"	    			 		l.role_id,l.lg_no,l.lg_issue_date AS start_date,l.lg_validity_date AS end_date,l.currency_code AS currency,c.currency AS currency_name,l.terms_condition_message AS termsNConditions,l.create_on   " + 
	    		"	    			 		   FROM T_UserMaster u    " + 
	    		"	    			 		INNER JOIN T_LGDetails d ON d.beneficiary_id = u.user_id    " + 
	    		"	    			 		INNER JOIN T_LG l ON l.beneficiary_id = d.beneficiary_id  " + 
	    		"	    			 		INNER JOIN T_CurrencyTypeMaster c ON c.currency_code = l.currency_code  " + 
	    		"	    			 		INNER JOIN T_BeneficiaryDetails b ON b.beneficiary_id = u.user_id    " + 
	    		"	    			 		INNER JOIN T_BankMaster m ON m.bank_code = d.bank_code   " + 
	    		"	    			 		WHERE u.login_id = :loginId   " + 
	    		"	    			 		AND l.active_status='ACTIVE'    " + 
	    		"	    			 		AND d.delete_flag = 'N'    " + 
	    		"	    			 		AND l.delete_flag = 'N' " + 
	    		"							AND l.role_id = '"+env.getProperty("ROLE_BENF_ID")+"'" + 
	    		"						UNION " + 
	    		"SELECT                      d.request_id, l.role_id,   " + 
	    		"	    			 		l.lg_issue_date AS request_date,    " + 
	    		"	    			 		d.lg_amount AS lG_amount,   " + 
	    		"	    			 		l.lg_type_id AS lG_type,   " + 
	    		"	    			 		b.supplier_name AS beneficiary_name,   " + 
	    		"							l.project_name AS project_name,   " + 
	    		"	    			 		m.bank_name AS bank_name,   " + 
	    		"	    			 		l.active_status AS status ," + 
	    		"	    			 			l.role_id,l.lg_no,l.lg_issue_date AS start_date,l.lg_validity_date AS end_date,l.currency_code AS currency,c.currency AS currency_name,l.terms_condition_message AS termsNConditions,l.create_on     " + 
	    		"	    			 		   FROM T_UserMaster u    " + 
	    		"	    			 		INNER JOIN T_LGDetails d ON d.beneficiary_id = u.user_id    " + 
	    		"	    			 		INNER JOIN T_LG l ON l.beneficiary_id = d.beneficiary_id  " + 
	    		"	    			 		INNER JOIN T_CurrencyTypeMaster c ON c.currency_code = l.currency_code  " + 
	    		"	    			 		INNER JOIN T_SupplierDetails b ON b.supplier_id = u.user_id    " + 
	    		"	    			 		INNER JOIN T_BankMaster m ON m.bank_code = d.bank_code   " + 
	    		"	    			 		WHERE u.login_id = :loginId   " + 
	    		"	    			 		AND l.active_status='ACTIVE'    " + 
	    		"	    			 		AND d.delete_flag = 'N'    " + 
	    		"	    			 		AND l.delete_flag = 'N' " + 
	    		"							AND l.role_id = '"+env.getProperty("ROLE_SUPP_ID")+"' order by l.create_on desc" ,LgIssuedDataDTO.class);
	    //query.setParameter("id", id);
	    query.setParameter("loginId", loginId);
	    groupList = query.getResultList();
	    return groupList;
	}








	@Override
	public List<LgListWithChangesAcceptSupplier> getAllLGChangedTnCForSupplierList(String loginId) {
		List<LgListWithChangesAcceptSupplier> groupList;
	    //Session session = ((SessionFactory) entityManager).getCurrentSession();
	    javax.persistence.Query query = entityManager.createNativeQuery("select b.lg_type ,a.terms_condition_type,a.contract_value,a.bank_code,c.bank_name,  " + 
	    		"				       CONVERT(VARCHAR(10), a.lg_start_date, 103)  lg_start_date,  " + 
	    		"				           CONVERT(VARCHAR(10), a.lg_end_date, 103)  lg_end_date,  " + 
	    		"				           a.project_id,a.project_name,  " + 
	    		"				           CONVERT(VARCHAR(10), a.zakat_start_date, 103)  zakat_start_date,  " + 
	    		"				           CONVERT(VARCHAR(10), a.zakat_start_date, 103)  zakat_end_date,  " + 
	    		"				 CONVERT(VARCHAR(10), d.request_date, 103)  request_date, " + 
	    		"				           a.purpose_of_bond ,a.iban_no,a.byan_no,a.active_status,  " + 
	    		"				           a.lg_issued_by,a.create_by,a.create_on,e.supplier_name,d.request_status_flag,f.currency,a.currency_code,a.request_id   " + 
	    		"				FROM T_LgrequestIssue a   " + 
	    		"				INNER JOIN T_LgTypeMaster b ON a.lg_type_id = b.lg_type_id   " + 
	    		"				INNER JOIN T_BankMaster c   ON c.bank_code = a.bank_code   " + 
	    		"				INNER JOIN T_servicerequest d ON d.request_id = a.request_id  " + 
	    		"				INNER JOIN T_SupplierDetails e ON e.supplier_id = a.supplier_id " + 
	    		"				INNER JOIN T_CurrencyTypeMaster f ON f.currency_code = a.currency_code   " + 
	    		"				WHERE d.login_id =:loginId  " + 
	    		"				 AND   ISNULL(a.delete_flag,'N') <> 'Y'  " + 
	    		"				AND  d.login_id = a.login_id   " + 
	    		"				AND  d.request_status_flag = 'NS_CHANGE'" ,LgListWithChangesAcceptSupplier.class);
	    //query.setParameter("id", id);
	    query.setParameter("loginId", loginId);
	    groupList = query.getResultList();
	    return groupList;
	}








	@Override
	public List<EntityModel> getBeneficiaryUnverifiedEntityList() {
		 List<EntityModel> groupList;
		    //Session session = ((SessionFactory) entityManager).getCurrentSession();
		    javax.persistence.Query query = entityManager.createNativeQuery("select a.role_id ,a.user_id,a.login_id,a.user_name,a.user_status,b.beneficiary_UID AS uniq_no,b.beneficiary_id AS entity_id,b.contact_info AS contact " + 
		    		"		    				  ,b.beneficiary_email AS email,b.create_on AS reg_date " + 
		    		"		    		  from t_usermaster a  " + 
		    		"		    		  INNER JOIN T_BeneficiaryDetails b ON b.login_id = a.login_id  " + 
		    		"		    		  where a.user_status ='"+env.getProperty("G_CC_PENDING")+"' AND role_id = '"+env.getProperty("ROLE_BENF_ID")+"'  " + 
		    		"		    		  ORDER BY b.create_on DESC" ,EntityModel.class);
		    //query.setParameter("id", id);
		    groupList = query.getResultList();
		    return groupList;
	}








	@Override
	public List<EntityModel> getSupplierUnverifiedEntityList() {
		 List<EntityModel> groupList;
		    //Session session = ((SessionFactory) entityManager).getCurrentSession();
		    javax.persistence.Query query = entityManager.createNativeQuery("select a.role_id ,a.user_id,a.login_id,a.user_name,a.user_status,b.cr_no AS uniq_no,b.supplier_id AS entity_id,b.contact_info AS contact " + 
		    		",b.rep_email AS email,b.create_on AS reg_date " + 
		    		"from t_usermaster a  " + 
		    		"INNER JOIN t_supplierdetails b ON b.login_id = a.login_id  " + 
		    		"where a.user_status ='"+env.getProperty("CC_PENDING")+"' AND role_id = '"+env.getProperty("ROLE_SUPP_ID")+"' " + 
		    		" ORDER BY b.create_on DESC " ,EntityModel.class);
		    //query.setParameter("id", id);
		    groupList = query.getResultList();
		    return groupList;
	}








	
}
