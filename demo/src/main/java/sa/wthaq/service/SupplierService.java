package sa.wthaq.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import sa.wthaq.DTO.LgDataDto;
import sa.wthaq.DTO.LgIssuedDataDTO;
import sa.wthaq.DTO.LgListWithChangesAcceptSupplier;
import sa.wthaq.DTO.LgPendingListDTO;
import sa.wthaq.Entities.TSupplierdetail;
import sa.wthaq.models.RegistrationPvtEntt;
import sa.wthaq.models.ResponseModel;

 
public interface SupplierService {

	
	  public abstract TSupplierdetail findSupplierCRNWise(String crnNo);
	
	  public abstract Boolean checkIfCRNOK(String crnNo);
	  public abstract List<TSupplierdetail> findSuppliers();
	  
	  public abstract List<TSupplierdetail> findAllSuppliersDetails();
	  public abstract List<Object[]> findSuppliersIdAndName();

	public abstract ResponseModel registerSupplier(RegistrationPvtEntt re, MultipartFile crFile, MultipartFile rniFile, MultipartFile lalFile, MultipartFile laglFile);

	public abstract ResponseModel profileUpdateSupplier(RegistrationPvtEntt re, MultipartFile crFile, MultipartFile rniFile, MultipartFile lalFile, MultipartFile laglFile);

	public abstract List<LgPendingListDTO> getLgWithSupplierId(String supId);
	
	public List<?> getCrnFromLogin(String loginId);
	
	public List<LgIssuedDataDTO> getAllLGApprovedDetailsForBeneficiaryServ(String loginId);
	
	public List<LgIssuedDataDTO> getAllLGApprovedDetailsForSupplierServ(String loginId);
	
	public List<LgListWithChangesAcceptSupplier> getAllLGChangedTnCForSupplierList(String loginId);

	public ResponseModel acceptTheLgIssue(String requestId);

	public ResponseModel cancelTheLgIssueBySupplier(String requestId);

	List<?> getBenNameFromCRN(BigDecimal crn);
	
	
	public abstract ResponseModel getSupplierUnverifiedEntityList(String adminId);

	public abstract ResponseModel getGovUnverifiedEntityList(String adminId);
	

	public abstract ResponseModel getPriEntityDetails(String nId, String crn, String adminId);

	public abstract ResponseModel getGovEntityDetails(String nId, String uid, String adminId); 




}
