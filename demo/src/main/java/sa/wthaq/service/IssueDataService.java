package sa.wthaq.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.xml.sax.SAXException;

import sa.wthaq.DTO.LgDataDto;
import sa.wthaq.Entities.TBankmaster;
import sa.wthaq.Entities.TLgrequestissue;
import sa.wthaq.Entities.TUserIbanMapping;
import sa.wthaq.Entities.T_WriteUpType;
import sa.wthaq.models.LGRequestIssue;

public interface IssueDataService {
	
	public List<?> selectIBAN(String loginid);
	
	public String changeNumToWord(int num);
	
	public List<?> selectBankName();
	
	public List<?> selectWriteUpType() ;
	
	public List<?> selectLGType();
	
	public List<?> selectCurrency();
	
	public TLgrequestissue saveLGRequestIssue(LGRequestIssue lGRequestIssue)throws ParseException;
	
	public String sendLgDetailsToBank(LGRequestIssue lGRequestIssue) throws SAXException, IOException, ParserConfigurationException, JSONException, ParseException, org.json.simple.parser.ParseException;
	
	public int savelgData(String requestId);
	
}
