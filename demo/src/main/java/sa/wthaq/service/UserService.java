package sa.wthaq.service;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import sa.wthaq.DTO.EntityModel;
import sa.wthaq.Entities.TUsermaster;
import sa.wthaq.models.LoginModel;
import sa.wthaq.models.RegistrationPvtEntt;
import sa.wthaq.models.ResponseModel;
import sa.wthaq.models.TabadulUser;
import sa.wthaq.models.UserMasterModel;

 
 
public interface UserService {
 
	   
	   public abstract   List<TUsermaster> findAll();

	   public abstract   List<TUsermaster> findUserByLoginIdAndPass(String lid, String pass);

	   public abstract   ResponseModel loginUser(LoginModel lm, HttpServletRequest req);



	public abstract ResponseModel checkValidLoginId(String tempLoginId);
	
	
	
	public abstract ResponseModel getLoginStatus(String tempLoginId, String Password);

	public abstract ResponseModel checkSupplierCrn(String crnno);

	 

	public abstract ResponseModel forgotPassword(String userIdForgotPass);

	public abstract ResponseModel profileInfo(String userIdForgotPass);

	public abstract ResponseModel updateProfileInfo(RegistrationPvtEntt RegistrationPvtEntt , MultipartFile crFile, MultipartFile rniFile, MultipartFile lalFile, MultipartFile laglFile);

	public abstract ResponseModel checkSessionStatus(HttpServletRequest req);

	public abstract ResponseModel createNewTabadulUser(TabadulUser tuser);
	
	public List<EntityModel> getPendingListForVerification();
	
	 //ResponseModel logout(HttpServletRequest req);

	 public ResponseModel logout(String loginId, HttpServletRequest req);

	 public abstract boolean isUserLoginIdValid(String loginId);

		public abstract boolean isRequestIdValid(String uid, String rid);

		public abstract ResponseModel getOtpByloginId(String loginId);
		
		
		public List<EntityModel> getBeneficiaryUnverifiedEntityList();
		
		public List<EntityModel> getSupplierUnverifiedEntityList();


	 
	 
}
