package sa.wthaq.service;

import sa.wthaq.models.PaymentModel;
import sa.wthaq.models.ResponseModel;

public interface PaymentService {

	public abstract ResponseModel getPaymentParams(PaymentModel paymentModel);

	public abstract ResponseModel savePaymentParams(PaymentModel paymentModel);
	
 	
	
}
