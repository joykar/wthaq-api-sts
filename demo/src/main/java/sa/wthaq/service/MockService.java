package sa.wthaq.service;

import sa.wthaq.DTO.MCIDataDto;
import sa.wthaq.DTO.MciRequestData;
import sa.wthaq.models.ResponseModel;

public interface MockService {
	
	public ResponseModel checkDataExists(MciRequestData reqData);

}
