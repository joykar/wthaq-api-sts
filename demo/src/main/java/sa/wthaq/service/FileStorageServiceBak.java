package sa.wthaq.service;


import org.apache.velocity.shaded.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import sa.wthaq.FileStorageProperties;
import sa.wthaq.Exception.FileStorageException;
import sa.wthaq.Exception.WethaqFileNotFoundException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;

@Service
public class FileStorageServiceBak {
	private static final Logger logger = LoggerFactory.getLogger(FileStorageService.class);
	
    private final Path fileStorageLocation;

    @Autowired
    public FileStorageServiceBak(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            
        	logger.error("FileStorageException: Could not create the directory where the uploaded files will be stored. "+ex.getLocalizedMessage()  );
       
        	throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
            
        }
    }

    public String storeFile(MultipartFile file, String commRegNo,String fileCat) {
        // Normalize file name
        String fileName = /*StringUtils.cleanPath(file.getOriginalFilename())*/fileCat+"_"+commRegNo+"_"/*+(new Timestamp(System.currentTimeMillis()))*/+""+FilenameUtils.getExtension(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
               // throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
                logger.error("FileStorageException: Sorry! Filename contains invalid path sequence. fileName: "+fileName +", original file name: "+ file.getOriginalFilename());
                 return null;
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            logger.error("storeFile successful:  fileName: "+fileName+" , original file name:  "+file.getOriginalFilename()+", location: "+fileStorageLocation.toAbsolutePath());
            
            return fileName;
        } catch (IOException ex) {
          //  throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        	
        	logger.error("FileStorageException: Could not store file. fileName: "+fileName +", original file name: "+ file.getOriginalFilename());
        	 return null;
        	
        }
    }

    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new WethaqFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new WethaqFileNotFoundException("File not found " + fileName, ex);
        }
    }
}