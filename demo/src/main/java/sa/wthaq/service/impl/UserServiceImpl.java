package sa.wthaq.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import io.jsonwebtoken.Claims;
import sa.wthaq.Constants;
import sa.wthaq.Communication.SendEmail;
import sa.wthaq.DTO.EntityModel;
import sa.wthaq.DTO.UserDTO;
import sa.wthaq.Entities.TRolemaster;
import sa.wthaq.Entities.TSession;
import sa.wthaq.Entities.TUsermaster;
import sa.wthaq.Entities.TUsermaster2;
import sa.wthaq.JPA.Repository.RoleMasterRepository;
import sa.wthaq.JPA.Repository.SessionRepository;
import sa.wthaq.JPA.Repository.SupplierMasterRepository;
import sa.wthaq.JPA.Repository.UserMasterRepository;
import sa.wthaq.JPA.Repository.UserMasterRepository2;
import sa.wthaq.Utilities.JWTUtils;
import sa.wthaq.Utilities.RandomPasswordGenerator;
import sa.wthaq.Utilities.Utilities;
import sa.wthaq.controller.TabadulController;
import sa.wthaq.dao.ElogDaoImpl;
import sa.wthaq.models.LoginModel;
import sa.wthaq.models.RegistrationPvtEntt;
import sa.wthaq.models.ResponseModel;
import sa.wthaq.models.SessionModel;
import sa.wthaq.models.TabadulUser;
import sa.wthaq.models.UserMasterModel;
import sa.wthaq.service.SupplierService;
import sa.wthaq.service.UserService;

@Service("UserService")
//@SessionAttributes("user")
//@Repository
//@Service
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private SessionRepository sessionRepository;

	@Autowired
	private Environment env;

	@PersistenceContext
	private EntityManager entityManager;

	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;

	@Autowired
	private UserMasterRepository u_repository;

	@Autowired
	private UserMasterRepository2 u_repository2;

	@Autowired
	private SupplierService supplierService;

	@Autowired
	private ElogDaoImpl dAOService;

	@Autowired
	private SupplierMasterRepository s_repository;

	@Autowired
	private RoleMasterRepository roleMasterRepository;

	/*
	 * @Override public void createUser(UserMasterModel umm) { // TODO
	 * Auto-generated method stub
	 * 
	 * }
	 * 
	 * @Override public void updateUser(UserMasterModel umm) { // TODO
	 * Auto-generated method stub
	 * 
	 * }
	 * 
	 * @Override public void deleteUser(UserMasterModel umm) { // TODO
	 * Auto-generated method stub
	 * 
	 * }
	 * 
	 * @Override public Collection<UserMasterModel> getUsers() { // TODO
	 * Auto-generated method stub return null; }
	 */

	@Override
	public List<TUsermaster> findAll() {
		// TODO Auto-generated method stub

		Session session = entityManagerFactory.unwrap(SessionFactory.class).openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(TUsermaster.class);

		Root contactRoot = criteria.from(TUsermaster.class);
		criteria.select(contactRoot);
		return session.createQuery(criteria).getResultList();

		// return repository.findAll();
	}

	/*
	 * @Override public ResponseModel loginUser(LoginModel lm, HttpServletRequest
	 * req) {
	 * 
	 * check if login is ok ResponseModel rm =new ResponseModel();
	 * 
	 * 
	 * 
	 * List<TUsermaster2> ums=u_repository2.findUserByLoginIdAndPass(lm.getUserId(),
	 * lm.getPassword());
	 * logger.info(""+ums.size()+", "+env.getProperty("CC_PENDING")); if(ums.size()
	 * <=0) { //no user rm.setResponseMessage(Constants.LOGIN_ERROR);
	 * rm.setResponseCode(Constants.ERROR); return rm; } else {
	 * 
	 * //check reg status TUsermaster2 um =ums.get(0); rm.setUserObject(um);
	 * 
	 * 
	 * 
	 *//*************************************************/

	/*
	 * 
	 * 
	 * 
	 * SessionModel sm=JWTUtils.createJWTToken(req, um);
	 * 
	 * if(sm.getToken()!=null ) { if(!sm.getToken().isEmpty()) {
	 * 
	 * //save to DB
	 * 
	 * int affctd= sessionRepository.saveUserSessionByLoginId(sm.getLoginId(),
	 * sm.getSessionId(), sm.getToken(), sm.getRemoteIp(), sm.getRemoteHost(),
	 * sm.getRemotePort(), sm.getRemoteUser(), sm.getRemoteAgent()); if(affctd>0) {
	 * rm.setJwtToken(sm.getToken()); } else {
	 * 
	 * rm.setLogout(Constants.MAKE_LOGOUT);
	 * rm.setResponseMessage(Constants.SESSION_CREATION_ERROR); return rm; } } else
	 * { rm.setLogout(Constants.MAKE_LOGOUT);
	 * rm.setResponseMessage(Constants.SESSION_CREATION_ERROR); return rm; } } else
	 * { rm.setLogout(Constants.MAKE_LOGOUT);
	 * rm.setResponseMessage(Constants.SESSION_CREATION_ERROR); return rm; }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 *//************************************************//*
															 * 
															 * 
															 * 
															 * 
															 * if(um.getUserStatus().trim().equals(env.getProperty(
															 * "CC_PENDING"))) {
															 * 
															 * // rm.setResponseMessage(Constants.CC_PENDING);
															 * rm.setResponseCode(Constants.CC_PENDING_CODE); return rm;
															 * } else
															 * if(um.getUserStatus().trim().equals(env.getProperty(
															 * "CC_SUCCESS"))) {
															 * 
															 * // rm.setResponseMessage(Constants.CC_SUCCESS);
															 * rm.setResponseCode(Constants.CC_SUCCESS_CODE); return rm;
															 * } else
															 * if(um.getUserStatus().trim().equals(env.getProperty(
															 * "CC_FAILED"))) {
															 * 
															 * // rm.setResponseMessage(Constants.CC_FAILED);
															 * rm.setResponseCode(Constants.CC_FAILED_CODE); return rm;
															 * }
															 * 
															 * else if(um.getUserStatus().trim().equals(env.getProperty(
															 * "BANK_VERIFIED"))) { //
															 * rm.setResponseMessage(Constants.BANK_VERIFIED);
															 * rm.setResponseCode(Constants.BANK_VERIFIED_CODE); return
															 * rm;
															 * 
															 * } else
															 * if(um.getUserStatus().trim().equals(env.getProperty(
															 * "BANK_NOT_VERIFIED"))) { //
															 * rm.setResponseMessage(Constants.BANK_NOT_VERIFIED);
															 * rm.setResponseCode(Constants.BANK_NOT_VERIFIED_CODE);
															 * return rm;
															 * 
															 * }
															 * 
															 * else if(um.getUserStatus().trim().equals(env.getProperty(
															 * "BANK_PENDING"))) { //
															 * rm.setResponseMessage(Constants.BANK_PENDING);
															 * rm.setResponseCode(Constants.BANK_PENDING_CODE); return
															 * rm;
															 * 
															 * }
															 * 
															 * else {
															 * 
															 * rm.setResponseMessage(Constants.LOGIN_SUCCESS);
															 * rm.setResponseCode(Constants.SUCCESS); return rm; }
															 * 
															 * 
															 * }
															 * 
															 * 
															 * }
															 */

	@Override
	public ResponseModel checkValidLoginId(String tempLoginId) {
		// TODO Auto-generated method stub

		ResponseModel rm = new ResponseModel();
		System.out.println("tempLogin"+tempLoginId);
		boolean um = dAOService.checkValidLoginId(tempLoginId);

		if (um) {
			// available

			rm.setResponseCode(Constants.SUCCESS);
			rm.setResponseMessage(Constants.USERNAME_AVAILABLE);
		} else {
			// taken
			rm.setResponseCode(Constants.ERROR);
			rm.setResponseMessage(Constants.USERNAME_UNAVAILABLE);

		}

		return rm;
	}

	@Override
	public ResponseModel getLoginStatus(String tempLoginId, String Password) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TUsermaster> findUserByLoginIdAndPass(String lid, String pass) {
		// TODO Auto-generated method stub
		List<TUsermaster> lum = u_repository.findUserByLoginIdAndPass(lid, pass);
		return lum;
	}

	@Override
	public ResponseModel checkSupplierCrn(String crnno) {
		// TODO Auto-generated method stub

		return null;
	}

	/*
	 * @Override public ResponseModel forgotPassword(String userIdForgotPass) { //
	 * TODO Auto-generated method stub
	 * 
	 * ResponseModel rm = new ResponseModel();
	 * 
	 * List<TUsermaster> um = u_repository.findUserByLoginId(userIdForgotPass); if
	 * (um.size() > 0) { // user exists
	 * 
	 * // find official email by login id
	 * 
	 * List<Object[]> udto =
	 * u_repository.findOfficialEmailByUserId(userIdForgotPass, userIdForgotPass);
	 * 
	 * if (udto.size() > 0) { // exists
	 * 
	 * String officialEmail = "" + udto.get(0)[2];// .getOfficeemail().trim();
	 * String officialNmae = "" + udto.get(0)[1];// .getName().trim(); // send email
	 * 
	 * String tempPass = RandomPasswordGenerator.generateSecureRandomPassword(14);
	 * try { new SendEmail().SendRegistrationFirstPhaseEmail(officialEmail,
	 * officialNmae, userIdForgotPass, tempPass);
	 * 
	 * rm.setResponseCode(Constants.SUCCESS);
	 * rm.setResponseMessage(Constants.FORGOT_PASSWORD_EMAIL_SUCCESS);
	 * 
	 * } catch (Exception e) { // TODO Auto-generated catch block
	 * logger.info("Failed to send email: getOfficeEMail: " + officialEmail +
	 * ", getName: " + officialNmae + "getUserId: " + userIdForgotPass);
	 * e.printStackTrace(); rm.setResponseCode(Constants.ERROR);
	 * rm.setResponseMessage(Constants.FORGOT_PASSWORD_EMAIL_ERROR); }
	 * 
	 * }
	 * 
	 * } else { rm.setResponseCode(Constants.ERROR);
	 * rm.setResponseMessage(Constants.FORGOT_PASSWORD_EMAIL_ERROR);
	 * 
	 * }
	 * 
	 * return rm; }
	 */

	@Override
	public ResponseModel profileInfo(String userIdForgotPass) {
		// TODO Auto-generated method stub

		ResponseModel rm = new ResponseModel();
		UserDTO udt = new UserDTO();
		List<Object[]> udto = u_repository.findProfileInfoByUserId(userIdForgotPass, userIdForgotPass);

		if (udto != null) {
			if (udto.size() > 0) {

				Object[] udtob = udto.get(0);

				udt.setSupplier_id("" + udtob[0]);
				udt.setSupplier_name("" + udtob[1]);
				udt.setContact_info("" + udtob[2]);
				udt.setLogin_id("" + udtob[3]);

				udt.setUser_name("" + udtob[4]);
				udt.setRoleType("" + udtob[5]);

				udt.setNational_id("" + udtob[6]);
				udt.setNational_id_doc_name("" + udtob[7]);
				udt.setRep_email("" + udtob[8]);
				udt.setRepMobileNo("" + udtob[9]);
				udt.setComm_reg_doc_name("" + udtob[10]);
				udt.setAuth_lettter_doc_name("" + udtob[11]);
				udt.setLeg_agree_doc_name("" + udtob[12]);
				udt.setLeg_auth_doc_name("" + udtob[13]);
				udt.setCrnNo("" + udtob[14]);
				// udt.getRoleType()

				rm.setResponseCode(Constants.SUCCESS);
				rm.setResponseMessage(Constants.PROFILE_SETTINGS_LOADED_SUCCESS);
				rm.setUserObject(udt);
			} else {

				rm.setResponseCode(Constants.ERROR);
				rm.setResponseMessage(Constants.PROFILE_SETTINGS_LOADED_ERROR);
				rm.setUserObject(udt);
			}

		} else {

			rm.setResponseCode(Constants.ERROR);
			rm.setResponseMessage(Constants.PROFILE_SETTINGS_LOADED_ERROR);
			rm.setUserObject(udt);

		}

		return rm;
	}

	@Override
	public ResponseModel updateProfileInfo(RegistrationPvtEntt rgRegistrationPvtEntt, MultipartFile crFile,
			MultipartFile rniFile, MultipartFile lalFile, MultipartFile laglFile) {
		// TODO Auto-generated method stub

		ResponseModel rmm = new ResponseModel();
		String userId = rgRegistrationPvtEntt.getUserId().trim();

		List<Object[]> urole = u_repository.findRoleByUserId(userId);
		if (urole != null) {
			if (urole.size() > 0) {
				String uroleId = "" + urole.get(0)[0];

				if (uroleId.length() > 0) {
					int roleNo = Utilities.isIntOrReturnDefault(uroleId, -1);

					if (roleNo > 0) {
						// match role
						String roleDesc = "" + urole.get(0)[1];
						if (roleDesc.equalsIgnoreCase(Constants.ROLE_SUPPLIER)) {
							// call supplier service
							rmm = supplierService.profileUpdateSupplier(rgRegistrationPvtEntt, crFile, rniFile, lalFile,
									laglFile);

						}

					}

				}

			}

		}

		return rmm;
	}

	@Override
	public ResponseModel checkSessionStatus(HttpServletRequest req) {
		// TODO Auto-generated method stub
		ResponseModel rm = new ResponseModel();

		try {
			String authHeader = req.getHeader("WTHAQ-AUTH-TOKEN");
			Claims cms = JWTUtils.decodeJWT(authHeader);

			if (cms == null) {
				rm.setLogout(Constants.MAKE_LOGOUT);
				rm.setResponseMessage(Constants.SESSION_EXPIRED);
				return rm;
			}

			String uid = cms.get("uid").toString();
			String starttime = cms.get("starttime").toString();
			String sessid = cms.get("sessid").toString();
			String remote_ip = cms.get("remote-ip").toString();
			String remote_host = cms.get("remote-host").toString();
			String remote_port = cms.get("remote-port").toString();
			String remote_user = cms.get("remote-user").toString();
			String remote_agent = cms.get("remote-agent").toString();

			List<TSession> ts = sessionRepository.findUserSessionByLoginId(uid, sessid, authHeader, remote_ip,
					remote_host, remote_port, remote_user, remote_agent);

			if (ts.size() <= 0) {
				rm.setLogout(Constants.MAKE_LOGOUT);
				rm.setResponseMessage(Constants.SESSION_EXPIRED);
			} else {

				int affRws = sessionRepository.updateUserSessionByLoginId(uid, sessid, authHeader, remote_ip,
						remote_host, remote_port, remote_user, remote_agent);

				if (affRws > 0) {
					rm.setLogout(Constants.MAKE_NOLOGOUT);
					rm.setResponseMessage(Constants.SESSION_OK);

				} else {

					rm.setLogout(Constants.MAKE_LOGOUT);
					rm.setResponseMessage(Constants.SESSION_EXPIRED);
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			rm.setLogout(Constants.MAKE_LOGOUT);
			rm.setResponseMessage(Constants.SESSION_CREATION_ERROR);
		}

		return rm;

	}
	
	
	
	
	

	/* JK */

	@Override
	public ResponseModel createNewTabadulUser(TabadulUser tuser) {
		// TODO Auto-generated method stub

		ResponseModel rm = new ResponseModel();
		String tempPass = RandomPasswordGenerator.generateSecureRandomPassword(14);
		tuser.setPassword(tempPass.substring(3));

		rm = dAOService.registerAdmin(tuser);

		if (rm.getResponseCode() == Constants.ERROR) {
			// return as is
			return rm;
		}

		else {
			// send email

			try {
				new SendEmail().SendTabadulRegistrationEmail(tuser.getEmail(), tuser.getName(), tuser.getUsername(),
						tuser.getPassword());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.info("Failed to send email: SendTabadulRegistrationEmail: " + tuser.getEmail() + ", getName: "
						+ tuser.getName() + "getUserId: " + tuser.getUsername());
				e.printStackTrace();
			}

			rm.setResponseCode(Constants.SUCCESS);
			rm.setResponseMessage(Constants.TABADUL_NEW_USER_CREATION_SUCCESS);

		}

		return rm;
	}

	
	
	
	
	
	
	@Override
    public ResponseModel loginUser(LoginModel lm, HttpServletRequest req) {
            
             /* check if login is ok */
            ResponseModel rm =new ResponseModel();
            
            
                            
            List<TUsermaster2>        ums=u_repository2.findUserByLoginIdAndPass(lm.getUserId(), lm.getPassword());
            logger.info(""+ums.size()+", "+env.getProperty("CC_PENDING"));
            if(ums.size() <=0) {
                     //no user
                     rm.setResponseMessage(Constants.LOGIN_ERROR);
                     rm.setResponseCode(Constants.ERROR);
                     return rm;
             }
             else {
                     
                     //check reg status
                     TUsermaster2 um =ums.get(0);
                     rm.setUserObject(um);
                     
                     
                     
                     /*************************************************/
                    
                    

                     SessionModel sm=JWTUtils.createJWTToken(req, um);
                     
                     if(sm.getToken()!=null ) {
                             if(!sm.getToken().isEmpty()) {
                                       
                                      //save to DB
                                      
    int affctd=        sessionRepository.saveUserSessionByLoginId(sm.getLoginId(), sm.getSessionId(), sm.getToken(), sm.getRemoteIp(),
                    sm.getRemoteHost(), sm.getRemotePort(), sm.getRemoteUser(), sm.getRemoteAgent());
                                      if(affctd>0) {
                                              rm.setJwtToken(sm.getToken());
                                       }
                                      else {
                                              
                                              rm.setLogout(Constants.MAKE_LOGOUT);
                                              rm.setResponseMessage(Constants.SESSION_CREATION_ERROR);
                                              return rm;
                                      }
                              }
                             else {
                                      rm.setLogout(Constants.MAKE_LOGOUT);
                                      rm.setResponseMessage(Constants.SESSION_CREATION_ERROR);
                             return rm;
                             }
                     }
                     else {
                             rm.setLogout(Constants.MAKE_LOGOUT);
                             rm.setResponseMessage(Constants.SESSION_CREATION_ERROR);
                             return rm;
                     }
                     
                    
                    
                    
                     
                    /************************************************/

                     //find role description
                     
                     String rdesc=null;
                     
                     Optional<TRolemaster> currentRole=        roleMasterRepository.findById(um.getRoleId());
                     
                     if(currentRole!=null) {
                                       if(currentRole.isPresent()) {
                                               rdesc=currentRole.get().getRoleDesc();
                             }
                     }
             
                     if(rdesc!=null) {
                             
                             if(rdesc.trim().equals(env.getProperty("ROLE_SUPP"))) {
                                     
                                     
                                     if(um.getUserStatus().trim().equals(env.getProperty("CC_PENDING"))) {
                                              
                                              // 
                                               rm.setResponseMessage(Constants.CC_PENDING);
                                               rm.setResponseCode(Constants.CC_PENDING_CODE);
                                               return rm;
                                      }
                                      else if(um.getUserStatus().trim().equals(env.getProperty("CC_SUCCESS"))) {
                                                      
                                      // 
                                               rm.setResponseMessage(Constants.CC_SUCCESS);
                                               rm.setResponseCode(Constants.CC_SUCCESS_CODE);
                                               return rm;
                             }
                                      else if(um.getUserStatus().trim().equals(env.getProperty("CC_FAILED"))) {
                                              
                                      // 
                                               rm.setResponseMessage(Constants.CC_FAILED);
                                               rm.setResponseCode(Constants.CC_FAILED_CODE);
                                               return rm;
                                      }
                                      
                          else if(um.getUserStatus().trim().equals(env.getProperty("BANK_VERIFIED"))) {
                                 // 
                                              rm.setResponseMessage(Constants.BANK_VERIFIED);
                                              rm.setResponseCode(Constants.BANK_VERIFIED_CODE);
                                              return rm;
                                              
                                      }
                          else if(um.getUserStatus().trim().equals(env.getProperty("BANK_NOT_VERIFIED"))) {
                                  // 
                                               rm.setResponseMessage(Constants.BANK_NOT_VERIFIED);
                                               rm.setResponseCode(Constants.BANK_NOT_VERIFIED_CODE);
                                               return rm;
                                               
                                       }
                                      
                          else if(um.getUserStatus().trim().equals(env.getProperty("BANK_PENDING"))) {
                                   // 
                                                rm.setResponseMessage(Constants.BANK_PENDING);
                                                rm.setResponseCode(Constants.BANK_PENDING_CODE);
                                                return rm;
                                                
                                        }
                          else {
                                  //else is not possible here now
                                  rm.setResponseMessage(Constants.UNKNOWN_STATUS);
                                       rm.setResponseCode(Constants.ERROR);
                                       return rm;
                                  
                          }
                         
                                     
                             }
                             else if(rdesc.trim().equals(env.getProperty("ROLE_BENF"))) {
                                     
                                     
                                     
          if(um.getUserStatus().trim().equals(env.getProperty("G_CC_PENDING"))) {
                                              rm.setResponseMessage(Constants.CC_PENDING);
                                               rm.setResponseCode(Constants.GCC_PENDING_CODE);
                                               return rm;
                                      }
                                      else {
                                         rm.setResponseMessage(Constants.CC_SUCCESS);
                                               rm.setResponseCode(Constants.G_CC_SUCCESS);
                                               return rm;
                                  }
                                     
                             }
         else if(rdesc.trim().equals(env.getProperty("ROLE_ADMIN"))) {
                                     
                 rm.setResponseMessage(Constants.ADMIN_LOGIN_SUCCESS);
                                      rm.setResponseCode(Constants.ADMIN_LOGIN);
                                      return rm;
         }
         else {
                 //we are not allowing more than 3 roles
                 
                 rm.setResponseMessage(Constants.UNKNOWN_ROLE_DESCRIPTION);
                              rm.setResponseCode(Constants.ERROR);
                              return rm;
         }        
                             
                     }
                     else {
                             
                             rm.setResponseMessage(Constants.UNKNOWN_ROLE_DESCRIPTION);
                             rm.setResponseCode(Constants.ERROR);
                             return rm;
                             
                     }
                     
                     
             
                     
                     
             }
             
            
    }
	
	
	
	@Override
	public ResponseModel forgotPassword(String userIdForgotPass) {
		// TODO Auto-generated method stub
		
		ResponseModel rm=new ResponseModel();
		List<Object[]>	udto=null;
		List < TUsermaster  > um=	u_repository.findUserByLoginId(userIdForgotPass);
    	if(um.size()>0) {
    		//user exists
    		
    		
    		
    		//find role
    		
    		//get role for admin
    		List<Object[]> rmList=roleMasterRepository.searchRoleUsingLoginId(userIdForgotPass);
    		
    		if(rmList.size()<1) {
    			rm.setResponseCode(Constants.ERROR);
    			rm.setResponseMessage(Constants.ROLE_UNDEFINED);
    			return rm;
    		}
    	 
    		
    		String roleDesc=rmList.get(0)[0].toString().trim();
    		
    		if(roleDesc.equals(env.getProperty("ROLE_SUPP"))) {
    			
    			//find official email by login id
        		
        		
        		 	udto=	u_repository.findOfficialEmailByUserId(userIdForgotPass,userIdForgotPass);
    			
    			
    		}
    		else if(roleDesc.equals(env.getProperty("ROLE_BENF"))) {
    			
    			//find official email by login id
        		 	udto=	u_repository.findOfficialEmailByUserId(userIdForgotPass,userIdForgotPass);
    		}
else if(roleDesc.equals(env.getProperty("ROLE_ADMIN"))) {
    			
 	udto=	u_repository.findOfficialEmailByAdminId(userIdForgotPass);		
     		}
    		
    		
    		
    		
    		
    		
    		
    		if(udto.size()>0) {
    			//exists
    			
    			String officialEmail=""+udto.get(0)[2];//.getOfficeemail().trim();
    			String officialNmae=""+udto.get(0)[1];//.getName().trim();
    			//send email
    		 
        		String tempPass=RandomPasswordGenerator.generateSecureRandomPassword(14).substring(3);
        		u_repository.changePasswordAfterRequest(tempPass,userIdForgotPass);
        		 try {
        				new SendEmail().SendForgotPasswordEmail(officialEmail, officialNmae, userIdForgotPass,tempPass );
        				
        				rm.setResponseCode(Constants.SUCCESS);
        				rm.setResponseMessage(Constants.FORGOT_PASSWORD_EMAIL_SUCCESS);
        				
        			} catch (Exception e) {
        				// TODO Auto-generated catch block
        				logger.info("Failed to send email: getOfficeEMail: "+officialEmail+", getName: " +officialNmae+"getUserId: " +userIdForgotPass);
        				e.printStackTrace();
        				rm.setResponseCode(Constants.ERROR);
        				rm.setResponseMessage(Constants.FORGOT_PASSWORD_EMAIL_ERROR);
        			}	
    			
    			 
    		}
    		 
    	}
    	else {
    		rm.setResponseCode(Constants.ERROR);
			rm.setResponseMessage(Constants.FORGOT_PASSWORD_EMAIL_ERROR);
    		
    		
    		
    	}
		
		
		
		return rm;
	}

	/* JK */
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public List<EntityModel> getPendingListForVerification() {
		List<EntityModel> ent = dAOService.getUnverifiedEntityList();
		
		return ent;
	}

	
	
	
	@Override
	public List<EntityModel> getBeneficiaryUnverifiedEntityList() {
		List<EntityModel> ent = dAOService.getBeneficiaryUnverifiedEntityList();
		
		return ent;
	}

	
	
	@Override
	public List<EntityModel> getSupplierUnverifiedEntityList() {
		List<EntityModel> ent = dAOService.getSupplierUnverifiedEntityList();
		
		return ent;
	}

	@Override
	public ResponseModel logout(String loginId,HttpServletRequest req) {
		// TODO Auto-generated method stub
	
		
		
		ResponseModel rm = new ResponseModel();

		try {
			String authHeader = req.getHeader("WTHAQ-AUTH-TOKEN");
			Claims cms = JWTUtils.decodeJWT(authHeader);

			if (cms == null) {
				rm.setLogout(Constants.MAKE_LOGOUT);
				rm.setResponseMessage(Constants.SESSION_EXPIRED);
				return rm;
			}

			String uid = cms.get("uid").toString();
			String starttime = cms.get("starttime").toString();
			String sessid = cms.get("sessid").toString();
			String remote_ip = cms.get("remote-ip").toString();
			String remote_host = cms.get("remote-host").toString();
			String remote_port = cms.get("remote-port").toString();
			String remote_user = cms.get("remote-user").toString();
			String remote_agent = cms.get("remote-agent").toString();

			List<TSession> ts = sessionRepository.findUserSessionByLoginId(uid, sessid, authHeader, remote_ip,
					remote_host, remote_port, remote_user, remote_agent);

			if (ts.size() <= 0) {
				rm.setLogout(Constants.MAKE_LOGOUT);
				rm.setResponseMessage(Constants.SESSION_EXPIRED);
			} else {

				int affRws = sessionRepository.logoutUserSessionByLoginId(loginId, sessid, authHeader, remote_ip,
						remote_host, remote_port, remote_user, remote_agent);

				if (affRws > 0) {
					rm.setLogout(Constants.MAKE_NOLOGOUT);
					rm.setResponseMessage(Constants.SESSION_OK);

				} else {

					rm.setLogout(Constants.MAKE_LOGOUT);
					rm.setResponseMessage(Constants.SESSION_EXPIRED);
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			rm.setLogout(Constants.MAKE_LOGOUT);
			rm.setResponseMessage(Constants.SESSION_CREATION_ERROR);
		}

		return rm;
		//return null;
	}

	 
	
	
	
	@Override
	public boolean isUserLoginIdValid(String loginId) {
		// TODO Auto-generated method stub
		
		List < TUsermaster  > um=	u_repository.findUserByLoginId(loginId);
		if(um.size()>0) {
			return true;
			
		}
		else {
			return false;
		}
		
		 
	}


	@Override
	public boolean isRequestIdValid(String uid, String rid) {
		// TODO Auto-generated method stub
		int um=u_repository.isRequestIdValid(uid,rid);
		if(um>0) {
			return true;
		}
		else {
			return false;
		}
}

	@Override
	public ResponseModel getOtpByloginId(String loginId) {
		// TODO Auto-generated method stub
		ResponseModel rm=new ResponseModel();
		String id = String.format("%04d", new Random().nextInt(10000));
		
Map<String,String> otpDetails=new HashMap<String,String>();
otpDetails.put("OTP",id);
otpDetails.put("maskedContact","9830****89");


		rm.setResponseCode(Constants.SUCCESS);
		rm.setResponseMessage(Constants.OTP_REQUEST_SUCCESS);
		rm.setUserObject(otpDetails);
		return rm;
	}
	
	
	
	 
	

	
	
	

}
