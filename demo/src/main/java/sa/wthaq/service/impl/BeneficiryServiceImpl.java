package sa.wthaq.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import sa.wthaq.Constants;
import sa.wthaq.Communication.SendEmail;
import sa.wthaq.DTO.LgDataDto;
import sa.wthaq.Entities.TBankmaster;
import sa.wthaq.Entities.TBeneficiarydetail;
import sa.wthaq.Entities.TLgrequestissue;
import sa.wthaq.Entities.TUnRegisteredBen;
import sa.wthaq.Entities.TUserIbanMapping;
import sa.wthaq.Entities.TUsermaster;
import sa.wthaq.JPA.Repository.BeneficiaryMasterRepository;
import sa.wthaq.JPA.Repository.TLgrequestissueRepo;
import sa.wthaq.JPA.Repository.TServicerequestRepo;
import sa.wthaq.JPA.Repository.UnregisteredBenRepo;
import sa.wthaq.JPA.Repository.UserMasterRepository;
import sa.wthaq.Utilities.RandomPasswordGenerator;
import sa.wthaq.dao.ElogDaoImpl;
import sa.wthaq.models.Beneficiarydetail;
import sa.wthaq.models.LGRequestIssue;
import sa.wthaq.models.ResponseModel;
import sa.wthaq.service.BeneficiryService;
import sa.wthaq.service.FileStorageService;

@Service
public class BeneficiryServiceImpl implements BeneficiryService {
	
	private static final Logger logger = LoggerFactory.getLogger(BeneficiryServiceImpl.class);
	
	@Autowired
	private BeneficiaryMasterRepository beneficiaryMasterRepository;
	
	@Autowired
    private FileStorageService fileStorageService;
	
	@Autowired
	private UserMasterRepository userMasterRepository;
	
	@Autowired
    private Environment env;
	
	@Autowired
	private UnregisteredBenRepo unregisterBenRepo;
	
	@Autowired
	private TServicerequestRepo tserviceReqRepo;
	
	@Autowired
	private TLgrequestissueRepo treqIssueRepo;
	
	@Autowired
    private ElogDaoImpl dAOService;
	
	@Override
	public List<?> selectGovermentEntity() {
		//return beneficiaryMasterRepository.findBeneficiary();
		String status = env.getProperty("G_CC_SUCCESS");
		return beneficiaryMasterRepository.findBeneficiaryWRTOStatus(status);
	}

	@Transactional
	@Override
	public ResponseModel saveGovermentEntity(Beneficiarydetail bd) {
		ResponseModel rmr=new ResponseModel();
		TBeneficiarydetail tbd=new TBeneficiarydetail();
		TUsermaster tu=new TUsermaster();
		try
		{
			if(bd.getLaglFile()!=null) {
				String fileNameLaglFile = fileStorageService.storeFile(bd.getLaglFile(),bd.getEntityUID(),"laglFile");
				
				if(fileNameLaglFile==null) {
					/* Not Uploaded Correctly */
					ResponseModel rm =new ResponseModel();
					rm.setResponseMessage(Constants.FILE_UPLOAD_ERROR+" : "+bd.getLaglFile().getOriginalFilename());
					return rm;
				}
				
				tbd.setLegAgreeDocName(fileNameLaglFile);
				tbd.setLegAgreeDocPath("uploads/"+fileNameLaglFile);
			}
			if(bd.getLalFile()!=null) {
				String fileNameLalFile = fileStorageService.storeFile(bd.getLalFile(),bd.getEntityUID(),"lalFile");
				
				if(fileNameLalFile==null) {
					/* Not Uploaded Correctly */
					ResponseModel rm =new ResponseModel();
					rm.setResponseMessage(Constants.FILE_UPLOAD_ERROR+" : "+bd.getLalFile().getOriginalFilename());
					return rm;
				}
				tbd.setLegAuthDocName(fileNameLalFile);
				tbd.setLegAuthDocPath("uploads/"+fileNameLalFile);				
			}
			if(bd.getRniFile()!=null) {
				String fileNameRniFile = fileStorageService.storeFile(bd.getRniFile(),bd.getEntityUID(),"rniFile");
				
				if(fileNameRniFile==null) {
					/* Not Uploaded Correctly */
					ResponseModel rm =new ResponseModel();
					rm.setResponseMessage(Constants.FILE_UPLOAD_ERROR+" : "+bd.getRniFile().getOriginalFilename());
					return rm;
				}
				tbd.setNationalIdDocName(fileNameRniFile);
				tbd.setNationalIdDocPath("uploads/"+fileNameRniFile);				
			}
			
			
			if(bd.getEntityTnC().equalsIgnoreCase("N")) {
				//TUnRegisteredBen ben = unregisterBenRepo.findByBeneficiaryId(Integer.parseInt(bd.getEntityUID()));
				TUnRegisteredBen ben = unregisterBenRepo.findBybeneficiary_UID(bd.getEntityUID());
				if(ben != null) {
					tbd.setBeneficiaryName(ben.getBeneficiaryName());
					System.out.println("ben.getBeneficiaryName()"+ben.getBeneficiaryName());
					tbd.setBeneficiaryUid(ben.getBeneficiary_UID());
					
				}
			}else {
				tbd.setBeneficiaryName(bd.getEntityname());
				tbd.setBeneficiaryUid(bd.getEntityUID());
				tbd.setBenStatusFlag(env.getProperty("G_CC_PENDING"));
			}

			tbd.setLoginId(bd.getUserId());
			tbd.setNationalid(bd.getEntityNationalID());
			tbd.setContactInfo(bd.getEntityMobNo());
			tbd.setBeneficiaryemail(bd.getEntityEmail());
			tbd.setBeneficiaryAddr1(bd.getAddress1());
			tbd.setBeneficiaryAddr2(bd.getAddress2());
			tbd.setCity(bd.getCity());
			tbd.setCountry(bd.getCountry());
			//tbd.setBenStatusFlag(env.getProperty("STATUS_FLAG"));
			
			tbd.setAgreeTag(bd.getEntityTnC());
			tbd.setRepName(bd.getName());
			tbd.setRepEmail(bd.getEntityEmail());
			tbd.setRepMobileNo(bd.getEntityMobNo());
			TBeneficiarydetail tben=beneficiaryMasterRepository.save(tbd);
			long id =tben.getBeneficiaryId();
			System.out.println("beneficiaryMasterRepository id:"+id);
			System.out.println("tbd.getBeneficiaryUid()"+tbd.getBeneficiaryUid());
			System.out.println("bd.getUserId()"+bd.getUserId());
			System.out.println("bd.getEntityTnC()"+bd.getEntityTnC());
			if(bd.getEntityTnC().equalsIgnoreCase("N")) {
				System.out.println("inside change");
				unregisterBenRepo.updateUnregisterBenStatus(Constants.YES, tbd.getBeneficiaryUid());
			}else if(bd.getEntityTnC().equalsIgnoreCase("Y")) {
			
				TUnRegisteredBen beneficiary_not_reg = new TUnRegisteredBen();
				beneficiary_not_reg.setBeneficiary_UID(bd.getEntityUID());
				beneficiary_not_reg.setBeneficiaryName(bd.getEntityname());
				beneficiary_not_reg.setDeletedFlag("R");
				unregisterBenRepo.save(beneficiary_not_reg);
			}
			tu.setLoginId(bd.getUserId());
			tu.setUserId(Integer.parseInt(""+new Long(id).intValue()));
			tu.setRoleId(2);
			String tempPass = RandomPasswordGenerator.generateSecureRandomPassword(14);
			tu.setPassword(tempPass.substring(3));
			tu.setUserStatus(env.getProperty("G_CC_PENDING"));
			tu.setLanguage(env.getProperty("USER_LANG"));
			userMasterRepository.save(tu);
			
			System.out.println("benificary id ... "+id);
			
			rmr.setResponseMessage(Constants.BEN_SUCCESS);
			rmr.setResponseCode(Constants.SUCCESS);
			
			
			if(id != 0) {
			
			/* send email   */
			
			logger.info("getOfficeEMail: "+bd.getEntityEmail()+", getEmail:"+bd.getEntityEmail()+", getName: " +bd.getName()+"getUserId: " +bd.getUserId());
			
			  try {
				new SendEmail().SendRegistrationFirstPhaseEmail(bd.getEntityEmail(), bd.getName(), bd.getUserId(),tempPass.substring(3) );
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.info("Failed to send email: getOfficeEMail: "+bd.getEntityEmail()+", getName: " +bd.getName()+"getUserId: " +bd.getUserId());
				e.printStackTrace();
			}

			
			}
			
			
			return rmr;
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			ResponseModel rm =new ResponseModel();
			rm.setResponseMessage(ex.getMessage());
			rm.setResponseCode(Constants.ERROR);
			return rm;
		}
	}

	@Override
	public ResponseModel checkEntityAvailability(String entityName) {
		ResponseModel rm = new ResponseModel();
		List<String[]> benList=beneficiaryMasterRepository.findBeneficiaryList(entityName);
		
		
		if(benList.size() >=0) {
				rm.setResponseCode(Constants.ERROR);
				rm.setResponseMessage("Entity Name Unavailable");
			}else {
				rm.setResponseCode(Constants.SUCCESS);
				rm.setResponseMessage("Entity Name Available");
				
			}
		
		
		return rm;
	}
	
	@Override
	public List<?> selectGovermentUnregisteredBeneficiaryEntity() {
		return unregisterBenRepo.findUnregisteredBeneficiary("N","R");
	}
	
	
	@Override
	public List<LgDataDto> getLgPendingWithBeneficiaryId(String login) {
		TUsermaster master = userMasterRepository.findByLoginId(login);
		String uId = "";
		List<LgDataDto> emlist = new ArrayList<LgDataDto>();
		
		if(master != null)
		{
		List<Object[]> list = dAOService.getLGDetailsPendingForBeneficiary(new Long(login));
		if(list.size() > 0 && list != null) {
		for(int i = 0;i<list.size();i++) {
		LgDataDto lgd = new LgDataDto();
		String rqDt = ""+list.get(i)[2];
		lgd.setReqDate(rqDt);
		String lgAmt = ""+list.get(i)[6];
		lgd.setLgAmount(lgAmt);
		String lgType = ""+list.get(i)[0];
		lgd.setLg_type(lgType);
		String benName = ""+list.get(i)[4];
		lgd.setBeneficiaryName(benName);
		String projNm=""+list.get(i)[3];
		lgd.setProjectName(projNm);
		String bnknm = ""+list.get(i)[8];
		lgd.setBankName(bnknm);
		String lgStat =  ""+list.get(i)[17];
		lgd.setLgStatus(lgStat);
		String termNCon = ""+list.get(i)[5];
		lgd.setTerms_conditions(termNCon);
		String bnkCo = ""+list.get(i)[7];
		lgd.setBank_code(bnkCo);
		String stDt = ""+list.get(i)[9];
		lgd.setLg_startDate(stDt);
		String endDt = ""+list.get(i)[10];
		lgd.setLg_endDate(endDt);
		String currCo = ""+list.get(i)[18];
		System.out.println("currCo"+currCo);
		lgd.setCurrencyCode(currCo);
		String currency = ""+list.get(i)[19];
		lgd.setCurrency(currency);
		String requestId = ""+list.get(i)[1];
		lgd.setRequestId(requestId);
		String terms_condition_message = ""+list.get(i)[20];
		lgd.setTerms_condition_message(terms_condition_message);
		String respCode = ""+list.get(i)[24];
		lgd.setResponse_code(respCode);
		String zktstrtDt = ""+list.get(i)[12];
		lgd.setZakat_start_date(zktstrtDt);
		String zktendDt = ""+list.get(i)[13];
		lgd.setZakat_end_date(zktendDt);
		String validDt = ""+list.get(i)[25];
		lgd.setLg_validity_date(validDt);
		String agrtg = ""+list.get(i)[26];
		lgd.setAgree_tag(agrtg);
		emlist.add(lgd);
		}
		}
		return emlist;
	}
		return emlist; 
	}

	@Override
	public ResponseModel changeOnlyStatusLgRequest(String requestId) {
		 ResponseModel rm = new ResponseModel();
		  if(requestId != null) {
			  tserviceReqRepo.changeRequestStatusByBeneficiaryamdRequest(Constants.PENDING,requestId);
			  treqIssueRepo.changeRequestStatusByBeneficiaryInReqIssue(Constants.PENDING,requestId);
			  rm.setResponseCode(Constants.PENDING_CODE);
			  rm.setResponseMessage("Your case request has been sent to bank. You will be notified once approved.");
		  }else {
			  rm.setResponseCode(Constants.ERROR);
			  rm.setResponseMessage("Sorry, Request Cannot be processed");
		  }
		  return rm;
	}

	@Override
	public ResponseModel changeStatusWithLgRequestNTerms(String requestId,String termsNconditionMessage) {
		ResponseModel rm = new ResponseModel();
		  if(requestId != null) {
			  tserviceReqRepo.changeRequestStatusAndTermsMessageByBeneficiary(Constants.NS_CHANGE,termsNconditionMessage,requestId);
			  treqIssueRepo.changeRequestStatusAndTermsMessageByBeneficiaryInReqIssue(Constants.NS_CHANGE,termsNconditionMessage,requestId);
			  rm.setResponseCode(Constants.NS_CHANGE_CODE);
			  rm.setResponseMessage("Your case request has been modified by the Beneficiary. Please accept for further processing");
		  }else {
			  rm.setResponseCode(Constants.ERROR);
			  rm.setResponseMessage("Sorry, Request Cannot be processed");
		  }
		  return rm;
	}
	
	
	
	@Override
	public ResponseModel changeRequestedStatusByBeneficiary(String requestId,String termsNconditionMessage) {
		ResponseModel rm = new ResponseModel();
		  if(requestId != null) {
			  tserviceReqRepo.changeRequestStatusAndTermsMessageByBeneficiary(Constants.NS_CHANGE,termsNconditionMessage,requestId);
			  treqIssueRepo.changeRequestStatusAndTermsMessageByBeneficiaryInReqIssue(Constants.NS_CHANGE,termsNconditionMessage,requestId);
			  rm.setResponseCode(Constants.SUCCESS);
			  rm.setResponseMessage("Request Approved By Beneficiary");
		  }else {
			  rm.setResponseCode(Constants.ERROR);
			  rm.setResponseMessage("Sorry, Request Cannot be processed");
		  }
		  return rm;
	}
	
	
	
	
	

	@Override
	public ResponseModel verifyStatusOfBeneficiary(String benName) {
		ResponseModel rm = new ResponseModel();
		  if(benName != null) {
			  beneficiaryMasterRepository.getBybenName(env.getProperty("G_CC_SUCCESS"),benName);
			  unregisterBenRepo.updateUnregisterBenStatusWithName("Y", benName);
			  TBeneficiarydetail tb = beneficiaryMasterRepository.getUserByName(benName);
			  if(tb != null) {
				  System.out.println("tb.getLoginId()"+tb.getLoginId());
			  userMasterRepository.setBeneficiaryStatus(env.getProperty("G_CC_SUCCESS"),tb.getLoginId());
			  }
			  rm.setResponseCode(Constants.SUCCESS);
			  rm.setResponseMessage("Verified By CC");
		  }else {
			  rm.setResponseCode(Constants.ERROR);
			  rm.setResponseMessage("No Benname received");
		  }
		  return rm;
	}

	@Override
	public ResponseModel checkBenNameAvailability(String benName) {
		
		ResponseModel rm = new ResponseModel();
		TBeneficiarydetail benList=beneficiaryMasterRepository.getUserByName(benName);
		
		
		if(benList != null) {
				rm.setResponseCode(Constants.ERROR);
				rm.setResponseMessage("Beneficiary Name Already Taken");
			}else {
				rm.setResponseCode(Constants.SUCCESS);
				rm.setResponseMessage("Beneficiary Name Available");
				
			}
		
		
		return rm;
	}

	@Override
	public ResponseModel getBeneficiaryUnverifiedEntityList(String adminId) {
		// TODO Auto-generated method stub
				ResponseModel rm =new ResponseModel();
			List<String[]> unvSupp=	beneficiaryMasterRepository.getBeneficiaryUnverifiedEntityList();
			rm.setResponseCode(Constants.SUCCESS);
			rm.setUserObject(unvSupp);	
				return rm;
			}
}
