package sa.wthaq.service.impl;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.xml.sax.SAXException;

import sa.wthaq.Constants;
import sa.wthaq.DTO.BankInfoDetails;
import sa.wthaq.Entities.TBankmaster;
import sa.wthaq.Entities.TBeneficiarydetail;
import sa.wthaq.Entities.TLg;
import sa.wthaq.Entities.TLgrequestissue;
import sa.wthaq.Entities.TServicerequest;
import sa.wthaq.Entities.TSupplierdetail;
import sa.wthaq.Entities.T_WriteUpType;
import sa.wthaq.JPA.Repository.BankMasterRepository;
import sa.wthaq.JPA.Repository.BeneficiaryMasterRepository;
import sa.wthaq.JPA.Repository.TCurrencytypemasterRepo;
import sa.wthaq.JPA.Repository.TLgDetailsRepo;
import sa.wthaq.JPA.Repository.TLgRepository;
import sa.wthaq.JPA.Repository.TLgrequestissueRepo;
import sa.wthaq.JPA.Repository.TLgtypemasterRepo;
import sa.wthaq.JPA.Repository.TServicerequestRepo;
import sa.wthaq.JPA.Repository.TSupplierdetailRepo;
import sa.wthaq.JPA.Repository.T_WriteUpTypeRepo;
import sa.wthaq.JPA.Repository.UserIbanRepo;
import sa.wthaq.JPA.Repository.UserMasterRepository;
import sa.wthaq.Utilities.Utilities;
import sa.wthaq.endpoint.SoapClient;
import sa.wthaq.models.LGRequestIssue;
import sa.wthaq.models.ResponseModel;
import sa.wthaq.request.BankInfo;
import sa.wthaq.request.BeneficiaryInfo;
import sa.wthaq.request.LGInfo;
import sa.wthaq.request.LgReturnInfo;
import sa.wthaq.request.SpecialConditions;
import sa.wthaq.request.SupplierInfo;
import sa.wthaq.service.IssueDataService;

@Service
public class IssueDataServiceImpl implements IssueDataService {

	
	@Autowired
	private UserIbanRepo userIbanRepo;
	
	@Autowired
	private BankMasterRepository masterRepo;
	
	@Autowired
	private TLgtypemasterRepo tLgtypemasterRepo;
	
	@Autowired
	private TCurrencytypemasterRepo tCurrencytypemasterRepo;
	
	@Autowired
	private TLgrequestissueRepo tLgrequestissueRepo;
	
	@Autowired
	private TServicerequestRepo tServicerequestRepo;
	
	@Autowired
	private UserMasterRepository userMasterRepository;
	
	@Autowired
	private TSupplierdetailRepo tSupplierdetailRepo;
	
	@Autowired
	private BeneficiaryMasterRepository benRepo;
	
	@Autowired
	 private   SoapClient soapClient;
	
	@Autowired
	private TLgRepository tlgRepo;
	
	@Autowired
	private TLgDetailsRepo tlgDetailsRepo;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private T_WriteUpTypeRepo twtRepo;
	

	@Override
	public List<?> selectIBAN(String loginid) {
		return userIbanRepo.findByLoginId(loginid);
	}

	@Override
	public List<?> selectLGType() {
		return tLgtypemasterRepo.findAllLgType();
	}

	@Override
	public List<?> selectCurrency() {
		return tCurrencytypemasterRepo.findAllCurrency();
	}

	@Transactional
	@Override
	public TLgrequestissue saveLGRequestIssue(LGRequestIssue lGRequestIssue) {
		try {
			TSupplierdetail tSupplierdetail=tSupplierdetailRepo.findByLoginId(lGRequestIssue.getUserId());
			//int roleid=userMasterRepository.findByLoginId(lGRequestIssue.getUserId()).getRoleId();
			TBankmaster mas = masterRepo.findByBankCode(lGRequestIssue.getBankCode().trim());			
			
			int lgTypeId = tLgtypemasterRepo.findByLgType(lGRequestIssue.getLGType());
			
			TServicerequest tServicerequest=new TServicerequest();
			tServicerequest.setBankCode(lGRequestIssue.getBankCode());
			tServicerequest.setBranchName(mas.getBranchName());
			tServicerequest.setCurrencyCode(lGRequestIssue.getCurrency());
			//tServicerequest.setLoginId(lGRequestIssue.getUserId());
			
			int rid = lGRequestIssue.getPrivateEntityCheckBox().equalsIgnoreCase("true")?Integer.parseInt(env.getProperty("ROLE_SUPP_ID")):Integer.parseInt(env.getProperty("ROLE_BENF_ID"));
			/*if(userMasterRepository.fetchLoginIdFromUID(Integer.parseInt(lGRequestIssue.getBeneficiaryId().toString()),rid)!= null)
			{*/
				tServicerequest.setLoginId(lGRequestIssue.getUserId());
			/* } */
			/*
			 * else { return null; }
			 */
			tServicerequest.setCreateBy(lGRequestIssue.getUserId());
			
			tServicerequest.setRoleId((lGRequestIssue.getPrivateEntityCheckBox().equalsIgnoreCase("true")?Integer.parseInt(env.getProperty("ROLE_SUPP_ID")):Integer.parseInt(env.getProperty("ROLE_BENF_ID"))));
			//tServicerequest.setRoleId(roleid);
			tServicerequest.setLgTypeId(lgTypeId);
			tServicerequest.setZakatStartDate(Utilities.convertDateToTimestamp(lGRequestIssue.getZakatPeriodStartDate()));
			tServicerequest.setZakatEndDate(Utilities.convertDateToTimestamp(lGRequestIssue.getZakatPeriodEndDate()));
			if(lGRequestIssue.getWriteUpType().equalsIgnoreCase(Constants.SAMA_TYPE)) {
			tServicerequest.setRequestStatusFlag(Constants.PENDING);
			}else {
				tServicerequest.setRequestStatusFlag(Constants.NS_PENDING);
			}
			tServicerequest.setTearmsConditionType(lGRequestIssue.getWriteUpType());
			tServicerequest.setTermsConditionMessage(lGRequestIssue.getTermsNconditionsMessage());
			TServicerequest re = tServicerequestRepo.save(tServicerequest);
			int reqid = (int) re.getRequestId();
			System.out.println("reqid"+reqid);
			
			TLgrequestissue tLgrequestissue=new TLgrequestissue();
			tLgrequestissue.setRequestId(reqid);
			tLgrequestissue.setLgTypeId(lgTypeId);
			tLgrequestissue.setTermsConditionType(lGRequestIssue.getWriteUpType());
			tLgrequestissue.setBankCode(lGRequestIssue.getBankCode());
			tLgrequestissue.setContractValue(Double.parseDouble(lGRequestIssue.getAmount()));
			tLgrequestissue.setLgValue(Double.parseDouble(lGRequestIssue.getAmount()));
			tLgrequestissue.setPercentageOfValue(new Double(lGRequestIssue.getPercentageOfContractValue()));
			System.out.println("strt"+lGRequestIssue.getLGStartDate());
			tLgrequestissue.setLgStartDate(Utilities.convertDateToTimestamp(lGRequestIssue.getLGStartDate()));
			System.out.println("end"+lGRequestIssue.getLGEndDate());
			tLgrequestissue.setLgEndDate(Utilities.convertDateToTimestamp(lGRequestIssue.getLGEndDate()));
			tLgrequestissue.setLgValidityDate(Utilities.convertDateToTimestamp(lGRequestIssue.getLGEndDate()));
			tLgrequestissue.setProjectId(lGRequestIssue.getProjectNumber());
			tLgrequestissue.setProjectName(lGRequestIssue.getProjectName());
			tLgrequestissue.setZakatStartDate(Utilities.convertDateToTimestamp(lGRequestIssue.getZakatPeriodStartDate()));
			tLgrequestissue.setZakatEndDate(Utilities.convertDateToTimestamp(lGRequestIssue.getZakatPeriodEndDate()));
			tLgrequestissue.setPurposeOfBond(lGRequestIssue.getPuroseOfBond());
			tLgrequestissue.setIbanNo(lGRequestIssue.getIBAN());
			tLgrequestissue.setByanNo(lGRequestIssue.getBayanNuymber());
			tLgrequestissue.setCurrencyCode(lGRequestIssue.getCurrency());
			//if(lGRequestIssue.getBeneficiaryId() != "") {
				System.out.println("chck"+lGRequestIssue.getPrivateEntityCheckBox());
			if(lGRequestIssue.getPrivateEntityCheckBox().equalsIgnoreCase("true")) {
				System.out.println("lGRequestIssue.getNinNumber()"+lGRequestIssue.getNinNumber());
/*if(tben != null) {*/
	tLgrequestissue.setBeneficiaryId(new Long(lGRequestIssue.getNinNumber()));
	
					/* } */
			}else {
				System.out.println("lGRequestIssue.getBeneficiaryId()"+lGRequestIssue.getBeneficiaryId());
				Optional<TBeneficiarydetail> tben = benRepo.findById(Long.valueOf(lGRequestIssue.getBeneficiaryId()));
				//List<?> tben = benRepo.findBeneficiaryWRTOUID(Long.valueOf(lGRequestIssue.getBeneficiary_UID()));
				tLgrequestissue.setBeneficiaryId(new Long(tben.get().getBeneficiaryUid()));
			}
		//}
			
			tLgrequestissue.setAgreeTag(lGRequestIssue.getCheckBox());
			
			//tLgrequestissue.setSupplierId((int) tSupplierdetail.getSupplierId());
			
			tLgrequestissue.setSupplierId(new Long(tSupplierdetail.getLoginId()));
			
			/*if(userMasterRepository.fetchLoginIdFromUID(Integer.parseInt(lGRequestIssue.getBeneficiaryId().toString()),rid)!= null)
			{*/
				tLgrequestissue.setLoginId(lGRequestIssue.getUserId());
			/*
			 * } else { return null; }
			 */
			
			tLgrequestissue.setCreateBy(lGRequestIssue.getUserId());
			tLgrequestissue.setRoleId((lGRequestIssue.getPrivateEntityCheckBox().equalsIgnoreCase("true")?Integer.parseInt(env.getProperty("ROLE_SUPP_ID")):Integer.parseInt(env.getProperty("ROLE_BENF_ID"))));
			//tLgrequestissue.setRoleId(roleid);
			if(lGRequestIssue.getWriteUpType().equalsIgnoreCase(Constants.SAMA_TYPE)) {
				tLgrequestissue.setActiveStatus(Constants.PENDING);
				}else {
					tLgrequestissue.setActiveStatus(Constants.NS_PENDING);
				}
			//tLgrequestissue.setActiveStatus("INACTIVE");
			tLgrequestissue.setLgIssuedBy(lGRequestIssue.getUserId());
			System.out.println("termsandcond message in lgreqisue"+re.getTermsConditionMessage());
			System.out.println("lGRequestIssue.getTermsNconditionsMessage()"+lGRequestIssue.getTermsNconditionsMessage());
			tLgrequestissue.setTermsConditionMessage(lGRequestIssue.getTermsNconditionsMessage());
			TLgrequestissue entity =tLgrequestissueRepo.save(tLgrequestissue);
			System.out.println("tLgrequestissueRepo.save ret:"+entity.getRequestId());
			if(entity.getActiveStatus().equalsIgnoreCase(Constants.PENDING)) {
			entity.setResponseCode(Constants.PENDING_CODE);
			}else if(entity.getActiveStatus().equalsIgnoreCase(Constants.NS_CHANGE)) {
				entity.setResponseCode(Constants.NS_CHANGE_CODE);	
			}else if(entity.getActiveStatus().equalsIgnoreCase(Constants.NS_PENDING)) {
				entity.setResponseCode(Constants.NS_PENDING_CODE);	
			}else if(entity.getActiveStatus().equalsIgnoreCase(Constants.NS_ACCEPT)) {
				entity.setResponseCode(Constants.NS_ACCEPT_CODE);	
			}else if(entity.getActiveStatus().equalsIgnoreCase(Constants.CANCELLED)) {
				entity.setResponseCode(Constants.CANCELLED_CODE);	
			}
			else {
				entity.setResponseCode(Constants.ERROR);
			}
			if(entity.getLgStartDate() != null) {
				entity.setStrtDt(Utilities.convertTimestampToDateString(entity.getLgStartDate()));
			}
			if(entity.getLgEndDate() != null) {
				entity.setEndDt(Utilities.convertTimestampToDateString(entity.getLgEndDate()));
			}
			
			
			return entity;
			
			
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public String sendLgDetailsToBank(LGRequestIssue lGRequestIssue) throws SAXException, IOException, ParserConfigurationException, JSONException,ParseException, org.json.simple.parser.ParseException {
		System.out.println("inside sendLg");
		/*
		 * LgReturnInfo lgInfo= new LgReturnInfo(); BeneficiaryInfo beneficiaryInfo =
		 * new BeneficiaryInfo(); beneficiaryInfo.setBeneficiaryID("");
		 * beneficiaryInfo.setBeneficiaryName(""); beneficiaryInfo.setContactNumber("");
		 * beneficiaryInfo.setEmailAddress(""); beneficiaryInfo.setFaxNumber("");
		 * beneficiaryInfo.setPObox(""); beneficiaryInfo.setPostalCode("");
		 * beneficiaryInfo.setAddress("");
		 * 
		 * lgInfo.setBeneficiaryInfo(beneficiaryInfo);
		 * 
		 * SupplierInfo supplierInfo = new SupplierInfo(); supplierInfo.setAddress("");
		 * supplierInfo.setApplicantCR(lGRequestIssue.getCR());
		 * supplierInfo.setApplicantIBAN(lGRequestIssue.getIBAN());
		 * supplierInfo.setContactNumber(""); supplierInfo.setEmailAddress("");
		 * supplierInfo.setFaxNumber(""); supplierInfo.setPObox("");
		 * supplierInfo.setPostalcode(""); supplierInfo.setSupplierName("");
		 * supplierInfo.setSupplierNameAr("");
		 * 
		 * lgInfo.setSupplierInfo(supplierInfo);
		 * 
		 * LGInfo info = new LGInfo(); Long amount =
		 * Long.valueOf(lGRequestIssue.getAmount()); info.setAmount(amount);
		 * info.setAmountText(lGRequestIssue.getAmount());
		 * info.setByanNumber(lGRequestIssue.getBayanNuymber());
		 * info.setCurrency(lGRequestIssue.getCurrency()); info.setHijriDate(""+new
		 * Date()); info.setLGNumber(new Random().nextInt()); //info.setLGValidity("");
		 * info.setPercentageOfContractValue("");
		 * info.setProjectName(lGRequestIssue.getProjectName());
		 * info.setProjectNumber(lGRequestIssue.getProjectNumber());
		 * info.setPurposeOfBbond(lGRequestIssue.getPuroseOfBond());
		 * //info.setZakatPeriodEndDate(new Date());
		 * //info.setZakatPeriodStart(lGRequestIssue.getZakatPeriodEndDate());
		 * 
		 * 
		 * lgInfo.setInfo(info);
		 * 
		 * 
		 * SpecialConditions specialConditions = new SpecialConditions();
		 * 
		 * specialConditions.setActualApplicantarName("");
		 * specialConditions.setActualApplicantenName("");
		 * specialConditions.setAddress(""); specialConditions.setAssignableLG(true);
		 * specialConditions.setAutoRenewalLG(true);
		 * specialConditions.setContactNumber("");
		 * specialConditions.setEmailAddress(""); specialConditions.setFaxNumber("");
		 * specialConditions.setOpenEndedLG(true); specialConditions.setPObox("");
		 * specialConditions.setPostalCode("");
		 * specialConditions.setTransferableLG(true);
		 * 
		 * lgInfo.setConditions(specialConditions);
		 */
		HttpHeaders hh=new HttpHeaders();
		hh.set("Content-Type","application/json");
		hh.set("SOAPAction","");
		hh.set("X-IBM-Client-Id","bbf1770c-9c6b-4703-bb41-cd9ea566ad2f");
		
		JSONObject obj=new JSONObject();
		obj.put("LGType", lGRequestIssue.getLGType());
		obj.put("writeUpType", lGRequestIssue.getWriteUpType());
		obj.put("bankCode", lGRequestIssue.getBankCode());
		obj.put("IBAN", lGRequestIssue.getIBAN());
		obj.put("CR", lGRequestIssue.getCR());
		obj.put("Amount", lGRequestIssue.getAmount());
		obj.put("amountInNumber", lGRequestIssue.getAmount());
		obj.put("currency", lGRequestIssue.getCurrency());
		//obj.put("LGStartDate", lGRequestIssue.getLGStartDate());
		//obj.put("LGEndDate", lGRequestIssue.getLGEndDate());
		obj.put("projectName", lGRequestIssue.getProjectName());
		obj.put("projectNumber", lGRequestIssue.getProjectNumber());
		//obj.put("ZakatPeriodStartDate", lGRequestIssue.getZakatPeriodStartDate());
		//obj.put("ZakatPeriodEndDate", lGRequestIssue.getZakatPeriodEndDate());
		obj.put("puroseOfBond", lGRequestIssue.getPuroseOfBond());
		obj.put("bayanNuymber", lGRequestIssue.getBayanNuymber());
		obj.put("checkBox", lGRequestIssue.getCheckBox());
		obj.put("userId", lGRequestIssue.getUserId());
		
		
		
		//HttpEntity<JSONObject> he=new HttpEntity<JSONObject>(obj,hh);
		HttpEntity<String> request = 
			      new HttpEntity<String>(obj.toString(), hh);
		String str = new RestTemplate().postForObject(
				"https://service.eu.apiconnect.ibmcloud.com/gws/apigateway/api/0959c418adcabe154e1169030ca09e2a10df0821ecc865b286f13a19938d6ca6/0JKEy4/lgInfo/sendIssueOfLGtoBankAckn", 
				request, String.class);
		
		System.out.println(str);
		//JSONParser parser = new JSONParser();
        
        JSONObject jsonObject = new JSONObject(str);
            System.out.println("Branches are :"+jsonObject);
            JSONObject jsonObject1 = new JSONObject(jsonObject.getString("responseMessage"));
            JSONObject jsonObject2 = new JSONObject(jsonObject1.getString("payload"));
            JSONObject jsonObject3 = new JSONObject(jsonObject2.getString("response"));
            JSONObject jsonObject4 = new JSONObject(jsonObject3.getString("SUCCESS"));
            //JSONObject obj3 = (JSONObject) obj2.get("return");
            
            

		    String string = jsonObject4.getString("return");
		    System.out.println("listOfBranches"+string);
		    
		return string;
		//return soapClient.sendLgIssueToBank(lgInfo);
	}

	@Override
	public List<?> selectBankName() {
		List<TBankmaster> mas =  masterRepo.findAll();
		List<BankInfoDetails> bnk = new ArrayList<BankInfoDetails>();
		if(mas.size() > 0) {
			for(TBankmaster tb:mas) {
				BankInfoDetails bn = new BankInfoDetails();
				bn.setBankCode(tb.getBankCode());
				bn.setBankName(tb.getBankName());
				bnk.add(bn);
			}
		}
		return bnk;
	}

	@Override
	public String changeNumToWord(int num) {
		return tLgrequestissueRepo.getNumberInWords(num);
		//return null;
	}

	@Override
	public int savelgData(String requestId) {
		Random rn = new Random();
		int val = rn.nextInt();
		//List<TLgrequestissue> tlg = tLgrequestissueRepo.getByRequest(Integer.parseInt(requestId));
		

		//List<TLgrequestissue> tlg = tLgrequestissueRepo.getByUid(requestId.trim());
		List<TLgrequestissue> tlg = tLgrequestissueRepo.getByRequest(Integer.parseInt(requestId.trim()));
		
		
		if(tlg.size() < 1) {
			
		}else {
			
			 try {
				 
				 
				/*
				 * for(int y=0;y<tlg.size();y++) {
				 * 
				 * System.out.println("tlg.get(y).getLgValidityDate()"+tlg.get(y).
				 * getLgValidityDate()+"tlg.get(y).getZakatStartDate()"+tlg.get(y).
				 * getZakatStartDate()+"tlg.get(y).getZakatEndDate()"+tlg.get(y).getZakatEndDate
				 * ()); tlgRepo.saveLg("LG"+rn.nextInt(), ""+tlg.get(y).getLgTypeId(),
				 * tlg.get(y).getTermsConditionType(), ""+tlg.get(y).getContractValue(),
				 * ""+tlg.get(y).getLgValue(), tlg.get(y).getCurrencyCode(),
				 * tlg.get(y).getProjectId(), tlg.get(y).getProjectName(),
				 * ""+tlg.get(y).getLgStartDate(), ""+tlg.get(y).getLgEndDate(), "", "",
				 * ""+tlg.get(y).getZakatStartDate(), ""+tlg.get(y).getZakatEndDate(),
				 * tlg.get(y).getByanNo(), ""+tlg.get(y).getBeneficiaryId(),
				 * ""+tlg.get(y).getSupplierId(),
				 * tlg.get(y).getRequestId(),tlg.get(y).getLoginId(),tlg.get(y).
				 * getTermsConditionMessage()); TLg tlgData =
				 * tlgRepo.getAllFromRequestId(""+tlg.get(y).getRequestId());
				 * System.out.println("cid"+tlgData.getActiveStatus()); TServicerequest servReq
				 * = tServicerequestRepo.getAllFromRequestId(tlg.get(y).getRequestId());
				 * System.out.println("servReq"+servReq.getBankCode());
				 * 
				 * 
				 * //tlgDetailsRepo.saveLgDetails(case_id, lg_no, lg_amount, request_id,
				 * bank_code, brnach_name, lg_issue_date, lg_validity_date, login_id, role_id,
				 * beneficiary_id, supplier_id, project_id, project_name, currency_code,
				 * zakat_start_date, zakat_end_date, from_date, dt, terms_condition_type,
				 * termsNConditionsmessage);
				 * tlgDetailsRepo.saveLgDetails(""+tlgData.getCaseId(),
				 * tlgData.getLgNo(),""+tlgData.getContractValue(), tlgData.getRequestId(),
				 * servReq.getBankCode(), servReq.getBranchName(), ""+tlgData.getLgIssueDate(),
				 * ""+tlgData.getLgValidityDate(), servReq.getLoginId(), servReq.getRoleId(),
				 * tlgData.getBeneficiaryId(),tlgData.getSupplierId().intValueExact(),
				 * tlgData.getProjectId(), tlgData.getProjectName(), tlgData.getCurrencyCode(),
				 * ""+tlgData.getZakatStartDate(), ""+tlgData.getZakatEndDate(),
				 * ""+servReq.getRequestDate(), tlgData.getTermsConditionType(),
				 * tlgData.getTermsConditionMessage());
				 * 
				 * }
				 */
				 
				 
				 
				 System.out.println("tlg.get(0).getLgValidityDate()"+tlg.get(0).getLgValidityDate()+"tlg.get(0).getZakatStartDate()"+tlg.get(0).getZakatStartDate()+"tlg.get(0).getZakatEndDate()"+tlg.get(0).getZakatEndDate());
				//int roleId =  (lGRequestIssue.getIsPrivate().equalsIgnoreCase("Y")?Integer.parseInt(env.getProperty("ROLE_SUPP_ID")):Integer.parseInt(env.getProperty("ROLE_BENF_ID"))
					tlgRepo.saveLg("LG"+rn.nextInt(), ""+tlg.get(0).getLgTypeId(), tlg.get(0).getTermsConditionType(), ""+tlg.get(0).getContractValue(), ""+tlg.get(0).getLgValue(), tlg.get(0).getCurrencyCode(), tlg.get(0).getProjectId(), tlg.get(0).getProjectName(), ""+tlg.get(0).getLgStartDate(), ""+tlg.get(0).getLgEndDate(), "", "", ""+tlg.get(0).getZakatStartDate(), ""+tlg.get(0).getZakatEndDate(), tlg.get(0).getByanNo(), ""+tlg.get(0).getBeneficiaryId(), ""+tlg.get(0).getSupplierId(), tlg.get(0).getRequestId(),tlg.get(0).getLoginId(),tlg.get(0).getTermsConditionMessage(),tlg.get(0).getRoleId());
					TLg tlgData = tlgRepo.getAllFromRequestId(""+tlg.get(0).getRequestId());
					System.out.println("cid"+tlgData.getActiveStatus());
					TServicerequest servReq = tServicerequestRepo.getAllFromRequestId(tlg.get(0).getRequestId());
					System.out.println("servReq"+servReq.getBankCode());
					
					
					//tlgDetailsRepo.saveLgDetails(case_id, lg_no, lg_amount, request_id, bank_code, brnach_name, lg_issue_date, lg_validity_date, login_id, role_id, beneficiary_id, supplier_id, project_id, project_name, currency_code, zakat_start_date, zakat_end_date, from_date, dt, terms_condition_type, termsNConditionsmessage);
					tlgDetailsRepo.saveLgDetails(""+tlgData.getCaseId(), tlgData.getLgNo(),""+tlgData.getContractValue(), tlgData.getRequestId(), servReq.getBankCode(), servReq.getBranchName(), ""+tlgData.getLgIssueDate(), ""+tlgData.getLgValidityDate(), servReq.getLoginId(), servReq.getRoleId(), tlgData.getBeneficiaryId(),tlgData.getSupplierId().intValueExact(), tlgData.getProjectId(), tlgData.getProjectName(), tlgData.getCurrencyCode(), ""+tlgData.getZakatStartDate(), ""+tlgData.getZakatEndDate(), ""+servReq.getRequestDate(), tlgData.getTermsConditionType(), tlgData.getTermsConditionMessage());
				 
				 
				 
			 	
			 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return 0;
	}

	@Override
	public List<?> selectWriteUpType() {
		List<T_WriteUpType> wrt = twtRepo.findAll();
		return wrt;
	}

	

}
