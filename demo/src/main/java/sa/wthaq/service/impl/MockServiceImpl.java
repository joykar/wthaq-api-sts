package sa.wthaq.service.impl;

import java.util.Date;
import java.util.Random;

import org.springframework.stereotype.Service;

import sa.wthaq.Constants;
import sa.wthaq.DTO.MCIDataDto;
import sa.wthaq.DTO.MciRequestData;
import sa.wthaq.Utilities.Utilities;
import sa.wthaq.models.ResponseModel;
import sa.wthaq.service.MockService;

@Service
public class MockServiceImpl implements MockService{

	@Override
	public ResponseModel checkDataExists(MciRequestData reqData) {
		ResponseModel rm =new ResponseModel();
		MCIDataDto mci_data =new MCIDataDto();
	if(reqData != null) {
		
		int natonal_Id = Utilities.ifCRNinList(reqData.getCr_no());
		if(natonal_Id != 0) {
			
			mci_data.setCr_no(reqData.getCr_no());
			mci_data.setCr_type("Main");
			mci_data.setCr_status("Active");
			mci_data.setAddress("AlOlaya St. AlSahafa Dist.");
			mci_data.setCity("Riyadh 3059");
			mci_data.setEntity_name("ABC"+new Random().nextInt(1000));
			mci_data.setCr_issue_dt(""+new Date());
			mci_data.setCr_expiry_dt(""+new Date());
			mci_data.setFname("Active");
			mci_data.setDob(reqData.getDob());
			mci_data.setMob_no("894751628");
			mci_data.setNIN(""+natonal_Id);
			rm.setResponseCode(Constants.SUCCESS);
			rm.setUserObject(mci_data);
			rm.setResponseMessage("Data found");
		}else {
			rm.setResponseCode(Constants.ERROR);
			//rm.setUserObject(mci_data);
			rm.setResponseMessage("No Data Available");
			
		}
		
	}
	return rm;
	}
	
	

}
