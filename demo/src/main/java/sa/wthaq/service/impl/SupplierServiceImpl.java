package sa.wthaq.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sun.javafx.tk.quantum.MasterTimer;

import sa.wthaq.Constants;
import sa.wthaq.Communication.SendEmail;
import sa.wthaq.DTO.EntityModelGov;
import sa.wthaq.DTO.EntityModelPrivate;
import sa.wthaq.DTO.LgDataDto;
import sa.wthaq.DTO.LgIssuedDataDTO;
import sa.wthaq.DTO.LgListWithChangesAcceptSupplier;
import sa.wthaq.DTO.LgPendingListDTO;
import sa.wthaq.Entities.TSupplierdetail;
import sa.wthaq.Entities.TUsermaster;
import sa.wthaq.Entities.TUsermaster2;
import sa.wthaq.JPA.Repository.SupplierMasterRepository;
import sa.wthaq.JPA.Repository.TLgrequestissueRepo;
import sa.wthaq.JPA.Repository.TServicerequestRepo;
import sa.wthaq.JPA.Repository.UserMasterRepository;
import sa.wthaq.Utilities.RandomPasswordGenerator;
import sa.wthaq.controller.TabadulController;
import sa.wthaq.dao.ElogDaoImpl;
import sa.wthaq.models.RegistrationPvtEntt;
import sa.wthaq.models.ResponseModel;
import sa.wthaq.service.FileStorageService;
import sa.wthaq.service.SupplierService;
 

@Service("SupplierService")
public class SupplierServiceImpl implements SupplierService  {

	private static final Logger logger = LoggerFactory.getLogger(SupplierServiceImpl.class);
	
	@Autowired
    private FileStorageService fileStorageService;
	
	@PersistenceContext
	private EntityManager entityManager;


	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;
	
	
	@Autowired
    private SupplierMasterRepository s_repository;
	
	
	@Autowired
    private ElogDaoImpl dAOService;
	
	@Autowired
    private UserMasterRepository u_repository;
	
	@Autowired
	private TServicerequestRepo tserviceReqRepo;
	
	@Autowired
	private TLgrequestissueRepo treqIssueRepo;
	
	
	@Autowired
	private Environment env;
	
	
	
	@Override
	 public  Boolean checkIfCRNOK(String crnNo) {
 		
javax.persistence.Query query = entityManager.createNativeQuery("SELECT count(cr_no) FROM T_SupplierDetails as em " +
	                "WHERE em.cr_no = ?", TSupplierdetail.class);
	        query.setParameter(1, crnNo);
	        if(query.getResultList().size()>0) {
	           return true;	        	
	        }
	        else {
	            return false;
	        }
	    
	 }



	@Override
	public TSupplierdetail findSupplierCRNWise(String crnNo) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<TSupplierdetail> findSuppliers() {
		// TODO Auto-generated method stub
		return s_repository.findAll();
	}



	@Override
	public List<Object[]> findSuppliersIdAndName() {
		// TODO Auto-generated method stub
	   return dAOService.findSuppliersIdAndName();
         
	}
	
	public TSupplierdetail save(TSupplierdetail stock) {
        return s_repository.save(stock);
    }



	@Override
	public ResponseModel registerSupplier(RegistrationPvtEntt re, MultipartFile crFile, MultipartFile rniFile, MultipartFile lalFile, MultipartFile laglFile) {
		// TODO Auto-generated method stub
		ResponseModel rmr=new ResponseModel();
		try {
			if(crFile!=null) {
					String fileNameCrFile = fileStorageService.storeFile(crFile,re.getCommRegNo(),"crFile");
					
					if(fileNameCrFile==null) {
						/* Not Uploaded Correctly */
						ResponseModel rm =new ResponseModel();
						rm.setResponseMessage(Constants.FILE_UPLOAD_ERROR+" : "+crFile.getOriginalFilename());
						return rm;
					}
					
					re.setCrFile(fileNameCrFile);
					re.setCrFilePath("uploads/"+fileNameCrFile);
			}
			if(laglFile!=null) {
								String fileNameLaglFile = fileStorageService.storeFile(laglFile,re.getCommRegNo(),"laglFile");
								
								if(fileNameLaglFile==null) {
									/* Not Uploaded Correctly */
									ResponseModel rm =new ResponseModel();
									rm.setResponseMessage(Constants.FILE_UPLOAD_ERROR+" : "+laglFile.getOriginalFilename());
									return rm;
								}
								
								re.setCrFile(fileNameLaglFile);
								re.setCrFilePath("uploads/"+fileNameLaglFile);
							 
			}
			if(lalFile!=null) {
			String fileNameLalFile = fileStorageService.storeFile(lalFile,re.getCommRegNo(),"lalFile");
			
			if(fileNameLalFile==null) {
				/* Not Uploaded Correctly */
				ResponseModel rm =new ResponseModel();
				rm.setResponseMessage(Constants.FILE_UPLOAD_ERROR+" : "+lalFile.getOriginalFilename());
				return rm;
			}
			
			
			re.setCrFile(fileNameLalFile);
			re.setCrFilePath("uploads/"+fileNameLalFile);
			}
			if(rniFile!=null) {
			String fileNameRniFile = fileStorageService.storeFile(rniFile,re.getCommRegNo(),"rniFile");
			
			if(fileNameRniFile==null) {
				/* Not Uploaded Correctly */
				ResponseModel rm =new ResponseModel();
				rm.setResponseMessage(Constants.FILE_UPLOAD_ERROR+" : "+rniFile.getOriginalFilename());
				return rm;
			}
			
			
			re.setCrFile(fileNameRniFile);
			re.setCrFilePath("uploads/"+fileNameRniFile);
			}

String tempPass=RandomPasswordGenerator.generateSecureRandomPassword(14);
re.setPassword(tempPass);
			rmr= dAOService.registerActors(re);//.registerSupplier(re);
if(rmr.getResponseCode()==Constants.SUCCESS) {
	
	
	/* Send to CC */
	
	
	
	
	/*  After CC returns we send email */
	/* send email   */
	
	logger.info("getOfficeEMail: "+re.getOfficeEMail()+", getEmail:"+re.getEmail()+", getName: " +re.getName()+"getUserId: " +re.getUserId());
	
	  try {
		new SendEmail().SendRegistrationFirstPhaseEmail(re.getEmail(), re.getName(), re.getUserId(),tempPass );
	} catch (Exception e) {
		// TODO Auto-generated catch block
		logger.info("Failed to send email: getOfficeEMail: "+re.getOfficeEMail()+", getName: " +re.getName()+"getUserId: " +re.getUserId());
		e.printStackTrace();
	}

	
	
	
}
return rmr;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			 
		}	
	
		
		
		ResponseModel rm =new ResponseModel();
		rm.setResponseMessage(Constants.REGISTRATION_ERROR);
		rm.setResponseCode(Constants.ERROR);
		return rm;
		
		
		
		 
	}



	@Override
	public ResponseModel profileUpdateSupplier(RegistrationPvtEntt re, MultipartFile crFile, MultipartFile rniFile,
			MultipartFile lalFile, MultipartFile laglFile) {
		// TODO Auto-generated method stub
		ResponseModel rmr=new ResponseModel();
		try {
			if(crFile!=null) {
					String fileNameCrFile = fileStorageService.storeFile(crFile,re.getCommRegNo(),"crFile");
					
					if(fileNameCrFile==null) {
						/* Not Uploaded Correctly */
						ResponseModel rm =new ResponseModel();
						rm.setResponseMessage(Constants.FILE_UPLOAD_ERROR+" : "+crFile.getOriginalFilename());
						return rm;
					}
					
					re.setCrFile(fileNameCrFile);
					re.setCrFilePath("uploads/"+fileNameCrFile);
			}
			if(laglFile!=null) {
								String fileNameLaglFile = fileStorageService.storeFile(laglFile,re.getCommRegNo(),"laglFile");
								
								if(fileNameLaglFile==null) {
									/* Not Uploaded Correctly */
									ResponseModel rm =new ResponseModel();
									rm.setResponseMessage(Constants.FILE_UPLOAD_ERROR+" : "+laglFile.getOriginalFilename());
									return rm;
								}
								
								re.setCrFile(fileNameLaglFile);
								re.setCrFilePath("uploads/"+fileNameLaglFile);
							 
			}
			if(lalFile!=null) {
			String fileNameLalFile = fileStorageService.storeFile(lalFile,re.getCommRegNo(),"lalFile");
			
			if(fileNameLalFile==null) {
				/* Not Uploaded Correctly */
				ResponseModel rm =new ResponseModel();
				rm.setResponseMessage(Constants.FILE_UPLOAD_ERROR+" : "+lalFile.getOriginalFilename());
				return rm;
			}
			
			
			re.setCrFile(fileNameLalFile);
			re.setCrFilePath("uploads/"+fileNameLalFile);
			}
			if(rniFile!=null) {
			String fileNameRniFile = fileStorageService.storeFile(rniFile,re.getCommRegNo(),"rniFile");
			
			if(fileNameRniFile==null) {
				/* Not Uploaded Correctly */
				ResponseModel rm =new ResponseModel();
				rm.setResponseMessage(Constants.FILE_UPLOAD_ERROR+" : "+rniFile.getOriginalFilename());
				return rm;
			}
			
			
			re.setCrFile(fileNameRniFile);
			re.setCrFilePath("uploads/"+fileNameRniFile);
			}

 
			rmr= dAOService.profileUpdateActors(re);//.registerSupplier(re);
if(rmr.getResponseCode()==Constants.SUCCESS) {
	
	
	/* Send to CC */
	
	
	
	
	/*  After CC returns we send email */
	/* send email   */
	
/*	logger.info("getOfficeEMail: "+re.getOfficeEMail()+", getName: " +re.getName()+"getUserId: " +re.getUserId());
	
	  try {
		new SendEmail().SendRegistrationFirstPhaseEmail(re.getOfficeEMail(), re.getName(), re.getUserId(),tempPass );
	} catch (Exception e) {
		// TODO Auto-generated catch block
		logger.info("Failed to send email: getOfficeEMail: "+re.getOfficeEMail()+", getName: " +re.getName()+"getUserId: " +re.getUserId());
		e.printStackTrace();
	}
*/
	
	
	
}
return rmr;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			 
		}	
	
		
		
		ResponseModel rm =new ResponseModel();
		rm.setResponseMessage(Constants.REGISTRATION_ERROR);
		rm.setResponseCode(Constants.ERROR);
		return rm;
		
		
		
		 
	}



	@Override
	public List<TSupplierdetail> findAllSuppliersDetails() {
		List<TSupplierdetail> list = dAOService.findSuppliersDetailsWithoutMapped();
		return list;
	}



	@Override
	public List<LgPendingListDTO> getLgWithSupplierId(String login) {
		//TUsermaster master = u_repository.findByLoginId(login);
		//String uId = "";
		
		
		if(login != null)
		{
		List<LgPendingListDTO> list = dAOService.getLGDetailsPendingForSupplier(new Long(login));
		return list;
	}
		return null; 
	}
	
	



	@Override
	public List<String[]> getCrnFromLogin(String loginId) {
		
		List<String[]> crnDetails=s_repository.getCrnFromLogin(loginId);
		
		 
		for (int y=0; y<crnDetails.size(); y++) {
			
			//fetch response message
			
			 
			if(crnDetails.get(y)[4].toString().equals(env.getProperty("CC_PENDING"))) {
				
				String[] result = Arrays.copyOf(crnDetails.get(y), crnDetails.get(y).length +1);
				result[crnDetails.get(y).length-1] = ""+Constants.CC_PENDING_CODE;
				result[crnDetails.get(y).length] = ""+Constants.CC_PENDING;
			    crnDetails.set(y, result);
			}
			else if(crnDetails.get(y)[4].toString().equals(env.getProperty("CC_SUCCESS"))) {
				String[] result = Arrays.copyOf(crnDetails.get(y), crnDetails.get(y).length +1);
				result[crnDetails.get(y).length-1] = ""+Constants.CC_SUCCESS_CODE;
			    result[crnDetails.get(y).length] = ""+Constants.CC_SUCCESS;
			    crnDetails.set(y, result);
			}
			else if(crnDetails.get(y)[4].toString().equals(env.getProperty("CC_FAILED"))) {
				String[] result = Arrays.copyOf(crnDetails.get(y), crnDetails.get(y).length +1);
				result[crnDetails.get(y).length-1] = ""+Constants.CC_FAILED_CODE;
			    result[crnDetails.get(y).length] = ""+Constants.CC_FAILED;
			    crnDetails.set(y, result);
		}
			else if(crnDetails.get(y)[4].toString().equals(env.getProperty("BANK_PENDING"))) {
				String[] result = Arrays.copyOf(crnDetails.get(y), crnDetails.get(y).length +1);
				result[crnDetails.get(y).length-1] = ""+Constants.BANK_PENDING_CODE;
			    result[crnDetails.get(y).length] = ""+Constants.BANK_PENDING;
			    crnDetails.set(y, result);
			}
			else if(crnDetails.get(y)[4].toString().equals(env.getProperty("BANK_VERIFIED"))) {
				String[] result = Arrays.copyOf(crnDetails.get(y), crnDetails.get(y).length +1);
				result[crnDetails.get(y).length-1] = ""+Constants.BANK_VERIFIED_CODE;
			    result[crnDetails.get(y).length] = ""+Constants.BANK_VERIFIED;
			    crnDetails.set(y, result);
			}
			else if(crnDetails.get(y)[4].toString().equals(env.getProperty("BANK_NOT_VERIFIED"))) {
				String[] result = Arrays.copyOf(crnDetails.get(y), crnDetails.get(y).length +1);
				result[crnDetails.get(y).length-1] = ""+Constants.BANK_NOT_VERIFIED_CODE;
			    result[crnDetails.get(y).length] = ""+Constants.BANK_NOT_VERIFIED;
			    crnDetails.set(y, result);
			}
			else if(crnDetails.get(y)[4].toString().equals(env.getProperty("G_CC_PENDING"))) {
				String[] result = Arrays.copyOf(crnDetails.get(y), crnDetails.get(y).length +1);
				result[crnDetails.get(y).length-1] = ""+Constants.CC_PENDING_CODE;
			    result[crnDetails.get(y).length] = ""+Constants.CC_PENDING;
			    crnDetails.set(y, result);
			}
			else if(crnDetails.get(y)[4].toString().equals(env.getProperty("G_CC_SUCCESS"))) {
				String[] result = Arrays.copyOf(crnDetails.get(y), crnDetails.get(y).length +1);
				result[crnDetails.get(y).length-1] = "";
			    result[crnDetails.get(y).length] = ""+Constants.G_CC_SUCCESS;
			    crnDetails.set(y, result);
			}
			else if(crnDetails.get(y)[4].toString().equals(env.getProperty("T_CC_PENDING"))) {
				String[] result = Arrays.copyOf(crnDetails.get(y), crnDetails.get(y).length +1);
				result[crnDetails.get(y).length-1] = ""+Constants.CC_PENDING_CODE;
			    result[crnDetails.get(y).length] = ""+Constants.CC_PENDING;
			    crnDetails.set(y, result);
			}
			else if(crnDetails.get(y)[4].toString().equals(env.getProperty("T_CC_SUCCESS"))) {
				String[] result = Arrays.copyOf(crnDetails.get(y), crnDetails.get(y).length +1);
				result[crnDetails.get(y).length-1] = ""+Constants.CC_SUCCESS_CODE;
			    result[crnDetails.get(y).length] = ""+Constants.CC_SUCCESS;
			    crnDetails.set(y, result);
			}
		}
		
		
		
		
		
		return crnDetails;
		
	}
	
	
	@Override
	public List<?> getBenNameFromCRN(BigDecimal crn) {
		return s_repository.findSupplierByCRN(crn);
		
	}



	@Override
	public List<LgIssuedDataDTO> getAllLGApprovedDetailsForBeneficiaryServ(String loginId) {
		List<LgIssuedDataDTO> list = dAOService.getAllLGApprovedDetailsForBeneficiary(loginId);
		return list;
	}



	@Override
	public List<LgIssuedDataDTO> getAllLGApprovedDetailsForSupplierServ(String loginId) {
		return dAOService.getAllLGApprovedDetailsForSupplier(loginId);
		
	}



	@Override
	public List<LgListWithChangesAcceptSupplier> getAllLGChangedTnCForSupplierList(String loginId) {
		return dAOService.getAllLGChangedTnCForSupplierList(loginId);
	}



	
	@Override
	public ResponseModel acceptTheLgIssue(String requestId) {
		ResponseModel rm = new ResponseModel();
		  if(requestId != null) {
			  tserviceReqRepo.changeRequestStatusByBeneficiaryamdRequest(Constants.NS_ACCEPT,requestId);
			  treqIssueRepo.changeRequestStatusByBeneficiaryInReqIssue(Constants.NS_ACCEPT,requestId);
			  rm.setResponseCode(Constants.NS_ACCEPT_CODE);
			  rm.setResponseMessage("Request Accepted By Supplier");
		  }else {
			  rm.setResponseCode(Constants.ERROR);
			  rm.setResponseMessage("Sorry, Request Cannot be processed");
		  }
		  return rm;
	}
	
	
	@Override
	public ResponseModel cancelTheLgIssueBySupplier(String requestId) {
		ResponseModel rm = new ResponseModel();
		  if(requestId != null) {
			  tserviceReqRepo.changeRequestStatusByBeneficiaryamdRequest(Constants.CANCELLED,requestId);
			  treqIssueRepo.changeRequestStatusByBeneficiaryInReqIssue(Constants.CANCELLED,requestId);
			  rm.setResponseCode(Constants.CANCELLED_CODE);
			  rm.setResponseMessage("Request Accepted By Supplier");
		  }else {
			  rm.setResponseCode(Constants.ERROR);
			  rm.setResponseMessage("Sorry, Request Cannot be processed");
		  }
		  return rm;
	}


	
	
	
	@Override
	public ResponseModel getSupplierUnverifiedEntityList(String adminId) {
		// TODO Auto-generated method stub
		ResponseModel rm =new ResponseModel();
	List<EntityModelPrivate> unvSupp=	s_repository.getSupplierUnverifiedEntityList();
	rm.setResponseCode(Constants.SUCCESS);
	rm.setUserObject(unvSupp);	
		return rm;
	}
	
	@Override
	public ResponseModel getGovUnverifiedEntityList(String adminId) {
		// TODO Auto-generated method stub
		ResponseModel rm =new ResponseModel();
	List<EntityModelGov> unvSupp=	s_repository.getGovUnverifiedEntityList();
	rm.setResponseCode(Constants.SUCCESS);
	rm.setUserObject(unvSupp);	
		return rm;
	}

	@Override
	public ResponseModel getPriEntityDetails(String nId,String crn,String adminId) {
		// TODO Auto-generated method stub
		ResponseModel rm =new ResponseModel();
		List<EntityModelPrivate> unvSupp=	s_repository.getPriEntityDetails(nId,new BigDecimal(""+crn.trim()));
		rm.setResponseCode(Constants.SUCCESS);
		rm.setUserObject(unvSupp);	
			return rm;
	}



	@Override
	public ResponseModel getGovEntityDetails(String nId, String uid, String adminId) {
		// TODO Auto-generated method stub
		ResponseModel rm =new ResponseModel();
		List<EntityModelGov> unvSupp=	s_repository.getGovEntityDetails(nId,uid.trim());
		rm.setResponseCode(Constants.SUCCESS);
		rm.setUserObject(unvSupp);	
			return rm;
	}


}
