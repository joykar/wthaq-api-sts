package sa.wthaq.service.impl;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import sa.wthaq.Constants;
 import sa.wthaq.Entities.TELGPaymentDetail;
import sa.wthaq.JPA.Repository.PaymentRepository;
import sa.wthaq.Utilities.JWTUtils;
import sa.wthaq.Utilities.Utilities;
import sa.wthaq.models.PaymentModel;
import sa.wthaq.models.ResponseModel;
import sa.wthaq.service.PaymentService;
import sa.wthaq.service.UserService;

 
@Service("PaymentService")
public class PaymentServiceImpl  implements PaymentService {

	@Autowired
	private Environment env;
	
	 @Autowired
	 private UserService userService;
	
	
	 
	 @Autowired
	 private PaymentRepository paymentRepository;
	 
	 
	@Override
	public ResponseModel getPaymentParams(PaymentModel paymentModel) {
		// TODO Auto-generated method stub
		
		ResponseModel rm=new ResponseModel();
		
		
		try {
			
			//validate gotten value
			 
			
     	    boolean validateState=true;
     	    
     	    
     	    //amount
     	    if(paymentModel.getAmount() != null && (paymentModel.getAmount().compareTo(new BigDecimal(0.00))==1)  ) {
     	     	validateState=validateState&&true;
     	         }
     	    else {
     	    	validateState=validateState&&false;
     	    	
     	    	rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Payment amount found invalid!");
     			return rm;
     	     }
     	   
     	    //currency
     	    
     	   if(paymentModel.getCurrency() != null && paymentModel.getCurrency().trim().length() == 3 &&  Utilities.checkIfValidCurrency(paymentModel.getCurrency().trim())  ) {
    	     	validateState=validateState&&true;
    	        }
    	    else {
    	    	validateState=validateState&&false;
    	    	
    	    	rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Selected currency found invalid!");
     			return rm;
    	     }
     	    
     	  //language
    	    
     	   if(paymentModel.getLanguage() != null && paymentModel.getLanguage().trim().length() > 0  ) {
    	     	validateState=validateState&&true;
    	     	  }
    	    else {
    	    	validateState=validateState&&false;
    	    	
    	    	rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Selected language found invalid!");
     			return rm;
    	     }
     	    
     	   
     	   
     	//customerEmail
   	    
     	   if(Utilities.isValidEmail(paymentModel.getCustomerEmail())  ) {
    	     	validateState=validateState&&true;
    	         }
    	    else {
    	    	validateState=validateState&&false;
    	    	
    	    	rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Customer email found invalid!");
     			return rm;
    	     }
     	  
     	    
     	   
     	   
     	   
     	//orderDescription
      	    
     	   if( paymentModel.getOrderDescription() != null && paymentModel.getOrderDescription().trim().length() >3 ) {
    	     	validateState=validateState&&true;
    	        }
    	    else {
    	    	validateState=validateState&&false;
    	    	
    	    	rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Order description found insufficient!");
     			return rm;
    	     }
     	   
     	   
     	//returnUrl
     	    
     	   if( paymentModel.getReturnUrl() != null && paymentModel.getReturnUrl().trim().length() >7 && Utilities.isValidURL(paymentModel.getReturnUrl().trim())) {
    	     	
     		   //dont allow non http
     		   if( Pattern.compile(Pattern.quote("http"), Pattern.CASE_INSENSITIVE).matcher(paymentModel.getReturnUrl().trim()).find()  ||  
     				  Pattern.compile(Pattern.quote("https"), Pattern.CASE_INSENSITIVE).matcher(paymentModel.getReturnUrl().trim()).find()   
     				   ) {
     			  validateState=validateState&&true;
     			   
     		   }
     		   else {
     			  validateState=validateState&&false;
     			  
     			 rm.setResponseCode(Constants.ERROR);
      			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Return URL found malformed (Do not forget to include http or https)!");
      			return rm;
     			   
     		   }
     		   
     		   
    	         }
    	    else {
    	    	validateState=validateState&&false;
    	    	
    	    	rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Return URL found invalid (Do not forget to include http or https)!");
     			return rm;
    	     }
     	   
     	   
     	   //loginId
     	  if( paymentModel.getLoginId() != null && paymentModel.getLoginId().trim().length() > 0  ) {
  	     	//is login id ok?
     		  boolean lidOK=userService.isUserLoginIdValid(paymentModel.getLoginId().trim());
     		  validateState=validateState&&lidOK;
  	         }
  	    else {
  	    	validateState=validateState&&false;
  	    	
  	    	rm.setResponseCode(Constants.ERROR);
 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Could not find the requested user!");
 			return rm;
  	     }
   	   
     	   
     	   
     	 //requestid
     	  if( paymentModel.getRequestId() != null && paymentModel.getRequestId().trim().length() > 0  ) {
  	     	//is requestid ok?
     		  boolean ridOK=userService.isRequestIdValid(paymentModel.getLoginId().trim(),paymentModel.getRequestId().trim());
     		 System.out.println("isRequestIdValid: "+ridOK);
     		 
     		 if(ridOK) {
     			validateState=validateState&&ridOK;
     			 
     		 }
     		 else {
     			rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Could not find the requested LG issue details!");
     			return rm;	 
     		 }
     	     }
  	    else {
  	    	validateState=validateState&&false;
  	    	
  	    	rm.setResponseCode(Constants.ERROR);
 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Request id seems to be invalid!");
 			return rm;
  	     }
   	   
     	   
			
			if(!validateState) {
				
				rm.setResponseCode(Constants.ERROR);
	 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE);
	 			return rm;
				
			}
			
			
			
			
			
			
			
			String accessCode=env.getProperty("ACCESS_CODE");
			String command=env.getProperty("COMMAND");
			String merchantIdentifier=env.getProperty("MERCHANT_IDENTIFIER");
			String merchantReference=env.getProperty("MERCHANT_REFERENCE");
			String signaturePrivate=env.getProperty("SIGNATURE_PRIVATE");
			
			String SHARequestPhrase=env.getProperty("SHA_Request_Phrase");
			String SHAType=env.getProperty("SHA_Type");
			
			
			paymentModel.setAmount(paymentModel.getAmount().multiply(new BigDecimal(100)));
			paymentModel.setAccess_code(accessCode);
			paymentModel.setCommand(command);
			paymentModel.setMerchant_identifier(merchantIdentifier);
			paymentModel.setMerchant_reference(merchantReference);
			
			paymentModel.setSHARequestPhrase(SHARequestPhrase);
			paymentModel.setSHAType(SHAType);
			
			//create signature string
			
String beforeSignatureString=	JWTUtils.createSignatureStringBySignaturePattern(paymentModel);
			 
			String DGtalSigHash=JWTUtils.createDGtalSignatureHashFromString(beforeSignatureString,SHAType);
			String DGtalSigHash2=JWTUtils.createDGtalSignatureHashFromString(beforeSignatureString,SHAType);
			 System.out.println("paymentModel.getSignature() sent: "+paymentModel.getSignature());
			 System.out.println("DGtalSigHash1: "+DGtalSigHash);		
			 System.out.println("DGtalSigHash2: "+DGtalSigHash2);	
			
			paymentModel.setSignature(DGtalSigHash);
			
			
			//save payment details
			
			//paymentRepository.saveBeforePaymentDetails();

			TELGPaymentDetail pgw=new TELGPaymentDetail(); 
			
			pgw.setAccess_code(paymentModel.getAccess_code());
			pgw.setCommand(paymentModel.getCommand());
			pgw.setCurrency_code(paymentModel.getCurrency());
			pgw.setEntity_email(paymentModel.getCustomerEmail());
			pgw.setLanguage(paymentModel.getLanguage());
			pgw.setLocal_payment_datetime(paymentModel.getLocalTime());
			pgw.setLogin_id(paymentModel.getLoginId());
			pgw.setMerchant_identifier(paymentModel.getMerchant_identifier());
			pgw.setMerchant_reference(paymentModel.getMerchant_reference());
			pgw.setOrder_desc(paymentModel.getOrderDescription());

			pgw.setPayment_amount(paymentModel.getAmount());
			pgw.setRequest_id(Integer.parseInt(paymentModel.getRequestId()));
			pgw.setSHAType(paymentModel.getSHAType());
			pgw.setSignature(paymentModel.getSignature());
			
			TELGPaymentDetail pgwR=	paymentRepository.save(pgw);
			
			if(  pgwR.getPayment_id() > 0) {
				
				paymentModel.setPaymentID(pgwR.getPayment_id());
				
				rm.setResponseCode(Constants.SUCCESS);
				rm.setResponseMessage(Constants.GENERAL_SUCCESS_MESSAGE);
				rm.setUserObject(paymentModel);
				return rm;
				
			}
			else {
				
				
				
				rm.setResponseCode(Constants.ERROR);
				rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE);
				 
				return rm;
			}
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		rm.setResponseCode(Constants.ERROR);
		rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE);
		
		return rm;
	}



	@Override
	public ResponseModel savePaymentParams(PaymentModel paymentModel) {
		// TODO Auto-generated method stub
	
		
ResponseModel rm=new ResponseModel();
		
		
		try {
			
			//validate gotten value
			 
			
     	    boolean validateState=true;
     	    
     	    
     	    //amount
     	    if(paymentModel.getAmount() != null && (paymentModel.getAmount().compareTo(new BigDecimal(0.00))==1)  ) {
     	     	validateState=validateState&&true;
     	         }
     	    else {
     	    	validateState=validateState&&false;
     	    	
     	    	rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Payment amount found invalid!");
     			return rm;
     	     }
     	   
     	    //currency
     	    
     	   if(paymentModel.getCurrency() != null && paymentModel.getCurrency().trim().length() == 3 &&  Utilities.checkIfValidCurrency(paymentModel.getCurrency().trim())  ) {
    	     	validateState=validateState&&true;
    	        }
    	    else {
    	    	validateState=validateState&&false;
    	    	
    	    	rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Selected currency found invalid!");
     			return rm;
    	     }
     	    
     	  //language
    	    
     	   if(paymentModel.getLanguage() != null && paymentModel.getLanguage().trim().length() > 0  ) {
    	     	validateState=validateState&&true;
    	     	  }
    	    else {
    	    	validateState=validateState&&false;
    	    	
    	    	rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Selected language found invalid!");
     			return rm;
    	     }
     	    
     	   
     	   
     	//customerEmail
   	    
     	   if(Utilities.isValidEmail(paymentModel.getCustomerEmail())  ) {
    	     	validateState=validateState&&true;
    	         }
    	    else {
    	    	validateState=validateState&&false;
    	    	
    	    	rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Customer email found invalid!");
     			return rm;
    	     }
     	  
     	    
     	   
     	   
     	   
     	//orderDescription
      	    
     	   if( paymentModel.getOrderDescription() != null && paymentModel.getOrderDescription().trim().length() >3 ) {
    	     	validateState=validateState&&true;
    	        }
    	    else {
    	    	validateState=validateState&&false;
    	    	
    	    	rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Order description found insufficient!");
     			return rm;
    	     }
     	   
     	   
     	//returnUrl
     	    
     	   if( paymentModel.getReturnUrl() != null && paymentModel.getReturnUrl().trim().length() >7 && Utilities.isValidURL(paymentModel.getReturnUrl().trim())) {
    	     	
     		   //dont allow non http
     		   if( Pattern.compile(Pattern.quote("http"), Pattern.CASE_INSENSITIVE).matcher(paymentModel.getReturnUrl().trim()).find()  ||  
     				  Pattern.compile(Pattern.quote("https"), Pattern.CASE_INSENSITIVE).matcher(paymentModel.getReturnUrl().trim()).find()   
     				   ) {
     			  validateState=validateState&&true;
     			   
     		   }
     		   else {
     			  validateState=validateState&&false;
     			  
     			 rm.setResponseCode(Constants.ERROR);
      			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Return URL found malformed (Do not forget to include http or https)!");
      			return rm;
     			   
     		   }
     		   
     		   
    	         }
    	    else {
    	    	validateState=validateState&&false;
    	    	
    	    	rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Return URL found invalid (Do not forget to include http or https)!");
     			return rm;
    	     }
     	   
     	   
     	   //loginId
     	  if( paymentModel.getLoginId() != null && paymentModel.getLoginId().trim().length() > 0  ) {
  	     	//is login id ok?
     		  boolean lidOK=userService.isUserLoginIdValid(paymentModel.getLoginId().trim());
     		  validateState=validateState&&lidOK;
  	         }
  	    else {
  	    	validateState=validateState&&false;
  	    	
  	    	rm.setResponseCode(Constants.ERROR);
 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Could not find the requested user!");
 			return rm;
  	     }
   	   
     	   
     	   
     	 //requestid
     	  if( paymentModel.getRequestId() != null && paymentModel.getRequestId().trim().length() > 0  ) {
  	     	//is requestid ok?
     		  boolean ridOK=userService.isRequestIdValid(paymentModel.getLoginId().trim(),paymentModel.getRequestId().trim());
     		 System.out.println("isRequestIdValid: "+ridOK);
     		 
     		 if(ridOK) {
     			validateState=validateState&&ridOK;
     			 
     		 }
     		 else {
     			rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Could not find the requested LG issue details!");
     			return rm;	 
     		 }
     	     }
  	    else {
  	    	validateState=validateState&&false;
  	    	
  	    	rm.setResponseCode(Constants.ERROR);
 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Request id seems to be invalid!");
 			return rm;
  	     }
   	   
     	 
    	   
      	  
    	  //responseMessage
     	  if( paymentModel.getResponseMessage() != null && paymentModel.getResponseMessage().trim().length() > 0  ) {
  	     	//is responseMessage ok?
     		  
     		  validateState=validateState&&true;
  	         }
  	    else {
  	    	validateState=validateState&&false;
  	    	
  	    	rm.setResponseCode(Constants.ERROR);
 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Could not find the response message!");
 			return rm;
  	     }
   	   
     	  
     	 //responseCode
     	  if( paymentModel.getResponseCode() != null && paymentModel.getResponseCode().trim().length() > 0  ) {
  	     	//is responseCode ok?
     		  if(Utilities.isIntOrReturnDefault(paymentModel.getResponseCode(), -1)<0) {
     			 validateState=validateState&&false;
       	    	
       	    	rm.setResponseCode(Constants.ERROR);
      			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Invalid response code received!");
      			return rm;
     		  }
     		  validateState=validateState&&true;
  	         }
  	    else {
  	    	validateState=validateState&&false;
  	    	
  	    	rm.setResponseCode(Constants.ERROR);
 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Could not find the response code!");
 			return rm;
  	     }
     	  
     	  
     	 
     	  
     	  
     	//status
    	  if( paymentModel.getStatus() != null && paymentModel.getStatus().trim().length() > 0  ) {
 	     	//is responseCode ok?
    		  if(Utilities.isIntOrReturnDefault(paymentModel.getStatus(), -1)<0) {
    			 validateState=validateState&&false;
      	    	
      	    	rm.setResponseCode(Constants.ERROR);
     			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Invalid status code received!");
     			return rm;
    		  }
    		  validateState=validateState&&true;
 	         }
 	    else {
 	    	validateState=validateState&&false;
 	    	
 	    	rm.setResponseCode(Constants.ERROR);
			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Could not find the status code!");
			return rm;
 	     }
    	  
     	  
     	  
     	  
     	  
     	  
     	  
			
			
			 	
			String accessCode=env.getProperty("ACCESS_CODE");
			String command=env.getProperty("COMMAND");
			String merchantIdentifier=env.getProperty("MERCHANT_IDENTIFIER");
			String merchantReference=env.getProperty("MERCHANT_REFERENCE");
			String signaturePrivate=env.getProperty("SIGNATURE_PRIVATE");
			
			String SHARequestPhrase=env.getProperty("SHA_Request_Phrase");
			String SHAType=env.getProperty("SHA_Type");
			
			
			
			/*  now validate secret data    */
			
			if(!accessCode.trim().equals(paymentModel.getAccess_code().trim())) {
				
				validateState=validateState&&false;
	  	    	
	  	    	rm.setResponseCode(Constants.ERROR);
	 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Access code seems to be invalid!");
	 			return rm;
				
			}
			
          if(!command.trim().equals(paymentModel.getCommand().trim())) {
				
				validateState=validateState&&false;
	  	    	
	  	    	rm.setResponseCode(Constants.ERROR);
	 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Command is invalid!");
	 			return rm;
				
			}
					
          if(!merchantIdentifier.trim().equals(paymentModel.getMerchant_identifier().trim())) {
				
				validateState=validateState&&false;
	  	    	
	  	    	rm.setResponseCode(Constants.ERROR);
	 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Merchant identifier is invalid!");
	 			return rm;
				
			}
          if(!merchantReference.trim().equals(paymentModel.getMerchant_reference().trim())) {
				
				validateState=validateState&&false;
	  	    	
	  	    	rm.setResponseCode(Constants.ERROR);
	 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Merchant reference is invalid!");
	 			return rm;
				
			}
			
          if(!SHARequestPhrase.trim().equals(paymentModel.getSHARequestPhrase().trim())) {
				
				validateState=validateState&&false;
	  	    	
	  	    	rm.setResponseCode(Constants.ERROR);
	 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" SHA request phrase is invalid!");
	 			return rm;
				
			}
			
          if(!SHAType.trim().equals(paymentModel.getSHAType().trim())) {
				
				validateState=validateState&&false;
	  	    	
	  	    	rm.setResponseCode(Constants.ERROR);
	 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" SHA type phrase is invalid!");
	 			return rm;
				
			}
			
			 
			
			

			if(!validateState) {
				
				rm.setResponseCode(Constants.ERROR);
	 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE);
	 			return rm;
				
			}
			
			
			
			
			
			
			//create signature string
			
            String beforeSignatureString=	JWTUtils.createSignatureStringBySignaturePattern(paymentModel);
			 
			String DGtalSigHash=JWTUtils.createDGtalSignatureHashFromString(beforeSignatureString,SHAType);
			String DGtalSigHash2=JWTUtils.createDGtalSignatureHashFromString(beforeSignatureString,SHAType);
			 System.out.println("paymentModel.getSignature(): "+paymentModel.getSignature());
			 System.out.println("DGtalSigHash1: "+DGtalSigHash);		
			 System.out.println("DGtalSigHash2: "+DGtalSigHash2);	
			 
			if(DGtalSigHash.equals(paymentModel.getSignature())) {
				
				//fetch from DB to further validate if there were really any request!!
				
			int isPaymentReqOK=	paymentRepository.fetchPaymentDetailsByPIDandHash(paymentModel.getPaymentID(),paymentModel.getSignature());
				
				if(isPaymentReqOK<1) {
					rm.setResponseCode(Constants.ERROR);
		 			rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" Sorry! We could not find any payment request with requested data!");
		 			return rm;
				}
				
				
				
				//save
				
	//save payment details
				
				//paymentRepository.saveBeforePaymentDetails();

				/*
				 * TELGPaymentDetail pgw=new TELGPaymentDetail();
				 * 
				 * pgw.setEsponseMessage(paymentModel.getResponseMessage());
				 * pgw.setResponseCode(paymentModel.getResponseCode());
				 * pgw.setPayment_id(paymentModel.getPaymentID());
				 * pgw.setPayment_status(paymentModel.getStatus());
				 * 
				 * 
				 * 
				 * 
				 * TELGPaymentDetail pgwR= pgw.(pgw);
				 */
				
				
			int affRows=	paymentRepository.savePaymentDetailsByPIDandHash(
						paymentModel.getResponseMessage(), 
						paymentModel.getStatus(), 
						paymentModel.getResponseCode(), 
						paymentModel.getPaymentID(), 
						paymentModel.getSignature());
				
				System.out.println("affRows: "+affRows);
				if(affRows >0) {
					

					rm.setResponseCode(Constants.SUCCESS);
					rm.setResponseMessage(Constants.PAYMENT_DETAILS_SAVED_SUCCESSFULLY);
 					return rm;
					
				}
				else {
					
					rm.setResponseCode(Constants.ERROR);
					rm.setResponseMessage(Constants.PAYMENT_DETAILS_SAVED_FAILED);
					 
					return rm;
					
				}
				
				
			}
			else {
				
				System.out.println("sig mismatch: ");
				
				rm.setResponseCode(Constants.ERROR);
				rm.setResponseMessage(Constants.GENERAL_ERROR_MESSAGE+" The signature provided seems to be invalid!");
					return rm;
			}
			  
			
			/*
			 * rm.setResponseCode(Constants.SUCCESS);
			 * rm.setResponseMessage(Constants.PAYMENT_DETAILS_SAVED_SUCCESSFULLY);
			 * rm.setUserObject(paymentModel); return rm;
			 */
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		rm.setResponseCode(Constants.ERROR);
		rm.setResponseMessage(Constants.PAYMENT_DETAILS_SAVED_FAILED);
		
		return rm;
		
		
		
		 
	}

	
	
	
	
	
	
	
	
	
	
}
