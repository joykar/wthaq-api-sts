package sa.wthaq.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import sa.wthaq.Entities.TBankmaster;
import sa.wthaq.Entities.TUserIbanMapping;
import sa.wthaq.models.Beneficiarydetail;
import sa.wthaq.models.LGRequestIssue;
import sa.wthaq.models.RegistrationPvtEntt;
import sa.wthaq.models.ResponseModel;

public interface BeneficiryService {
	
	public List<?> selectGovermentEntity();
	
	public ResponseModel checkEntityAvailability(String entityName);
	
	public ResponseModel saveGovermentEntity(Beneficiarydetail beneficiarydetail);

	List<?> selectGovermentUnregisteredBeneficiaryEntity();

	List<?> getLgPendingWithBeneficiaryId(String login);
	
	public ResponseModel changeOnlyStatusLgRequest(String requestId);
	
	public ResponseModel changeStatusWithLgRequestNTerms(String requestId,String termsNconditionMessage);
	
	public ResponseModel verifyStatusOfBeneficiary(String benName);

	public ResponseModel checkBenNameAvailability(String benName);

	ResponseModel changeRequestedStatusByBeneficiary(String requestId, String termsNconditionMessage);

	public ResponseModel getBeneficiaryUnverifiedEntityList(String adminId);
}
