package sa.wthaq;

public class Constants {
public static final int ERROR=0;
public static final int SUCCESS=1;
public static final int NOT_FOUND=2;
public static final String FILE_UPLOAD_ERROR="Sorry! Could not upload your file successfully.";
public static final String FILE_UPLOAD_SUCCESS="Your file has been uploaded successfully.";
public static final String REGISTRATION_ERROR="Sorry! Registration process was not completed successfully.";
public static final String REGISTRATION_SUCCESS="Registration process was completed successfully.";
public static final String REGISTRATION_SUCCESS_PENDING_VERIFICATION="Your documents and details are pending for verification. You will be notified through the communication details you provided.";
public static final String FORGOT_PASSWORD_EMAIL_SUCCESS="An email with a new password has been sent to you";
public static final String FORGOT_PASSWORD_EMAIL_ERROR="Your documents and details are pending for verification. You will be notified through the communication details you provided.";
public static final String PROFILE_SETTINGS_LOADED_SUCCESS="Profile settings loaded successfully.";
public static final String PROFILE_SETTINGS_LOADED_ERROR="Sorry! Could not load profile settings.";
public static final String PROFILE_SETTINGS_UPDATED_ERROR="Sorry! Could not update profile settings.";
public static final String PROFILE_SETTINGS_UPDATED_SUCCESS="Profile settings successfully updated.";
public static final int CRN_ACTIVE_CODE=140;
public static final String CRN_ACTIVE="Your CRN is active";
public static final int CRN_CANCELLED_CODE=141;
public static final String CRN_CANCELLED="Your CRN is canceled";
public static final int CRN_SUSPENDED_CODE=142;
public static final String CRN_SUSPENDED="Your CRN is suspended";
public static final int CRN_EXPIRED_CODE=143;
public static final String CRN_EXPIRED="Your CRN is expired";


public static final String LOGIN_ERROR="Sorry! Username or Password does not match.";
public static final String LOGIN_SUCCESS="You are logged in.";

public static final String CC_PENDING="Sorry! Your CC verification is pending.";
public static final String CC_SUCCESS="Your CC verification has been completed successfully.";
public static final String CC_FAILED="Sorry! Your CC verification failed.";
public static final String BANK_VERIFIED="Your bank details has been verified successfully.";
public static final String BANK_NOT_VERIFIED="Your bank details could not be verified successfully.";
public static final String BANK_PENDING="Sorry! Your bank verification is pending."; 


public static final int CC_PENDING_CODE=100;
public static final int CC_SUCCESS_CODE=101;
public static final int CC_FAILED_CODE=102;
public static final int BANK_VERIFIED_CODE=104;
public static final int BANK_NOT_VERIFIED_CODE=105; 
public static final int BANK_PENDING_CODE=106; 



public static final String USERNAME_AVAILABLE="This username is available."; 
public static final String USERNAME_UNAVAILABLE="Sorry! This username is not available.";
public static final String USERNAME_EMPTY="Please Provide useranme.";


public static final String CRN_AVAILABLE="This CRN is available."; 
public static final String CRN_UNAVAILABLE="Sorry! This CRN is not available.";

public static final String CRN_VALID="This CRN is Valid."; 
public static final String CRN_NOT_VALID="Sorry! This CRN is not Valid.";

public static final String ROLE_SUPPLIER="Supplier";
public static final String ROLE_BENEFICIARY="Beneficiary";


public static final String ACTIVE="ACTIVE";
public static final String INACTIVE="INACTIVE";
public static final String VERIFIED = "Verified";

public static final String LG_ISSUE_SUCCESS="LgIssue Request Sent successfully";
public static final String LG_ISSUE_ERROR="Could not sent LgIssue..Error Occurred";

public static final String BEN_SUCCESS="Registration completed successfully";
public static final String BEN_ERROR="Transacction error";




public static final int MAKE_LOGOUT = 0;
public static final int MAKE_NOLOGOUT = 1;
public static final String SESSION_CREATION_ERROR = "Sorry! We could not create your session. Please refresh the page and try again.";
public static final String SESSION_EXPIRED = "The current session has been expired! Please login again.";
public static final String SESSION_OK = "You can continue with this session";
public static final String SAMA = "";
public static final String NON_SAMA = "Your Lg Request is pending.";
public static final String SAMA_TYPE = "SAMA";
public static final String NON_SAMA_TYPE = "Non SAMA";
public static final String NS_PENDING = "NS_PENDING";
public static final String PENDING = "PENDING";
public static final String BANK_VERIFIED_CON = "BANK_VERIFIED";
public static final String YES = "Y";
public static final String RESTRICTED = "R";


/* JK  */

public static final int GCC_PENDING_CODE=107;
public static final int G_CC_SUCCESS=108;
public static final int ADMIN_LOGIN=109;



 public static final String TABADUL_NEW_USER_CREATION_SUCCESS="Successfully created new Tabadul user!";
public static final String TABADUL_NEW_USER_CREATION_FAILURE="Sorry! Could not create Tabadul User!";
public static final String ADMIN_LOGIN_SUCCESS = "You have successfully logged in.";
public static final String UNKNOWN_ROLE_DESCRIPTION = "Sorry! We could not determine the role you are privileged to.";
public static final String UNKNOWN_STATUS = "Sorry! We could not determine your current status.";
public static final String ROLE_UNDEFINED = "The specified role does not exists!";
public static final String NS_CHANGE = "NS_CHANGE";
public static final String NS_ACCEPT = "NS_ACCEPT";
public static final String CANCELLED = "CANCELLED";

public static final int NS_CHANGE_CODE=121;
public static final int PENDING_CODE=120;
public static final int NS_ACCEPT_CODE = 122;
public static final int CANCELLED_CODE = 123;
public static final int NS_PENDING_CODE = 124;


 
public static final String GENERAL_SUCCESS_MESSAGE = "Request processed successfully!";
public static final String GENERAL_ERROR_MESSAGE = "An error occurred while processing your request!";



public static final String[] ACCEPTED_CURRENCY=new String[] {};
public static final String PAYMENT_DETAILS_SAVED_SUCCESSFULLY = "Payment details saved successfully!";
public static final String PAYMENT_DETAILS_SAVED_FAILED = "Failed to save payment details!";
public static final String OTP_REQUEST_SUCCESS = "OTP sent successfully!!";

/*  JK  */

}
