package sa.wthaq;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;

import sa.wthaq.bankSoapClient.BankClient;
import sa.wthaq.bankSoapClient.BankClientReq;
import sa.wthaq.bankSoapClient.MciRequestClient;

///@Configuration
public class WethaqClientConfig {
	
	
	
	
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		// this package must match the package in the  specified in
		// pom.xml
		marshaller.setContextPath("sa.wthaq.webservices");
		return marshaller;
	}

	@Bean
	public BankClient bankClient(Jaxb2Marshaller marshaller) {
		BankClient client = new BankClient();
		client.setDefaultUri("http://localhost:8090/eLG-WebServicesRequest/services/ExternalSoapRequestImpl");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}
	
	@Bean
	public BankClientReq bankReqClient(Jaxb2Marshaller marshaller) {
		BankClientReq client = new BankClientReq();
		client.setDefaultUri("http://localhost:8090/eLG-WebServicesRequest/services/ExternalSoapRequestImpl");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}
	
	@Bean
	public MciRequestClient getRequestData(Jaxb2Marshaller marshaller) {
		MciRequestClient client = new MciRequestClient();
		client.setDefaultUri("http://localhost:8090/eLG-WebServicesRequest/services/ExternalSoapRequestImpl");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}
	
	@Bean
    public WebServiceTemplate webServiceTemplate(Jaxb2Marshaller marshaller) {
		WebServiceTemplate client = new WebServiceTemplate();
        client.setDefaultUri("http://localhost:8090/eLG-WebServicesRequest/services/ExternalSoapRequestImpl");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
      //  client.setInterceptors(interceptors);
        return client;
    }

	
	/*@Bean
    public PayloadValidatingInterceptor payloadValidatingInterceptor(Jaxb2Marshaller marshaller) {
		WebServiceTemplate client = new WebServiceTemplate();
        client.setDefaultUri("http://localhost:8090/eLG-WebServicesRequest/services/ExternalSoapRequestImpl");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        client.setInterceptors(interceptors);
        return client;
    }
    https://blog.espenberntsen.net/tag/webservicetemplate/
    *<bean id="payloadValidatingInterceptor"
    class="org.springframework.ws.client.support.interceptor.PayloadValidatingInterceptor">
    <property name="schema"
        value="file:WebContent/WEB-INF/schemas/account-balance-service.xsd" />
    <property name="validateRequest" value="true" />
    <property name="validateResponse" value="true" />
</bean>
    *
    */
}
