package sa.wthaq.bankSoapClient;

 
	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;

	import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

//import com.wethaq.elog.cxf.webservices_1.VerifyIbanDetailResponse;
import sa.wthaq.webservices.VerifyIbanDetail;
import sa.wthaq.webservices.VerifyIbanDetailResponse;

	public class BankClient extends WebServiceGatewaySupport {

		private static final Logger log = LoggerFactory.getLogger(BankClient.class);

		public VerifyIbanDetailResponse getBankVerifyByIban(String bi) {
			VerifyIbanDetail request = new VerifyIbanDetail();
			//request.setMovieId(id);
			request.setBi(bi);

			log.info("Requesting Movie By id= " + bi);
			return (VerifyIbanDetailResponse) getWebServiceTemplate().marshalSendAndReceive(request);

		}

	}
