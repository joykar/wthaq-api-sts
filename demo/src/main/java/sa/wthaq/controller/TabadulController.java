package sa.wthaq.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import sa.wthaq.request.BankInfo;
import sa.wthaq.request.LgReturnInfo;
import sa.wthaq.request.ProfileAdmin;
import sa.wthaq.request.UserDetail;
import sa.wthaq.webservices.GetRequestData;
import sa.wthaq.webservices.GetRequestDataResponse;
import sa.wthaq.webservices.VerificationAdminDetails;
import sa.wthaq.webservices.VerificationAdminDetailsResponse;
import sa.wthaq.webservices.VerifyIbanDetail;
import sa.wthaq.webservices.VerifyIbanDetailResponse;
import sa.wthaq.Constants;
import sa.wthaq.Communication.SendEmail;
import sa.wthaq.DTO.EntityModel;
import sa.wthaq.DTO.LgDataDto;
import sa.wthaq.DTO.LgIssuedDataDTO;
import sa.wthaq.DTO.LgListWithChangesAcceptSupplier;
import sa.wthaq.DTO.LgPendingListDTO;
import sa.wthaq.DTO.MCIDataDto;
import sa.wthaq.DTO.MciRequestData;
import sa.wthaq.Entities.TAckfrombank;
import sa.wthaq.Entities.TBankmaster;
import sa.wthaq.Entities.TBeneficiarydetail;
import sa.wthaq.Entities.TIbanAcknowledgement;
import sa.wthaq.Entities.TLgrequestissue;
import sa.wthaq.Entities.TServicerequest;
import sa.wthaq.Entities.TSupplierdetail;
import sa.wthaq.Entities.TUsermaster;
import sa.wthaq.Entities.T_WriteUpType;
import sa.wthaq.JPA.Repository.BankMasterRepository;
import sa.wthaq.JPA.Repository.BeneficiaryMasterRepository;
import sa.wthaq.JPA.Repository.SupplierMasterRepository;
import sa.wthaq.JPA.Repository.TAckfrombankRepository;
import sa.wthaq.JPA.Repository.TIbanAcknowledgementRepository;
import sa.wthaq.JPA.Repository.TLgRepository;
import sa.wthaq.JPA.Repository.TLgrequestissueRepo;
import sa.wthaq.JPA.Repository.TServicerequestRepo;
import sa.wthaq.JPA.Repository.UserIbanRepo;
import sa.wthaq.JPA.Repository.UserMasterRepository;
import sa.wthaq.endpoint.SoapClient;
import sa.wthaq.models.Beneficiarydetail;
import sa.wthaq.models.LGRequestIssue;
import sa.wthaq.models.LoginModel;
import sa.wthaq.models.PaymentModel;
import sa.wthaq.models.RegistrationPvtEntt;
import sa.wthaq.models.ResponseModel;
import sa.wthaq.models.StatusInfo;
import sa.wthaq.models.TabadulUser;
import sa.wthaq.request.BankInfo;
import sa.wthaq.request.ProfileAdmin;
import sa.wthaq.service.BeneficiryService;
import sa.wthaq.service.FileStorageService;
import sa.wthaq.service.IssueDataService;
import sa.wthaq.service.MockService;
import sa.wthaq.service.PaymentService;
import sa.wthaq.service.SupplierService;
import sa.wthaq.service.UserService;
import sa.wthaq.webservices.GetRequestData;
import sa.wthaq.webservices.GetRequestDataResponse;

/*import sa.wthaq.request.BankInfo;
import sa.wthaq.request.ProfileAdmin;
import sa.wthaq.webservices.GetRequestData;
import sa.wthaq.webservices.GetRequestDataResponse;
import sa.wthaq.webservices.VerificationAdminDetails;
import sa.wthaq.webservices.VerificationAdminDetailsResponse;
import sa.wthaq.webservices.VerifyIbanDetail;
import sa.wthaq.webservices.VerifyIbanDetailResponse;*/

//@Configuration
//@Component
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
//@RequestMapping("/")
public class TabadulController {

	private static final Logger logger = LoggerFactory.getLogger(TabadulController.class);

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private DataSource elgDS;

	@Autowired
	private UserMasterRepository userRepositoryService;

	@Autowired
	private SupplierMasterRepository s_repository;

	@Autowired
	private UserService userService;

	@Autowired
	private SupplierService supplierService;

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	@Autowired
	private Environment env;

	@Autowired
	private SoapClient soapClient;

	@Autowired
	private IssueDataService issueDataService;

	@Autowired
	private BeneficiryService beneficiryService;

	@Autowired
	private TIbanAcknowledgementRepository ibanRepository;

	@Autowired
	private TAckfrombankRepository tAckfrombankRepository;

	@Autowired
	TLgrequestissueRepo tLgrequestissueRepo;

	@Autowired
	private BankMasterRepository bankMasterRepository;

	@Autowired
	private BeneficiaryMasterRepository beneficiaryMasterRepository;

	@Autowired
	private UserIbanRepo usrIbanRepo;

	@Autowired
	private TLgRepository tlgRepo;

	@Autowired
	private TServicerequestRepo tserviceReqRepo;

	@Autowired
	private PaymentService paymentService;
	
	@Autowired
	private MockService mckService;

//	@Autowired
//    private DefaultTokenServices tokenServices;

	@GetMapping("/IsLogin")
	public ResponseModel IsLogin(HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();

		/*********************************/

		rm = userService.checkSessionStatus(req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			return rm;
		}
		return rm;

	}

	@RequestMapping(value = "/logout", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseModel Logout(@RequestParam(value = "loginId") String loginId, HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();

		/*********************************/

		rm = userService.logout(loginId, req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			return rm;
		}
		return rm;

	}

	@RequestMapping(value = "/sendM", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE/*
																												 * ,
																												 * consumes
																												 * =
																												 * MediaType
																												 * .
																												 * MULTIPART_FORM_DATA_VALUE
																												 */)
	@ResponseBody
	public void sendMail() {

		try {
			new SendEmail().SendRegistrationFirstPhaseEmail("priyankag4j@gmail.com", "Priyanka", "joydip", "pppp@123");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// logger.info("Failed to send email: getOfficeEMail: "+re.getOfficeEMail()+",
			// getName: " +re.getName()+"getUserId: " +re.getUserId());
			e.printStackTrace();
		}
	}
	/* SOAP request/response control */

	// @PostMapping("/mciData")

	@RequestMapping(value = "/mciData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public UserDetail item(@ModelAttribute GetRequestData itemRequest, HttpServletRequest req) throws SOAPException,
			JAXBException, SAXException, IOException, ParserConfigurationException, JSONException {

		logger.info("MCI data: " + itemRequest.getCrn());

		HttpHeaders hh = new HttpHeaders();
		hh.set("Content-Type", "application/json");
		hh.set("SOAPAction", "");
		hh.set("X-IBM-Client-Id", "83c16518-aa61-4c13-8e39-05b3f84b1e43");

		JSONObject obj = new JSONObject();
		obj.put("crn", itemRequest.getCrn());
		obj.put("mno", itemRequest.getMno());

		// HttpEntity<JSONObject> he=new HttpEntity<JSONObject>(obj,hh);
		HttpEntity<String> request = new HttpEntity<String>(obj.toString(), hh);
		String str = new RestTemplate().postForObject(
				"https://service.eu.apiconnect.ibmcloud.com/gws/apigateway/api/0959c418adcabe154e1169030ca09e2a10df0821ecc865b286f13a19938d6ca6/dzGa7r/RegisterUser/getmcidetails",
				request, String.class);

		System.out.println(str);
		JSONObject jsonObject1 = new JSONObject(str);
		JSONObject jsonObject2 = new JSONObject(jsonObject1.getString("responseData"));
		JSONObject jsonObject3 = new JSONObject(jsonObject2.getString("payload"));
		JSONObject jsonObject4 = new JSONObject(jsonObject3.getString("response"));
		JSONObject jsonObject5 = new JSONObject(jsonObject4.getString("SUCCESS"));
		JSONObject jsonObject6 = new JSONObject(jsonObject5.getString("return"));
		System.out.println(jsonObject6.getString("address1"));
		UserDetail us = new UserDetail();
		us.setAddress1(jsonObject6.getString("address1"));
		us.setAddress2(jsonObject6.getString("address2"));
		us.setContactNo(jsonObject6.getString("contactNo"));
		us.setCrNumber(jsonObject6.getString("crNumber"));
		us.setEmailAddress("ipsita.beas19@gmail.com");
		us.setEntityName(jsonObject6.getString("entityName"));

		return us;
		// return soapClient.getInfo(itemRequest);
	}

	@RequestMapping(value = "/chamberOfCommerceAcknowledge", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = {
			"multipart/form-data" })
	@ResponseBody
	public ProfileAdmin chamberOfCommerceAcknowledge(@RequestParam("rep_Id") String rep_Id,
			@RequestParam("rep_name") String rep_name, @RequestParam("rep_email") String rep_email,
			@RequestParam("rep_mobile_No") String rep_mobile_No, @RequestParam("rep_name_ar") String rep_name_ar,
			@RequestParam("user_Id") String user_Id, HttpServletRequest req/* ProfileAdmin itemRequest */)
			throws SAXException, IOException, ParserConfigurationException, JSONException, ParseException {
		VerificationAdminDetails adminDetails = new VerificationAdminDetails();
		ProfileAdmin pf = new ProfileAdmin();
		pf.setRepEmail(rep_email);
		// System.out.println("itemRequest.getRepEmail()"+itemRequest.getRepEmail());
		pf.setRepId(rep_name);
		pf.setRepMobileNo(rep_mobile_No);
		pf.setRepName(rep_name);
		pf.setRepNameAr(rep_name_ar);
		pf.setUserId(user_Id);

		adminDetails.setAd(pf);

		HttpHeaders hh = new HttpHeaders();
		hh.set("Content-Type", "application/json");
		hh.set("SOAPAction", "");
		hh.set("X-IBM-Client-Id", "c2003472-d3c6-458a-a902-2ee7082a61b2");

		JSONObject obj = new JSONObject();
		obj.put("rep_email", rep_email);
		obj.put("rep_name", rep_name);
		obj.put("rep_mobile_No", rep_mobile_No);
		obj.put("rep_name_ar", rep_name_ar);
		obj.put("user_Id", user_Id);
		obj.put("rep_Id", rep_name);

		// HttpEntity<JSONObject> he=new HttpEntity<JSONObject>(obj,hh);
		HttpEntity<String> request = new HttpEntity<String>(obj.toString(), hh);
		String str = new RestTemplate().postForObject(
				"https://service.eu.apiconnect.ibmcloud.com/gws/apigateway/api/0959c418adcabe154e1169030ca09e2a10df0821ecc865b286f13a19938d6ca6/ih613p/repDetails/getAcknowledgeMessage",
				request, String.class);

		System.out.println(str);
		// JSONParser parser = new JSONParser();
		JSONObject jsonObject = new JSONObject(str);
		JSONObject jsonObject1 = new JSONObject(jsonObject.getString("message"));
		// JSONObject jsonObject2=new JSONObject(jsonObject1.getString("responseData"));
		JSONObject jsonObject3 = new JSONObject(jsonObject1.getString("payload"));
		JSONObject jsonObject4 = new JSONObject(jsonObject3.getString("response"));
		JSONObject jsonObject5 = new JSONObject(jsonObject4.getString("SUCCESS"));
		System.out.println(jsonObject5.getString("return"));
		ProfileAdmin info = new ProfileAdmin();
		info.setRepMsg(jsonObject5.getString("return"));
		return info;
	}

	@RequestMapping(value = "/ibanVerify", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = {
			"multipart/form-data" })
	@ResponseBody
	public ResponseModel item(@ModelAttribute VerifyIbanDetail itemRequest, HttpServletRequest req)
			throws SAXException, IOException, ParserConfigurationException, JSONException, ParseException {
		ResponseModel rm = new ResponseModel();
		try {
			System.out.println("itemRequest.getUserId()" + itemRequest.getUserId());
			TBankmaster bnk = bankMasterRepository.findByBankName(itemRequest.getBankName());
			System.out.println("bnk" + bnk.getBankCode());
			int i = s_repository.saveSupplierIban(itemRequest.getUserId(), itemRequest.getBi(), bnk.getBankCode());
			System.out.println("itemRequest.getUserId()" + itemRequest.getUserId());
			userRepositoryService.setBankVerificationStatus("BANK_PENDING", itemRequest.getUserId());
			System.out.println("i val" + i);
		} catch (Exception e) {
			e.printStackTrace();

		}

		HttpHeaders hh = new HttpHeaders();
		hh.set("Content-Type", "application/json");
		hh.set("SOAPAction", "");
		hh.set("X-IBM-Client-Id", "df1429da-7b78-451e-b373-f83fd6625c62");

		JSONObject obj = new JSONObject();
		obj.put("bi", itemRequest.getBi());
		obj.put("bankName", itemRequest.getBankName());
		obj.put("userId", itemRequest.getUserId());

		// HttpEntity<JSONObject> he=new HttpEntity<JSONObject>(obj,hh);
		HttpEntity<String> request = new HttpEntity<String>(obj.toString(), hh);
		String str = new RestTemplate().postForObject(
				"https://service.eu.apiconnect.ibmcloud.com/gws/apigateway/api/0959c418adcabe154e1169030ca09e2a10df0821ecc865b286f13a19938d6ca6/atPy1L/getUserDetail/getBankAckMsgAfterReqSend",
				request, String.class);

		System.out.println(str);
		// JSONParser parser = new JSONParser();

		JSONObject jsonObject = new JSONObject(str);
		JSONObject jsonObject1 = new JSONObject(jsonObject.getString("respMsg"));
		// JSONObject jsonObject2=new JSONObject(jsonObject1.getString("responseData"));
		JSONObject jsonObject3 = new JSONObject(jsonObject1.getString("payload"));
		JSONObject jsonObject4 = new JSONObject(jsonObject3.getString("response"));
		JSONObject jsonObject5 = new JSONObject(jsonObject4.getString("SUCCESS"));
		System.out.println(jsonObject5.getString("return"));
		String st = jsonObject5.getString("return");

		System.out.println("listOfBranches" + jsonObject5);

		TIbanAcknowledgement acknowledgement = new TIbanAcknowledgement();
		acknowledgement.setAckDate(new Timestamp(new Date().getTime()));
		acknowledgement.setBankmsgId("BOO25" + new Random().nextInt());
		acknowledgement.setLoginId(itemRequest.getUserId());
		System.out.println("TimeStamp" + new Timestamp(new Date().getTime()));
		acknowledgement.setBranchName(itemRequest.getBankName());
		acknowledgement.setMessageDesc("Acknowledge Message");
		acknowledgement.setMessageDescAr(st);
		ibanRepository.save(acknowledgement);

		BankInfo info = new BankInfo();
		info.setMsg(jsonObject5.getString("return"));

		rm.setUserObject(info);
		rm.setResponseCode(Constants.SUCCESS);
		return rm;
		// return soapClient.getAcknowledgeMessageFromBank(itemRequest);
	}

	@RequestMapping(value = "/submit_cc_verification", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseModel submitCCVerification() {
		ResponseModel rm = new ResponseModel();
		return rm;
	}

	@RequestMapping(value = "/forgotPassword", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseModel forgotPassword(@RequestParam String userIdForgotPass, HttpServletRequest req) {

		ResponseModel um = userService.forgotPassword(userIdForgotPass);

		return um;
	}

	@RequestMapping(value = "/profileInfo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseModel profileInfo(@RequestParam String userId, HttpServletRequest req) {

		ResponseModel rm = new ResponseModel();

		/*********************************/

		rm = userService.checkSessionStatus(req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			return rm;
		}

		/**************************************/

		rm = userService.profileInfo(userId);
		return rm;
	}

	@RequestMapping(value = "/updateProfileInfo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseModel updateProfileInfo(@ModelAttribute RegistrationPvtEntt rg, HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();
		/*********************************/

		rm = userService.checkSessionStatus(req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			return rm;
		}

		/**************************************/
		ResponseModel um = userService.updateProfileInfo(rg, rg.getCrFileContent(), rg.getRniFileContent(),
				rg.getLalFileContent(), rg.getLaglFileContent());

		return um;
	}

	@RequestMapping(value = "/getHi", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	@ResponseBody
	public String getDateAndTime() {
		System.out.println("dhukeche");
		System.out.println("dhukeche " + userService.findAll().size());
		return "Hi";
	}

	@RequestMapping(value = "/getBankDetailsAfterVerify", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BankInfo getBankDetailsAfterVerify(@RequestParam("bankCode") String bankCode,

			@RequestParam("userId") String loginId, @RequestParam("msg") String msg,
			@RequestParam("ibanNo") String ibanNo) {

		BankInfo bankInfo = new BankInfo();
		bankInfo.setBankCode(bankCode);
		bankInfo.setUserId(loginId);
		// bankInfo.setBranchName(branchName);
		bankInfo.setIbanNo(ibanNo);
		bankInfo.setMsg(msg);
		// System.out.println("itemRequest"+bankInfo.getBankCode());

		try {
			if (msg.equalsIgnoreCase(Constants.VERIFIED)) {
				System.out.println("inside BANK_VERIFIEDtlg");

				s_repository.setCCVerificationStatusOnSupplier(env.getProperty("BANK_VERIFIED"), loginId);
				userRepositoryService.setBankVerificationStatus(env.getProperty("BANK_VERIFIED"), loginId);
				usrIbanRepo.updateStatus(Constants.ACTIVE, loginId, ibanNo);
				// userRepositoryService.saveSupplierIBAN(loginId, ibanNo, bankCode);
			} else {

				s_repository.setCCVerificationStatusOnSupplier(env.getProperty("BANK_NOT_VERIFIED"), loginId);
				userRepositoryService.setBankVerificationStatus(env.getProperty("BANK_NOT_VERIFIED"), loginId);
			}
			// BankInfo bankInfo = new BankInfo();
			return bankInfo;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/getCCDetailsAfterVerify", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ProfileAdmin getCCDetailsAfterVerify(@RequestParam("rep_Id") String rep_Id,
			@RequestParam("rep_email") String rep_email, @RequestParam("rep_mobile_No") String rep_mobile_No,
			@RequestParam("rep_msg") String rep_msg, @RequestParam("rep_name") String rep_name,
			@RequestParam("rep_name_ar") String rep_name_ar, @RequestParam("user_Id") String user_Id,
			@RequestParam("rep_status") String rep_status) {
		ProfileAdmin admin = new ProfileAdmin();
		admin.setRepId(rep_Id);
		admin.setRepEmail(env.getProperty("emailID"));
		admin.setRepMobileNo(rep_mobile_No);
		admin.setRepMsg(rep_msg);
		admin.setRepName(rep_name);
		admin.setRepNameAr(rep_name_ar);
		admin.setRepStatus(true);
		admin.setUserId(user_Id);
		System.out.println("usrid" + user_Id);

		try {
			if (user_Id != "") {
				// userRepositoryService.setCCVerificationStatus("CC_SUCCESS",user_Id);
				s_repository.setCCVerificationStatusOnSupplier("CC_SUCCESS", user_Id);
				userRepositoryService.setCCVerificationStatus("CC_SUCCESS", user_Id);

				// userRepositoryService.saveSupplierIBAN(loginId, ibanNo);
			} else {
				s_repository.setCCVerificationStatusOnSupplier("CC_FAILED", user_Id);
				userRepositoryService.setBankVerificationStatus("CC_FAILED", user_Id);
			}
			// BankInfo bankInfo = new BankInfo();
			return admin;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	/*
	 * @GetMapping(value = "/") public String getLandingPage() {
	 * System.out.println("dhukeche Home");
	 * 
	 * return "Home"; }
	 */

	@RequestMapping(value = "/CheckValidLoginId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseModel checkValidLoginId(@RequestParam(required = false) String tempLoginId, HttpServletRequest req) {

		return userService.checkValidLoginId(tempLoginId);

	}

	@RequestMapping(value = "/LogInUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseModel check_login_details(@ModelAttribute LoginModel lm, HttpServletRequest req) {
		logger.info("LogInUser: " + lm.getUserId());

		ResponseModel rm = userService.loginUser(lm, req);
		logger.info(rm.getResponseMessage());
		return rm;
	}

	@RequestMapping(value = "/CheckSupplierCrn", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseModel check_supplier_crn(@RequestParam(required = false) String crnno, HttpServletRequest req) {
		logger.info("CheckSupplierCrn: " + crnno);

		ResponseModel rm = new ResponseModel();
		if (crnno != null) {
			System.out.println("crnno" + crnno);
			List<TSupplierdetail> sbcrn = s_repository.findSupplierCRNByCRN(new BigDecimal(crnno));

			if (sbcrn == null) {
				// no record
				rm.setResponseCode(Constants.SUCCESS);
				rm.setResponseMessage(Constants.CRN_AVAILABLE);
			} else {

				if (sbcrn.size() > 0) {
					// got it
					rm.setResponseCode(Constants.ERROR);
					rm.setResponseMessage(Constants.CRN_UNAVAILABLE);

				} else {
					rm.setResponseCode(Constants.SUCCESS);
					rm.setResponseMessage(Constants.CRN_AVAILABLE);

				}

			}
		} else {
			// e.printStackTrace();
			rm.setResponseCode(Constants.ERROR);
			rm.setResponseMessage("Please select correct crnno");

		}

		logger.info(rm.getResponseMessage());
		return rm;
	}

	@RequestMapping(value = "/user-list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<TUsermaster> get_user_list(HttpServletRequest req) {
		logger.info("get_user_list");
		return userService.findAll();
	}

	@RequestMapping(value = "/save-user", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseModel save_user(@ModelAttribute RegistrationPvtEntt re
	/*
	 * @RequestParam(required = true) String userId,
	 * 
	 * @RequestParam(required = false) String password,
	 * 
	 * @RequestParam(required = false) String otp,
	 * 
	 * @RequestParam(required = true) String mobileNo,
	 * 
	 * @RequestParam(required = true) String commRegNo,
	 * 
	 * @RequestParam(required = true) String EntityName,
	 * 
	 * @RequestParam(required = true) String ContactInformation,
	 * 
	 * @RequestParam(required = true) String Address1,
	 * 
	 * @RequestParam(required = true) String Address2,
	 * 
	 * @RequestParam(required = false) String State,
	 * 
	 * @RequestParam(required = false) String PINCode,
	 * 
	 * @RequestParam(required = false) String Country,
	 * 
	 * @RequestParam(required = false) String IBANNumber,
	 * 
	 * @RequestParam(required = false) String BankName,
	 * 
	 * @RequestParam(required = false) String RepresentativeName,
	 * 
	 * @RequestParam(required = false) String RepresentativeID,
	 * 
	 * @RequestParam(required = true) String OfficeEMail,
	 * 
	 * @RequestParam(required = true) String OfficeMobileNumber,
	 * 
	 * @RequestParam(required = true) String name,
	 * 
	 * @RequestParam(required = true) String userName,
	 * 
	 * @RequestParam(required = false) String Name,
	 * 
	 * @RequestParam(required = true) String Email,
	 * 
	 * @RequestParam(required = true) String NationalId,
	 * 
	 * @RequestParam(required = false) String Mobile,
	 * 
	 * @RequestParam(required = true) String commRegNoMCI,
	 */

	/* @RequestBody RegistrationPvtEntt re, */
	/*
	 * @RequestParam(name="crFile",required = false) MultipartFile crFile,
	 * 
	 * @RequestParam(name="rniFile",required = false) MultipartFile rniFile,
	 * 
	 * @RequestParam(name="lalFile",required = false) MultipartFile lalFile,
	 * 
	 * @RequestParam(name="laglFile",required = false) MultipartFile laglFile
	 */
			, HttpServletRequest req) {

		ResponseModel rm = new ResponseModel();
		logger.info("save-user");

		/*
		 * RegistrationPvtEntt re=new RegistrationPvtEntt();
		 * 
		 * re.setAddress1(Address1); re.setAddress2(Address2); re.setMobileNo(mobileNo);
		 * re.setBankName(BankName); re.setCommRegNo(commRegNo);
		 * re.setContactInformation(ContactInformation); re.setCountry(Country);
		 * re.setEntityName(EntityName); re.setIBANNumber(IBANNumber); re.setName(name);
		 * re.setUserName(userName); re.setUserId(userId); re.setState(State);
		 * re.setRepresentativeName(RepresentativeName);
		 * re.setRepresentativeID(RepresentativeID); re.setPINCode(PINCode);
		 * re.setPassword(password); re.setOtp(otp);
		 * re.setOfficeMobileNumber(OfficeMobileNumber); re.setOfficeEMail(OfficeEMail);
		 */

		logger.info("save-user: " + re.getUserId());

		try {

			rm = supplierService.registerSupplier(re, re.getCrFileContent(), re.getRniFileContent(),
					re.getLalFileContent(), re.getLaglFileContent());

			if (rm.getResponseCode() == Constants.SUCCESS) {

				// soapClient.getInfo(itemRequest);

				return rm;
			} else {

				rm.setResponseMessage(Constants.REGISTRATION_ERROR);
				rm.setResponseCode(Constants.ERROR);

				return rm;

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			rm.setResponseMessage(Constants.REGISTRATION_ERROR);
			rm.setResponseCode(Constants.ERROR);
			return rm;

		}

	}

	@RequestMapping(value = "/get-supplier-list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<TSupplierdetail> get_supplier_list(HttpServletRequest req) {
		logger.info("get-supplier-list");
		return supplierService.findSuppliers();
	}

	@RequestMapping(value = "/EntityName-list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Object[]> get_supplier_id_name_list(HttpServletRequest req) {
		logger.info("get-supplier-id-name-list");
		return supplierService.findSuppliersIdAndName();
	}

	@GetMapping("/selectIBAN/{loginid}")
	public List<?> selectIBAN(@PathVariable String loginid/* , HttpServletRequest req */) {

		ResponseModel rm = new ResponseModel();
		/*********************************/

		/*
		 * rm= userService.checkSessionStatus(req);
		 * if(rm.getLogout()==Constants.MAKE_LOGOUT) { return rm; }
		 */

		/**************************************/

		List<?> res = issueDataService.selectIBAN(loginid);
		// rm.setUserObject(res);
		return res;
	}

	@GetMapping("/selectLGType")
	public ResponseEntity<?> selectLGType(HttpServletRequest req) {
		List<?> res = issueDataService.selectLGType();
		if (res == null || res.size() <= 0)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(res);
	}

	@GetMapping("/selectCurrency")
	public ResponseEntity<?> selectCurrency(HttpServletRequest req) {
		List<?> res = issueDataService.selectCurrency();
		if (res == null || res.size() <= 0)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(res);
	}

	@PostMapping("/lgIssueDataSubmit")
	public ResponseModel lgIssueDataSubmit(@ModelAttribute LGRequestIssue lGRequestIssue, HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();

		/*********************************/

		/*
		 * rm= userService.checkSessionStatus(req);
		 * if(rm.getLogout()==Constants.MAKE_LOGOUT) { return rm; }
		 */

		/**************************************/
		try {
			TLgrequestissue i = issueDataService.saveLGRequestIssue(lGRequestIssue);
			System.out.println("i" + i);
			if (i != null) {
//    			String msg=issueDataService.sendLgDetailsToBank(lGRequestIssue);
//    			System.out.println("msg"+msg);
				String msg = issueDataService.sendLgDetailsToBank(lGRequestIssue);

				// TLgrequestissue issu =
				// tLgrequestissueRepo.findByLoginId(lGRequestIssue.getUserId());
				System.out.println("issu.getRequestId()" + i.getRequestId());

				TAckfrombank ackfrombank = new TAckfrombank();
				ackfrombank.setRequestId(i.getRequestId());
				ackfrombank.setBankmsgId("LG25" + new Random().nextInt());
				ackfrombank.setAckDate(new Timestamp(new Date().getTime()));
				// TBankmaster bnkMaster = (TBankmaster)
				// bankMasterRepository.findByBankCode(lGRequestIssue.getBankCode());
				ackfrombank.setBankCode(lGRequestIssue.getBankCode());
				ackfrombank.setMessageDesc(msg);
				// ackfrombank.setMessageDescAr(msg);
				tAckfrombankRepository.save(ackfrombank);
				System.out.println("msg" + msg);

			}

			else {
				rm.setResponseCode(Constants.ERROR);
				rm.setResponseMessage(Constants.LG_ISSUE_ERROR);
				return rm;
			}
			System.out.println("strt" + i.getLgStartDate());
			System.out.println("strt" + i.getLgEndDate());
			rm.setResponseCode(Constants.SUCCESS);
			rm.setUserObject(i);
			rm.setResponseMessage(lGRequestIssue.getWriteUpType().equalsIgnoreCase("sama") ? Constants.LG_ISSUE_SUCCESS
					: Constants.NON_SAMA);
			return rm;
		} catch (Exception ex) {
			ex.printStackTrace();
			rm.setResponseCode(Constants.ERROR);
			rm.setResponseMessage(Constants.LG_ISSUE_ERROR);
			return rm;
		}
	}

	@GetMapping("/selectGovermentEntity")
	public ResponseEntity<?> selectGovermentEntity(HttpServletRequest req) {
		// return ResponseEntity.ok(beneficiryService.selectGovermentEntity());
		return ResponseEntity.ok(beneficiryService.selectGovermentUnregisteredBeneficiaryEntity());
	}

	@GetMapping("/selectGovermentEntityRegistered")
	public ResponseEntity<?> selectGovermentEntityRegistered(HttpServletRequest req) {
		// return ResponseEntity.ok(beneficiryService.selectGovermentEntity());
		return ResponseEntity.ok(beneficiryService.selectGovermentEntity());
	}

	@PostMapping("/govtEntitySave")
	public ResponseModel govtEntitySave(@ModelAttribute Beneficiarydetail beneficiarydetail, HttpServletRequest req) {
		return beneficiryService.saveGovermentEntity(beneficiarydetail);
	}

	@PostMapping("pub/login")
	public ResponseEntity<?> login(HttpServletRequest hs) {
		HttpHeaders hh = new HttpHeaders();
		hh.set("Authorization", "Basic eHl6Onh5eg==");

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("grant_type", "password");
		map.add("username", hs.getHeader("username"));
		map.add("password", hs.getHeader("password"));
		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map, hh);

		Object ob = new RestTemplate().postForObject("http://192.168.1.75:8080/oauth/token", entity, Object.class);
		return ResponseEntity.ok(ob);
	}

	/* */
	@GetMapping("/selectBankName")
	public ResponseEntity<?> selectBankName() {
		List<?> res = issueDataService.selectBankName();
		return ResponseEntity.ok(res);
	}

	@GetMapping("/selectWriteUpType")
	public ResponseEntity<?> selectWriteUpType() {
		List<?> res = issueDataService.selectWriteUpType();
		return ResponseEntity.ok(res);
	}

//    @GetMapping("pub/logout")
//    public ResponseEntity<?> revokeToken(HttpServletRequest hs) {
//        tokenServices.revokeToken(hs.getHeader("token"));
//        return ResponseEntity.ok("logout");
//    }

	@PostMapping("/checkGovtEntityName")
	public ResponseModel availableBeneficiaryName(@RequestParam(value = "entityName") String entityName,
			HttpServletRequest req) {
		return beneficiryService.checkEntityAvailability(entityName);
	}

	@PostMapping("/checkBenName")
	public ResponseModel checkBenName(@RequestParam(value = "benName") String benName, HttpServletRequest req) {
		return beneficiryService.checkBenNameAvailability(benName);
	}

	@GetMapping("/changeNumToWord/{num}")
	public String changeNumToWord(@PathVariable int num, HttpServletRequest req) {
		return issueDataService.changeNumToWord(num);
	}

	// To get all supplier details

	@GetMapping("/getAllSupplierDetails")
	public ResponseModel getAllSupplierDetails(HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();

		/*********************************/

		rm = userService.checkSessionStatus(req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			return rm;
		}

		/**************************************/
		List<?> res = supplierService.findAllSuppliersDetails();
		rm.setUserObject(res);
		return rm;
	}

	// Beneficiary Pending lg list

	@GetMapping("/getAllLgRequestBeneficiarySpecific/{benId}")
	public ResponseModel getAllLgRequestBeneficiarySpecific(@PathVariable String benId, HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();

		try {
			List<?> res = beneficiryService.getLgPendingWithBeneficiaryId(benId);
			rm.setUserObject(res);
			rm.setResponseCode(Constants.SUCCESS);
			rm.setResponseMessage("Value retrieved");

		} catch (Exception e) {
			e.printStackTrace();
			rm.setResponseCode(Constants.ERROR);
			rm.setResponseMessage("Value not retrieved");
		}

		return rm;
	}

	// Supplier Pending lg list

	@GetMapping("/getAllLgRequestSupplierSpecific/{loginId}")
	public ResponseModel getAllLgRequestPendingSupplierSpecific(@PathVariable String loginId, HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();
		/*********************************/

		rm = userService.checkSessionStatus(req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			/* return rm; */
		}

		/**************************************/

		List<LgPendingListDTO> res = supplierService.getLgWithSupplierId(loginId);

		rm.setUserObject(res);
		rm.setResponseCode(Constants.SUCCESS);
		rm.setResponseMessage("List retrieved");

		return rm;
	}

	// Lg Bank verified List for beneficiary

	@GetMapping("/getAllLgVerifiedBeneficiarySpecific/{loginId}")
	public ResponseModel getAllLgVerifiedBeneficiarySpecific(@PathVariable String loginId, HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();
		/*********************************/

		rm = userService.checkSessionStatus(req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			/* return rm; */
		}

		/**************************************/

		List<LgIssuedDataDTO> res = supplierService.getAllLGApprovedDetailsForBeneficiaryServ(loginId);

		rm.setUserObject(res);
		rm.setResponseCode(Constants.SUCCESS);
		rm.setResponseMessage("List retrieved");

		return rm;
	}

	// Lg Bank verified List for supplier

	@GetMapping("/getAllLgApprovedSupplierSpecific/{loginId}")
	public ResponseModel getAllLgApprovedSupplierSpecific(@PathVariable String loginId, HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();
		/*********************************/

		rm = userService.checkSessionStatus(req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			/* return rm; */
		}

		/**************************************/

		List<LgIssuedDataDTO> res = supplierService.getAllLGApprovedDetailsForSupplierServ(loginId);

		rm.setUserObject(res);
		rm.setResponseCode(Constants.SUCCESS);
		rm.setResponseMessage("List retrieved");

		return rm;
	}

	@RequestMapping(value = "/getLgDetailsAfterVerify", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getLgDetailsAfterVerify(@RequestParam("user_Id") String user_Id,

			@RequestParam(name = "amount", required = false) String amnt,
			@RequestParam(name = "msg", required = false) String msg) {

		int request_Id = tLgrequestissueRepo.getRequestIdFromAmount(user_Id, Double.valueOf(amnt));
		// System.out.println("request_Id"+req_Id.size());
		try {
			// for(Integer request_Id:req_Id) {
			if (msg.equalsIgnoreCase(Constants.VERIFIED) && request_Id != 0) {

				System.out.println("request_Id" + request_Id);
				tLgrequestissueRepo.setLGRequestIssueStatus("ACTIVE", request_Id);
				// tlgRepo.saveLg(lg_no, lg_typ_id, terms_condition_type, contract_value,
				// lg_value, currency_code, project_id, project_name, lg_issue,
				// lg_validity_date, lg_doc_path, lg_doc_name, zakat_start_date, zakat_end_date,
				// byan_no, beneficiary_id, supplier_id, request_id, login_id)
				tserviceReqRepo.changeRequestStatusByBeneficiaryamdRequest(Constants.BANK_VERIFIED_CON,
						"" + request_Id);
				issueDataService.savelgData("" + request_Id);
			} else {
				tLgrequestissueRepo.setLGRequestIssueStatus("INACTIVE", request_Id);
			}
			// BankInfo bankInfo = new BankInfo();
			return msg;
			/* } */
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		// return msg;
	}

	@RequestMapping(value = "/verifyBenEntity", method = RequestMethod.POST)
	public @ResponseBody ResponseModel verifyBenEntity(@RequestParam("benName") String benName) {
		ResponseModel rm = new ResponseModel();
		if (benName != null) {

			rm = beneficiryService.verifyStatusOfBeneficiary(benName);
		}
		return rm;
	}

	/* JK */

	@RequestMapping(value = "/createNewTabadulUser", method = RequestMethod.POST)
	public @ResponseBody ResponseModel createNewTabadulUser(@ModelAttribute TabadulUser tuser, HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();

		/*********************************/

		rm = userService.checkSessionStatus(req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			return rm;
		}

		/**************************************/

		rm = userService.createNewTabadulUser(tuser);

		return rm;
	}

	/* JK */

	@GetMapping("/getListUnverifiedEntity")
	public List<?> getListUnverifiedEntity(HttpServletRequest req) {
		List<EntityModel> ent = userService.getPendingListForVerification();
		// return ResponseEntity.ok(beneficiryService.selectGovermentEntity());
		// return
		// ResponseEntity.ok(beneficiryService.selectGovermentUnregisteredBeneficiaryEntity());

		return ent;
	}
	
	
	
	
	
	
	@GetMapping("/getBeneficiaryUnverifiedEntityList")
	public List<?> getBeneficiaryUnverifiedEntityList(HttpServletRequest req) {
		List<EntityModel> ent = userService.getBeneficiaryUnverifiedEntityList();
		// return ResponseEntity.ok(beneficiryService.selectGovermentEntity());
		// return
		// ResponseEntity.ok(beneficiryService.selectGovermentUnregisteredBeneficiaryEntity());

		return ent;
	}
	
	
	
	
	/*@GetMapping("/getSupplierUnverifiedEntityList")
	public List<?> getSupplierUnverifiedEntityList(HttpServletRequest req) {
		List<EntityModel> ent = userService.getSupplierUnverifiedEntityList();
		// return ResponseEntity.ok(beneficiryService.selectGovermentEntity());
		// return
		// ResponseEntity.ok(beneficiryService.selectGovermentUnregisteredBeneficiaryEntity());

		return ent;
	}
*/
	@GetMapping("/selectCRN/{loginid}")
	public ResponseModel selectCRN(@PathVariable String loginid/* , HttpServletRequest req */) {

		ResponseModel rm = new ResponseModel();
		/*********************************/

		/*
		 * rm= userService.checkSessionStatus(req);
		 * if(rm.getLogout()==Constants.MAKE_LOGOUT) { return rm; }
		 */

		/**************************************/

		List<?> res = supplierService.getCrnFromLogin(loginid);
		// rm.setUserObject(res);
		rm.setUserObject(res);
		rm.setResponseCode(Constants.SUCCESS);
		rm.setResponseMessage("Crn Number Received");
		return rm;
	}

	@GetMapping("/getLgRequestListForTermsChangeForSupplier/{loginId}")
	public List<?> getLgRequestListForTermsChangeForSupplier(@PathVariable String loginId, HttpServletRequest req) {
		List<LgListWithChangesAcceptSupplier> ent = supplierService.getAllLGChangedTnCForSupplierList(loginId);
		// return ResponseEntity.ok(beneficiryService.selectGovermentEntity());
		// return
		// ResponseEntity.ok(beneficiryService.selectGovermentUnregisteredBeneficiaryEntity());
		return ent;
	}

//Step one clicking edit button ,changes ns_pending to ns_chnge  

	@RequestMapping(value = "/changeStatusOfLgRequestBeneficiary", method = RequestMethod.POST)
	public @ResponseBody ResponseModel approvedByBeneficiary(@RequestParam("requestId") String requestId,
			@RequestParam(required = false, value = "termsNconditions") String termsNconditions) {
		ResponseModel rm = new ResponseModel();
		if (requestId != null) {
			if (termsNconditions != null) {
				rm = beneficiryService.changeStatusWithLgRequestNTerms(requestId, termsNconditions);
			} else {
				rm = beneficiryService.changeOnlyStatusLgRequest(requestId);
			}
		}
		return rm;
	}

	// Step two after accepted by supplier modified lg request,ns_change converts
	// ns_accept
	// Case I

	@RequestMapping(value = "/AcceptingLgRequestFromSupplier", method = RequestMethod.POST)
	public @ResponseBody ResponseModel AcceptingLgRequestFromSupplier(
			@RequestParam("requestId") String requestId/*
														 * ,@RequestParam(required = false,value = "termsNconditions")
														 * String termsNconditions
														 */) {
		ResponseModel rm = new ResponseModel();
		if (requestId != null) {
			if (requestId != null) {
				rm = supplierService.acceptTheLgIssue(requestId/* ,termsNconditions */);
			}
		}
		return rm;
	}

	// Case II
	@RequestMapping(value = "/CancellingLgRequestFromSupplier", method = RequestMethod.POST)
	public @ResponseBody ResponseModel CancellingLgRequestFromSupplier(
			@RequestParam("requestId") String requestId/*
														 * ,@RequestParam(required = false,value = "termsNconditions")
														 * String termsNconditions
														 */) {
		ResponseModel rm = new ResponseModel();
		if (requestId != null) {
			if (requestId != null) {
				rm = supplierService.cancelTheLgIssueBySupplier(requestId/* ,termsNconditions */);
			}
		}
		return rm;
	}

	// Step three finally lg approved by beneficiary,changes ns_accept converted to
	// pending

	@RequestMapping(value = "/changeRequestedStatusByBeneficiary", method = RequestMethod.POST)
	public @ResponseBody ResponseModel changeRequestedStatusByBeneficiary(@RequestParam("requestId") String requestId) {
		ResponseModel rm = new ResponseModel();
		if (requestId != null) {
			if (requestId != null) {
				rm = beneficiryService.changeOnlyStatusLgRequest(requestId);
			}
		}
		return rm;
	}

	@RequestMapping(value = "/getPaymentParams", method = RequestMethod.POST)
	public @ResponseBody ResponseModel getPaymentParams(@ModelAttribute PaymentModel paymentModel,
			HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();

		/*********************************/

		rm = userService.checkSessionStatus(req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			// return rm;
		}

		/**************************************/

		rm = paymentService.getPaymentParams(paymentModel);

		return rm;
	}

	// left

	@RequestMapping(value = "/CrnValidatedByMCI", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseModel CrnValidatedByMCI(@RequestParam(required = false) String crnno, HttpServletRequest req) {
		logger.info("CheckSupplierCrn: " + crnno);

		ResponseModel rm = new ResponseModel();

		HttpHeaders hh = new HttpHeaders();

		hh.set("apiKey", "oNNLs3iHPG5BRhog7iCyOPCK1ff8W1xa");
		hh.set("accept", "application/json");

		String nationalId_url = env.getProperty("nationalId_url");
		HttpEntity<String> request = new HttpEntity<String>(hh);
		String mci_url = env.getProperty("mci_status_url") + crnno;

		// String str = new RestTemplate().getForObject(mci_url,hh,String.class);
		try {
			ResponseEntity<String> result = new RestTemplate().exchange(mci_url, HttpMethod.GET, request, String.class);

			System.out.println("result" + result.getBody());
			// System.out.println("result"+result.getBody());

			/*
			 * Gson g = new Gson(); StatusInfo dto = g.fromJson(result.getBody(),
			 * StatusInfo.class);
			 */

			JsonElement je = new JsonParser().parse(result.getBody());

			String value = je.getAsJsonObject().get("id").getAsString();
			System.out.println(value);

			System.out.println("dto" + value);
			if (value.equalsIgnoreCase("active")) {
				// no record
				System.out.println("result.getBody()" + value);
				rm.setResponseCode(Constants.CRN_ACTIVE_CODE);
				rm.setResponseMessage(Constants.CRN_ACTIVE);
			} else if (value.equalsIgnoreCase("canceled")) {
				// no record
				System.out.println("result.getBody()" + value);
				rm.setResponseCode(Constants.CRN_CANCELLED_CODE);
				rm.setResponseMessage(Constants.CRN_CANCELLED);
			} else if (value.equalsIgnoreCase("suspended")) {
				// no record
				System.out.println("result.getBody()" + value);
				rm.setResponseCode(Constants.CRN_SUSPENDED_CODE);
				rm.setResponseMessage(Constants.CRN_SUSPENDED);
			} else if (value.equalsIgnoreCase("expired")) {
				// no record
				System.out.println("result.getBody()" + value);
				rm.setResponseCode(Constants.CRN_EXPIRED_CODE);
				rm.setResponseMessage(Constants.CRN_EXPIRED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			rm.setResponseCode(Constants.ERROR);
			rm.setResponseMessage(Constants.CRN_NOT_VALID);
		}

		logger.info(rm.getResponseMessage());
		return rm;
	}

	@GetMapping("/mciGetBenName/{crn}")
	public ResponseModel mciGetBenName(@PathVariable BigDecimal crn/* , HttpServletRequest req */) {

		ResponseModel rm = new ResponseModel();
		/*********************************/

		/*
		 * rm= userService.checkSessionStatus(req);
		 * if(rm.getLogout()==Constants.MAKE_LOGOUT) { return rm; }
		 */

		/**************************************/

		List<?> res = supplierService.getBenNameFromCRN(crn);
		System.out.println("" + res);
		// rm.setUserObject(res);

		if (res.size() > 0) {
			rm.setUserObject(res);
			rm.setResponseCode(Constants.SUCCESS);
			rm.setResponseMessage("List retrieved successfully");
		} else {
			// rm.setUserObject(res);
			rm.setResponseCode(Constants.NOT_FOUND);
			rm.setResponseMessage("This cr no is not valid");
		}

		return rm;
	}

	@RequestMapping(value = "/getCRNDetailsByNID", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseModel getCRNDetailsByNID(@RequestParam(required = false) String loginId, HttpServletRequest req) {

		ResponseModel rm = new ResponseModel();
		/*********************************/

		rm = userService.checkSessionStatus(req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			/* return rm; */ }

		int isLogout = rm.getLogout();

		/**************************************/

		List<?> res = supplierService.getCrnFromLogin(loginId);
		// rm.setUserObject(res);
		rm.setUserObject(res);
		rm.setResponseCode(Constants.SUCCESS);
		rm.setResponseMessage("Crn Number Received");
		rm.setLogout(isLogout);
		return rm;

	}

	@RequestMapping(value = "/getMCIFromLink", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public UserDetail getMCIFromLink(@RequestParam(required = false) String crnno, HttpServletRequest req) {
		logger.info("if crn: " + crnno);

		ResponseModel rm = new ResponseModel();
		UserDetail us = new UserDetail();

		HttpHeaders hh = new HttpHeaders();

		hh.set("apiKey", "oNNLs3iHPG5BRhog7iCyOPCK1ff8W1xa");
		hh.set("accept", "application/json");

		String crn_num_url = env.getProperty("mci_cr_url") + crnno;
		HttpEntity<String> request = new HttpEntity<String>(hh);
		String crn_address_url = env.getProperty("mci_address_url") + crnno;

		// String str = new RestTemplate().getForObject(mci_url,hh,String.class);
		try {
			ResponseEntity<String> result = new RestTemplate().exchange(crn_num_url, HttpMethod.GET, request,
					String.class);
			ResponseEntity<String> result1 = new RestTemplate().exchange(crn_address_url, HttpMethod.GET, request,
					String.class);

			System.out.println("result" + result.getBody());
			System.out.println("result" + result1.getBody());
			// System.out.println("result"+result.getBody());

			/*
			 * Gson g = new Gson(); StatusInfo dto = g.fromJson(result.getBody(),
			 * StatusInfo.class);
			 */

			JsonElement je = new JsonParser().parse(result.getBody());
			JsonElement je1 = new JsonParser().parse(result1.getBody());

			String value = je.getAsJsonObject().get("crName").getAsString();
			String value1 = je1.getAsJsonObject().getAsJsonObject("general").get("zipcode").getAsString();
			System.out.println(value);

			System.out.println("dto" + value1);

			us.setAddress1(je1.getAsJsonObject().getAsJsonObject("general").get("address").getAsString());
			us.setAddress2(je1.getAsJsonObject().getAsJsonObject("general").get("zipcode").getAsString());
			if (je1.getAsJsonObject().getAsJsonObject("general").get("telephone1") != null) {
				us.setContactNo(je1.getAsJsonObject().getAsJsonObject("general").get("telephone1").getAsString());

			} else {
				us.setContactNo(je.getAsJsonObject().getAsJsonObject("general").get("telephone2").getAsString());
			}

			us.setCrNumber(je.getAsJsonObject().get("crNumber").getAsString());
			// us.setEmailAddress(je1.getAsJsonObject().getAsJsonObject("general").get("email").toString());

			if (!je1.getAsJsonObject().getAsJsonObject("general").get("email").equals("null")) {
				// us.setEmailAddress(je1.getAsJsonObject().getAsJsonObject("general").get("email").getAsString());
				us.setEmailAddress(je1.getAsJsonObject().getAsJsonObject("general").get("email").getAsJsonPrimitive()
						.getAsString());
			} else {
				us.setEmailAddress("");
			}
			us.setEntityName(je.getAsJsonObject().getAsJsonObject("status").getAsJsonArray("nameEn").getAsString());

		} catch (Exception e) {
			e.printStackTrace();
			rm.setResponseCode(Constants.ERROR);
			rm.setResponseMessage(Constants.CRN_NOT_VALID);
		}

		logger.info(rm.getResponseMessage());
		System.out.println("us" + us.getAddress1() + us.getAddress2() + us.getContactNo() + us.getCrNumber());
		return us;
	}

	@RequestMapping(value = "/savePaymentParams", method = RequestMethod.POST)
	public @ResponseBody ResponseModel savePaymentParams(@ModelAttribute PaymentModel paymentModel,
			HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();

		/*********************************/

		rm = userService.checkSessionStatus(req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			// return rm;
		}

		/**************************************/

		rm = paymentService.savePaymentParams(paymentModel);

		return rm;
	}

	@RequestMapping(value = "/getOtpByloginId", method = RequestMethod.POST)
	public @ResponseBody ResponseModel getOtpByloginId(@RequestParam String loginId, HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();

		/*********************************/

		rm = userService.checkSessionStatus(req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			return rm;
		}

		int isLoggedOut = rm.getLogout();

		/**************************************/

		rm = userService.getOtpByloginId(loginId);
		rm.setLogout(isLoggedOut == Constants.MAKE_LOGOUT ? Constants.MAKE_LOGOUT : Constants.MAKE_NOLOGOUT);

		return rm;
	}

	@RequestMapping(value = "/checkSessionExpired", method = RequestMethod.POST)
	public @ResponseBody int checkSessionExpired(@RequestParam String loginId, HttpServletRequest req) {
		ResponseModel rm = new ResponseModel();

		/*********************************/

		rm = userService.checkSessionStatus(req);
		if (rm.getLogout() == Constants.MAKE_LOGOUT) {
			return Constants.MAKE_LOGOUT;
		} else {
			return Constants.MAKE_NOLOGOUT;
		}

		/**************************************/

	}


	@RequestMapping(value = "/mciValidData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseModel item(@ModelAttribute MciRequestData itemRequest, HttpServletRequest req)  {

		logger.info("MCI VAlid data: " + itemRequest.getCr_no());

		ResponseModel mData = mckService.checkDataExists(itemRequest);

		return mData;
		// return soapClient.getInfo(itemRequest);
	}
	
	
	
	 @RequestMapping(value="/getSupplierUnverifiedEntityList",method=RequestMethod.GET)
	    public @ResponseBody
	    ResponseModel getSupplierUnverifiedEntityList(@RequestParam(required=false) String adminId, HttpServletRequest req) {
		  ResponseModel rm = new ResponseModel();
		  
		  /*********************************/


	    	rm= userService.checkSessionStatus(req);
	    	if(rm.getLogout()==Constants.MAKE_LOGOUT) {
	    		// return rm;
	     	}
	    	
	    	
/**************************************/	    
		  
		  
	    	rm=	supplierService.getSupplierUnverifiedEntityList(adminId);	    	
	    	
	    	 
		  
		  return rm;
		  }
	  
	  
	  
	 @RequestMapping(value="/getGovUnverifiedEntityList",method=RequestMethod.GET)
	    public @ResponseBody
	    ResponseModel getGovUnverifiedEntityList(@RequestParam(required=false) String adminId, HttpServletRequest req) {
		  ResponseModel rm = new ResponseModel();
		  
		  /*********************************/


	    	rm= userService.checkSessionStatus(req);
	    	if(rm.getLogout()==Constants.MAKE_LOGOUT) {
	    		// return rm;
	     	}
	    	
	    	
/**************************************/	    
		  
		  
	    	rm=	supplierService.getGovUnverifiedEntityList(adminId);	    	
	    	
	    	 
		  
		  return rm;
		  }
	 
	 
	 
	  @RequestMapping(value="/getPriEntityDetails",method=RequestMethod.POST)
	    public @ResponseBody
	    ResponseModel getPriEntityDetails(@RequestParam(required=true) String nId,@RequestParam(required=true) String crn,@RequestParam(required=false) String adminId, HttpServletRequest req) {
		  ResponseModel rm = new ResponseModel();
		  
		  /*********************************/


	    	rm= userService.checkSessionStatus(req);
	    	if(rm.getLogout()==Constants.MAKE_LOGOUT) {
	    		// return rm;
	     	}
	    	
	    	
/**************************************/	    
		  
		  
	    	rm=	supplierService.getPriEntityDetails(nId,crn,adminId);	    	
	    	
	    	 
		  
		  return rm;
		  }
	  
	  @RequestMapping(value="/getGovEntityDetails",method=RequestMethod.POST)
	    public @ResponseBody
	    ResponseModel getGovEntityDetails(@RequestParam(required=true) String nId,@RequestParam(required=true) String uid,@RequestParam(required=false) String adminId, HttpServletRequest req) {
		  ResponseModel rm = new ResponseModel();
		  
		  /*********************************/


	    	rm= userService.checkSessionStatus(req);
	    	if(rm.getLogout()==Constants.MAKE_LOGOUT) {
	    		// return rm;
	     	}
	    	
	    	
/**************************************/	    
		  
		  
	    	rm=	supplierService.getGovEntityDetails(nId,uid,adminId);	    	
	    	
	    	 
		  
		  return rm;
		  }




	
}
