package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the T_LGTYPEMASTER database table.
 * 
 */
@Entity
@Table(name="T_LGTYPEMASTER")
@NamedQuery(name="TLgtypemaster.findAll", query="SELECT t FROM TLgtypemaster t")
public class TLgtypemaster implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Column(name="LG_TYPE")
	private String lgType;

	@Id
	@Column(name="LG_TYPE_ID")
	private BigDecimal lgTypeId;

	//bi-directional many-to-one association to TLg
	@OneToMany(mappedBy="TLgtypemaster")
	private List<TLg> TLgs;

	//bi-directional many-to-one association to TServicerequest
	//@OneToMany(mappedBy="TLgtypemaster")
	//private List<TServicerequest> TServicerequests;

	public TLgtypemaster() {
	}

	public String getLgType() {
		return this.lgType;
	}

	public void setLgType(String lgType) {
		this.lgType = lgType;
	}

	public BigDecimal getLgTypeId() {
		return this.lgTypeId;
	}

	public void setLgTypeId(BigDecimal lgTypeId) {
		this.lgTypeId = lgTypeId;
	}

	public List<TLg> getTLgs() {
		return this.TLgs;
	}

	public void setTLgs(List<TLg> TLgs) {
		this.TLgs = TLgs;
	}

	public TLg addTLg(TLg TLg) {
		getTLgs().add(TLg);
		TLg.setTLgtypemaster(this);

		return TLg;
	}

	public TLg removeTLg(TLg TLg) {
		getTLgs().remove(TLg);
		TLg.setTLgtypemaster(null);

		return TLg;
	}

	/*
	 * public List<TServicerequest> getTServicerequests() { return
	 * this.TServicerequests; }
	 * 
	 * public void setTServicerequests(List<TServicerequest> TServicerequests) {
	 * this.TServicerequests = TServicerequests; }
	 * 
	 * public TServicerequest addTServicerequest(TServicerequest TServicerequest) {
	 * getTServicerequests().add(TServicerequest);
	 * TServicerequest.setTLgtypemaster(this);
	 * 
	 * return TServicerequest; }
	 * 
	 * public TServicerequest removeTServicerequest(TServicerequest TServicerequest)
	 * { getTServicerequests().remove(TServicerequest);
	 * TServicerequest.setTLgtypemaster(null);
	 * 
	 * return TServicerequest; }
	 */

}