package sa.wthaq.Entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the T_SUPPLIERDETAILS database table.
 * 
 */
@Entity
@Table(name="T_SUPPLIERDETAILS")
@NamedQuery(name="TSupplierdetail.findAll", query="SELECT t FROM TSupplierdetail t")
public class TSupplierdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SUPPLIER_ID")
	private long supplierId;

	@Column(name="AGREE_TAG")
	private String agreeTag;

	@Column(name="AUTH_LETTTER_DOC_NAME")
	private String authLettterDocName;

	@Column(name="AUTH_LETTTER_DOC_PATH")
	private String authLettterDocPath;

	@Column(name="COMM_REG_DOC_NAME")
	private String commRegDocName;

	@Column(name="COMM_REG_DOC_PATH")
	private String commRegDocPath;

	@Column(name="CONTACT_INFO")
	private String contactInfo;

	private String country;


	@Column(name="CR_EXPIRY_DATE")
	private Timestamp crExpiryDate;

	@Column(name="CR_ISSUE_DATE")
	private Timestamp crIssueDate;

	@Column(name="CR_STATUS")
	private String crStatus;

	@Column(name="CR_TYPE")
	private String crType;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="CR_NO")
	private BigDecimal crnNo;

	@Column(name="DATE_OF_BIRTH")
	private Timestamp dateOfBirth;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="LEG_AGREE_DOC_NAME")
	private String legAgreeDocName;

	@Column(name="LEG_AGREE_DOC_PATH")
	private String legAgreeDocPath;

	@Column(name="LOGIN_ID")
	private String loginId;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="NATIONAL_ID_DOC_NAME")
	private String nationalIdDocName;

	@Column(name="NATIONAL_ID")
	private BigDecimal nationalId;
		
	@Column(name="NATIONAL_ID_DOC_PATH")
	private String nationalIdDocPath;

	private String passwd;

	@Column(name="PIN_CODE")
	private String pinCode;

	@Column(name="REP_DOB")
	private Timestamp repDob;

	@Column(name="REP_EMAIL")
	private String repEmail;

	@Column(name="REP_ID")
	private String repId;

	@Column(name="REP_LAST_NAME")
	private String repLastName;

	@Column(name="REP_MOBILE_NO")
	private String repMobileNo;

	@Column(name="REP_NAME")
	private String repName;

	@Column(name="REP_NAME_AR")
	private String repNameAr;

	@Column(name="REP_NATIONAL_ID")
	private BigDecimal repNationalId;

	@Column(name="\"STATE\"")
	private String state;

	@Column(name="SUPPLIER_ADDR1")
	private String supplierAddr1;

	@Column(name="SUPPLIER_ADDR1_AR")
	private String supplierAddr1Ar;

	@Column(name="SUPPLIER_ADDR2")
	private String supplierAddr2;

	@Column(name="SUPPLIER_ADDR2_AR")
	private String supplierAddr2Ar;

	@Column(name="SUPPLIER_NAME")
	private String supplierName;

	@Column(name="SUPPLIER_NAME_AR")
	private String supplierNameAr;

	@Column(name="SUPPLIER_STATUS_FLAG")
	private String supplierStatusFlag;

	//bi-directional many-to-one association to TLgdetail
	/*
	 * @OneToMany(mappedBy="TSupplierdetail") private List<TLgdetail> TLgdetails;
	 */

	//bi-directional many-to-one association to TLgonboarding
	/*
	 * @OneToMany(mappedBy="TSupplierdetail") private List<TLgonboarding>
	 * TLgonboardings;
	 */

	//bi-directional many-to-one association to TLgrequestamend
	/*
	 * @OneToMany(mappedBy="TSupplierdetail") private List<TLgrequestamend>
	 * TLgrequestamends;
	 */

	//bi-directional many-to-one association to TLgrequestconfiscate
	/*
	 * @OneToMany(mappedBy="TSupplierdetail") private List<TLgrequestconfiscate>
	 * TLgrequestconfiscates;
	 */

	//bi-directional many-to-one association to TLgrequestissue
	/*
	 * @OneToMany(mappedBy="TSupplierdetail") private List<TLgrequestissue>
	 * TLgrequestissues;
	 */

	//bi-directional many-to-one association to TLgrequestrelease
	/*
	 * @OneToMany(mappedBy="TSupplierdetail") private List<TLgrequestrelease>
	 * TLgrequestreleases;
	 */

	public TSupplierdetail() {
	}

	
	
	
	
	public BigDecimal getNationalId() {
		return nationalId;
	}
    public void setNationalId(BigDecimal nationalId) {
		this.nationalId = nationalId;
	}

   public long getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}

	public String getAgreeTag() {
		return this.agreeTag;
	}

	public void setAgreeTag(String agreeTag) {
		this.agreeTag = agreeTag;
	}

	public String getAuthLettterDocName() {
		return this.authLettterDocName;
	}

	public void setAuthLettterDocName(String authLettterDocName) {
		this.authLettterDocName = authLettterDocName;
	}

	public String getAuthLettterDocPath() {
		return this.authLettterDocPath;
	}

	public void setAuthLettterDocPath(String authLettterDocPath) {
		this.authLettterDocPath = authLettterDocPath;
	}

	public String getCommRegDocName() {
		return this.commRegDocName;
	}

	public void setCommRegDocName(String commRegDocName) {
		this.commRegDocName = commRegDocName;
	}

	public String getCommRegDocPath() {
		return this.commRegDocPath;
	}

	public void setCommRegDocPath(String commRegDocPath) {
		this.commRegDocPath = commRegDocPath;
	}

	public String getContactInfo() {
		return this.contactInfo;
	}

	public void setContactInfo(String contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public BigDecimal getCrnNo() {
		return this.crnNo;
	}

	public void setCrnNo(BigDecimal crnNo) {
		this.crnNo = crnNo;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getLegAgreeDocName() {
		return this.legAgreeDocName;
	}

	public void setLegAgreeDocName(String legAgreeDocName) {
		this.legAgreeDocName = legAgreeDocName;
	}

	public String getLegAgreeDocPath() {
		return this.legAgreeDocPath;
	}

	public void setLegAgreeDocPath(String legAgreeDocPath) {
		this.legAgreeDocPath = legAgreeDocPath;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getNationalIdDocName() {
		return this.nationalIdDocName;
	}

	public void setNationalIdDocName(String nationalIdDocName) {
		this.nationalIdDocName = nationalIdDocName;
	}

	public String getNationalIdDocPath() {
		return this.nationalIdDocPath;
	}

	public void setNationalIdDocPath(String nationalIdDocPath) {
		this.nationalIdDocPath = nationalIdDocPath;
	}

	public String getPasswd() {
		return this.passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getPinCode() {
		return this.pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getRepEmail() {
		return this.repEmail;
	}

	public void setRepEmail(String repEmail) {
		this.repEmail = repEmail;
	}

	public String getRepId() {
		return this.repId;
	}

	public void setRepId(String repId) {
		this.repId = repId;
	}

	public String getRepMobileNo() {
		return this.repMobileNo;
	}

	public void setRepMobileNo(String repMobileNo) {
		this.repMobileNo = repMobileNo;
	}

	public String getRepName() {
		return this.repName;
	}

	public void setRepName(String repName) {
		this.repName = repName;
	}

	public String getRepNameAr() {
		return this.repNameAr;
	}

	public void setRepNameAr(String repNameAr) {
		this.repNameAr = repNameAr;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSupplierAddr1() {
		return this.supplierAddr1;
	}

	public void setSupplierAddr1(String supplierAddr1) {
		this.supplierAddr1 = supplierAddr1;
	}

	public String getSupplierAddr1Ar() {
		return this.supplierAddr1Ar;
	}

	public void setSupplierAddr1Ar(String supplierAddr1Ar) {
		this.supplierAddr1Ar = supplierAddr1Ar;
	}

	public String getSupplierAddr2() {
		return this.supplierAddr2;
	}

	public void setSupplierAddr2(String supplierAddr2) {
		this.supplierAddr2 = supplierAddr2;
	}

	public String getSupplierAddr2Ar() {
		return this.supplierAddr2Ar;
	}

	public void setSupplierAddr2Ar(String supplierAddr2Ar) {
		this.supplierAddr2Ar = supplierAddr2Ar;
	}

	public String getSupplierName() {
		return this.supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierNameAr() {
		return this.supplierNameAr;
	}

	public void setSupplierNameAr(String supplierNameAr) {
		this.supplierNameAr = supplierNameAr;
	}

	public String getSupplierStatusFlag() {
		return this.supplierStatusFlag;
	}

	public void setSupplierStatusFlag(String supplierStatusFlag) {
		this.supplierStatusFlag = supplierStatusFlag;
	}
	
	public Timestamp getCrExpiryDate() {
		return this.crExpiryDate;
	}

	public void setCrExpiryDate(Timestamp crExpiryDate) {
		this.crExpiryDate = crExpiryDate;
	}

	public Timestamp getCrIssueDate() {
		return this.crIssueDate;
	}

	public void setCrIssueDate(Timestamp crIssueDate) {
		this.crIssueDate = crIssueDate;
	}
	
	public String getCrStatus() {
		return this.crStatus;
	}

	public void setCrStatus(String crStatus) {
		this.crStatus = crStatus;
	}

	public String getCrType() {
		return this.crType;
	}

	public void setCrType(String crType) {
		this.crType = crType;
	}
	public Timestamp getRepDob() {
		return this.repDob;
	}

	public void setRepDob(Timestamp repDob) {
		this.repDob = repDob;
	}
	public String getRepLastName() {
		return this.repLastName;
	}

	public void setRepLastName(String repLastName) {
		this.repLastName = repLastName;
	}
	public BigDecimal getRepNationalId() {
		return this.repNationalId;
	}

	public void setRepNationalId(BigDecimal repNationalId) {
		this.repNationalId = repNationalId;
	}
	
	public Timestamp getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Timestamp dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}


	/*
	 * public List<TLgdetail> getTLgdetails() { return this.TLgdetails; }
	 * 
	 * public void setTLgdetails(List<TLgdetail> TLgdetails) { this.TLgdetails =
	 * TLgdetails; }
	 */

	/*
	 * public TLgdetail addTLgdetail(TLgdetail TLgdetail) {
	 * getTLgdetails().add(TLgdetail); TLgdetail.setTSupplierdetail(this);
	 * 
	 * return TLgdetail; }
	 * 
	 * public TLgdetail removeTLgdetail(TLgdetail TLgdetail) {
	 * getTLgdetails().remove(TLgdetail); TLgdetail.setTSupplierdetail(null);
	 * 
	 * return TLgdetail; }
	 */

	/*
	 * public List<TLgonboarding> getTLgonboardings() { return this.TLgonboardings;
	 * }
	 * 
	 * public void setTLgonboardings(List<TLgonboarding> TLgonboardings) {
	 * this.TLgonboardings = TLgonboardings; }
	 * 
	 * public TLgonboarding addTLgonboarding(TLgonboarding TLgonboarding) {
	 * getTLgonboardings().add(TLgonboarding);
	 * TLgonboarding.setTSupplierdetail(this);
	 * 
	 * return TLgonboarding; }
	 * 
	 * public TLgonboarding removeTLgonboarding(TLgonboarding TLgonboarding) {
	 * getTLgonboardings().remove(TLgonboarding);
	 * TLgonboarding.setTSupplierdetail(null);
	 * 
	 * return TLgonboarding; }
	 * 
	 * public List<TLgrequestamend> getTLgrequestamends() { return
	 * this.TLgrequestamends; }
	 * 
	 * public void setTLgrequestamends(List<TLgrequestamend> TLgrequestamends) {
	 * this.TLgrequestamends = TLgrequestamends; }
	 * 
	 * public TLgrequestamend addTLgrequestamend(TLgrequestamend TLgrequestamend) {
	 * getTLgrequestamends().add(TLgrequestamend);
	 * TLgrequestamend.setTSupplierdetail(this);
	 * 
	 * return TLgrequestamend; }
	 * 
	 * public TLgrequestamend removeTLgrequestamend(TLgrequestamend TLgrequestamend)
	 * { getTLgrequestamends().remove(TLgrequestamend);
	 * TLgrequestamend.setTSupplierdetail(null);
	 * 
	 * return TLgrequestamend; }
	 * 
	 * public List<TLgrequestconfiscate> getTLgrequestconfiscates() { return
	 * this.TLgrequestconfiscates; }
	 * 
	 * public void setTLgrequestconfiscates(List<TLgrequestconfiscate>
	 * TLgrequestconfiscates) { this.TLgrequestconfiscates = TLgrequestconfiscates;
	 * }
	 * 
	 * public TLgrequestconfiscate addTLgrequestconfiscate(TLgrequestconfiscate
	 * TLgrequestconfiscate) { getTLgrequestconfiscates().add(TLgrequestconfiscate);
	 * TLgrequestconfiscate.setTSupplierdetail(this);
	 * 
	 * return TLgrequestconfiscate; }
	 * 
	 * public TLgrequestconfiscate removeTLgrequestconfiscate(TLgrequestconfiscate
	 * TLgrequestconfiscate) {
	 * getTLgrequestconfiscates().remove(TLgrequestconfiscate);
	 * TLgrequestconfiscate.setTSupplierdetail(null);
	 * 
	 * return TLgrequestconfiscate; }
	 */

	/*
	 * public List<TLgrequestissue> getTLgrequestissues() { return
	 * this.TLgrequestissues; }
	 * 
	 * public void setTLgrequestissues(List<TLgrequestissue> TLgrequestissues) {
	 * this.TLgrequestissues = TLgrequestissues; }
	 */

	/*
	 * public TLgrequestissue addTLgrequestissue(TLgrequestissue TLgrequestissue) {
	 * getTLgrequestissues().add(TLgrequestissue);
	 * TLgrequestissue.setTSupplierdetail(this);
	 * 
	 * return TLgrequestissue; }
	 * 
	 * public TLgrequestissue removeTLgrequestissue(TLgrequestissue TLgrequestissue)
	 * { getTLgrequestissues().remove(TLgrequestissue);
	 * TLgrequestissue.setTSupplierdetail(null);
	 * 
	 * return TLgrequestissue; }
	 */

	/*
	 * public List<TLgrequestrelease> getTLgrequestreleases() { return
	 * this.TLgrequestreleases; }
	 * 
	 * public void setTLgrequestreleases(List<TLgrequestrelease> TLgrequestreleases)
	 * { this.TLgrequestreleases = TLgrequestreleases; }
	 * 
	 * public TLgrequestrelease addTLgrequestreleas(TLgrequestrelease
	 * TLgrequestreleas) { getTLgrequestreleases().add(TLgrequestreleas);
	 * TLgrequestreleas.setTSupplierdetail(this);
	 * 
	 * return TLgrequestreleas; }
	 * 
	 * public TLgrequestrelease removeTLgrequestreleas(TLgrequestrelease
	 * TLgrequestreleas) { getTLgrequestreleases().remove(TLgrequestreleas);
	 * TLgrequestreleas.setTSupplierdetail(null);
	 * 
	 * return TLgrequestreleas; }
	 */

}