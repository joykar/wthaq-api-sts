package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the T_USERMASTER database table.
 * 
 */
@Entity
@Table(name="T_USERMASTER")
public class TUsermaster implements Serializable  {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="LOGIN_ID")
	private String loginId;

	@Column(name="CREATE_BY",insertable = false,updatable = false)
	private String createBy;

	@Column(name="CREATE_ON",insertable = false,updatable = false)
	private Timestamp createOn;

	@Column(name="DELETE_FLAG",insertable = false,updatable = false)
	private String deleteFlag;

	@Column(name="\"LANGUAGE\"")
	private String language;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	private String password;

	@Column(name="ROLE_ID")
	private int roleId;

	@Column(name="USER_ID")
	private int userId;

	@Column(name="USER_NAME")
	private String userName;

	@Column(name="USER_NAME_AR")
	private String userNameAr;

	@Column(name="USER_STATUS")
	private String userStatus;

	//bi-directional many-to-one association to TActivitylog
	@OneToMany(mappedBy="TUsermaster")
	private List<TActivitylog> TActivitylogs;

	//bi-directional many-to-one association to TNotification
	@OneToMany(mappedBy="TUsermaster")
	private List<TNotification> TNotifications;

	//bi-directional many-to-one association to TPaymentsettlement
	@OneToMany(mappedBy="TUsermaster")
	private List<TPaymentsettlement> TPaymentsettlements;

	//bi-directional many-to-one association to TPaymentsubscription
	@OneToMany(mappedBy="TUsermaster")
	private List<TPaymentsubscription> TPaymentsubscriptions;

	//bi-directional many-to-one association to TServicerequest
	//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	//@OneToMany(mappedBy="TUsermaster")
	//private List<TServicerequest> TServicerequests;

	public TUsermaster() {
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserNameAr() {
		return this.userNameAr;
	}

	public void setUserNameAr(String userNameAr) {
		this.userNameAr = userNameAr;
	}

	public String getUserStatus() {
		return this.userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public List<TActivitylog> getTActivitylogs() {
		return this.TActivitylogs;
	}

	public void setTActivitylogs(List<TActivitylog> TActivitylogs) {
		this.TActivitylogs = TActivitylogs;
	}

	public TActivitylog addTActivitylog(TActivitylog TActivitylog) {
		getTActivitylogs().add(TActivitylog);
		TActivitylog.setTUsermaster(this);

		return TActivitylog;
	}

	public TActivitylog removeTActivitylog(TActivitylog TActivitylog) {
		getTActivitylogs().remove(TActivitylog);
		TActivitylog.setTUsermaster(null);

		return TActivitylog;
	}

	public List<TNotification> getTNotifications() {
		return this.TNotifications;
	}

	public void setTNotifications(List<TNotification> TNotifications) {
		this.TNotifications = TNotifications;
	}

	public TNotification addTNotification(TNotification TNotification) {
		getTNotifications().add(TNotification);
		TNotification.setTUsermaster(this);

		return TNotification;
	}

	public TNotification removeTNotification(TNotification TNotification) {
		getTNotifications().remove(TNotification);
		TNotification.setTUsermaster(null);

		return TNotification;
	}

	public List<TPaymentsettlement> getTPaymentsettlements() {
		return this.TPaymentsettlements;
	}

	public void setTPaymentsettlements(List<TPaymentsettlement> TPaymentsettlements) {
		this.TPaymentsettlements = TPaymentsettlements;
	}

	public TPaymentsettlement addTPaymentsettlement(TPaymentsettlement TPaymentsettlement) {
		getTPaymentsettlements().add(TPaymentsettlement);
		TPaymentsettlement.setTUsermaster(this);

		return TPaymentsettlement;
	}

	public TPaymentsettlement removeTPaymentsettlement(TPaymentsettlement TPaymentsettlement) {
		getTPaymentsettlements().remove(TPaymentsettlement);
		TPaymentsettlement.setTUsermaster(null);

		return TPaymentsettlement;
	}

	public List<TPaymentsubscription> getTPaymentsubscriptions() {
		return this.TPaymentsubscriptions;
	}

	public void setTPaymentsubscriptions(List<TPaymentsubscription> TPaymentsubscriptions) {
		this.TPaymentsubscriptions = TPaymentsubscriptions;
	}

	public TPaymentsubscription addTPaymentsubscription(TPaymentsubscription TPaymentsubscription) {
		getTPaymentsubscriptions().add(TPaymentsubscription);
		TPaymentsubscription.setTUsermaster(this);

		return TPaymentsubscription;
	}

	public TPaymentsubscription removeTPaymentsubscription(TPaymentsubscription TPaymentsubscription) {
		getTPaymentsubscriptions().remove(TPaymentsubscription);
		TPaymentsubscription.setTUsermaster(null);

		return TPaymentsubscription;
	}

	/*
	 * public List<TServicerequest> getTServicerequests() { return
	 * this.TServicerequests; }
	 * 
	 * public void setTServicerequests(List<TServicerequest> TServicerequests) {
	 * this.TServicerequests = TServicerequests; }
	 * 
	 * public TServicerequest addTServicerequest(TServicerequest TServicerequest) {
	 * getTServicerequests().add(TServicerequest);
	 * TServicerequest.setTUsermaster(this);
	 * 
	 * return TServicerequest; }
	 */

	/*
	 * public TServicerequest removeTServicerequest(TServicerequest TServicerequest)
	 * { getTServicerequests().remove(TServicerequest);
	 * TServicerequest.setTUsermaster(null);
	 * 
	 * return TServicerequest; }
	 */

}