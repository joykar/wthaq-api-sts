package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the T_ACTIVITYLOG database table.
 * 
 */
@Embeddable
public class TActivitylogPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="LOGIN_ID")
	private String loginId;

	@Column(name="MENU_ID")
	private long menuId;

	@Column(name="ACTIVITY_DATE")
	private String activityDate;

	public TActivitylogPK() {
	}
	public String getLoginId() {
		return this.loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public long getMenuId() {
		return this.menuId;
	}
	public void setMenuId(long menuId) {
		this.menuId = menuId;
	}
	public String getActivityDate() {
		return this.activityDate;
	}
	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TActivitylogPK)) {
			return false;
		}
		TActivitylogPK castOther = (TActivitylogPK)other;
		return 
			this.loginId.equals(castOther.loginId)
			&& (this.menuId == castOther.menuId)
			&& this.activityDate.equals(castOther.activityDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.loginId.hashCode();
		hash = hash * prime + ((int) (this.menuId ^ (this.menuId >>> 32)));
		hash = hash * prime + this.activityDate.hashCode();
		
		return hash;
	}
}