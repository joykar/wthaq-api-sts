package sa.wthaq.Entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_Session")
public class TSession  implements Serializable {
	private static final long serialVersionUID = 1L;


@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="session_count")	
int sessionCount;

@Column(name="login_id")	
String loginId;       

@Column(name="session_id")	
String	sessionId;

@Column(name="token")	
String 	token;

@Column(name="session_start_time")	
String	sessionStartTime;

@Column(name="last_access_time")	
String	lastAccessTime;

@Column(name="remote_ip")	
String	remoteIp;       


@Column(name="remote_host")	
String	remoteHost;   

@Column(name="remote_port")	
String	remotePort ; 

@Column(name="remote_user")	
String	remoteUser;    

@Column(name="remote_agent")	
String	remoteAgent;






public int getSessionCount() {
	return sessionCount;
}
public void setSessionCount(int sessionCount) {
	this.sessionCount = sessionCount;
}
public String getLoginId() {
	return loginId;
}
public void setLoginId(String loginId) {
	this.loginId = loginId;
}
public String getSessionId() {
	return sessionId;
}
public void setSessionId(String sessionId) {
	this.sessionId = sessionId;
}
public String getToken() {
	return token;
}
public void setToken(String token) {
	this.token = token;
}
public String getSessionStartTime() {
	return sessionStartTime;
}
public void setSessionStartTime(String sessionStartTime) {
	this.sessionStartTime = sessionStartTime;
}
public String getLastAccessTime() {
	return lastAccessTime;
}
public void setLastAccessTime(String lastAccessTime) {
	this.lastAccessTime = lastAccessTime;
}
public String getRemoteIp() {
	return remoteIp;
}
public void setRemoteIp(String remoteIp) {
	this.remoteIp = remoteIp;
}
public String getRemoteHost() {
	return remoteHost;
}
public void setRemoteHost(String remoteHost) {
	this.remoteHost = remoteHost;
}
public String getRemotePort() {
	return remotePort;
}
public void setRemotePort(String remotePort) {
	this.remotePort = remotePort;
}
public String getRemoteUser() {
	return remoteUser;
}
public void setRemoteUser(String remoteUser) {
	this.remoteUser = remoteUser;
}
public String getRemoteAgent() {
	return remoteAgent;
}
public void setRemoteAgent(String remoteAgent) {
	this.remoteAgent = remoteAgent;
}
	
	
	
}
