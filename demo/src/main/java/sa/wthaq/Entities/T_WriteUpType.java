package sa.wthaq.Entities;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Nationalized;


@Entity
@Table(name="T_WRITEUPTYPE")
@NamedQuery(name="T_WriteUpType.findAll", query="SELECT t FROM T_WriteUpType t")
public class T_WriteUpType implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private String writeuptype;

	
	@Column(name="terms_condition_message")
	private String termsConditionMessage;

	@Nationalized
	@Column(name="terms_condition_message_ar")
	private String termsConditionMessageAr;
	
	public T_WriteUpType() {
	}

	public String getWriteuptype() {
		return this.writeuptype;
	}

	public void setWriteuptype(String writeuptype) {
		this.writeuptype = writeuptype;
	}

	public String getTermsConditionMessage() {
		return this.termsConditionMessage;
	}

	public void setTermsConditionMessage(String termsConditionMessage) {
		this.termsConditionMessage = termsConditionMessage;
	}

	public String getTermsConditionMessageAr() {
		return this.termsConditionMessageAr;
	}

	public void setTermsConditionMessageAr(String termsConditionMessageAr) {
		this.termsConditionMessageAr = termsConditionMessageAr;
	}

}
