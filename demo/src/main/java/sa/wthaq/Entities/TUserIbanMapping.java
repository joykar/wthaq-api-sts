package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the T_UserIbanMapping database table.
 * 
 */
@Entity
@Table(name="T_USERIBANMAPPING")
public class TUserIbanMapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int srlno;
	
//	@Column(name="bank_code")
//	private String bankCode;

	@Column(name="branch_name")
	private String branchName;

	@Column(name="create_by")
	private String createBy;

	@Column(name="create_on")
	private Timestamp createOn;

	@Column(name="delete_flag")
	private String deleteFlag;

	@Column(name="iban_no")
	private String ibanNo;

	@Column(name="login_id")
	private String loginId;

	@Column(name="modified_by")
	private String modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;

	@OneToOne
	@JoinColumn(name = "bank_code",insertable = false,updatable = false)
	private TBankmaster tBankmaster;

	public int getSrlno() {
		return this.srlno;
	}

	public void setSrlno(int srlno) {
		this.srlno = srlno;
	}

//	public String getBankCode() {
//		return this.bankCode;
//	}
//
//	public void setBankCode(String bankCode) {
//		this.bankCode = bankCode;
//	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getIbanNo() {
		return this.ibanNo;
	}

	public void setIbanNo(String ibanNo) {
		this.ibanNo = ibanNo;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public TBankmaster gettBankmaster() {
		return tBankmaster;
	}

	public void settBankmaster(TBankmaster tBankmaster) {
		this.tBankmaster = tBankmaster;
	}

}