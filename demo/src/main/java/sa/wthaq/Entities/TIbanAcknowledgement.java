package sa.wthaq.Entities;
import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the T_IbanAcknowledgement database table.
 * 
 */
@Entity
@Table(name="T_IBANACKNOWLEDGEMENT")
@NamedQuery(name="TIbanAcknowledgement.findAll", query="SELECT t FROM TIbanAcknowledgement t")
public class TIbanAcknowledgement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="bankmsg_id")
	private String bankmsgId;

	@Column(name="ack_date")
	private Timestamp ackDate;

	@Column(name="branch_name")
	private String branchName;

	@Column(name="login_id")
	private String loginId;

	@Column(name="message_desc")
	private String messageDesc;

	@Column(name="message_desc_ar")
	private String messageDescAr;

	//bi-directional many-to-one association to T_BankMaster
	@ManyToOne
	@JoinColumn(name="bank_code")
	private TBankmaster TBankMaster;

	//bi-directional many-to-one association to T_StatusCodeMaster
	@ManyToOne
	@JoinColumn(name="status_id")
	private TStatuscodemaster TStatusCodeMaster;

	public TIbanAcknowledgement() {
	}

	public String getBankmsgId() {
		return this.bankmsgId;
	}

	public void setBankmsgId(String bankmsgId) {
		this.bankmsgId = bankmsgId;
	}

	public Timestamp getAckDate() {
		return this.ackDate;
	}

	public void setAckDate(Timestamp ackDate) {
		this.ackDate = ackDate;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getMessageDesc() {
		return this.messageDesc;
	}

	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}

	public String getMessageDescAr() {
		return this.messageDescAr;
	}

	public void setMessageDescAr(String messageDescAr) {
		this.messageDescAr = messageDescAr;
	}

	public TBankmaster getTBankMaster() {
		return this.TBankMaster;
	}

	public void setTBankMaster(TBankmaster TBankMaster) {
		this.TBankMaster = TBankMaster;
	}

	public TStatuscodemaster getTStatusCodeMaster() {
		return this.TStatusCodeMaster;
	}

	public void setTStatusCodeMaster(TStatuscodemaster TStatusCodeMaster) {
		this.TStatusCodeMaster = TStatusCodeMaster;
	}

}