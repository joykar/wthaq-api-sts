package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the T_SUBSCRIPTION database table.
 * 
 */
@Entity
@Table(name="T_SUBSCRIPTION")
@NamedQuery(name="TSubscription.findAll", query="SELECT t FROM TSubscription t")
public class TSubscription implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SUBSCRIPTION_ID")
	private long subscriptionId;

	@Column(name="SUBSCRIPTION_NAME")
	private String subscriptionName;

	@Column(name="SUBSCRIPTION_TYPE")
	private String subscriptionType;

	//bi-directional many-to-one association to TServicecatalog
	@OneToMany(mappedBy="TSubscription")
	private List<TServicecatalog> TServicecatalogs;

	public TSubscription() {
	}

	public long getSubscriptionId() {
		return this.subscriptionId;
	}

	public void setSubscriptionId(long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getSubscriptionName() {
		return this.subscriptionName;
	}

	public void setSubscriptionName(String subscriptionName) {
		this.subscriptionName = subscriptionName;
	}

	public String getSubscriptionType() {
		return this.subscriptionType;
	}

	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public List<TServicecatalog> getTServicecatalogs() {
		return this.TServicecatalogs;
	}

	public void setTServicecatalogs(List<TServicecatalog> TServicecatalogs) {
		this.TServicecatalogs = TServicecatalogs;
	}

	public TServicecatalog addTServicecatalog(TServicecatalog TServicecatalog) {
		getTServicecatalogs().add(TServicecatalog);
		TServicecatalog.setTSubscription(this);

		return TServicecatalog;
	}

	public TServicecatalog removeTServicecatalog(TServicecatalog TServicecatalog) {
		getTServicecatalogs().remove(TServicecatalog);
		TServicecatalog.setTSubscription(null);

		return TServicecatalog;
	}

}