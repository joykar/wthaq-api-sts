package sa.wthaq.Entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="t_elgpaymentdetail")
public class TELGPaymentDetail  implements Serializable  {
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="payment_id")	
int	payment_id;
	
	@Column(name="payment_date")
Timestamp	payment_date;
	
	@Column(name="local_payment_datetime")
String	local_payment_datetime;
	
	@Column(name="request_id")
int	request_id;
	
	@Column(name="login_id")
String	login_id;
	
	@Column(name="payment_amount")
BigDecimal	payment_amount;
	
	@Column(name="currency_code")
String	currency_code;
	
	@Column(name="language")
String		language;
	
	@Column(name="entity_email")
String		entity_email;
	
	@Column(name="order_desc")
String		order_desc;
	
	@Column(name="command")
String		command;
	
	@Column(name="access_code")
String		access_code;
	
	@Column(name="merchant_identifier")
String		merchant_identifier;
	
	@Column(name="merchant_reference")
String		merchant_reference;
	
	@Column(name="signature")
String		signature;
	
	@Column(name="sharequestphrase")
String		SHARequestPhrase;
	
	@Column(name="SHAType")
String		SHAType;
	
	@Column(name="esponsemessage")
String		esponseMessage;
	
	@Column(name="payment_status")
String		payment_status;
	
	@Column(name="responsecode")
String		responseCode;

	
	
	
	
	
	
	
	
	
	
	
	public int getPayment_id() {
		return payment_id;
	}

	public void setPayment_id(int payment_id) {
		this.payment_id = payment_id;
	}

	public Timestamp getPayment_date() {
		return payment_date;
	}

	public void setPayment_date(Timestamp payment_date) {
		this.payment_date = payment_date;
	}

	public String getLocal_payment_datetime() {
		return local_payment_datetime;
	}

	public void setLocal_payment_datetime(String local_payment_datetime) {
		this.local_payment_datetime = local_payment_datetime;
	}

	public int getRequest_id() {
		return request_id;
	}

	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public BigDecimal getPayment_amount() {
		return payment_amount;
	}

	public void setPayment_amount(BigDecimal payment_amount) {
		this.payment_amount = payment_amount;
	}

	public String getCurrency_code() {
		return currency_code;
	}

	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getEntity_email() {
		return entity_email;
	}

	public void setEntity_email(String entity_email) {
		this.entity_email = entity_email;
	}

	public String getOrder_desc() {
		return order_desc;
	}

	public void setOrder_desc(String order_desc) {
		this.order_desc = order_desc;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getAccess_code() {
		return access_code;
	}

	public void setAccess_code(String access_code) {
		this.access_code = access_code;
	}

	public String getMerchant_identifier() {
		return merchant_identifier;
	}

	public void setMerchant_identifier(String merchant_identifier) {
		this.merchant_identifier = merchant_identifier;
	}

	public String getMerchant_reference() {
		return merchant_reference;
	}

	public void setMerchant_reference(String merchant_reference) {
		this.merchant_reference = merchant_reference;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getSHARequestPhrase() {
		return SHARequestPhrase;
	}

	public void setSHARequestPhrase(String sHARequestPhrase) {
		SHARequestPhrase = sHARequestPhrase;
	}

	public String getSHAType() {
		return SHAType;
	}

	public void setSHAType(String sHAType) {
		SHAType = sHAType;
	}

	public String getEsponseMessage() {
		return esponseMessage;
	}

	public void setEsponseMessage(String esponseMessage) {
		this.esponseMessage = esponseMessage;
	}

	public String getPayment_status() {
		return payment_status;
	}

	public void setPayment_status(String payment_status) {
		this.payment_status = payment_status;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	
	
	
	
	
	
	
	
	
	
	
}
