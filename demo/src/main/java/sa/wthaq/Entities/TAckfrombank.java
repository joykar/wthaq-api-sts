package sa.wthaq.Entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The persistent class for the T_ACKFROMBANK database table.
 * 
 */
@Entity
@Table(name="T_ACKFROMBANK")
@NamedQuery(name="TAckfrombank.findAll", query="SELECT t FROM TAckfrombank t")
public class TAckfrombank implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REQUEST_ID")
	private long requestId;

	@Column(name="ACK_DATE")
	private Timestamp ackDate;

	@Column(name="BANKMSG_ID")
	private String bankmsgId;

	@Column(name="BRANCH_NAME")
	private String branchName;

	@Column(name="MESSAGE_DESC")
	private String messageDesc;

	@Column(name="MESSAGE_DESC_AR")
	private String messageDescAr;
	
	@Column(name="BANK_CODE")
	private String bankCode;


	//bi-directional many-to-one association to TBankmaster
	/*
	 * @JsonIgnoreProperties("TAckfrombanks")
	 * 
	 * @ManyToOne( fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name="BANK_CODE") private TBankmaster TBankmaster;
	 */
	//bi-directional many-to-one association to TStatuscodemaster
	@ManyToOne
	@JoinColumn(name="STATUS_ID")
	private TStatuscodemaster TStatuscodemaster;

	public TAckfrombank() {
	}

	public long getRequestId() {
		return this.requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	public Timestamp getAckDate() {
		return this.ackDate;
	}

	public void setAckDate(Timestamp ackDate) {
		this.ackDate = ackDate;
	}

	public String getBankmsgId() {
		return this.bankmsgId;
	}

	public void setBankmsgId(String bankmsgId) {
		this.bankmsgId = bankmsgId;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getMessageDesc() {
		return this.messageDesc;
	}

	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}

	public String getMessageDescAr() {
		return this.messageDescAr;
	}

	public void setMessageDescAr(String messageDescAr) {
		this.messageDescAr = messageDescAr;
	}

	/*
	 * public TBankmaster getTBankmaster() { return this.TBankmaster; }
	 * 
	 * public void setTBankmaster(TBankmaster TBankmaster) { this.TBankmaster =
	 * TBankmaster; }
	 */

	public TStatuscodemaster getTStatuscodemaster() {
		return this.TStatuscodemaster;
	}

	public void setTStatuscodemaster(TStatuscodemaster TStatuscodemaster) {
		this.TStatuscodemaster = TStatuscodemaster;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	
	

}