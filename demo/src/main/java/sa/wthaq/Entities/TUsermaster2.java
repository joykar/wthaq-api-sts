package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the T_USERMASTER database table.
 * 
 */
@Entity
@Table(name="T_USERMASTER")
public class TUsermaster2 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="LOGIN_ID")
	private String loginId;

	@Column(name="CREATE_BY",insertable = false,updatable = false)
	private String createBy;

	@Column(name="CREATE_ON",insertable = false,updatable = false)
	private Timestamp createOn;

	@Column(name="DELETE_FLAG",insertable = false,updatable = false)
	private String deleteFlag;

	@Column(name="\"LANGUAGE\"")
	private String language;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	private String password;

	@Column(name="ROLE_ID")
	private int roleId;

	@Column(name="USER_ID")
	private BigDecimal userId;

	@Column(name="USER_NAME")
	private String userName;

	@Column(name="USER_NAME_AR")
	private String userNameAr;

	@Column(name="USER_STATUS")
	private String userStatus;

	public TUsermaster2() {
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateOn() {
		return createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public BigDecimal getUserId() {
		return userId;
	}

	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserNameAr() {
		return userNameAr;
	}

	public void setUserNameAr(String userNameAr) {
		this.userNameAr = userNameAr;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
}