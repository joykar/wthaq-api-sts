package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t_UnRegisteredBen database table.
 * 
 */
@Entity
@Table(name="T_UNREGISTEREDBEN")
@NamedQuery(name="TUnRegisteredBen.findAll", query="SELECT t FROM TUnRegisteredBen t")
public class TUnRegisteredBen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="beneficiary_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int beneficiaryId;

	@Column(name="beneficiary_name")
	private String beneficiaryName;

	private String beneficiary_UID;

	@Column(name="deleted_flag")
	private String deletedFlag;

	public TUnRegisteredBen() {
	}

	public int getBeneficiaryId() {
		return this.beneficiaryId;
	}

	public void setBeneficiaryId(int beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}

	public String getBeneficiaryName() {
		return this.beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiary_UID() {
		return this.beneficiary_UID;
	}

	public void setBeneficiary_UID(String beneficiary_UID) {
		this.beneficiary_UID = beneficiary_UID;
	}

	public String getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
}