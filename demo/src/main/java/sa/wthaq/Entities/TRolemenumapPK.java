package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the T_ROLEMENUMAP database table.
 * 
 */
@Embeddable
public class TRolemenumapPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ROLE_ID")
	private long roleId;

	@Column(name="MENU_ID")
	private long menuId;

	public TRolemenumapPK() {
	}
	public long getRoleId() {
		return this.roleId;
	}
	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}
	public long getMenuId() {
		return this.menuId;
	}
	public void setMenuId(long menuId) {
		this.menuId = menuId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TRolemenumapPK)) {
			return false;
		}
		TRolemenumapPK castOther = (TRolemenumapPK)other;
		return 
			(this.roleId == castOther.roleId)
			&& (this.menuId == castOther.menuId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.roleId ^ (this.roleId >>> 32)));
		hash = hash * prime + ((int) (this.menuId ^ (this.menuId >>> 32)));
		
		return hash;
	}
}