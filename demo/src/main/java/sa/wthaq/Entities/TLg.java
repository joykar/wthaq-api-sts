package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Nationalized;


import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the T_LG database table.
 * 
 */
@Entity
@Table(name="T_LG")
@NamedQuery(name="TLg.findAll", query="SELECT t FROM TLg t")
public class TLg implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CASE_ID")
	private long caseId;

	@Column(name="ACTIVE_STATUS")
	private String activeStatus;

	@Column(name="BYAN_NO")
	private String byanNo;

	@Column(name="CONTRACT_VALUE")
	private BigDecimal contractValue;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="CURRENCY_CODE")
	private String currencyCode;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="IS_ASSIGNABLE_FLAG")
	private String isAssignableFlag;

	@Column(name="IS_AUTORENEWABLE_FLAG")
	private String isAutorenewableFlag;

	@Column(name="IS_OPEN_ENDED_FLAG")
	private String isOpenEndedFlag;

	@Column(name="IS_TRANSFERABLE_FLAG")
	private String isTransferableFlag;

	@Column(name="LG_DOC_NAME")
	private String lgDocName;

	@Column(name="LG_DOC_PATH")
	private String lgDocPath;

	@Column(name="LG_ISSUE_DATE")
	private Timestamp lgIssueDate;

	@Column(name="LG_ISSUED_BY")
	private String lgIssuedBy;

	@Column(name="LG_NO")
	private String lgNo;

	@Column(name="LG_VALIDITY_DATE")
	private Timestamp lgValidityDate;

	@Column(name="LG_VALUE")
	private BigDecimal lgValue;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="PERCENTAGE_OF_VALUE")
	private BigDecimal percentageOfValue;

	@Column(name="PROJECT_ID")
	private String projectId;

	@Column(name="PROJECT_NAME")
	private String projectName;

	@Column(name="PROJECT_NAME_AR")
	private String projectNameAr;

	@Column(name="REQUEST_ID")
	private String requestId;

	@Column(name="SUPPLIER_ID")
	private BigDecimal supplierId;

	@Column(name="TERMS_CONDITION_TYPE")
	private String termsConditionType;

	@Column(name="ZAKAT_END_DATE")
	private Timestamp zakatEndDate;

	@Column(name="ZAKAT_START_DATE")
	private Timestamp zakatStartDate;
	
	@Nationalized
	@Column(name="terms_condition_message")
	private String termsConditionMessage;
	
	@Column(name="beneficiary_id")
	private int beneficiaryId;
	
	@Column(name="role_id")
	private int roleId;

	//bi-directional many-to-one association to TBeneficiarydetail
	//@ManyToOne
	//@JoinColumn(name="BENEFICIARY_ID",insertable = false,updatable = false)
	//private TBeneficiarydetail TBeneficiarydetail;

	//bi-directional many-to-one association to TLgtypemaster
	@ManyToOne
	@JoinColumn(name="LG_TYPE_ID")
	private TLgtypemaster TLgtypemaster;

	//bi-directional many-to-one association to TLgdetail
	@OneToMany(mappedBy="TLg")
	private List<TLgdetail> TLgdetails;

	//bi-directional one-to-one association to TLgonboarding
	//@OneToOne(mappedBy="TLg1")
	//private TLgonboarding TLgonboarding1;

	//bi-directional one-to-one association to TLgonboarding
	//@OneToOne(mappedBy="TLg2")
	//private TLgonboarding TLgonboarding2;

	//bi-directional one-to-one association to TLgrequestamend
	//@OneToOne(mappedBy="TLg")
	//private TLgrequestamend TLgrequestamend;

	//bi-directional many-to-one association to TLgrequestconfiscate
	//@OneToMany(mappedBy="TLg")
	//private List<TLgrequestconfiscate> TLgrequestconfiscates;

	//bi-directional one-to-one association to TLgrequestissue
	/*
	 * @OneToOne(mappedBy="TLg") private TLgrequestissue TLgrequestissue;
	 */

	//bi-directional one-to-one association to TLgrequestrelease
	//@OneToOne(mappedBy="TLg")
	//private TLgrequestrelease TLgrequestrelease;

	public TLg() {
	}

	public long getCaseId() {
		return this.caseId;
	}

	public void setCaseId(long caseId) {
		this.caseId = caseId;
	}

	public String getActiveStatus() {
		return this.activeStatus;
	}

	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getByanNo() {
		return this.byanNo;
	}

	public void setByanNo(String byanNo) {
		this.byanNo = byanNo;
	}

	public BigDecimal getContractValue() {
		return this.contractValue;
	}

	public void setContractValue(BigDecimal contractValue) {
		this.contractValue = contractValue;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getIsAssignableFlag() {
		return this.isAssignableFlag;
	}

	public void setIsAssignableFlag(String isAssignableFlag) {
		this.isAssignableFlag = isAssignableFlag;
	}

	public String getIsAutorenewableFlag() {
		return this.isAutorenewableFlag;
	}

	public void setIsAutorenewableFlag(String isAutorenewableFlag) {
		this.isAutorenewableFlag = isAutorenewableFlag;
	}

	public String getIsOpenEndedFlag() {
		return this.isOpenEndedFlag;
	}

	public void setIsOpenEndedFlag(String isOpenEndedFlag) {
		this.isOpenEndedFlag = isOpenEndedFlag;
	}

	public String getIsTransferableFlag() {
		return this.isTransferableFlag;
	}

	public void setIsTransferableFlag(String isTransferableFlag) {
		this.isTransferableFlag = isTransferableFlag;
	}

	public String getLgDocName() {
		return this.lgDocName;
	}

	public void setLgDocName(String lgDocName) {
		this.lgDocName = lgDocName;
	}

	public String getLgDocPath() {
		return this.lgDocPath;
	}

	public void setLgDocPath(String lgDocPath) {
		this.lgDocPath = lgDocPath;
	}

	public Timestamp getLgIssueDate() {
		return this.lgIssueDate;
	}

	public void setLgIssueDate(Timestamp lgIssueDate) {
		this.lgIssueDate = lgIssueDate;
	}

	public String getLgIssuedBy() {
		return this.lgIssuedBy;
	}

	public void setLgIssuedBy(String lgIssuedBy) {
		this.lgIssuedBy = lgIssuedBy;
	}

	public String getLgNo() {
		return this.lgNo;
	}

	public void setLgNo(String lgNo) {
		this.lgNo = lgNo;
	}

	public Timestamp getLgValidityDate() {
		return this.lgValidityDate;
	}

	public void setLgValidityDate(Timestamp lgValidityDate) {
		this.lgValidityDate = lgValidityDate;
	}

	public BigDecimal getLgValue() {
		return this.lgValue;
	}

	public void setLgValue(BigDecimal lgValue) {
		this.lgValue = lgValue;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public BigDecimal getPercentageOfValue() {
		return this.percentageOfValue;
	}

	public void setPercentageOfValue(BigDecimal percentageOfValue) {
		this.percentageOfValue = percentageOfValue;
	}

	public String getProjectId() {
		return this.projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return this.projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectNameAr() {
		return this.projectNameAr;
	}

	public void setProjectNameAr(String projectNameAr) {
		this.projectNameAr = projectNameAr;
	}

	public String getRequestId() {
		return this.requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public BigDecimal getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(BigDecimal supplierId) {
		this.supplierId = supplierId;
	}

	public String getTermsConditionType() {
		return this.termsConditionType;
	}

	public void setTermsConditionType(String termsConditionType) {
		this.termsConditionType = termsConditionType;
	}

	public Timestamp getZakatEndDate() {
		return this.zakatEndDate;
	}

	public void setZakatEndDate(Timestamp zakatEndDate) {
		this.zakatEndDate = zakatEndDate;
	}

	public Timestamp getZakatStartDate() {
		return this.zakatStartDate;
	}

	public void setZakatStartDate(Timestamp zakatStartDate) {
		this.zakatStartDate = zakatStartDate;
	}

	/*
	 * public TBeneficiarydetail getTBeneficiarydetail() { return
	 * this.TBeneficiarydetail; }
	 * 
	 * public void setTBeneficiarydetail(TBeneficiarydetail TBeneficiarydetail) {
	 * this.TBeneficiarydetail = TBeneficiarydetail; }
	 */
	
	

	public TLgtypemaster getTLgtypemaster() {
		return this.TLgtypemaster;
	}

	public int getBeneficiaryId() {
		return beneficiaryId;
	}

	public void setBeneficiaryId(int beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}

	public void setTLgtypemaster(TLgtypemaster TLgtypemaster) {
		this.TLgtypemaster = TLgtypemaster;
	}

	public List<TLgdetail> getTLgdetails() {
		return this.TLgdetails;
	}

	public void setTLgdetails(List<TLgdetail> TLgdetails) {
		this.TLgdetails = TLgdetails;
	}

	public TLgdetail addTLgdetail(TLgdetail TLgdetail) {
		getTLgdetails().add(TLgdetail);
		TLgdetail.setTLg(this);

		return TLgdetail;
	}

	public TLgdetail removeTLgdetail(TLgdetail TLgdetail) {
		getTLgdetails().remove(TLgdetail);
		TLgdetail.setTLg(null);

		return TLgdetail;
	}

	public String getTermsConditionMessage() {
		return termsConditionMessage;
	}

	public void setTermsConditionMessage(String termsConditionMessage) {
		this.termsConditionMessage = termsConditionMessage;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	
	
	

	/*
	 * public TLgonboarding getTLgonboarding1() { return this.TLgonboarding1; }
	 * 
	 * public void setTLgonboarding1(TLgonboarding TLgonboarding1) {
	 * this.TLgonboarding1 = TLgonboarding1; }
	 * 
	 * public TLgonboarding getTLgonboarding2() { return this.TLgonboarding2; }
	 * 
	 * public void setTLgonboarding2(TLgonboarding TLgonboarding2) {
	 * this.TLgonboarding2 = TLgonboarding2; }
	 * 
	 * public TLgrequestamend getTLgrequestamend() { return this.TLgrequestamend; }
	 */

	/*
	 * public void setTLgrequestamend(TLgrequestamend TLgrequestamend) {
	 * this.TLgrequestamend = TLgrequestamend; }
	 * 
	 * public List<TLgrequestconfiscate> getTLgrequestconfiscates() { return
	 * this.TLgrequestconfiscates; }
	 * 
	 * public void setTLgrequestconfiscates(List<TLgrequestconfiscate>
	 * TLgrequestconfiscates) { this.TLgrequestconfiscates = TLgrequestconfiscates;
	 * }
	 */

	/*
	 * public TLgrequestconfiscate addTLgrequestconfiscate(TLgrequestconfiscate
	 * TLgrequestconfiscate) { getTLgrequestconfiscates().add(TLgrequestconfiscate);
	 * TLgrequestconfiscate.setTLg(this);
	 * 
	 * return TLgrequestconfiscate; }
	 */

	/*
	 * public TLgrequestconfiscate removeTLgrequestconfiscate(TLgrequestconfiscate
	 * TLgrequestconfiscate) {
	 * getTLgrequestconfiscates().remove(TLgrequestconfiscate);
	 * TLgrequestconfiscate.setTLg(null);
	 * 
	 * return TLgrequestconfiscate; }
	 */
	/*
	 * public TLgrequestissue getTLgrequestissue() { return this.TLgrequestissue; }
	 * 
	 * public void setTLgrequestissue(TLgrequestissue TLgrequestissue) {
	 * this.TLgrequestissue = TLgrequestissue; }
	 */

	/*
	 * public TLgrequestrelease getTLgrequestrelease() { return
	 * this.TLgrequestrelease; }
	 * 
	 * public void setTLgrequestrelease(TLgrequestrelease TLgrequestrelease) {
	 * this.TLgrequestrelease = TLgrequestrelease; }
	 */
	

}