package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the T_PAYMENTSUBSCRIPTION database table.
 * 
 */
@Entity
@Table(name="T_PAYMENTSUBSCRIPTION")
@NamedQuery(name="TPaymentsubscription.findAll", query="SELECT t FROM TPaymentsubscription t")
public class TPaymentsubscription implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TPaymentsubscriptionPK id;

	@Column(name="BALANCE_AMEND_TRAN")
	private BigDecimal balanceAmendTran;

	@Column(name="BALANCE_ISSUE_TRAN")
	private BigDecimal balanceIssueTran;

	@Column(name="NO_OF_AMEND_TRAN")
	private BigDecimal noOfAmendTran;

	@Column(name="NO_OF_ISSUE_TRAN")
	private BigDecimal noOfIssueTran;

	@Column(name="PAYMENT_AMT")
	private BigDecimal paymentAmt;

	@Column(name="PAYMENT_METHOD")
	private String paymentMethod;

	@Column(name="ROLE_ID")
	private BigDecimal roleId;

	@Column(name="VALID_FROM")
	private Timestamp validFrom;

	@Column(name="VALID_TO")
	private Timestamp validTo;

	//bi-directional many-to-one association to TServicecatalog
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="FROM_DATE", referencedColumnName="FROM_DATE"),
		@JoinColumn(name="SERVICE_CATALOG_ID", referencedColumnName="SERVICE_CATALOG_ID")
		})
	private TServicecatalog TServicecatalog;

	//bi-directional many-to-one association to TUsermaster
	@ManyToOne
	@JoinColumn(name="LOGIN_ID", insertable=false, updatable=false)
	private TUsermaster TUsermaster;

	public TPaymentsubscription() {
	}

	public TPaymentsubscriptionPK getId() {
		return this.id;
	}

	public void setId(TPaymentsubscriptionPK id) {
		this.id = id;
	}

	public BigDecimal getBalanceAmendTran() {
		return this.balanceAmendTran;
	}

	public void setBalanceAmendTran(BigDecimal balanceAmendTran) {
		this.balanceAmendTran = balanceAmendTran;
	}

	public BigDecimal getBalanceIssueTran() {
		return this.balanceIssueTran;
	}

	public void setBalanceIssueTran(BigDecimal balanceIssueTran) {
		this.balanceIssueTran = balanceIssueTran;
	}

	public BigDecimal getNoOfAmendTran() {
		return this.noOfAmendTran;
	}

	public void setNoOfAmendTran(BigDecimal noOfAmendTran) {
		this.noOfAmendTran = noOfAmendTran;
	}

	public BigDecimal getNoOfIssueTran() {
		return this.noOfIssueTran;
	}

	public void setNoOfIssueTran(BigDecimal noOfIssueTran) {
		this.noOfIssueTran = noOfIssueTran;
	}

	public BigDecimal getPaymentAmt() {
		return this.paymentAmt;
	}

	public void setPaymentAmt(BigDecimal paymentAmt) {
		this.paymentAmt = paymentAmt;
	}

	public String getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public BigDecimal getRoleId() {
		return this.roleId;
	}

	public void setRoleId(BigDecimal roleId) {
		this.roleId = roleId;
	}

	public Timestamp getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Timestamp validFrom) {
		this.validFrom = validFrom;
	}

	public Timestamp getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Timestamp validTo) {
		this.validTo = validTo;
	}

	public TServicecatalog getTServicecatalog() {
		return this.TServicecatalog;
	}

	public void setTServicecatalog(TServicecatalog TServicecatalog) {
		this.TServicecatalog = TServicecatalog;
	}

	public TUsermaster getTUsermaster() {
		return this.TUsermaster;
	}

	public void setTUsermaster(TUsermaster TUsermaster) {
		this.TUsermaster = TUsermaster;
	}

}