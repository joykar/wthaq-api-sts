package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the T_LGREQUESTCONFISCATE database table.
 * 
 */
@Entity
@Table(name="T_LGREQUESTCONFISCATE")
@NamedQuery(name="TLgrequestconfiscate.findAll", query="SELECT t FROM TLgrequestconfiscate t")
public class TLgrequestconfiscate implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name="case_id")
	private int caseId;
	
	
	@Column(name="ACTIVE_STATUS")
	private String activeStatus;

	@Column(name="BANK_CODE")
	private String bankCode;

	@Column(name="BRANCH_NAME")
	private String branchName;

	@Column(name="CONFISCATE_REASON")
	private String confiscateReason;

	@Column(name="CONFISCATE_REASON_AR")
	private String confiscateReasonAr;

	@Column(name="CONTRACT_VALUE")
	private BigDecimal contractValue;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="CRN_NO")
	private String crnNo;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="IS_ASSIGNABLE_FLAG")
	private String isAssignableFlag;

	@Column(name="IS_AUTORENEWABLE_FLAG")
	private String isAutorenewableFlag;

	@Column(name="IS_OPEN_ENDED_FLAG")
	private String isOpenEndedFlag;

	@Column(name="IS_TRANSFERABLE_FLAG")
	private String isTransferableFlag;

	@Column(name="LG_AMEND_AMT")
	private BigDecimal lgAmendAmt;

	@Column(name="LG_CONFISCATE_AMT")
	private BigDecimal lgConfiscateAmt;

	@Column(name="LG_DATE")
	private Timestamp lgDate;

	@Column(name="LG_ISSUED_BY")
	private String lgIssuedBy;

	@Column(name="LG_NO")
	private String lgNo;

	@Column(name="LG_RELEASE_AMT")
	private BigDecimal lgReleaseAmt;

	@Column(name="LG_TYPE")
	private String lgType;

	@Column(name="LG_VALIDITY_DATE")
	private Timestamp lgValidityDate;

	@Column(name="LG_VALUE")
	private BigDecimal lgValue;

	@Column(name="LOGIN_ID")
	private String loginId;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="PERCENTAGE_OF_VALUE")
	private BigDecimal percentageOfValue;

	@Column(name="PROJECT_ID")
	private String projectId;

	@Column(name="PROJECT_NAME")
	private String projectName;

	@Column(name="PROJECT_NAME_AR")
	private String projectNameAr;

	@Column(name="REQUEST_ID")
	private BigDecimal requestId;

	@Column(name="ROLE_ID")
	private BigDecimal roleId;

	@Column(name="TERMS_CONDITION_TYPE")
	private String termsConditionType;

	@Column(name="TRANS_ID")
	private String transId;

	//bi-directional many-to-one association to TBeneficiarydetail
	@ManyToOne
	@JoinColumn(name="BENEFICIARY_ID",insertable = false,updatable = false)
	private TBeneficiarydetail TBeneficiarydetail;

	//bi-directional many-to-one association to TLg
	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="CASE_ID", insertable=false, updatable=false) private TLg
	 * TLg;
	 */
	//bi-directional many-to-one association to TSupplierdetail
	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="SUPPLIER_ID") private TSupplierdetail TSupplierdetail;
	 */

	public TLgrequestconfiscate() {
	}

	
	public int getCaseId() {
		return this.caseId;
	}

	public void setCaseId(int caseId) {
		this.caseId = caseId;
	}
	
	
	public String getActiveStatus() {
		return this.activeStatus;
	}

	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getBankCode() {
		return this.bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getConfiscateReason() {
		return this.confiscateReason;
	}

	public void setConfiscateReason(String confiscateReason) {
		this.confiscateReason = confiscateReason;
	}

	public String getConfiscateReasonAr() {
		return this.confiscateReasonAr;
	}

	public void setConfiscateReasonAr(String confiscateReasonAr) {
		this.confiscateReasonAr = confiscateReasonAr;
	}

	public BigDecimal getContractValue() {
		return this.contractValue;
	}

	public void setContractValue(BigDecimal contractValue) {
		this.contractValue = contractValue;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getCrnNo() {
		return this.crnNo;
	}

	public void setCrnNo(String crnNo) {
		this.crnNo = crnNo;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getIsAssignableFlag() {
		return this.isAssignableFlag;
	}

	public void setIsAssignableFlag(String isAssignableFlag) {
		this.isAssignableFlag = isAssignableFlag;
	}

	public String getIsAutorenewableFlag() {
		return this.isAutorenewableFlag;
	}

	public void setIsAutorenewableFlag(String isAutorenewableFlag) {
		this.isAutorenewableFlag = isAutorenewableFlag;
	}

	public String getIsOpenEndedFlag() {
		return this.isOpenEndedFlag;
	}

	public void setIsOpenEndedFlag(String isOpenEndedFlag) {
		this.isOpenEndedFlag = isOpenEndedFlag;
	}

	public String getIsTransferableFlag() {
		return this.isTransferableFlag;
	}

	public void setIsTransferableFlag(String isTransferableFlag) {
		this.isTransferableFlag = isTransferableFlag;
	}

	public BigDecimal getLgAmendAmt() {
		return this.lgAmendAmt;
	}

	public void setLgAmendAmt(BigDecimal lgAmendAmt) {
		this.lgAmendAmt = lgAmendAmt;
	}

	public BigDecimal getLgConfiscateAmt() {
		return this.lgConfiscateAmt;
	}

	public void setLgConfiscateAmt(BigDecimal lgConfiscateAmt) {
		this.lgConfiscateAmt = lgConfiscateAmt;
	}

	public Timestamp getLgDate() {
		return this.lgDate;
	}

	public void setLgDate(Timestamp lgDate) {
		this.lgDate = lgDate;
	}

	public String getLgIssuedBy() {
		return this.lgIssuedBy;
	}

	public void setLgIssuedBy(String lgIssuedBy) {
		this.lgIssuedBy = lgIssuedBy;
	}

	public String getLgNo() {
		return this.lgNo;
	}

	public void setLgNo(String lgNo) {
		this.lgNo = lgNo;
	}

	public BigDecimal getLgReleaseAmt() {
		return this.lgReleaseAmt;
	}

	public void setLgReleaseAmt(BigDecimal lgReleaseAmt) {
		this.lgReleaseAmt = lgReleaseAmt;
	}

	public String getLgType() {
		return this.lgType;
	}

	public void setLgType(String lgType) {
		this.lgType = lgType;
	}

	public Timestamp getLgValidityDate() {
		return this.lgValidityDate;
	}

	public void setLgValidityDate(Timestamp lgValidityDate) {
		this.lgValidityDate = lgValidityDate;
	}

	public BigDecimal getLgValue() {
		return this.lgValue;
	}

	public void setLgValue(BigDecimal lgValue) {
		this.lgValue = lgValue;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public BigDecimal getPercentageOfValue() {
		return this.percentageOfValue;
	}

	public void setPercentageOfValue(BigDecimal percentageOfValue) {
		this.percentageOfValue = percentageOfValue;
	}

	public String getProjectId() {
		return this.projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return this.projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectNameAr() {
		return this.projectNameAr;
	}

	public void setProjectNameAr(String projectNameAr) {
		this.projectNameAr = projectNameAr;
	}

	public BigDecimal getRequestId() {
		return this.requestId;
	}

	public void setRequestId(BigDecimal requestId) {
		this.requestId = requestId;
	}

	public BigDecimal getRoleId() {
		return this.roleId;
	}

	public void setRoleId(BigDecimal roleId) {
		this.roleId = roleId;
	}

	public String getTermsConditionType() {
		return this.termsConditionType;
	}

	public void setTermsConditionType(String termsConditionType) {
		this.termsConditionType = termsConditionType;
	}

	public String getTransId() {
		return this.transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public TBeneficiarydetail getTBeneficiarydetail() {
		return this.TBeneficiarydetail;
	}

	public void setTBeneficiarydetail(TBeneficiarydetail TBeneficiarydetail) {
		this.TBeneficiarydetail = TBeneficiarydetail;
	}

	/*
	 * public TLg getTLg() { return this.TLg; }
	 * 
	 * public void setTLg(TLg TLg) { this.TLg = TLg; }
	 */

	/*
	 * public TSupplierdetail getTSupplierdetail() { return this.TSupplierdetail; }
	 * 
	 * public void setTSupplierdetail(TSupplierdetail TSupplierdetail) {
	 * this.TSupplierdetail = TSupplierdetail; }
	 */

}