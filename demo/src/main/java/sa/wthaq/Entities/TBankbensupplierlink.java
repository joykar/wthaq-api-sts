package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the T_BANKBENSUPPLIERLINK database table.
 * 
 */
@Entity
@Table(name="T_BANKBENSUPPLIERLINK")
@NamedQuery(name="TBankbensupplierlink.findAll", query="SELECT t FROM TBankbensupplierlink t")
public class TBankbensupplierlink implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TBankbensupplierlinkPK id;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="IBAN_NO")
	private BigDecimal ibanNo;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;
	
	


	//bi-directional many-to-one association to TBankmaster
	/*
	 * @JsonIgnoreProperties("TBankbensupplierlinks")
	 * 
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name="BANK_CODE", insertable=false, updatable=false) private
	 * TBankmaster TBankmaster;
	 */

	public TBankbensupplierlink() {
	}

	public TBankbensupplierlinkPK getId() {
		return this.id;
	}

	public void setId(TBankbensupplierlinkPK id) {
		this.id = id;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public BigDecimal getIbanNo() {
		return this.ibanNo;
	}

	public void setIbanNo(BigDecimal ibanNo) {
		this.ibanNo = ibanNo;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Object getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	
	
	

	/*
	 * public TBankmaster getTBankmaster() { return this.TBankmaster; }
	 * 
	 * public void setTBankmaster(TBankmaster TBankmaster) { this.TBankmaster =
	 * TBankmaster; }
	 */

}