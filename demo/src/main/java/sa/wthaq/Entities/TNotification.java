package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the T_NOTIFICATION database table.
 * 
 */
@Entity
@Table(name="T_NOTIFICATION")
@NamedQuery(name="TNotification.findAll", query="SELECT t FROM TNotification t")
public class TNotification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REQUEST_ID")
	private long requestId;

	@Column(name="BANKMSG_ID")
	private String bankmsgId;

	@Column(name="BRANCH_NAME")
	private String branchName;
	
	@Column(name="BANK_CODE")
	private String bankCode;


	@Column(name="BRANCH_NAME_AR")
	private String branchNameAr;

	@Column(name="MESSAGE_DATE")
	private String messageDate;

	@Column(name="MESSAGE_DESC")
	private String messageDesc;

	@Column(name="MESSAGE_DESC_AR")
	private String messageDescAr;

	@Column(name="REQUEST_DATE")
	private Timestamp requestDate;

	@Column(name="ROLE_ID")
	private BigDecimal roleId;

	//bi-directional many-to-one association to TBankmaster
	/*
	 * @JsonIgnoreProperties("TNotifications")
	 * 
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name="BANK_CODE") private TBankmaster TBankmaster;
	 */

	//bi-directional many-to-one association to TUsermaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="LOGIN_ID",insertable = false,updatable = false)
	private TUsermaster TUsermaster;

	//bi-directional one-to-one association to TServicerequest
	/*
	 * @OneToOne(mappedBy="TNotification",fetch = FetchType.LAZY) private
	 * TServicerequest TServicerequest;
	 */

	public TNotification() {
	}

	public long getRequestId() {
		return this.requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	public String getBankmsgId() {
		return this.bankmsgId;
	}

	public void setBankmsgId(String bankmsgId) {
		this.bankmsgId = bankmsgId;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchNameAr() {
		return this.branchNameAr;
	}

	public void setBranchNameAr(String branchNameAr) {
		this.branchNameAr = branchNameAr;
	}

	public String getMessageDate() {
		return this.messageDate;
	}

	public void setMessageDate(String messageDate) {
		this.messageDate = messageDate;
	}

	public String getMessageDesc() {
		return this.messageDesc;
	}

	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}

	public String getMessageDescAr() {
		return this.messageDescAr;
	}

	public void setMessageDescAr(String messageDescAr) {
		this.messageDescAr = messageDescAr;
	}

	public Timestamp getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Timestamp requestDate) {
		this.requestDate = requestDate;
	}

	public BigDecimal getRoleId() {
		return this.roleId;
	}

	public void setRoleId(BigDecimal roleId) {
		this.roleId = roleId;
	}

	/*
	 * public TBankmaster getTBankmaster() { return this.TBankmaster; }
	 * 
	 * public void setTBankmaster(TBankmaster TBankmaster) { this.TBankmaster =
	 * TBankmaster; }
	 */

	public TUsermaster getTUsermaster() {
		return this.TUsermaster;
	}

	public void setTUsermaster(TUsermaster TUsermaster) {
		this.TUsermaster = TUsermaster;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	/*
	 * public TServicerequest getTServicerequest() { return this.TServicerequest; }
	 * 
	 * public void setTServicerequest(TServicerequest TServicerequest) {
	 * this.TServicerequest = TServicerequest; }
	 */
	
	

}