package sa.wthaq.Entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the T_BENEFICIARYDETAILS database table.
 * 
 */
@Entity
@Table(name="T_BENEFICIARYDETAILS")
public class TBeneficiarydetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BENEFICIARY_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long beneficiaryId;

	@Column(name="AGREE_TAG")
	private String agreeTag;

	@Column(name="BEN_STATUS_FLAG")
	private String benStatusFlag;

	@Column(name="BENEFICIARY_ADDR1")
	private String beneficiaryAddr1;

	@Column(name="BENEFICIARY_ADDR1_AR")
	private String beneficiaryAddr1Ar;

	@Column(name="BENEFICIARY_ADDR2")
	private String beneficiaryAddr2;

	@Column(name="BENEFICIARY_ADDR2_AR")
	private String beneficiaryAddr2Ar;

	@Column(name="BENEFICIARY_NAME")
	private String beneficiaryName;

	@Column(name="BENEFICIARY_NAME_AR")
	private String beneficiaryNameAr;

	@Column(name="BENEFICIARY_REG_DATE")
	private Timestamp beneficiaryRegDate;

	@Column(name="BENEFICIARY_UID")
	private String beneficiaryUid;

	@Column(name="CONTACT_INFO")
	private String contactInfo;

	@Column(name="COUNTRY")
	private String country;
	
	@Column(name="CITY")
	private String city;

	@Column(name="CREATE_BY",insertable = false,updatable = false)
	private String createBy;

	@Column(name="CREATE_ON",insertable = false,updatable = false)
	private Timestamp createOn;

	@Column(name="DELETE_FLAG",insertable = false,updatable = false)
	private String deleteFlag;

	@Column(name="LEG_AGREE_DOC_NAME")
	private String legAgreeDocName;

	@Column(name="LEG_AGREE_DOC_PATH")
	private String legAgreeDocPath;

	@Column(name="LEG_AUTH_DOC_NAME")
	private String legAuthDocName;

	@Column(name="LEG_AUTH_DOC_PATH")
	private String legAuthDocPath;

	@Column(name="LOGIN_ID")
	private String loginId;
	
	@Column(name="NATIONAL_ID")
	private String nationalid;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="NATIONAL_ID_DOC_NAME")
	private String nationalIdDocName;

	@Column(name="NATIONAL_ID_DOC_PATH")
	private String nationalIdDocPath;

	private String passwd;

	@Column(name="PIN_CODE")
	private String pinCode;

	@Column(name="REP_EMAIL")
	private String repEmail;
	
	@Column(name="BENEFICIARY_EMAIL")
	private String beneficiaryemail;

	@Column(name="REP_ID")
	private String repId;

	@Column(name="REP_MOBILE_NO")
	private String repMobileNo;

	@Column(name="REP_NAME")
	private String repName;

	@Column(name="REP_NAME_AR")
	private String repNameAr;

	@Column(name="\"STATE\"")
	private String state;

	//bi-directional many-to-one association to TLg
	//@OneToMany(mappedBy="TBeneficiarydetail")
	//private List<TLg> TLgs;

	//bi-directional many-to-one association to TLgdetail
	@OneToMany(mappedBy="TBeneficiarydetail")
	private List<TLgdetail> TLgdetails;

	//bi-directional many-to-one association to TLgonboarding
	@OneToMany(mappedBy="TBeneficiarydetail")
	private List<TLgonboarding> TLgonboardings;

	//bi-directional many-to-one association to TLgrequestamend
	@OneToMany(mappedBy="TBeneficiarydetail")
	private List<TLgrequestamend> TLgrequestamends;

	//bi-directional many-to-one association to TLgrequestconfiscate
	@OneToMany(mappedBy="TBeneficiarydetail")
	private List<TLgrequestconfiscate> TLgrequestconfiscates;

	//bi-directional many-to-one association to TLgrequestissue
	/*
	 * @OneToMany(mappedBy="TBeneficiarydetail") private List<TLgrequestissue>
	 * TLgrequestissues;
	 */

	//bi-directional many-to-one association to TLgrequestrelease
	@OneToMany(mappedBy="TBeneficiarydetail")
	private List<TLgrequestrelease> TLgrequestreleases;

	public TBeneficiarydetail() {
	}

	public long getBeneficiaryId() {
		return this.beneficiaryId;
	}

	public void setBeneficiaryId(long beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}

	public String getAgreeTag() {
		return this.agreeTag;
	}

	public void setAgreeTag(String agreeTag) {
		this.agreeTag = agreeTag;
	}

	public String getBenStatusFlag() {
		return this.benStatusFlag;
	}

	public void setBenStatusFlag(String benStatusFlag) {
		this.benStatusFlag = benStatusFlag;
	}

	public String getBeneficiaryAddr1() {
		return this.beneficiaryAddr1;
	}

	public void setBeneficiaryAddr1(String beneficiaryAddr1) {
		this.beneficiaryAddr1 = beneficiaryAddr1;
	}

	public String getBeneficiaryAddr1Ar() {
		return this.beneficiaryAddr1Ar;
	}

	public void setBeneficiaryAddr1Ar(String beneficiaryAddr1Ar) {
		this.beneficiaryAddr1Ar = beneficiaryAddr1Ar;
	}

	public String getBeneficiaryAddr2() {
		return this.beneficiaryAddr2;
	}

	public void setBeneficiaryAddr2(String beneficiaryAddr2) {
		this.beneficiaryAddr2 = beneficiaryAddr2;
	}

	public String getBeneficiaryAddr2Ar() {
		return this.beneficiaryAddr2Ar;
	}

	public void setBeneficiaryAddr2Ar(String beneficiaryAddr2Ar) {
		this.beneficiaryAddr2Ar = beneficiaryAddr2Ar;
	}

	public String getBeneficiaryName() {
		return this.beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryNameAr() {
		return this.beneficiaryNameAr;
	}

	public void setBeneficiaryNameAr(String beneficiaryNameAr) {
		this.beneficiaryNameAr = beneficiaryNameAr;
	}

	public Timestamp getBeneficiaryRegDate() {
		return this.beneficiaryRegDate;
	}

	public void setBeneficiaryRegDate(Timestamp beneficiaryRegDate) {
		this.beneficiaryRegDate = beneficiaryRegDate;
	}

	public String getBeneficiaryUid() {
		return this.beneficiaryUid;
	}

	public void setBeneficiaryUid(String beneficiaryUid) {
		this.beneficiaryUid = beneficiaryUid;
	}

	public String getContactInfo() {
		return this.contactInfo;
	}

	public void setContactInfo(String contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getLegAgreeDocName() {
		return this.legAgreeDocName;
	}

	public void setLegAgreeDocName(String legAgreeDocName) {
		this.legAgreeDocName = legAgreeDocName;
	}

	public String getLegAgreeDocPath() {
		return this.legAgreeDocPath;
	}

	public void setLegAgreeDocPath(String legAgreeDocPath) {
		this.legAgreeDocPath = legAgreeDocPath;
	}

	public String getLegAuthDocName() {
		return this.legAuthDocName;
	}

	public void setLegAuthDocName(String legAuthDocName) {
		this.legAuthDocName = legAuthDocName;
	}

	public String getLegAuthDocPath() {
		return this.legAuthDocPath;
	}

	public void setLegAuthDocPath(String legAuthDocPath) {
		this.legAuthDocPath = legAuthDocPath;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Object getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getNationalIdDocName() {
		return this.nationalIdDocName;
	}

	public void setNationalIdDocName(String nationalIdDocName) {
		this.nationalIdDocName = nationalIdDocName;
	}

	public String getNationalIdDocPath() {
		return this.nationalIdDocPath;
	}

	public void setNationalIdDocPath(String nationalIdDocPath) {
		this.nationalIdDocPath = nationalIdDocPath;
	}

	public String getPasswd() {
		return this.passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getPinCode() {
		return this.pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getRepEmail() {
		return this.repEmail;
	}

	public void setRepEmail(String repEmail) {
		this.repEmail = repEmail;
	}

	public String getRepId() {
		return this.repId;
	}

	public void setRepId(String repId) {
		this.repId = repId;
	}

	public String getRepMobileNo() {
		return this.repMobileNo;
	}

	public void setRepMobileNo(String repMobileNo) {
		this.repMobileNo = repMobileNo;
	}

	public String getRepName() {
		return this.repName;
	}

	public void setRepName(String repName) {
		this.repName = repName;
	}

	public String getRepNameAr() {
		return this.repNameAr;
	}

	public void setRepNameAr(String repNameAr) {
		this.repNameAr = repNameAr;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/*
	 * public List<TLg> getTLgs() { return this.TLgs; }
	 * 
	 * public void setTLgs(List<TLg> TLgs) { this.TLgs = TLgs; }
	 */

	/*
	 * public TLg addTLg(TLg TLg) { getTLgs().add(TLg);
	 * TLg.setTBeneficiarydetail(this);
	 * 
	 * return TLg; }
	 * 
	 * public TLg removeTLg(TLg TLg) { getTLgs().remove(TLg);
	 * TLg.setTBeneficiarydetail(null);
	 * 
	 * return TLg; }
	 */

	public List<TLgdetail> getTLgdetails() {
		return this.TLgdetails;
	}

	public void setTLgdetails(List<TLgdetail> TLgdetails) {
		this.TLgdetails = TLgdetails;
	}

	public TLgdetail addTLgdetail(TLgdetail TLgdetail) {
		getTLgdetails().add(TLgdetail);
		TLgdetail.setTBeneficiarydetail(this);

		return TLgdetail;
	}

	public TLgdetail removeTLgdetail(TLgdetail TLgdetail) {
		getTLgdetails().remove(TLgdetail);
		TLgdetail.setTBeneficiarydetail(null);

		return TLgdetail;
	}

	public List<TLgonboarding> getTLgonboardings() {
		return this.TLgonboardings;
	}

	public void setTLgonboardings(List<TLgonboarding> TLgonboardings) {
		this.TLgonboardings = TLgonboardings;
	}

	public TLgonboarding addTLgonboarding(TLgonboarding TLgonboarding) {
		getTLgonboardings().add(TLgonboarding);
		TLgonboarding.setTBeneficiarydetail(this);

		return TLgonboarding;
	}

	public TLgonboarding removeTLgonboarding(TLgonboarding TLgonboarding) {
		getTLgonboardings().remove(TLgonboarding);
		TLgonboarding.setTBeneficiarydetail(null);

		return TLgonboarding;
	}

	public List<TLgrequestamend> getTLgrequestamends() {
		return this.TLgrequestamends;
	}

	public void setTLgrequestamends(List<TLgrequestamend> TLgrequestamends) {
		this.TLgrequestamends = TLgrequestamends;
	}

	public TLgrequestamend addTLgrequestamend(TLgrequestamend TLgrequestamend) {
		getTLgrequestamends().add(TLgrequestamend);
		TLgrequestamend.setTBeneficiarydetail(this);

		return TLgrequestamend;
	}

	public TLgrequestamend removeTLgrequestamend(TLgrequestamend TLgrequestamend) {
		getTLgrequestamends().remove(TLgrequestamend);
		TLgrequestamend.setTBeneficiarydetail(null);

		return TLgrequestamend;
	}

	public List<TLgrequestconfiscate> getTLgrequestconfiscates() {
		return this.TLgrequestconfiscates;
	}

	public void setTLgrequestconfiscates(List<TLgrequestconfiscate> TLgrequestconfiscates) {
		this.TLgrequestconfiscates = TLgrequestconfiscates;
	}

	public TLgrequestconfiscate addTLgrequestconfiscate(TLgrequestconfiscate TLgrequestconfiscate) {
		getTLgrequestconfiscates().add(TLgrequestconfiscate);
		TLgrequestconfiscate.setTBeneficiarydetail(this);

		return TLgrequestconfiscate;
	}

	public TLgrequestconfiscate removeTLgrequestconfiscate(TLgrequestconfiscate TLgrequestconfiscate) {
		getTLgrequestconfiscates().remove(TLgrequestconfiscate);
		TLgrequestconfiscate.setTBeneficiarydetail(null);

		return TLgrequestconfiscate;
	}

	/*
	 * public List<TLgrequestissue> getTLgrequestissues() { return
	 * this.TLgrequestissues; }
	 * 
	 * public void setTLgrequestissues(List<TLgrequestissue> TLgrequestissues) {
	 * this.TLgrequestissues = TLgrequestissues; }
	 */

	/*
	 * public TLgrequestissue addTLgrequestissue(TLgrequestissue TLgrequestissue) {
	 * getTLgrequestissues().add(TLgrequestissue);
	 * TLgrequestissue.setTBeneficiarydetail(this);
	 * 
	 * return TLgrequestissue; }
	 * 
	 * public TLgrequestissue removeTLgrequestissue(TLgrequestissue TLgrequestissue)
	 * { getTLgrequestissues().remove(TLgrequestissue);
	 * TLgrequestissue.setTBeneficiarydetail(null);
	 * 
	 * return TLgrequestissue; }
	 */

	public List<TLgrequestrelease> getTLgrequestreleases() {
		return this.TLgrequestreleases;
	}

	public void setTLgrequestreleases(List<TLgrequestrelease> TLgrequestreleases) {
		this.TLgrequestreleases = TLgrequestreleases;
	}

	public TLgrequestrelease addTLgrequestreleas(TLgrequestrelease TLgrequestreleas) {
		getTLgrequestreleases().add(TLgrequestreleas);
		TLgrequestreleas.setTBeneficiarydetail(this);

		return TLgrequestreleas;
	}

	public TLgrequestrelease removeTLgrequestreleas(TLgrequestrelease TLgrequestreleas) {
		getTLgrequestreleases().remove(TLgrequestreleas);
		TLgrequestreleas.setTBeneficiarydetail(null);

		return TLgrequestreleas;
	}

	public String getNationalid() {
		return nationalid;
	}

	public void setNationalid(String nationalid) {
		this.nationalid = nationalid;
	}

	public String getBeneficiaryemail() {
		return beneficiaryemail;
	}

	public void setBeneficiaryemail(String beneficiaryemail) {
		this.beneficiaryemail = beneficiaryemail;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}