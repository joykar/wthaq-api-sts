package sa.wthaq.Entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the T_MENUMASTER database table.
 * 
 */
@Entity
@Table(name="T_MENUMASTER")
@NamedQuery(name="TMenumaster.findAll", query="SELECT t FROM TMenumaster t")
public class TMenumaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MENU_ID")
	private long menuId;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="MENU_NAME")
	private String menuName;

	@Column(name="MENU_NAME_AR")
	private String menuNameAr;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="MODOFIED_BY")
	private String modofiedBy;

	//bi-directional many-to-one association to TActivitylog
	@OneToMany(mappedBy="TMenumaster")
	private List<TActivitylog> TActivitylogs;

	//bi-directional many-to-one association to TRolemenumap
	@OneToMany(mappedBy="TMenumaster1")
	private List<TRolemenumap> TRolemenumaps1;

	/*//bi-directional many-to-one association to TRolemenumap
	@OneToMany(mappedBy="TMenumaster2")
	private List<TRolemenumap> TRolemenumaps2;
*/
	public TMenumaster() {
	}

	public long getMenuId() {
		return this.menuId;
	}

	public void setMenuId(long menuId) {
		this.menuId = menuId;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Object getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getMenuName() {
		return this.menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuNameAr() {
		return this.menuNameAr;
	}

	public void setMenuNameAr(String menuNameAr) {
		this.menuNameAr = menuNameAr;
	}

	public Object getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getModofiedBy() {
		return this.modofiedBy;
	}

	public void setModofiedBy(String modofiedBy) {
		this.modofiedBy = modofiedBy;
	}

	public List<TActivitylog> getTActivitylogs() {
		return this.TActivitylogs;
	}

	public void setTActivitylogs(List<TActivitylog> TActivitylogs) {
		this.TActivitylogs = TActivitylogs;
	}

	public TActivitylog addTActivitylog(TActivitylog TActivitylog) {
		getTActivitylogs().add(TActivitylog);
		TActivitylog.setTMenumaster(this);

		return TActivitylog;
	}

	public TActivitylog removeTActivitylog(TActivitylog TActivitylog) {
		getTActivitylogs().remove(TActivitylog);
		TActivitylog.setTMenumaster(null);

		return TActivitylog;
	}

	public List<TRolemenumap> getTRolemenumaps1() {
		return this.TRolemenumaps1;
	}

	public void setTRolemenumaps1(List<TRolemenumap> TRolemenumaps1) {
		this.TRolemenumaps1 = TRolemenumaps1;
	}

	public TRolemenumap addTRolemenumaps1(TRolemenumap TRolemenumaps1) {
		getTRolemenumaps1().add(TRolemenumaps1);
		TRolemenumaps1.setTMenumaster1(this);

		return TRolemenumaps1;
	}

	public TRolemenumap removeTRolemenumaps1(TRolemenumap TRolemenumaps1) {
		getTRolemenumaps1().remove(TRolemenumaps1);
		TRolemenumaps1.setTMenumaster1(null);

		return TRolemenumaps1;
	}

	/*public List<TRolemenumap> getTRolemenumaps2() {
		return this.TRolemenumaps2;
	}

	public void setTRolemenumaps2(List<TRolemenumap> TRolemenumaps2) {
		this.TRolemenumaps2 = TRolemenumaps2;
	}

	public TRolemenumap addTRolemenumaps2(TRolemenumap TRolemenumaps2) {
		getTRolemenumaps2().add(TRolemenumaps2);
		TRolemenumaps2.setTMenumaster2(this);

		return TRolemenumaps2;
	}

	public TRolemenumap removeTRolemenumaps2(TRolemenumap TRolemenumaps2) {
		getTRolemenumaps2().remove(TRolemenumaps2);
		TRolemenumaps2.setTMenumaster2(null);

		return TRolemenumaps2;
	}*/

}