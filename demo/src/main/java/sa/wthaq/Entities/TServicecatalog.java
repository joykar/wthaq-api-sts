package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the T_SERVICECATALOG database table.
 * 
 */
@Entity
@Table(name="T_SERVICECATALOG")
@NamedQuery(name="TServicecatalog.findAll", query="SELECT t FROM TServicecatalog t")
public class TServicecatalog implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TServicecatalogPK id;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="NO_OF_AMEND")
	private BigDecimal noOfAmend;

	@Column(name="NO_OF_ISSUE")
	private BigDecimal noOfIssue;

	private String periodicity;

	@Column(name="SERVICE_ACTIVE_STATUS")
	private String serviceActiveStatus;

	@Column(name="SERVICE_CATALOG_TYPE")
	private String serviceCatalogType;

	@Column(name="SERVICE_CHARGES")
	private BigDecimal serviceCharges;

	@Column(name="SERVICE_NAME")
	private String serviceName;

	@Column(name="SERVICE_NAME_AR")
	private String serviceNameAr;

	@Column(name="SERVICE_VALID_FROM")
	private Timestamp serviceValidFrom;

	@Column(name="SERVICE_VALID_TO")
	private Timestamp serviceValidTo;

	//bi-directional many-to-one association to TLgdetail
	@OneToMany(mappedBy="TServicecatalog")
	private List<TLgdetail> TLgdetails;

	//bi-directional many-to-one association to TPaymentsubscription
	@OneToMany(mappedBy="TServicecatalog")
	private List<TPaymentsubscription> TPaymentsubscriptions;

	//bi-directional many-to-one association to TSubscription
	@ManyToOne
	@JoinColumn(name="SUBSCRIPTION_ID")
	private TSubscription TSubscription;

	//bi-directional many-to-one association to TServicerequest
	@OneToMany(mappedBy="TServicecatalog")
	private List<TServicerequest> TServicerequests;

	public TServicecatalog() {
	}

	public TServicecatalogPK getId() {
		return this.id;
	}

	public void setId(TServicecatalogPK id) {
		this.id = id;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Object getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Object getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public BigDecimal getNoOfAmend() {
		return this.noOfAmend;
	}

	public void setNoOfAmend(BigDecimal noOfAmend) {
		this.noOfAmend = noOfAmend;
	}

	public BigDecimal getNoOfIssue() {
		return this.noOfIssue;
	}

	public void setNoOfIssue(BigDecimal noOfIssue) {
		this.noOfIssue = noOfIssue;
	}

	public String getPeriodicity() {
		return this.periodicity;
	}

	public void setPeriodicity(String periodicity) {
		this.periodicity = periodicity;
	}

	public String getServiceActiveStatus() {
		return this.serviceActiveStatus;
	}

	public void setServiceActiveStatus(String serviceActiveStatus) {
		this.serviceActiveStatus = serviceActiveStatus;
	}

	public String getServiceCatalogType() {
		return this.serviceCatalogType;
	}

	public void setServiceCatalogType(String serviceCatalogType) {
		this.serviceCatalogType = serviceCatalogType;
	}

	public BigDecimal getServiceCharges() {
		return this.serviceCharges;
	}

	public void setServiceCharges(BigDecimal serviceCharges) {
		this.serviceCharges = serviceCharges;
	}

	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceNameAr() {
		return this.serviceNameAr;
	}

	public void setServiceNameAr(String serviceNameAr) {
		this.serviceNameAr = serviceNameAr;
	}

	public Object getServiceValidFrom() {
		return this.serviceValidFrom;
	}

	public void setServiceValidFrom(Timestamp serviceValidFrom) {
		this.serviceValidFrom = serviceValidFrom;
	}

	public Object getServiceValidTo() {
		return this.serviceValidTo;
	}

	public void setServiceValidTo(Timestamp serviceValidTo) {
		this.serviceValidTo = serviceValidTo;
	}

	public List<TLgdetail> getTLgdetails() {
		return this.TLgdetails;
	}

	public void setTLgdetails(List<TLgdetail> TLgdetails) {
		this.TLgdetails = TLgdetails;
	}

	public TLgdetail addTLgdetail(TLgdetail TLgdetail) {
		getTLgdetails().add(TLgdetail);
		TLgdetail.setTServicecatalog(this);

		return TLgdetail;
	}

	public TLgdetail removeTLgdetail(TLgdetail TLgdetail) {
		getTLgdetails().remove(TLgdetail);
		TLgdetail.setTServicecatalog(null);

		return TLgdetail;
	}

	public List<TPaymentsubscription> getTPaymentsubscriptions() {
		return this.TPaymentsubscriptions;
	}

	public void setTPaymentsubscriptions(List<TPaymentsubscription> TPaymentsubscriptions) {
		this.TPaymentsubscriptions = TPaymentsubscriptions;
	}

	public TPaymentsubscription addTPaymentsubscription(TPaymentsubscription TPaymentsubscription) {
		getTPaymentsubscriptions().add(TPaymentsubscription);
		TPaymentsubscription.setTServicecatalog(this);

		return TPaymentsubscription;
	}

	public TPaymentsubscription removeTPaymentsubscription(TPaymentsubscription TPaymentsubscription) {
		getTPaymentsubscriptions().remove(TPaymentsubscription);
		TPaymentsubscription.setTServicecatalog(null);

		return TPaymentsubscription;
	}

	public TSubscription getTSubscription() {
		return this.TSubscription;
	}

	public void setTSubscription(TSubscription TSubscription) {
		this.TSubscription = TSubscription;
	}

	public List<TServicerequest> getTServicerequests() {
		return this.TServicerequests;
	}

	public void setTServicerequests(List<TServicerequest> TServicerequests) {
		this.TServicerequests = TServicerequests;
	}

	public TServicerequest addTServicerequest(TServicerequest TServicerequest) {
		getTServicerequests().add(TServicerequest);
		TServicerequest.setTServicecatalog(this);

		return TServicerequest;
	}

	public TServicerequest removeTServicerequest(TServicerequest TServicerequest) {
		getTServicerequests().remove(TServicerequest);
		TServicerequest.setTServicecatalog(null);

		return TServicerequest;
	}

}