package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the T_PAYMENTSETTLEMENT database table.
 * 
 */
@Entity
@Table(name="T_PAYMENTSETTLEMENT")
@NamedQuery(name="TPaymentsettlement.findAll", query="SELECT t FROM TPaymentsettlement t")
public class TPaymentsettlement implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TPaymentsettlementPK id;

	@Column(name="PAYMENT_AMT")
	private BigDecimal paymentAmt;

	@Column(name="PAYMENT_METHOD")
	private String paymentMethod;

	@Column(name="ROLE_ID")
	private BigDecimal roleId;

	//bi-directional many-to-one association to TServicerequest
	@ManyToOne
	@JoinColumn(name="REQUEST_ID",insertable = false,updatable = false)
	private TServicerequest TServicerequest;

	//bi-directional many-to-one association to TUsermaster
	@ManyToOne
	@JoinColumn(name="LOGIN_ID", insertable=false, updatable=false)
	private TUsermaster TUsermaster;

	public TPaymentsettlement() {
	}

	public TPaymentsettlementPK getId() {
		return this.id;
	}

	public void setId(TPaymentsettlementPK id) {
		this.id = id;
	}

	public BigDecimal getPaymentAmt() {
		return this.paymentAmt;
	}

	public void setPaymentAmt(BigDecimal paymentAmt) {
		this.paymentAmt = paymentAmt;
	}

	public String getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public BigDecimal getRoleId() {
		return this.roleId;
	}

	public void setRoleId(BigDecimal roleId) {
		this.roleId = roleId;
	}

	public TServicerequest getTServicerequest() {
		return this.TServicerequest;
	}

	public void setTServicerequest(TServicerequest TServicerequest) {
		this.TServicerequest = TServicerequest;
	}

	public TUsermaster getTUsermaster() {
		return this.TUsermaster;
	}

	public void setTUsermaster(TUsermaster TUsermaster) {
		this.TUsermaster = TUsermaster;
	}

}