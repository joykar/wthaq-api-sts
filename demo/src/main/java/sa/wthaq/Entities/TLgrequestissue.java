package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Nationalized;

import java.sql.Timestamp;


/**
 * The persistent class for the T_LGREQUESTISSUE database table.
 * 
 */
@Entity
@Table(name="T_LGREQUESTISSUE")
@NamedQuery(name="TLgrequestissue.findAll", query="SELECT t FROM TLgrequestissue t")
public class TLgrequestissue implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="request_id")
	private int requestId;

	@Column(name="active_status")
	private String activeStatus;

	@Column(name="agree_tag")
	private String agreeTag;

	@Column(name="bank_code")
	private String bankCode;

	@Column(name="beneficiary_id")
	private Long beneficiaryId;

	@Column(name="byan_no")
	private String byanNo;

	@Column(name="case_id")
	private int caseId;

	@Column(name="contract_value")
	private Double contractValue;

	@Column(name="create_by")
	private String createBy;

	@Column(name="create_on")
	private Timestamp createOn;

	@Column(name="delete_flag")
	private String deleteFlag="N";

	@Column(name="iban_no")
	private String ibanNo;

	@Column(name="is_assignable_flag")
	private String isAssignableFlag;

	@Column(name="is_autorenewable_flag")
	private String isAutorenewableFlag;

	@Column(name="is_open_ended_flag")
	private String isOpenEndedFlag;

	@Column(name="is_transferable_flag")
	private String isTransferableFlag;

	@Column(name="lg_end_date")
	private Timestamp lgEndDate;

	@Column(name="lg_issued_by")
	private String lgIssuedBy;

	@Column(name="lg_start_date")
	private Timestamp lgStartDate;

	@Column(name="lg_type_id")
	private int lgTypeId;

	@Column(name="lg_validity_date")
	private Timestamp lgValidityDate;

	@Column(name="lg_value")
	private Double lgValue;

	@Column(name="login_id")
	private String loginId;

	@Column(name="modified_by")
	private String modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;

	@Column(name="percentage_of_value")
	private Double percentageOfValue;

	@Column(name="project_id")
	private String projectId;

	@Column(name="project_name")
	private String projectName;

	@Column(name="project_name_ar")
	private String projectNameAr;

	@Column(name="purpose_of_bond")
	private String purposeOfBond;

	@Column(name="role_id")
	private int roleId;

	@Column(name="supplier_id")
	private Long supplierId;

	@Column(name="terms_condition_type")
	private String termsConditionType;

	@Column(name="zakat_end_date")
	private Timestamp zakatEndDate;

	@Column(name="zakat_start_date")
	private Timestamp zakatStartDate;
	
	@Column(name="currency_code")
	private String currencyCode;
	
	@Nationalized
	@Column(name="terms_condition_message")
	private String termsConditionMessage;
	
	@Transient
    private int responseCode;
	
	@Transient
    private String strtDt;
	
	@Transient
    private String endDt;
	
	@Transient
    private String LgvalidDt;
	
	
	/*
	 * @Column(name="is_Private") private String is_Private;
	 */

	//bi-directional many-to-one association to TBeneficiarydetail
	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="BENEFICIARY_ID",insertable = false,updatable = false)
	 * private TBeneficiarydetail TBeneficiarydetail;
	 */

		//bi-directional one-to-one association to TLg
	/*
	 * @OneToOne
	 * 
	 * @JoinColumn(name="CASE_ID",insertable = false,updatable = false) private TLg
	 * TLg;
	 */

		//bi-directional many-to-one association to TSupplierdetail
	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="SUPPLIER_ID",insertable = false,updatable = false) private
	 * TSupplierdetail TSupplierdetail;
	 */
	
	public int getRequestId() {
		return this.requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getActiveStatus() {
		return this.activeStatus;
	}

	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getAgreeTag() {
		return this.agreeTag;
	}

	public void setAgreeTag(String agreeTag) {
		this.agreeTag = agreeTag;
	}

	public String getBankCode() {
		return this.bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public Long getBeneficiaryId() {
		return this.beneficiaryId;
	}

	public void setBeneficiaryId(Long beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}

	public String getByanNo() {
		return this.byanNo;
	}

	public void setByanNo(String byanNo) {
		this.byanNo = byanNo;
	}

	public int getCaseId() {
		return this.caseId;
	}

	public void setCaseId(int caseId) {
		this.caseId = caseId;
	}

	public Double getContractValue() {
		return this.contractValue;
	}

	public void setContractValue(Double contractValue) {
		this.contractValue = contractValue;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getIbanNo() {
		return this.ibanNo;
	}

	public void setIbanNo(String ibanNo) {
		this.ibanNo = ibanNo;
	}

	public String getIsAssignableFlag() {
		return this.isAssignableFlag;
	}

	public void setIsAssignableFlag(String isAssignableFlag) {
		this.isAssignableFlag = isAssignableFlag;
	}

	public String getIsAutorenewableFlag() {
		return this.isAutorenewableFlag;
	}

	public void setIsAutorenewableFlag(String isAutorenewableFlag) {
		this.isAutorenewableFlag = isAutorenewableFlag;
	}

	public String getIsOpenEndedFlag() {
		return this.isOpenEndedFlag;
	}

	public void setIsOpenEndedFlag(String isOpenEndedFlag) {
		this.isOpenEndedFlag = isOpenEndedFlag;
	}

	public String getIsTransferableFlag() {
		return this.isTransferableFlag;
	}

	public void setIsTransferableFlag(String isTransferableFlag) {
		this.isTransferableFlag = isTransferableFlag;
	}

	public Timestamp getLgEndDate() {
		return this.lgEndDate;
	}

	public void setLgEndDate(Timestamp lgEndDate) {
		this.lgEndDate = lgEndDate;
	}

	public String getLgIssuedBy() {
		return this.lgIssuedBy;
	}

	public void setLgIssuedBy(String lgIssuedBy) {
		this.lgIssuedBy = lgIssuedBy;
	}

	public Timestamp getLgStartDate() {
		return this.lgStartDate;
	}

	public void setLgStartDate(Timestamp lgStartDate) {
		this.lgStartDate = lgStartDate;
	}

	

	public int getLgTypeId() {
		return lgTypeId;
	}

	public void setLgTypeId(int lgTypeId) {
		this.lgTypeId = lgTypeId;
	}

	public Timestamp getLgValidityDate() {
		return this.lgValidityDate;
	}

	public void setLgValidityDate(Timestamp lgValidityDate) {
		this.lgValidityDate = lgValidityDate;
	}

	public Double getLgValue() {
		return this.lgValue;
	}

	public void setLgValue(Double lgValue) {
		this.lgValue = lgValue;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Double getPercentageOfValue() {
		return this.percentageOfValue;
	}

	public void setPercentageOfValue(Double percentageOfValue) {
		this.percentageOfValue = percentageOfValue;
	}

	public String getProjectId() {
		return this.projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return this.projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectNameAr() {
		return this.projectNameAr;
	}

	public void setProjectNameAr(String projectNameAr) {
		this.projectNameAr = projectNameAr;
	}

	public String getPurposeOfBond() {
		return this.purposeOfBond;
	}

	public void setPurposeOfBond(String purposeOfBond) {
		this.purposeOfBond = purposeOfBond;
	}

	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public Long getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getTermsConditionType() {
		return this.termsConditionType;
	}

	public void setTermsConditionType(String termsConditionType) {
		this.termsConditionType = termsConditionType;
	}

	public Timestamp getZakatEndDate() {
		return this.zakatEndDate;
	}

	public void setZakatEndDate(Timestamp zakatEndDate) {
		this.zakatEndDate = zakatEndDate;
	}

	public Timestamp getZakatStartDate() {
		return this.zakatStartDate;
	}

	public void setZakatStartDate(Timestamp zakatStartDate) {
		this.zakatStartDate = zakatStartDate;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getTermsConditionMessage() {
		return termsConditionMessage;
	}

	public void setTermsConditionMessage(String termsConditionMessage) {
		this.termsConditionMessage = termsConditionMessage;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getStrtDt() {
		return strtDt;
	}

	public void setStrtDt(String strtDt) {
		this.strtDt = strtDt;
	}

	public String getEndDt() {
		return endDt;
	}

	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}

	

	

	/*
	 * public TBeneficiarydetail getTBeneficiarydetail() { return
	 * this.TBeneficiarydetail; }
	 * 
	 * public void setTBeneficiarydetail(TBeneficiarydetail TBeneficiarydetail) {
	 * this.TBeneficiarydetail = TBeneficiarydetail; }
	 * 
	 * public TLg getTLg() { return this.TLg; }
	 * 
	 * public void setTLg(TLg TLg) { this.TLg = TLg; }
	 * 
	 * public TSupplierdetail getTSupplierdetail() { return this.TSupplierdetail; }
	 * 
	 * public void setTSupplierdetail(TSupplierdetail TSupplierdetail) {
	 * this.TSupplierdetail = TSupplierdetail; }
	 */
	
	

}