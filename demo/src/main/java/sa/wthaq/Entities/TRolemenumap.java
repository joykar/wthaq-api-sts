package sa.wthaq.Entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the T_ROLEMENUMAP database table.
 * 
 */
@Entity
@Table(name="T_ROLEMENUMAP")
@NamedQuery(name="TRolemenumap.findAll", query="SELECT t FROM TRolemenumap t")
public class TRolemenumap implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TRolemenumapPK id;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_KEY")
	private String createKey;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="DELETE_KEY")
	private String deleteKey;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="READ_KEY")
	private String readKey;

	@Column(name="UPDATE_KEY")
	private String updateKey;

	//bi-directional many-to-one association to TMenumaster
	@ManyToOne
	@JoinColumn(name="MENU_ID", insertable=false, updatable=false)
	private TMenumaster TMenumaster1;

	/*//bi-directional many-to-one association to TMenumaster
	@ManyToOne
	@JoinColumn(name="MENU_ID")
	private TMenumaster TMenumaster2;*/

	//bi-directional many-to-one association to TRolemaster
	@ManyToOne
	@JoinColumn(name="ROLE_ID", insertable=false, updatable=false)
	private TRolemaster TRolemaster1;

	/*//bi-directional many-to-one association to TRolemaster
	@ManyToOne
	@JoinColumn(name="ROLE_ID")
	private TRolemaster TRolemaster2;*/

	public TRolemenumap() {
	}

	public TRolemenumapPK getId() {
		return this.id;
	}

	public void setId(TRolemenumapPK id) {
		this.id = id;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getCreateKey() {
		return this.createKey;
	}

	public void setCreateKey(String createKey) {
		this.createKey = createKey;
	}

	public Timestamp getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getDeleteKey() {
		return this.deleteKey;
	}

	public void setDeleteKey(String deleteKey) {
		this.deleteKey = deleteKey;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getReadKey() {
		return this.readKey;
	}

	public void setReadKey(String readKey) {
		this.readKey = readKey;
	}

	public String getUpdateKey() {
		return this.updateKey;
	}

	public void setUpdateKey(String updateKey) {
		this.updateKey = updateKey;
	}

	public TMenumaster getTMenumaster1() {
		return this.TMenumaster1;
	}

	public void setTMenumaster1(TMenumaster TMenumaster1) {
		this.TMenumaster1 = TMenumaster1;
	}

	/*public TMenumaster getTMenumaster2() {
		return this.TMenumaster2;
	}

	public void setTMenumaster2(TMenumaster TMenumaster2) {
		this.TMenumaster2 = TMenumaster2;
	}*/

	public TRolemaster getTRolemaster1() {
		return this.TRolemaster1;
	}

	public void setTRolemaster1(TRolemaster TRolemaster1) {
		this.TRolemaster1 = TRolemaster1;
	}
/*
	public TRolemaster getTRolemaster2() {
		return this.TRolemaster2;
	}*/

	/*public void setTRolemaster2(TRolemaster TRolemaster2) {
		this.TRolemaster2 = TRolemaster2;
	}*/

}