package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the T_ACTIVITYLOG database table.
 * 
 */
@Entity
@Table(name="T_ACTIVITYLOG")
@NamedQuery(name="TActivitylog.findAll", query="SELECT t FROM TActivitylog t")
public class TActivitylog implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TActivitylogPK id;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="ERROR_DESC")
	private String errorDesc;

	@Column(name="EVENT_NAME")
	private String eventName;

	@Column(name="HIT_STATUS")
	private String hitStatus;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="ROLE_ID", insertable=false, updatable=false)
	private BigDecimal roleId;

	//bi-directional many-to-one association to TMenumaster
	@ManyToOne
	@JoinColumn(name="MENU_ID", insertable=false, updatable=false)
	private TMenumaster TMenumaster;

	//bi-directional many-to-one association to TUsermaster
	@ManyToOne
	@JoinColumn(name="LOGIN_ID", insertable=false, updatable=false)
	private TUsermaster TUsermaster;

	public TActivitylog() {
	}

	public TActivitylogPK getId() {
		return this.id;
	}

	public void setId(TActivitylogPK id) {
		this.id = id;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getErrorDesc() {
		return this.errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	public String getEventName() {
		return this.eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getHitStatus() {
		return this.hitStatus;
	}

	public void setHitStatus(String hitStatus) {
		this.hitStatus = hitStatus;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public BigDecimal getRoleId() {
		return this.roleId;
	}

	public void setRoleId(BigDecimal roleId) {
		this.roleId = roleId;
	}

	public TMenumaster getTMenumaster() {
		return this.TMenumaster;
	}

	public void setTMenumaster(TMenumaster TMenumaster) {
		this.TMenumaster = TMenumaster;
	}

	public TUsermaster getTUsermaster() {
		return this.TUsermaster;
	}

	public void setTUsermaster(TUsermaster TUsermaster) {
		this.TUsermaster = TUsermaster;
	}

}