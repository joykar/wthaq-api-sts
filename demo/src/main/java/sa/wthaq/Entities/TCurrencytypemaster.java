package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the T_CURRENCYTYPEMASTER database table.
 * 
 */
@Entity
@Table(name="T_CURRENCYTYPEMASTER")
@NamedQuery(name="TCurrencytypemaster.findAll", query="SELECT t FROM TCurrencytypemaster t")
public class TCurrencytypemaster implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	private String country;

	private String currency;

	@Column(name="CURRENCY_CODE")
	private String currencyCode;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="SRL_NO")
	private BigDecimal srlNo;

	public TCurrencytypemaster() {
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public BigDecimal getSrlNo() {
		return this.srlNo;
	}

	public void setSrlNo(BigDecimal srlNo) {
		this.srlNo = srlNo;
	}

}