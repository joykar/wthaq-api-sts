package sa.wthaq.Entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the T_NOTIFICATIONTEMPLATE database table.
 * 
 */
@Entity
@Table(name="T_NOTIFICATIONTEMPLATE")
@NamedQuery(name="TNotificationtemplate.findAll", query="SELECT t FROM TNotificationtemplate t")
public class TNotificationtemplate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="TEMPLATE_ID")
	private long templateId;

	@Column(name="BODY_TEXT")
	private String bodyText;

	@Column(name="BODY_TEXT_AR")
	private String bodyTextAr;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="GATEWAY_ID")
	private String gatewayId;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	private String subject;

	@Column(name="SUBJECT_AR")
	private String subjectAr;

	@Column(name="TEMPLATE_NAME")
	private String templateName;

	@Column(name="TEMPLATE_NAME_AR")
	private String templateNameAr;

	//bi-directional many-to-one association to TStatuscodemaster
	@ManyToOne
	@JoinColumn(name="STATUS_ID")
	private TStatuscodemaster TStatuscodemaster;

	public TNotificationtemplate() {
	}

	public long getTemplateId() {
		return this.templateId;
	}

	public void setTemplateId(long templateId) {
		this.templateId = templateId;
	}

	public String getBodyText() {
		return this.bodyText;
	}

	public void setBodyText(String bodyText) {
		this.bodyText = bodyText;
	}

	public String getBodyTextAr() {
		return this.bodyTextAr;
	}

	public void setBodyTextAr(String bodyTextAr) {
		this.bodyTextAr = bodyTextAr;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Object getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getGatewayId() {
		return this.gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Object getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubjectAr() {
		return this.subjectAr;
	}

	public void setSubjectAr(String subjectAr) {
		this.subjectAr = subjectAr;
	}

	public String getTemplateName() {
		return this.templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getTemplateNameAr() {
		return this.templateNameAr;
	}

	public void setTemplateNameAr(String templateNameAr) {
		this.templateNameAr = templateNameAr;
	}

	public TStatuscodemaster getTStatuscodemaster() {
		return this.TStatuscodemaster;
	}

	public void setTStatuscodemaster(TStatuscodemaster TStatuscodemaster) {
		this.TStatuscodemaster = TStatuscodemaster;
	}

}