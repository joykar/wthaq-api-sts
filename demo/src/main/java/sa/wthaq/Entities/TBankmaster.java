package sa.wthaq.Entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Set;


/**
 * The persistent class for the T_BANKMASTER database table.
 * 
 */
@Entity
@Table(name="T_BANKMASTER")
@NamedQuery(name="TBankmaster.findAll", query="SELECT t FROM TBankmaster t")
public class TBankmaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BANK_CODE")
	private String bankCode;

	@Column(name="BANK_ADDR1")
	private String bankAddr1;

	@Column(name="BANK_ADDR1_AR")
	private String bankAddr1Ar;

	@Column(name="BANK_ADDR2")
	private String bankAddr2;

	@Column(name="BANK_ADDR2_AR")
	private String bankAddr2Ar;

	@Column(name="BANK_CITY")
	private String bankCity;

	@Column(name="BANK_COUNTRY")
	private String bankCountry;

	@Column(name="BANK_NAME")
	private String bankName;

	@Column(name="BANK_NAME_AR")
	private String bankNameAr;

	@Column(name="BANK_STATE")
	private String bankState;

	@Column(name="BRANCH_NAME")
	private String branchName;

	@Column(name="BRANCH_NAME_AR")
	private String branchNameAr;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="IFSC_CODE")
	private String ifscCode;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="POSTAL_CODE")
	private String postalCode;

	/*
	 * //bi-directional many-to-one association to TAckfrombank
	 * 
	 * @JsonIgnoreProperties("TBankmaster")
	 * 
	 * @OneToMany(mappedBy="TBankmaster", fetch = FetchType.LAZY) private
	 * Set<TAckfrombank> TAckfrombanks;
	 * 
	 * //bi-directional many-to-one association to TAcktobank
	 * 
	 * @JsonIgnoreProperties("TBankmaster")
	 * 
	 * @OneToMany(mappedBy="TBankmaster", fetch = FetchType.LAZY) private
	 * Set<TAcktobank> TAcktobanks;
	 * 
	 * //bi-directional many-to-one association to TBankbensupplierlink
	 * 
	 * @JsonIgnoreProperties("TBankmaster")
	 * 
	 * @OneToMany(mappedBy="TBankmaster", fetch = FetchType.LAZY) private
	 * Set<TBankbensupplierlink> TBankbensupplierlinks;
	 * 
	 * //bi-directional many-to-one association to TNotification
	 * 
	 * @JsonIgnoreProperties("TBankmaster")
	 * 
	 * @OneToMany(mappedBy="TBankmaster", fetch = FetchType.LAZY) private
	 * Set<TNotification> TNotifications;
	 */
	//bi-directional many-to-one association to TServicerequest
	/*
	 * @JsonIgnoreProperties("TBankmaster")
	 * 
	 * @OneToMany(mappedBy="TBankmaster", fetch = FetchType.LAZY) private
	 * Set<TServicerequest> TServicerequests;
	 */

	public TBankmaster() {
	}

	public String getBankCode() {
		return this.bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankAddr1() {
		return this.bankAddr1;
	}

	public void setBankAddr1(String bankAddr1) {
		this.bankAddr1 = bankAddr1;
	}

	public String getBankAddr1Ar() {
		return this.bankAddr1Ar;
	}

	public void setBankAddr1Ar(String bankAddr1Ar) {
		this.bankAddr1Ar = bankAddr1Ar;
	}

	public String getBankAddr2() {
		return this.bankAddr2;
	}

	public void setBankAddr2(String bankAddr2) {
		this.bankAddr2 = bankAddr2;
	}

	public String getBankAddr2Ar() {
		return this.bankAddr2Ar;
	}

	public void setBankAddr2Ar(String bankAddr2Ar) {
		this.bankAddr2Ar = bankAddr2Ar;
	}

	public String getBankCity() {
		return this.bankCity;
	}

	public void setBankCity(String bankCity) {
		this.bankCity = bankCity;
	}

	public String getBankCountry() {
		return this.bankCountry;
	}

	public void setBankCountry(String bankCountry) {
		this.bankCountry = bankCountry;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankNameAr() {
		return this.bankNameAr;
	}

	public void setBankNameAr(String bankNameAr) {
		this.bankNameAr = bankNameAr;
	}

	public String getBankState() {
		return this.bankState;
	}

	public void setBankState(String bankState) {
		this.bankState = bankState;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchNameAr() {
		return this.branchNameAr;
	}

	public void setBranchNameAr(String branchNameAr) {
		this.branchNameAr = branchNameAr;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getIfscCode() {
		return this.ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/*
	 * public Set<TAckfrombank> getTAckfrombanks() { return this.TAckfrombanks; }
	 * 
	 * public void setTAckfrombanks(Set<TAckfrombank> TAckfrombanks) {
	 * this.TAckfrombanks = TAckfrombanks; }
	 */

	/*
	 * public TAckfrombank addTAckfrombank(TAckfrombank TAckfrombank) {
	 * getTAckfrombanks().add(TAckfrombank); TAckfrombank.setTBankmaster(this);
	 * 
	 * return TAckfrombank; }
	 * 
	 * public TAckfrombank removeTAckfrombank(TAckfrombank TAckfrombank) {
	 * getTAckfrombanks().remove(TAckfrombank); TAckfrombank.setTBankmaster(null);
	 * 
	 * return TAckfrombank; }
	 */

	/*
	 * public Set<TAcktobank> getTAcktobanks() { return this.TAcktobanks; }
	 * 
	 * public void setTAcktobanks(Set<TAcktobank> TAcktobanks) { this.TAcktobanks =
	 * TAcktobanks; }
	 */
	/*
	 * public TAcktobank addTAcktobank(TAcktobank TAcktobank) {
	 * getTAcktobanks().add(TAcktobank); TAcktobank.setTBankmaster(this);
	 * 
	 * return TAcktobank; }
	 * 
	 * public TAcktobank removeTAcktobank(TAcktobank TAcktobank) {
	 * getTAcktobanks().remove(TAcktobank); TAcktobank.setTBankmaster(null);
	 * 
	 * return TAcktobank; }
	 * 
	 * public Set<TBankbensupplierlink> getTBankbensupplierlinks() { return
	 * this.TBankbensupplierlinks; }
	 * 
	 * public void setTBankbensupplierlinks(Set<TBankbensupplierlink>
	 * TBankbensupplierlinks) { this.TBankbensupplierlinks = TBankbensupplierlinks;
	 * }
	 */

	/*
	 * public TBankbensupplierlink addTBankbensupplierlink(TBankbensupplierlink
	 * TBankbensupplierlink) { getTBankbensupplierlinks().add(TBankbensupplierlink);
	 * TBankbensupplierlink.setTBankmaster(this);
	 * 
	 * return TBankbensupplierlink; }
	 * 
	 * public TBankbensupplierlink removeTBankbensupplierlink(TBankbensupplierlink
	 * TBankbensupplierlink) {
	 * getTBankbensupplierlinks().remove(TBankbensupplierlink);
	 * TBankbensupplierlink.setTBankmaster(null);
	 * 
	 * return TBankbensupplierlink; }
	 * 
	 * public Set<TNotification> getTNotifications() { return this.TNotifications; }
	 * 
	 * public void setTNotifications(Set<TNotification> TNotifications) {
	 * this.TNotifications = TNotifications; }
	 * 
	 * public TNotification addTNotification(TNotification TNotification) {
	 * getTNotifications().add(TNotification); TNotification.setTBankmaster(this);
	 * 
	 * return TNotification; }
	 */
	/*
	 * public TNotification removeTNotification(TNotification TNotification) {
	 * getTNotifications().remove(TNotification);
	 * TNotification.setTBankmaster(null);
	 * 
	 * return TNotification; }
	 */
	/*
	 * public Set<TServicerequest> getTServicerequests() { return
	 * this.TServicerequests; }
	 * 
	 * public void setTServicerequests(Set<TServicerequest> TServicerequests) {
	 * this.TServicerequests = TServicerequests; }
	 */

	/*
	 * public TServicerequest addTServicerequest(TServicerequest TServicerequest) {
	 * getTServicerequests().add(TServicerequest);
	 * TServicerequest.setTBankmaster(this);
	 * 
	 * return TServicerequest; }
	 * 
	 * public TServicerequest removeTServicerequest(TServicerequest TServicerequest)
	 * { getTServicerequests().remove(TServicerequest);
	 * TServicerequest.setTBankmaster(null);
	 * 
	 * return TServicerequest; }
	 */

}