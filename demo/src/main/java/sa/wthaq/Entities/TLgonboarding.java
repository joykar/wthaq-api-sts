package sa.wthaq.Entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the T_LGONBOARDING database table.
 * 
 */
@Entity
@Table(name="T_LGONBOARDING")
@NamedQuery(name="TLgonboarding.findAll", query="SELECT t FROM TLgonboarding t")
public class TLgonboarding implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CASE_ID")
	private long caseId;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="CRN_NO")
	private String crnNo;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="LG_NO")
	private String lgNo;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="PROJECT_ID")
	private String projectId;

	@Column(name="PROJECT_NAME")
	private String projectName;

	@Column(name="PROJECT_NAME_AR")
	private String projectNameAr;

	@Column(name="TERMS_CONDITION_TYPE")
	private String termsConditionType;

	@Column(name="UPLD_DOC_NAME")
	private String upldDocName;

	@Column(name="UPLD_DOC_PATH")
	private String upldDocPath;

	//bi-directional many-to-one association to TBeneficiarydetail
	@ManyToOne
	@JoinColumn(name="BENEFICIARY_ID",insertable = false,updatable = false)
	private TBeneficiarydetail TBeneficiarydetail;

	//bi-directional one-to-one association to TLg
	/*
	 * @OneToOne
	 * 
	 * @JoinColumn(name="CASE_ID") private TLg TLg1;
	 */

	//bi-directional one-to-one association to TLg
	/*
	 * @OneToOne
	 * 
	 * @JoinColumn(name="CASE_ID") private TLg TLg2;
	 */

	//bi-directional many-to-one association to TSupplierdetail
	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="SUPPLIER_ID") private TSupplierdetail TSupplierdetail;
	 */

	public TLgonboarding() {
	}

	public long getCaseId() {
		return this.caseId;
	}

	public void setCaseId(long caseId) {
		this.caseId = caseId;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Object getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getCrnNo() {
		return this.crnNo;
	}

	public void setCrnNo(String crnNo) {
		this.crnNo = crnNo;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getLgNo() {
		return this.lgNo;
	}

	public void setLgNo(String lgNo) {
		this.lgNo = lgNo;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Object getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getProjectId() {
		return this.projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return this.projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectNameAr() {
		return this.projectNameAr;
	}

	public void setProjectNameAr(String projectNameAr) {
		this.projectNameAr = projectNameAr;
	}

	public String getTermsConditionType() {
		return this.termsConditionType;
	}

	public void setTermsConditionType(String termsConditionType) {
		this.termsConditionType = termsConditionType;
	}

	public String getUpldDocName() {
		return this.upldDocName;
	}

	public void setUpldDocName(String upldDocName) {
		this.upldDocName = upldDocName;
	}

	public String getUpldDocPath() {
		return this.upldDocPath;
	}

	public void setUpldDocPath(String upldDocPath) {
		this.upldDocPath = upldDocPath;
	}

	public TBeneficiarydetail getTBeneficiarydetail() {
		return this.TBeneficiarydetail;
	}

	public void setTBeneficiarydetail(TBeneficiarydetail TBeneficiarydetail) {
		this.TBeneficiarydetail = TBeneficiarydetail;
	}

	/*
	 * public TLg getTLg1() { return this.TLg1; }
	 * 
	 * public void setTLg1(TLg TLg1) { this.TLg1 = TLg1; }
	 * 
	 * public TLg getTLg2() { return this.TLg2; }
	 * 
	 * public void setTLg2(TLg TLg2) { this.TLg2 = TLg2; }
	 */

	/*
	 * public TSupplierdetail getTSupplierdetail() { return this.TSupplierdetail; }
	 * 
	 * public void setTSupplierdetail(TSupplierdetail TSupplierdetail) {
	 * this.TSupplierdetail = TSupplierdetail; }
	 */

}