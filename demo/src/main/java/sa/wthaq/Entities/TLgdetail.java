package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Nationalized;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the T_LGDETAILS database table.
 * 
 */
@Entity
@Table(name="T_LGDETAILS")
@NamedQuery(name="TLgdetail.findAll", query="SELECT t FROM TLgdetail t")
public class TLgdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TLgdetailPK id;

	@Column(name="BANK_CODE")
	private String bankCode;

	@Column(name="BRANCH_NAME")
	private String branchName;

	@Column(name="BRANCH_NAME_AR")
	private String branchNameAr;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="CURRENCY_CODE")
	private String currencyCode;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="LG_AMOUNT")
	private BigDecimal lgAmount;

	@Column(name="LG_DATE")
	private Timestamp lgDate;

	@Column(name="LG_ISSUE_DATE")
	private Timestamp lgIssueDate;

	@Column(name="LG_NO")
	private String lgNo;

	@Column(name="LG_VALIDITY_DATE")
	private Timestamp lgValidityDate;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="PROJECT_ID")
	private String projectId;

	@Column(name="PROJECT_NAME")
	private String projectName;

	@Column(name="PROJECT_NAME_AR")
	private String projectNameAr;

	@Column(name="REQUEST_ID")
	private String requestId;

	@Column(name="ROLE_ID")
	private BigDecimal roleId;

	@Column(name="TERMS_CONDITION_TYPE")
	private String termsConditionType;

	@Column(name="TRANS_DATE")
	private Timestamp transDate;

	@Column(name="LOGIN_ID")
	private String userId;

	@Column(name="ZAKAT_END_DATE")
	private Timestamp zakatEndDate;

	@Column(name="ZAKAT_START_DATE")
	private Timestamp zakatStartDate;
	
	@Nationalized
	@Column(name="terms_condition_message")
	private String termsConditionMessage;

	//bi-directional many-to-one association to TBeneficiarydetail
	@ManyToOne
	@JoinColumn(name="BENEFICIARY_ID",insertable = false,updatable = false)
	private TBeneficiarydetail TBeneficiarydetail;

	//bi-directional many-to-one association to TLg
	@ManyToOne
	@JoinColumn(name="CASE_ID", insertable=false, updatable=false)
	private TLg TLg;

	//bi-directional many-to-one association to TServicecatalog
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="FROM_DATE", referencedColumnName="FROM_DATE"),
		@JoinColumn(name="SERVICE_CATALOG_ID", referencedColumnName="SERVICE_CATALOG_ID")
		})
	private TServicecatalog TServicecatalog;

	//bi-directional many-to-one association to TSupplierdetail
	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="SUPPLIER_ID") private TSupplierdetail TSupplierdetail;
	 */

	public TLgdetail() {
	}

	public TLgdetailPK getId() {
		return this.id;
	}

	public void setId(TLgdetailPK id) {
		this.id = id;
	}

	public String getBankCode() {
		return this.bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchNameAr() {
		return this.branchNameAr;
	}

	public void setBranchNameAr(String branchNameAr) {
		this.branchNameAr = branchNameAr;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Object getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public BigDecimal getLgAmount() {
		return this.lgAmount;
	}

	public void setLgAmount(BigDecimal lgAmount) {
		this.lgAmount = lgAmount;
	}

	public Object getLgDate() {
		return this.lgDate;
	}

	public void setLgDate(Timestamp lgDate) {
		this.lgDate = lgDate;
	}

	public Object getLgIssueDate() {
		return this.lgIssueDate;
	}

	public void setLgIssueDate(Timestamp lgIssueDate) {
		this.lgIssueDate = lgIssueDate;
	}

	public String getLgNo() {
		return this.lgNo;
	}

	public void setLgNo(String lgNo) {
		this.lgNo = lgNo;
	}

	public Object getLgValidityDate() {
		return this.lgValidityDate;
	}

	public void setLgValidityDate(Timestamp lgValidityDate) {
		this.lgValidityDate = lgValidityDate;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Object getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getProjectId() {
		return this.projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return this.projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectNameAr() {
		return this.projectNameAr;
	}

	public void setProjectNameAr(String projectNameAr) {
		this.projectNameAr = projectNameAr;
	}

	public String getRequestId() {
		return this.requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public BigDecimal getRoleId() {
		return this.roleId;
	}

	public void setRoleId(BigDecimal roleId) {
		this.roleId = roleId;
	}

	public String getTermsConditionType() {
		return this.termsConditionType;
	}

	public void setTermsConditionType(String termsConditionType) {
		this.termsConditionType = termsConditionType;
	}

	public Object getTransDate() {
		return this.transDate;
	}

	public void setTransDate(Timestamp transDate) {
		this.transDate = transDate;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Object getZakatEndDate() {
		return this.zakatEndDate;
	}

	public void setZakatEndDate(Timestamp zakatEndDate) {
		this.zakatEndDate = zakatEndDate;
	}

	public Object getZakatStartDate() {
		return this.zakatStartDate;
	}

	public void setZakatStartDate(Timestamp zakatStartDate) {
		this.zakatStartDate = zakatStartDate;
	}

	public TBeneficiarydetail getTBeneficiarydetail() {
		return this.TBeneficiarydetail;
	}

	public void setTBeneficiarydetail(TBeneficiarydetail TBeneficiarydetail) {
		this.TBeneficiarydetail = TBeneficiarydetail;
	}

	public TLg getTLg() {
		return this.TLg;
	}

	public void setTLg(TLg TLg) {
		this.TLg = TLg;
	}

	public TServicecatalog getTServicecatalog() {
		return this.TServicecatalog;
	}

	public void setTServicecatalog(TServicecatalog TServicecatalog) {
		this.TServicecatalog = TServicecatalog;
	}

	/*
	 * public TSupplierdetail getTSupplierdetail() { return this.TSupplierdetail; }
	 * 
	 * public void setTSupplierdetail(TSupplierdetail TSupplierdetail) {
	 * this.TSupplierdetail = TSupplierdetail; }
	 */

	public String getTermsConditionMessage() {
		return termsConditionMessage;
	}

	public void setTermsConditionMessage(String termsConditionMessage) {
		this.termsConditionMessage = termsConditionMessage;
	}
	
	

}