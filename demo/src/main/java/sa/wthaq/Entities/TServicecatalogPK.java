package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the T_SERVICECATALOG database table.
 * 
 */
@Embeddable
public class TServicecatalogPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="SERVICE_CATALOG_ID")
	private long serviceCatalogId;

	@Column(name="FROM_DATE")
	private String fromDate;

	public TServicecatalogPK() {
	}
	public long getServiceCatalogId() {
		return this.serviceCatalogId;
	}
	public void setServiceCatalogId(long serviceCatalogId) {
		this.serviceCatalogId = serviceCatalogId;
	}
	public String getFromDate() {
		return this.fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TServicecatalogPK)) {
			return false;
		}
		TServicecatalogPK castOther = (TServicecatalogPK)other;
		return 
			(this.serviceCatalogId == castOther.serviceCatalogId)
			&& this.fromDate.equals(castOther.fromDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.serviceCatalogId ^ (this.serviceCatalogId >>> 32)));
		hash = hash * prime + this.fromDate.hashCode();
		
		return hash;
	}
}