package sa.wthaq.Entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the T_ROLEMASTER database table.
 * 
 */
@Entity
@Table(name="T_ROLEMASTER")
@NamedQuery(name="TRolemaster.findAll", query="SELECT t FROM TRolemaster t")
public class TRolemaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ROLE_ID")
	private int roleId;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="ROLE_DESC")
	private String roleDesc;

	@Column(name="ROLE_DESC_AR")
	private String roleDescAr;

	//bi-directional many-to-one association to TRolemenumap
	@OneToMany(mappedBy="TRolemaster1")
	private List<TRolemenumap> TRolemenumaps1;

	/*//bi-directional many-to-one association to TRolemenumap
	@OneToMany(mappedBy="TRolemaster2")
	private List<TRolemenumap> TRolemenumaps2;
*/
	public TRolemaster() {
	}

	public long getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Object getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Object getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getRoleDesc() {
		return this.roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public String getRoleDescAr() {
		return this.roleDescAr;
	}

	public void setRoleDescAr(String roleDescAr) {
		this.roleDescAr = roleDescAr;
	}

	public List<TRolemenumap> getTRolemenumaps1() {
		return this.TRolemenumaps1;
	}

	public void setTRolemenumaps1(List<TRolemenumap> TRolemenumaps1) {
		this.TRolemenumaps1 = TRolemenumaps1;
	}

	public TRolemenumap addTRolemenumaps1(TRolemenumap TRolemenumaps1) {
		getTRolemenumaps1().add(TRolemenumaps1);
		TRolemenumaps1.setTRolemaster1(this);

		return TRolemenumaps1;
	}

	public TRolemenumap removeTRolemenumaps1(TRolemenumap TRolemenumaps1) {
		getTRolemenumaps1().remove(TRolemenumaps1);
		TRolemenumaps1.setTRolemaster1(null);

		return TRolemenumaps1;
	}

	/*public List<TRolemenumap> getTRolemenumaps2() {
		return this.TRolemenumaps2;
	}

	public void setTRolemenumaps2(List<TRolemenumap> TRolemenumaps2) {
		this.TRolemenumaps2 = TRolemenumaps2;
	}

	public TRolemenumap addTRolemenumaps2(TRolemenumap TRolemenumaps2) {
		getTRolemenumaps2().add(TRolemenumaps2);
		TRolemenumaps2.setTRolemaster2(this);

		return TRolemenumaps2;
	}

	public TRolemenumap removeTRolemenumaps2(TRolemenumap TRolemenumaps2) {
		getTRolemenumaps2().remove(TRolemenumaps2);
		TRolemenumaps2.setTRolemaster2(null);

		return TRolemenumaps2;
	}
*/
}