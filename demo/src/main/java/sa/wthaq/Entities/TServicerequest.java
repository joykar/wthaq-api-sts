package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Nationalized;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the T_SERVICEREQUEST database table.
 * 
 */
@Entity
@Table(name="T_SERVICEREQUEST")
@NamedQuery(name="TServicerequest.findAll", query="SELECT t FROM TServicerequest t")
public class TServicerequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="REQUEST_ID")
	private int requestId;

	@Column(name="BRANCH_NAME")
	private String branchName;

	@Column(name="BRANCH_NAME_AR")
	private String branchNameAr;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="CURRENCY_CODE")
	private String currencyCode;

	@Column(name="DELETE_FLAG")
	private String deleteFlag="N";

	@Column(name="MODIFIED_BY")
	private String modifiedBy="SYSTEM";

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="REQUEST_DATE")
	private Timestamp requestDate=new Timestamp(new Date().getTime());

	@Column(name="REQUEST_STATUS_FLAG")
	private String requestStatusFlag;

	@Column(name="ROLE_ID")
	private int roleId;

	@Column(name="TERMS_CONDITION_TYPE")
	private String tearmsConditionType;

	@Column(name="ZAKAT_END_DATE")
	private Timestamp zakatEndDate;

	@Column(name="ZAKAT_START_DATE")
	private Timestamp zakatStartDate;
	
	@Column(name="BANK_CODE")
	private String bankCode;
	
	@Column(name="lg_type_id")
	private int lgTypeId;
	
	@Column(name="LOGIN_ID")
	private String loginId;
	
	@Nationalized
	@Column(name="terms_condition_message")
	private String termsConditionMessage;

	//bi-directional many-to-one association to TPaymentsettlement
	@OneToMany(mappedBy="TServicerequest",fetch = FetchType.LAZY)
	private List<TPaymentsettlement> TPaymentsettlements;

	//bi-directional many-to-one association to TBankmaster
	/*
	 * @JsonIgnoreProperties("TServicerequests")
	 * 
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name="BANK_CODE",insertable = false,updatable = false) private
	 * TBankmaster TBankmaster;
	 */
	
	

	//bi-directional many-to-one association to TLgtypemaster
	//@JsonIgnore
	//@ManyToOne(fetch = FetchType.LAZY)
	//@JoinColumn(name="LG_TYPE_ID",insertable = false,updatable = false)
	//private TLgtypemaster TLgtypemaster;

	//bi-directional one-to-one association to TNotification
	/*
	 * @OneToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name="REQUEST_ID",insertable = false,updatable = false) private
	 * TNotification TNotification;
	 */

	//bi-directional many-to-one association to TServicecatalog
	@JsonIgnore()
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="FROM_DATE", referencedColumnName="FROM_DATE",insertable = false,updatable = false),
		@JoinColumn(name="SERVICE_CATALOG_ID", referencedColumnName="SERVICE_CATALOG_ID",insertable = false,updatable = false)
		})
	private TServicecatalog TServicecatalog;

	//bi-directional many-to-one association to TUsermaster
	
	//@ManyToOne(fetch = FetchType.LAZY)
	//@JoinColumn(name="LOGIN_ID",insertable = false,updatable = false)
	//private TUsermaster TUsermaster;
	
	public TServicerequest() {
	}

	public int getRequestId() {
		return this.requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchNameAr() {
		return this.branchNameAr;
	}

	public void setBranchNameAr(String branchNameAr) {
		this.branchNameAr = branchNameAr;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Object getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Object getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Object getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Timestamp requestDate) {
		this.requestDate = requestDate;
	}

	public String getRequestStatusFlag() {
		return this.requestStatusFlag;
	}

	public void setRequestStatusFlag(String requestStatusFlag) {
		this.requestStatusFlag = requestStatusFlag;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getTearmsConditionType() {
		return this.tearmsConditionType;
	}

	public void setTearmsConditionType(String tearmsConditionType) {
		this.tearmsConditionType = tearmsConditionType;
	}

	public Object getZakatEndDate() {
		return this.zakatEndDate;
	}

	public void setZakatEndDate(Timestamp zakatEndDate) {
		this.zakatEndDate = zakatEndDate;
	}

	public Object getZakatStartDate() {
		return this.zakatStartDate;
	}

	public void setZakatStartDate(Timestamp zakatStartDate) {
		this.zakatStartDate = zakatStartDate;
	}

	public List<TPaymentsettlement> getTPaymentsettlements() {
		return this.TPaymentsettlements;
	}

	public void setTPaymentsettlements(List<TPaymentsettlement> TPaymentsettlements) {
		this.TPaymentsettlements = TPaymentsettlements;
	}

	public TPaymentsettlement addTPaymentsettlement(TPaymentsettlement TPaymentsettlement) {
		getTPaymentsettlements().add(TPaymentsettlement);
		TPaymentsettlement.setTServicerequest(this);

		return TPaymentsettlement;
	}

	public TPaymentsettlement removeTPaymentsettlement(TPaymentsettlement TPaymentsettlement) {
		getTPaymentsettlements().remove(TPaymentsettlement);
		TPaymentsettlement.setTServicerequest(null);

		return TPaymentsettlement;
	}

	/*
	 * public TBankmaster getTBankmaster() { return this.TBankmaster; }
	 * 
	 * public void setTBankmaster(TBankmaster TBankmaster) { this.TBankmaster =
	 * TBankmaster; }
	 */

	/*
	 * public TLgtypemaster getTLgtypemaster() { return this.TLgtypemaster; }
	 * 
	 * public void setTLgtypemaster(TLgtypemaster TLgtypemaster) {
	 * this.TLgtypemaster = TLgtypemaster; }
	 */

	/*
	 * public TNotification getTNotification() { return this.TNotification; }
	 * 
	 * public void setTNotification(TNotification TNotification) {
	 * this.TNotification = TNotification; }
	 */

	public TServicecatalog getTServicecatalog() {
		return this.TServicecatalog;
	}

	public void setTServicecatalog(TServicecatalog TServicecatalog) {
		this.TServicecatalog = TServicecatalog;
	}

	/*
	 * public TUsermaster getTUsermaster() { return this.TUsermaster; }
	 * 
	 * public void setTUsermaster(TUsermaster TUsermaster) { this.TUsermaster =
	 * TUsermaster; }
	 */

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	

	public int getLgTypeId() {
		return lgTypeId;
	}

	public void setLgTypeId(int lgTypeId) {
		this.lgTypeId = lgTypeId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getTermsConditionMessage() {
		return termsConditionMessage;
	}

	public void setTermsConditionMessage(String termsConditionMessage) {
		this.termsConditionMessage = termsConditionMessage;
	}
	
	

}