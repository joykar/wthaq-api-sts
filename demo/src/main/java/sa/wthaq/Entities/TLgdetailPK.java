package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the T_LGDETAILS database table.
 * 
 */
@Embeddable
public class TLgdetailPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="CASE_ID")
	private long caseId;

	@Column(name="TRANS_ID")
	private long transId;

	public TLgdetailPK() {
	}
	public long getCaseId() {
		return this.caseId;
	}
	public void setCaseId(long caseId) {
		this.caseId = caseId;
	}
	public long getTransId() {
		return this.transId;
	}
	public void setTransId(long transId) {
		this.transId = transId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TLgdetailPK)) {
			return false;
		}
		TLgdetailPK castOther = (TLgdetailPK)other;
		return 
			(this.caseId == castOther.caseId)
			&& (this.transId == castOther.transId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.caseId ^ (this.caseId >>> 32)));
		hash = hash * prime + ((int) (this.transId ^ (this.transId >>> 32)));
		
		return hash;
	}
}