package sa.wthaq.Entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the T_STATUSCODEMASTER database table.
 * 
 */
@Entity
@Table(name="T_STATUSCODEMASTER")
@NamedQuery(name="TStatuscodemaster.findAll", query="SELECT t FROM TStatuscodemaster t")
public class TStatuscodemaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="STATUS_ID")
	private long statusId;

	@Column(name="CREATE_BY")
	private String createBy;

	@Column(name="CREATE_ON")
	private Timestamp createOn;

	@Column(name="DELETE_FLAG")
	private String deleteFlag;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="MODIFIED_ON")
	private Timestamp modifiedOn;

	@Column(name="STATUS_DESC")
	private String statusDesc;

	@Column(name="STATUS_DESC_AR")
	private String statusDescAr;

	@Column(name="STATUS_TYPE")
	private String statusType;

	//bi-directional many-to-one association to TAckfrombank
	@OneToMany(mappedBy="TStatuscodemaster")
	private List<TAckfrombank> TAckfrombanks;

	//bi-directional many-to-one association to TAcktobank
	@OneToMany(mappedBy="TStatuscodemaster")
	private List<TAcktobank> TAcktobanks;

	//bi-directional many-to-one association to TNotificationtemplate
	@OneToMany(mappedBy="TStatuscodemaster")
	private List<TNotificationtemplate> TNotificationtemplates;

	public TStatuscodemaster() {
	}

	public long getStatusId() {
		return this.statusId;
	}

	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Timestamp getCreateOn() {
		return this.createOn;
	}

	public void setCreateOn(Timestamp createOn) {
		this.createOn = createOn;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Object getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getStatusDesc() {
		return this.statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getStatusDescAr() {
		return this.statusDescAr;
	}

	public void setStatusDescAr(String statusDescAr) {
		this.statusDescAr = statusDescAr;
	}

	public String getStatusType() {
		return this.statusType;
	}

	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}

	public List<TAckfrombank> getTAckfrombanks() {
		return this.TAckfrombanks;
	}

	public void setTAckfrombanks(List<TAckfrombank> TAckfrombanks) {
		this.TAckfrombanks = TAckfrombanks;
	}

	public TAckfrombank addTAckfrombank(TAckfrombank TAckfrombank) {
		getTAckfrombanks().add(TAckfrombank);
		TAckfrombank.setTStatuscodemaster(this);

		return TAckfrombank;
	}

	public TAckfrombank removeTAckfrombank(TAckfrombank TAckfrombank) {
		getTAckfrombanks().remove(TAckfrombank);
		TAckfrombank.setTStatuscodemaster(null);

		return TAckfrombank;
	}

	public List<TAcktobank> getTAcktobanks() {
		return this.TAcktobanks;
	}

	public void setTAcktobanks(List<TAcktobank> TAcktobanks) {
		this.TAcktobanks = TAcktobanks;
	}

	public TAcktobank addTAcktobank(TAcktobank TAcktobank) {
		getTAcktobanks().add(TAcktobank);
		TAcktobank.setTStatuscodemaster(this);

		return TAcktobank;
	}

	public TAcktobank removeTAcktobank(TAcktobank TAcktobank) {
		getTAcktobanks().remove(TAcktobank);
		TAcktobank.setTStatuscodemaster(null);

		return TAcktobank;
	}

	public List<TNotificationtemplate> getTNotificationtemplates() {
		return this.TNotificationtemplates;
	}

	public void setTNotificationtemplates(List<TNotificationtemplate> TNotificationtemplates) {
		this.TNotificationtemplates = TNotificationtemplates;
	}

	public TNotificationtemplate addTNotificationtemplate(TNotificationtemplate TNotificationtemplate) {
		getTNotificationtemplates().add(TNotificationtemplate);
		TNotificationtemplate.setTStatuscodemaster(this);

		return TNotificationtemplate;
	}

	public TNotificationtemplate removeTNotificationtemplate(TNotificationtemplate TNotificationtemplate) {
		getTNotificationtemplates().remove(TNotificationtemplate);
		TNotificationtemplate.setTStatuscodemaster(null);

		return TNotificationtemplate;
	}

}