package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the T_BANKBENSUPPLIERLINK database table.
 * 
 */
@Embeddable
public class TBankbensupplierlinkPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="LINK_ID")
	private long linkId;

	@Column(name="ROLE_ID")
	private long roleId;

	@Column(name="BANK_CODE")
	private String bankCode;

	@Column(name="BRANCH_NAME")
	private String branchName;

	public TBankbensupplierlinkPK() {
	}
	public long getLinkId() {
		return this.linkId;
	}
	public void setLinkId(long linkId) {
		this.linkId = linkId;
	}
	public long getRoleId() {
		return this.roleId;
	}
	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}
	public String getBankCode() {
		return this.bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getBranchName() {
		return this.branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TBankbensupplierlinkPK)) {
			return false;
		}
		TBankbensupplierlinkPK castOther = (TBankbensupplierlinkPK)other;
		return 
			(this.linkId == castOther.linkId)
			&& (this.roleId == castOther.roleId)
			&& this.bankCode.equals(castOther.bankCode)
			&& this.branchName.equals(castOther.branchName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.linkId ^ (this.linkId >>> 32)));
		hash = hash * prime + ((int) (this.roleId ^ (this.roleId >>> 32)));
		hash = hash * prime + this.bankCode.hashCode();
		hash = hash * prime + this.branchName.hashCode();
		
		return hash;
	}
}