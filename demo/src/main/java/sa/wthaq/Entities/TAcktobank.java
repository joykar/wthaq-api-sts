package sa.wthaq.Entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The persistent class for the T_ACKTOBANK database table.
 * 
 */
@Entity
@Table(name="T_ACKTOBANK")
@NamedQuery(name="TAcktobank.findAll", query="SELECT t FROM TAcktobank t")
public class TAcktobank implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REQUEST_ID")
	private long requestId;

	@Column(name="ACK_DATE")
	private Timestamp ackDate;
	
	@Column(name="BANK_CODE")
	private String bankCode;


	@Column(name="BRANCH_NAME")
	private String branchName;

	@Column(name="MESSAGE_DESC")
	private String messageDesc;

	@Column(name="MESSAGE_DESC_AR")
	private String messageDescAr;

	@Column(name="MESSAGE_ID")
	private String messegeId;

	//bi-directional many-to-one association to TBankmaster
	/*
	 * @JsonIgnoreProperties("TAcktobanks")
	 * 
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name="BANK_CODE") private TBankmaster TBankmaster;
	 */

	//bi-directional many-to-one association to TStatuscodemaster
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="STATUS_ID")
	private TStatuscodemaster TStatuscodemaster;

	public TAcktobank() {
	}

	public long getRequestId() {
		return this.requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	public Object getAckDate() {
		return this.ackDate;
	}

	public void setAckDate(Timestamp ackDate) {
		this.ackDate = ackDate;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getMessageDesc() {
		return this.messageDesc;
	}

	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}

	public String getMessageDescAr() {
		return this.messageDescAr;
	}

	public void setMessageDescAr(String messageDescAr) {
		this.messageDescAr = messageDescAr;
	}

	public String getMessegeId() {
		return this.messegeId;
	}

	public void setMessegeId(String messegeId) {
		this.messegeId = messegeId;
	}

	/*
	 * public TBankmaster getTBankmaster() { return this.TBankmaster; }
	 * 
	 * public void setTBankmaster(TBankmaster TBankmaster) { this.TBankmaster =
	 * TBankmaster; }
	 */

	public TStatuscodemaster getTStatuscodemaster() {
		return this.TStatuscodemaster;
	}

	public void setTStatuscodemaster(TStatuscodemaster TStatuscodemaster) {
		this.TStatuscodemaster = TStatuscodemaster;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	
	

}