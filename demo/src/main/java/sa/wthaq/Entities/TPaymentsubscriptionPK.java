package sa.wthaq.Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the T_PAYMENTSUBSCRIPTION database table.
 * 
 */
@Embeddable
public class TPaymentsubscriptionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="PAY_DATE")
	private String payDate;

	@Column(name="LOGIN_ID")
	private String loginId;

	public TPaymentsubscriptionPK() {
	}
	public String getPayDate() {
		return this.payDate;
	}
	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}
	public String getLoginId() {
		return this.loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TPaymentsubscriptionPK)) {
			return false;
		}
		TPaymentsubscriptionPK castOther = (TPaymentsubscriptionPK)other;
		return 
			this.payDate.equals(castOther.payDate)
			&& this.loginId.equals(castOther.loginId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.payDate.hashCode();
		hash = hash * prime + this.loginId.hashCode();
		
		return hash;
	}
}