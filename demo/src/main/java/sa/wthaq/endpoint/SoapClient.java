package sa.wthaq.endpoint;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



import sa.wthaq.request.BankInfo;
import sa.wthaq.request.LgReturnInfo;
import sa.wthaq.request.ProfileAdmin;
import sa.wthaq.request.UserDetail;
import sa.wthaq.webservices.GetRequestData;
import sa.wthaq.webservices.GetRequestDataResponse;
import sa.wthaq.webservices.VerificationAdminDetails;
import sa.wthaq.webservices.VerificationAdminDetailsResponse;
import sa.wthaq.webservices.VerifyIbanDetail;
import sa.wthaq.webservices.VerifyIbanDetailResponse;

@Service
public class SoapClient {
//	@Autowired
   // private Jaxb2Marshaller jaxb2Marshaller;
	
	@Autowired
	private Environment env;

    //private WebServiceTemplate webServiceTemplate;

   
	public UserDetail getInfo(GetRequestData itemRequest) throws SOAPException, JAXBException, SAXException, IOException, ParserConfigurationException{
		UserDetail detail = new UserDetail();
	
		 
		  String xmlString = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sch=\"http://webservices.wthaq.sa\">" + 
		  		"   <soapenv:Header/>" + 
		  		"   <soapenv:Body>" + 
		  		"      <sch:getRequestData>" + 
		  		"         <sch:crn>"+itemRequest.getCrn()+"</sch:crn>" + 
		  		"         <sch:mno>" +itemRequest.getMno()+ "</sch:mno>" + 
		  		"      </sch:getRequestData>" + 
		  		"   </soapenv:Body>" + 
		  		"</soapenv:Envelope>";
		  RestTemplate restTemplate =  new RestTemplate();
		  HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.TEXT_PLAIN);
		    HttpEntity<String> request = new HttpEntity<String>(xmlString, headers);

		    ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8090/eLG-WebServicesRequest/services/ExternalSoapRequestImpl/getRequestData", request, String.class);

		  System.out.println("xml data"+response.getBody());
		  Document doc = DocumentBuilderFactory.newInstance()
                  .newDocumentBuilder()
                  .parse(new InputSource(new StringReader(response.getBody())));
		  doc.getDocumentElement().normalize();

		  NodeList nList = doc.getElementsByTagName("ns:return");
		  Node node = nList.item(0);
		  Element e = (Element) node;
		  System.out.println("length of node "+e.getElementsByTagName("ax21:address1").item(0).getTextContent());
		  detail.setAddress1(e.getElementsByTagName("ax21:address1").item(0).getTextContent());
		  detail.setAddress2(e.getElementsByTagName("ax21:address2").item(0).getTextContent());
		  detail.setContactNo(e.getElementsByTagName("ax21:contactNo").item(0).getTextContent());
		  detail.setCrNumber(e.getElementsByTagName("ax21:crNumber").item(0).getTextContent());
		  detail.setEmailAddress(e.getElementsByTagName("ax21:emailAddress").item(0).getTextContent());
		  detail.setEntityName(e.getElementsByTagName("ax21:entityName").item(0).getTextContent());
		 
		  return detail;
    }
	
	public ProfileAdmin getAcknowledgeMessage(VerificationAdminDetails itemRequest) throws SAXException, IOException, ParserConfigurationException{
		
        
		ProfileAdmin admin = new ProfileAdmin();
        
        String xmlString =   "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sch=\"http://webservices.wthaq.sa\">"+
        "<soapenv:Header/>"+
        "<soapenv:Body>"+
         "  <sch:verificationAdminDetails>"+
         "    <sch:ad>"+
         "      <sch:rep_Id>"+itemRequest.getAd().getRepId()+"</sch:rep_Id>"+
         "      <sch:rep_name>"+itemRequest.getAd().getRepName()+"</sch:rep_name>"+
         "       <sch:rep_email>"+itemRequest.getAd().getRepEmail()+"</sch:rep_email>"+
         "       <sch:rep_mobile_No>"+itemRequest.getAd().getRepMobileNo()+"</sch:rep_mobile_No>"+
         "      <sch:rep_name_ar>"+itemRequest.getAd().getRepNameAr()+"</sch:rep_name_ar>"+
         "       <sch:user_Id>"+itemRequest.getAd().getUserId()+"</sch:user_Id>"+
         "       </sch:ad>"+
         "     </sch:verificationAdminDetails>"+
         "   </soapenv:Body>"+
         "   </soapenv:Envelope>";
        
        
        RestTemplate restTemplate =  new RestTemplate();
		  HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.TEXT_PLAIN);
		    HttpEntity<String> request = new HttpEntity<String>(xmlString, headers);

		    ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8090/eLG-WebServicesRequest/services/ExternalSoapRequestImpl/verificationAdminDetails", request, String.class);

		  System.out.println("xml data"+response.getBody());
		  Document doc = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder()
                .parse(new InputSource(new StringReader(response.getBody())));
		  doc.getDocumentElement().normalize();

		  NodeList nList = doc.getElementsByTagName("ns:return");
		  Node node = nList.item(0);
		  Element e = (Element) node;
		  System.out.println("length of node "+node.getTextContent());
		  admin.setRepMsg(node.getTextContent());
       
         return admin;
    }
	
	public BankInfo getAcknowledgeMessageFromBank(VerifyIbanDetail itemRequest) throws SAXException, IOException, ParserConfigurationException{
	
        
		BankInfo info = new BankInfo();
        String xmlString =   "  <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sch=\"http://webservices.wthaq.sa\">"+
        "<soapenv:Header/>"+
        "<soapenv:Body>"+
           "<sch:verifyIbanDetail>"+
              "<sch:bi>"+itemRequest.getBi()+"</sch:bi>"+
              "<sch:bankName>"+itemRequest.getBankName()+"</sch:bankName>"+
           "</sch:verifyIbanDetail>"+
       " </soapenv:Body>"+
     "</soapenv:Envelope>";
        
        
        
        RestTemplate restTemplate =  new RestTemplate();
		  HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.TEXT_PLAIN);
		    HttpEntity<String> request = new HttpEntity<String>(xmlString, headers);

		    ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8090/eLG-WebServicesRequest/services/ExternalSoapRequestImpl/verifyIbanDetail", request, String.class);

		  System.out.println("xml data"+response.getBody());
		  Document doc = DocumentBuilderFactory.newInstance()
              .newDocumentBuilder()
              .parse(new InputSource(new StringReader(response.getBody())));
		  doc.getDocumentElement().normalize();

		  NodeList nList = doc.getElementsByTagName("ns:return");
		  Node node = nList.item(0);
		  Element e = (Element) node;
		  System.out.println("length of node "+node.getTextContent());
		  info.setMsg(node.getTextContent());
     
       return info;
        
        
        
        
        
        
        
        
        
        
        
        
        
		
    }
	
	

public String sendLgIssueToBank(LgReturnInfo itemRequest) throws SAXException, IOException, ParserConfigurationException{
	
        
		//BankInfo info = new BankInfo();
        String xmlString =   "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:hs=\"http://webservices.wthaq.sa\">"
       +"<soapenv:Header/>"
       +"<soapenv:Body>"
       +"<hs:sendIssueOfLGtoBankToVerify>"
             +"<hs:request_Id>"+itemRequest.getRequestId()+"</hs:request_Id>"
            		+"<hs:info>"
                		+"<hs:supplierName>"+itemRequest.getSupplierInfo().getSupplierName()+"</hs:supplierName>"
                		+"<hs:applicantCR>"+itemRequest.getSupplierInfo().getApplicantCR()+"</hs:applicantCR>"
                		 +"<hs:applicantIBAN>"+itemRequest.getSupplierInfo().getApplicantIBAN()+"</hs:applicantIBAN>"
                		+"<hs:PObox>"+itemRequest.getSupplierInfo().getPObox()+"</hs:PObox>"
                		 +"<hs:postalcode>"+itemRequest.getSupplierInfo().getPostalcode()+"</hs:postalcode>"
                		  +"<hs:Address>"+itemRequest.getSupplierInfo().getAddress()+"</hs:Address>"
                		   +"<hs:contactNumber>"+itemRequest.getSupplierInfo().getContactNumber()+"</hs:contactNumber>"
                		   +"<hs:faxNumber>"+itemRequest.getSupplierInfo().getFaxNumber()+"</hs:faxNumber>"
                
+"<hs:emailAddress>"+itemRequest.getSupplierInfo().getEmailAddress()+"</hs:emailAddress>"
                		+"</hs:info>"
                		+"<hs:info2>"
                		+"<hs:beneficiaryName>"+itemRequest.getBeneficiaryInfo().getBeneficiaryName()+"</hs:beneficiaryName>"
                		+"<hs:beneficiaryID>"+itemRequest.getBeneficiaryInfo().getBeneficiaryID()+"</hs:beneficiaryID>"
                
+"<hs:PObox>Riyadh</hs:PObox>"
                		 +"<hs:postalcode>"+itemRequest.getBeneficiaryInfo().getPostalCode()+"</hs:postalcode>"
                		  +"<hs:Address>"+itemRequest.getBeneficiaryInfo().getAddress()+"</hs:Address>"
                		   +"<hs:contactNumber>"+itemRequest.getBeneficiaryInfo().getContactNumber()+"</hs:contactNumber>"
                		   +"<hs:FaxNumber>"+itemRequest.getBeneficiaryInfo().getFaxNumber()+"</hs:FaxNumber>"
                
+"<hs:emailAddress>"+itemRequest.getBeneficiaryInfo().getEmailAddress()+"</hs:emailAddress>"
                		+"</hs:info2>"
                
+"<hs:lg_Info>"
                		+"<hs:LGType>"+itemRequest.getInfo().getLGType()+"</hs:LGType>"
                		+"<hs:projectName>"+itemRequest.getInfo().getProjectName()+"</hs:projectName>"
                		+"<hs:projectNumber>"+itemRequest.getInfo().getProjectNumber()+"</hs:projectNumber>"
                		+"<hs:byanNumber>"+itemRequest.getInfo().getByanNumber()+"</hs:byanNumber>"
                		+"<hs:purposeOfBbond>"+itemRequest.getInfo().getPurposeOfBbond()+"</hs:purposeOfBbond>"
                		+"<hs:zakatPeriodStart></hs:zakatPeriodStart>"
                		+"<hs:zakatPeriodEndDate></hs:zakatPeriodEndDate>"
                		+"<hs:LGValidity></hs:LGValidity>"
                		+"<hs:hijriDate></hs:hijriDate>"
                		+"<hs:amount>"+itemRequest.getInfo().getAmount()+"</hs:amount>"
                		+"<hs:amountText>"+itemRequest.getInfo().getAmountText()+"</hs:amountText>"
                		+"<hs:percentageOfContract_value>"+itemRequest.getInfo().getPercentageOfContractValue()+"</hs:percentageOfContract_value>"
                		+"<hs:currency>"+itemRequest.getInfo().getCurrency()+"</hs:currency>"
                
                
+"</hs:lg_Info>"
                
+"<hs:conditions>"
                		+"<hs:actualApplicantarName>"+itemRequest.getConditions().getActualApplicantarName()+"</hs:actualApplicantarName>"
                		 +"<hs:actualApplicantenName>"+itemRequest.getConditions().getActualApplicantenName()+"</hs:actualApplicantenName>"
                		 +"<hs:openEndedLG>true</hs:openEndedLG>"
            		  +"<hs:autoRenewalLG>true</hs:autoRenewalLG>"
            		  +"<hs:assignableLG>true</hs:assignableLG>"
            		  +"<hs:transferableLG>true</hs:transferableLG>"
            		  +"<hs:PObox>"+itemRequest.getConditions().getPObox()+"</hs:PObox>"
                		 +"<hs:postalcode>"+itemRequest.getConditions().getPostalCode()+"</hs:postalcode>"
                		  +"<hs:Address>"+itemRequest.getConditions().getAddress()+"</hs:Address>"
                		   +"<hs:contactNumber>"+itemRequest.getConditions().getContactNumber()+"</hs:contactNumber>"
                		   +"<hs:faxNumber>"+itemRequest.getConditions().getFaxNumber()+"</hs:faxNumber>"              
+"<hs:emailAddress>"+itemRequest.getConditions().getEmailAddress()+"</hs:emailAddress>"               
+" </hs:conditions>"                
+"</hs:sendIssueOfLGtoBankToVerify>"
            		 +"</soapenv:Body>"
       +"</soapenv:Envelope>";
        
        int dev =45;
        System.out.println("dev"+dev);
        
        RestTemplate restTemplate =  new RestTemplate();
		  HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.TEXT_PLAIN);
		    HttpEntity<String> request = new HttpEntity<String>(xmlString, headers);

		    ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8080/eLG-WebServicesRequest/services/ExternalSoapRequestImpl/sendIssueOfLGtoBankToVerify", request, String.class);

		  System.out.println("xml data"+response.getBody());
		  Document doc = DocumentBuilderFactory.newInstance()
              .newDocumentBuilder()
              .parse(new InputSource(new StringReader(response.getBody())));
		  doc.getDocumentElement().normalize();

		  NodeList nList = doc.getElementsByTagName("ns:return");
		  Node node = nList.item(0);
		  Element e = (Element) node;
		  System.out.println("length of node "+node.getTextContent());
		  String message = node.getTextContent();
     
       return message;
        
        
        
        
        
        
        
        
        
        
        
        
        
		
    }
	
	

}
