package sa.wthaq.JPA.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sa.wthaq.Entities.TServicerequest;
import sa.wthaq.Entities.TSupplierdetail;

public interface TSupplierdetailRepo extends JpaRepository<TSupplierdetail, Long> {

	TSupplierdetail findByLoginId(String loginid);
}
