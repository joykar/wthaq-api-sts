package sa.wthaq.JPA.Repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import sa.wthaq.Entities.TUnRegisteredBen;

public interface UnregisteredBenRepo extends JpaRepository<TUnRegisteredBen, Integer>{
	
	@Query("select tb.beneficiaryId,tb.beneficiaryName,tb.beneficiary_UID,tb.deletedFlag from TUnRegisteredBen tb where tb.deletedFlag IN (?1,?2)")
	 List <?> findUnregisteredBeneficiary(String val,String val2);
	
	TUnRegisteredBen findByBeneficiaryId(int id);
	
	//TUnRegisteredBen findBybeneficiary_UID(String uid);
	
	@Transactional
	@Modifying
	@Query(value="Update t_UnRegisteredBen SET deleted_flag = ?1 WHERE beneficiary_UID = ?2",nativeQuery=true)
	void updateUnregisterBenStatus(String str, String uId );
	
	
	@Transactional
	@Modifying
	@Query(value="Update t_UnRegisteredBen SET deleted_flag =?1 WHERE beneficiary_name = ?2",nativeQuery=true)
	void updateUnregisterBenStatusWithName(String str, String benName );
	
	
	
	  @Query("select tb from TUnRegisteredBen tb where tb.beneficiary_UID = ?1") 
	  TUnRegisteredBen findBybeneficiary_UID(String val);
	 

}
