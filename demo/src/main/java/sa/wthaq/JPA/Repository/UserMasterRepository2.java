package sa.wthaq.JPA.Repository;

 
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.transaction.Transactional;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import sa.wthaq.DTO.UserDTO;
import sa.wthaq.Entities.TUsermaster2;
import sa.wthaq.models.LoginModel;

 
@Repository
//@EnableJpaRepositories(basePackages="eLG", entityManagerFactoryRef="entityManagerFactory")
//@EntityScan(basePackages ="com.wethaq.elog.Entities")
public interface UserMasterRepository2 extends JpaRepository< TUsermaster2, Long> {
	
	
	@Query("SELECT u FROM TUsermaster2 u WHERE u.loginId = ?1 and u.password = ?2")
	List < TUsermaster2  > findUserByLoginIdAndPass(String loginId, String password);
	
	TUsermaster2 findByLoginId(String username);

}




