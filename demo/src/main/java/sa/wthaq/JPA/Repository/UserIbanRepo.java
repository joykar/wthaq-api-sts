package sa.wthaq.JPA.Repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import sa.wthaq.Entities.TUserIbanMapping;

interface TBankmaster {
    String getBankCode();
    String getBankName();
}

interface UserIbanRepoList {
    String getIbanNo();
    sa.wthaq.Entities.TBankmaster gettBankmaster();
}

public interface UserIbanRepo extends JpaRepository<TUserIbanMapping, Integer> {

	List<UserIbanRepoList> findByLoginId(String loginid);
	
	@Query("from sa.wthaq.Entities.TBankmaster")
	List<?> findallBank();
	
	@Query("select t.bankCode,t.bankName from sa.wthaq.Entities.TBankmaster t")
	List<?> findBankCodeandName();
	
	@Transactional
	@Modifying
	@Query(value="Update T_UserIbanMapping SET status =?1 WHERE login_id = ?2 and iban_no = ?3",nativeQuery=true)
	void updateStatus(String str, String uId,String iban );
}
