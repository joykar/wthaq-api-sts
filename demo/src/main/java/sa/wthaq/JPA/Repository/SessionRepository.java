package sa.wthaq.JPA.Repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sa.wthaq.Entities.TSession;
 

 
@Repository
public interface SessionRepository extends JpaRepository< TSession, Long>   {

	@Query("SELECT u FROM TSession u WHERE u.loginId = ?1 "
			+ "and u.sessionId = ?2 "
			+ "and u.token = ?3 "
			+ "and u.remoteIp = ?4  "
			+ "and u.remoteHost = ?5  "
			+ "and u.remotePort = ?6  "
			+ "and u.remoteUser = ?7  "
			+ "and u.remoteAgent = ?8 "
			+ "and u.sessionStartTime + '00:30:00' > getdate()")
	List < TSession  > findUserSessionByLoginId(String loginId, 
			String sessionId
			, String token
			, String remoteIp
			, String remoteHost
			, String remotePort
			, String remoteUser
			, String remoteAgent );
	
	
	@Transactional
	@Modifying
	@Query(value="INSERT INTO T_Session (login_id,session_id, token, remote_ip, remote_host, remote_port, remote_user, remote_agent ) VALUES (?1,?2,?3, ?4, ?5, ?6, ?7,?8)",nativeQuery=true)
	int saveUserSessionByLoginId(
				String loginId, 
				String sessionId
				, String token
				, String remoteIp
				, String remoteHost
				, String remotePort
				, String remoteUser
				, String remoteAgent );
	
	
	@Transactional
	@Modifying
	@Query(value="UPDATE T_Session " + 
			"set last_access_time = getdate() " + 
			"where login_id     = ?1 " + 
			"and   session_id   = ?2 " + 
			"and   token        = ?3 " + 
			"and   remote_ip    = ?4 " + 
			"and   remote_host  = ?5 " + 
			"and   remote_port  = ?6 " + 
			"and   remote_user  = ?7 " + 
			"and   remote_agent = ?8 ",nativeQuery=true)
	int updateUserSessionByLoginId(
				String loginId, 
				String sessionId
				, String token
				, String remoteIp
				, String remoteHost
				, String remotePort
				, String remoteUser
				, String remoteAgent );
	
	
	
	@Transactional
	@Modifying
	@Query(value="UPDATE T_Session " + 
			"set last_access_time = getdate(),token = NULL " + 
			"where login_id     = ?1 " + 
			"and   session_id   = ?2 " + 
			"and   token        = ?3 " + 
			"and   remote_ip    = ?4 " + 
			"and   remote_host  = ?5 " + 
			"and   remote_port  = ?6 " + 
			"and   remote_user  = ?7 " + 
			"and   remote_agent = ?8 ",nativeQuery=true)
	int logoutUserSessionByLoginId(
				String loginId, 
				String sessionId
				, String token
				, String remoteIp
				, String remoteHost
				, String remotePort
				, String remoteUser
				, String remoteAgent );
	
	
	
}
