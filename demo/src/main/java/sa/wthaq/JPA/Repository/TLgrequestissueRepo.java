package sa.wthaq.JPA.Repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import sa.wthaq.Entities.TLgrequestissue;

public interface TLgrequestissueRepo extends JpaRepository<TLgrequestissue, Integer> {
	TLgrequestissue findByLoginId(String loginid);
	
	@Query(value="SELECT dbo.NumberToWords (?1);",nativeQuery = true)
	public String getNumberInWords(int num);
	
	
	@Transactional
	@Modifying
	@Query(value="Update T_LGRequestIssue SET active_status =?1 WHERE request_id = ?2",nativeQuery=true)
	void setLGRequestIssueStatus(String str, int reqId);
	
	@Query("SELECT u FROM TLgrequestissue u WHERE u.beneficiaryId = ?1")
	public List<TLgrequestissue> getAllLgRequestForBeneficiary(int benId);
	
	@Query("SELECT u FROM TLgrequestissue u WHERE u.requestId = ?1")
	public List<TLgrequestissue> getByRequest(int requestId);
	
	@Query("SELECT u FROM TLgrequestissue u WHERE u.loginId = ?1")
	public List<TLgrequestissue> getByUid(String userId);
	
	@Query("select t.requestId from TLgrequestissue t where t.loginId = ?1 and t.contractValue = ?2")
	int getRequestIdFromAmount(String userId,Double amount);
	
	@Query("select t.requestId from TLgrequestissue t where t.loginId = ?1")
	List<Integer> getRequestIdFromUid(String userId);
	
	@Transactional
	@Modifying
	@Query(value="Update T_LGRequestIssue SET active_status =?1, terms_condition_message = ?2 WHERE request_id = ?3",nativeQuery=true)
	void changeRequestStatusAndTermsMessageByBeneficiaryInReqIssue(String status,String termsNconditionsMessage, String reqId);

	@Transactional
	@Modifying
	@Query(value="Update T_LGRequestIssue SET active_status =?1 WHERE request_id = ?2",nativeQuery=true)
	void changeRequestStatusByBeneficiaryInReqIssue(String pending, String requestId);
}
