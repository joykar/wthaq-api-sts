package sa.wthaq.JPA.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import sa.wthaq.Entities.TLgdetail;

public interface TLgDetailsRepo extends JpaRepository<TLgdetail, String>{
	
	@Modifying
	@Transactional
	@Query(value="INSERT INTO T_LGDETAILS	        ( " + 
			"    case_id          ," + 
			"	lg_no            ," + 
			"	lg_date          ," + 
			"	lg_amount        ," + 
			"	request_id       ," + 
			"	bank_code        ," + 
			"	branch_name      ," + 
			"	lg_issue_date    ," + 
			"	lg_validity_date ," + 
			"	user_id          ," + 
			"	role_id          ," + 
			"	beneficiary_id   ," + 
			"	supplier_id      ," + 
			"	project_id       ," + 
			"	project_name     ," + 
			"	currency_code    ," + 
			"	service_catalog_id ," + 
			"	zakat_start_date ," + 
			"	zakat_end_date   ," + 
			"	from_date        ," + 
			"	trans_date       ," + 
			"	terms_condition_type	," +
			"	terms_condition_message	)" +
			"VALUES ( ?1 ,?2,getDate(),?3," + 
			"         ?4,?5,?6," + 
			"		 ?7,?8,?9," + 
			"		 ?10,?11,?12," + 
			"		 ?13,?14," + 
			"		 ?15,NULL," + 
			"		 ?16,?17," + 
			"		 ?18,getDate(),?19,?20 )", nativeQuery=true)
void saveLgDetails(String case_id,
		String lg_no,
		String lg_amount,
		String request_id,
		String bank_code,
		String brnach_name
		,String lg_issue_date,
		String lg_validity_date,String login_id,int role_id,int beneficiary_id,int supplier_id
		,String project_id,String project_name,String currency_code,String zakat_start_date,String zakat_end_date,String from_date
		,String terms_condition_type,String termsNConditionsmessage);

}
