package sa.wthaq.JPA.Repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sa.wthaq.Entities.TCurrencytypemaster;

public interface TCurrencytypemasterRepo extends JpaRepository<TCurrencytypemaster, String> {
	
	@Query("select t.currencyCode,t.currency from TCurrencytypemaster t")
	List<?> findAllCurrency();
}
