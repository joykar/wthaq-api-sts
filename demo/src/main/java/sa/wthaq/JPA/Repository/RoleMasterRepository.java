package sa.wthaq.JPA.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sa.wthaq.Entities.TRolemaster;
 
 
@Repository
//@EnableJpaRepositories(basePackages="eLG", entityManagerFactoryRef="entityManagerFactory")
//@EntityScan(basePackages ="com.wethaq.elog.Entities")
public interface RoleMasterRepository extends JpaRepository< TRolemaster, Integer> {
	
	 @Query("from TRolemaster")
	 List < TRolemaster  > findAll();
	 
	 
	 List < TRolemaster  > findByRoleId(int id );
	
	 
	 
	 /*  JK   */
	 
	 @Query("select u from TRolemaster u where u.roleDesc =?1")
     List < TRolemaster  > findByRoleDesc(String d );

	 
	 
	@Query(value="select r.role_desc from T_RoleMaster r inner join T_UserMaster u on u.role_id=r.role_id and u.login_id =?1",nativeQuery = true)
	 List<Object[]> searchRoleUsingLoginId(String d );
	 
}