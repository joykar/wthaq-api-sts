package sa.wthaq.JPA.Repository;
 
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import sa.wthaq.Entities.TBeneficiarydetail;
import sa.wthaq.Entities.TUnRegisteredBen;

public interface BeneficiaryMasterRepository extends JpaRepository<TBeneficiarydetail, Long> {
	
	 @Query("select tb.beneficiaryId,tb.beneficiaryName from TBeneficiarydetail tb")
	 List <?> findBeneficiary();
	 
	 @Query("select tb.beneficiaryId,tb.beneficiaryName from TBeneficiarydetail tb where tb.beneficiaryName = ?1")
	 List <String[]> findBeneficiaryList(String entityName);
	 
	 @Query("select tb.beneficiaryId,tb.beneficiaryName from TBeneficiarydetail tb where tb.benStatusFlag = ?1")
	 List <?> findBeneficiaryWRTOStatus(String status);
	 
	 
	 //benUid respective benName
	 @Query("select tb.beneficiaryId,tb.beneficiaryName from TBeneficiarydetail tb where tb.beneficiaryUid = ?1")
	 List <?> findBeneficiaryWRTOUID(String ben_UID);
	 
	/*
	 * @Query("select tb from TBeneficiarydetails tb WHERE tb.loginId = ?1")
	 * TBeneficiarydetail getByloginId(String loginId);
	 */
	 
	 @Transactional
	 @Modifying
	 @Query(value="Update T_Beneficiarydetails SET ben_status_flag =?1 WHERE beneficiary_name = ?2",nativeQuery = true)
	 void getBybenName(String status,String benName);
	 
	 @Query("select tb from TBeneficiarydetail tb WHERE tb.beneficiaryName = ?1")
	 TBeneficiarydetail getUserByName(String benName);

	 @Query(value ="select a.beneficiary_name,CONCAT(a.beneficiary_addr1,' ',a.beneficiary_addr2,' ,', a.city, ' , ',a.country) Address," + 
	 		"a.beneficiary_UID,a.login_id national_id,a.rep_name, a.rep_email,a.rep_mobile_no " + 
	 		"from t_beneficiarydetails a " + 
	 		"inner join t_usermaster b on a.login_id = b.login_id " + 
	 		"where b.role_id = 2" + 
	 		" and   UPPER(b.user_status) = 'G_CC_PENDING'" + 
	 		"",nativeQuery = true)
	List<String[]> getBeneficiaryUnverifiedEntityList();

	
	
	
}