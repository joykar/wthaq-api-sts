package sa.wthaq.JPA.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sa.wthaq.Entities.TBankmaster;
import sa.wthaq.Entities.T_WriteUpType;

public interface T_WriteUpTypeRepo extends JpaRepository<T_WriteUpType, String>{

}
