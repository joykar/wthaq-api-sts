package sa.wthaq.JPA.Repository;
 
import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sa.wthaq.DTO.EntityModelGov;
import sa.wthaq.DTO.EntityModelPrivate;
import sa.wthaq.Entities.TSupplierdetail;
import sa.wthaq.Entities.TUsermaster;

  
 
@Repository
//@EnableJpaRepositories(basePackages="eLG", entityManagerFactoryRef="entityManagerFactory")
//@EntityScan(basePackages ="com.wethaq.elog.Entities")
public interface SupplierMasterRepository extends JpaRepository< TSupplierdetail, Long>/*, SupplierMasterRepositoryCustom */{

	//List<T_SupplierDetail> findSuppliers();
	
	// @Query("from T_SupplierDetail tsd where tsd.crnNo = ?")
	// List < T_SupplierDetail  > findAll(int crn); 
	
	@Query("SELECT u FROM TSupplierdetail u WHERE u.crnNo = ?1 ")
	List < TSupplierdetail  > findSupplierCRNByCRN(BigDecimal crn);
	
	@Query("SELECT u FROM TSupplierdetail u WHERE u.loginId = ?1 ")
	List < TSupplierdetail  > findSupplierByLoginId(String loginId);
	
	@Query("SELECT u FROM TSupplierdetail u WHERE u.crnNo = ?1 ")
	List < TSupplierdetail  > findSupplierByCRN(BigDecimal crnNo);
	
	  @Modifying
      @Transactional
	@Query(value="INSERT Into T_USERIBANMAPPING(login_id,iban_no,bank_code) values(?1,?2,?3)",nativeQuery = true)
	int saveSupplierIban(String loginId,String ibanNo,String bankCode);
	
	  @Query(value ="SELECT u FROM T_SupplierDetails u WHERE u.login_id = ?1",nativeQuery = true)
		List < TSupplierdetail  > findSupplierCrnByLogin(String loginId);
	  
	  @Transactional
		@Modifying
		@Query(value="UPDATE T_SupplierDetails SET supplier_status_flag = ?1 WHERE login_id = ?2",nativeQuery=true)
		void setCCVerificationStatusOnSupplier(String rep_status, String user_Id);

	  
	@Query(value ="select ROW_NUMBER() OVER ( ORDER BY supplier_name) " + 
			" row_num  ,supplier_name as companyName, cr_no as CRN, 'Representative' as Authority,supplier_status_flag AS responseCode from t_supplierdetails " + 
			"where login_id = ?1",nativeQuery = true)
	List<String[]> getCrnFromLogin(String loginId);
	
	 @Query(value="select a.supplier_name entity_name,CONCAT(a.supplier_addr1,' ',a.supplier_addr2," + 
	 		"CASE WHEN a.state is not null then ',' ELSE '' END , " + 
	 		"CASE WHEN a.state is not NULL THEN a.state ELSE ''END, " + 
	 		"CASE WHEN a.COUNTRY is not null then ',' END, " + 
	 		"CASE WHEN a.country is not null THEN a.country ELSE ''END) Address," + 
	 		"CONCAT(a.rep_name,a.rep_last_name) Rep_name," + 
	 		"a.rep_mobile_no,a.rep_email,a.cr_no,a.login_id national_id" + 
	 		" from t_supplierdetails a" + 
	 		" inner join t_usermaster b on a.login_id = b.login_id" + 
	 		" where b.role_id = 1" + 
	 		" and   UPPER(b.user_status) = 'CC_PENDING' " + 
		  		" ",nativeQuery = true)
		List<EntityModelPrivate> getSupplierUnverifiedEntityList();

		  
		  
		  @Query(value="select a.beneficiary_name,CONCAT(a.beneficiary_addr1,' ',a.beneficiary_addr2," + 
		  		"CASE WHEN a.city is not null then ',' ELSE '' END," + 
		  		"CASE WHEN a.city is not null then a.city ELSE '' END," + 
		  		"CASE WHEN a.country is not null then ',' ELSE '' END," + 
		  		"CASE WHEN a.country is not null THEN a.country ELSE '' END) Address," + 
		  		"a.beneficiary_UID,a.login_id national_id,a.rep_name, a.rep_email,a.rep_mobile_no " + 
		  		" from t_beneficiarydetails a " + 
		  		"inner join t_usermaster b on a.login_id = b.login_id " + 
		  		"where b.role_id = 2 " + 
		  		" and   UPPER(b.user_status) = 'G_CC_PENDING' " + 
		  		" " + 
		  		" " ,nativeQuery = true)
		List<EntityModelGov> getGovUnverifiedEntityList();
		  
		  
		  @Query(value="select a.supplier_name entity_name,CONCAT(a.supplier_addr1,' ',a.supplier_addr2, " + 
			  		"CASE when a.state is not null then ' ,' ELSE '' END , " + 
			  		"CASE when a.state is not null then a.state ELSE '' END, " + 
			  		"CASE WHEN a.COUNTRY is not null then ' ,' END, " + 
			  		"CASE WHEN a.country is not null then a.country ELSE '' END) Address,CONCAT(a.rep_name,a.rep_last_name) Rep_name, " + 
			  		"a.rep_mobile_no,a.rep_email,a.cr_no,a.cr_type,a.cr_status, " + 
			  		"CONVERT(VARCHAR(10), a.cr_issue_date, 103)  cr_issue_date, " + 
			  		"CONVERT(VARCHAR(10), a.cr_expiry_date, 103) cr_expiry_date, " + 
			  		"a.login_id national_id, a.rep_national_id, " + 
			  		"a.comm_reg_doc_name,a.comm_reg_doc_path,a.auth_lettter_doc_name,a.auth_lettter_doc_path, " + 
			  		"a.leg_agree_doc_name,a.leg_agree_doc_path,a.rep_dob " + 
			  		"from t_supplierdetails a " + 
			  		"inner join t_usermaster b on a.login_id = b.login_id " + 
			  		"where b.role_id = 1 " + 
			  		"and   UPPER(b.user_status) = 'CC_PENDING' " + 
			  		"and  a.login_id = ?1 " + 
			  		"and  a.cr_no = ?2  " + 
				  		" " ,nativeQuery = true)
			List<EntityModelPrivate> getPriEntityDetails(String nid, BigDecimal crn);

			  
			  
			  @Query(value="select a.beneficiary_name ,CONCAT(a.beneficiary_addr1,' ',a.beneficiary_addr2, " + 
			  		"CASE WHEN a.city is not NULL THEN ',' ELSE '' END, " + 
			  		"CASE WHEN a.city is not NULL THEN a.city ELSE '' END, " + 
			  		"CASE WHEN a.country is not NULL THEN ',' ELSE '' END, " + 
			  		"CASE WHEN a.country is not NULL THEN a.country ELSE '' END) Address, " + 
			  		"a.beneficiary_UID,a.login_id national_id,a.rep_name, a.rep_email,a.rep_mobile_no , " + 
			  		"CONVERT(VARCHAR(10), a.beneficiary_reg_date, 103)  beneficiary_reg_date,a.beneficiary_email, " + 
			  		"a.national_id_doc_name,a.national_id_doc_path, " + 
			  		"a.leg_auth_doc_name,a.leg_auth_doc_path, " + 
			  		"a.leg_agree_doc_name,a.leg_agree_doc_path " + 
			  		"from t_beneficiarydetails a " + 
			  		"inner join t_usermaster b on a.login_id = b.login_id " + 
			  		"where b.role_id = 2 " + 
			  		"and   UPPER(b.user_status) = 'G_CC_PENDING' " + 
			  		"and  a.login_id = ?1 " + 
			  		"and a.beneficiary_uid = ?2  " + 
					  		" " ,nativeQuery = true) 
			List<EntityModelGov> getGovEntityDetails(String nId, String uid);
				

}