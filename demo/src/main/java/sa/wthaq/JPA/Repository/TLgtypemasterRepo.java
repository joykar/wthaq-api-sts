package sa.wthaq.JPA.Repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import sa.wthaq.Entities.TLgtypemaster;

public interface TLgtypemasterRepo extends JpaRepository<TLgtypemaster, String> {
	
	@Query("select t.lgTypeId,t.lgType from TLgtypemaster t")
	List<?> findAllLgType();

	@Query("select t.lgTypeId from TLgtypemaster t where t.lgType = ?1")
	int findByLgType(String lgType);
}
