package sa.wthaq.JPA.Repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import sa.wthaq.Entities.TLg;
import sa.wthaq.Entities.TServicerequest;

public interface TServicerequestRepo extends JpaRepository<TServicerequest, Integer> {

	@Query(value="SELECT u FROM TServicerequest u WHERE u.requestId = ?1 and u.requestStatusFlag = ?2")
	List<TServicerequest> getByStatus(int reqId,String status);
	
	@Transactional
	@Modifying
	@Query(value="Update T_ServiceRequest SET request_status_flag =?1 WHERE request_id = ?2",nativeQuery=true)
	void changeRequestStatusByBeneficiaryamdRequest(String status, String reqId);
	
	
	@Transactional
	@Modifying
	@Query(value="Update T_ServiceRequest SET request_status_flag =?1 /*WHERE request_id = ?2*/",nativeQuery=true)
	void changeRequestStatusByBeneficiary(String status/*, String reqId*/);
	
	@Transactional
	@Modifying
	@Query(value="Update T_ServiceRequest SET request_status_flag =?1, terms_condition_message = ?2 WHERE request_id = ?3",nativeQuery=true)
	void changeRequestStatusAndTermsMessageByBeneficiary(String status,String termsNconditionsMessage, String reqId);
	
	
	@Query(value="SELECT tl FROM TServicerequest tl WHERE tl.requestId = ?1")
	public TServicerequest getAllFromRequestId(int requestId);
	

}
