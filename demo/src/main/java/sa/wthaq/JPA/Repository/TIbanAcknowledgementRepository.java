package sa.wthaq.JPA.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sa.wthaq.Entities.TIbanAcknowledgement;



public interface TIbanAcknowledgementRepository extends JpaRepository<TIbanAcknowledgement, Long>{

}
