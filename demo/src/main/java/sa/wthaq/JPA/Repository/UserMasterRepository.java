package sa.wthaq.JPA.Repository;

 
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.transaction.Transactional;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import sa.wthaq.DTO.UserDTO;
import sa.wthaq.Entities.TUsermaster;
import sa.wthaq.Entities.TUsermaster2;
import sa.wthaq.models.LoginModel;
 
@Repository
//@EnableJpaRepositories(basePackages="eLG", entityManagerFactoryRef="entityManagerFactory")
//@EntityScan(basePackages ="com.wethaq.elog.Entities")
public interface UserMasterRepository extends JpaRepository< TUsermaster, Long> {
	
	//@Query("from UserMasterEntity")
	List < TUsermaster  > findAll();
	
	@Query("SELECT u FROM TUsermaster u WHERE u.loginId = ?1 and u.password = ?2")
	List < TUsermaster  > findUserByLoginIdAndPass(String loginId, String password);

	@Query("SELECT u FROM TUsermaster u WHERE u.loginId = ?1")
	List < TUsermaster  > findUserByLoginId(String loginId);
	
	@Query("SELECT u.loginId FROM TUsermaster u WHERE u.userId = ?1 and u.roleId = ?2")
	 String fetchLoginIdFromUID(int uId,int rId);
	
	@Query(value="(SELECT a.supplier_id as id,supplier_name as name,rep_email  as officialemail FROM t_supplierdetails a INNER JOIN t_usermaster b ON b.login_id = a.login_id WHERE a.login_id = ?1) UNION (SELECT a.beneficiary_id as id ,beneficiary_name as name ,beneficiary_email as officialemail  FROM t_beneficiarydetails a INNER JOIN t_usermaster b ON b.login_id = a.login_id WHERE a.login_id = ?2)", nativeQuery=true)
	List<Object[]> findOfficialEmailByUserId(String loginId, String loginId2);
	
	@Query(value="SELECT a.supplier_id,"
			+ "a.supplier_name, "
			+ "a.contact_info, "
			+ "b.login_id, "
			+ "b.user_name, " 
			+ "b.role_id, "+
			"           a.national_id, "
			+ "a.national_id_doc_name,  " + 
			"			rep_email, "
			+ "rep_mobile_no, "
			+ "comm_reg_doc_name, "
			+ "auth_lettter_doc_name, " + 
			"			leg_agree_doc_name,"
			+ "NULL leg_auth_doc_name, "
			+ "cr_no  " + 
			"			FROM t_supplierdetails a" + 
			"			INNER JOIN T_UserMaster b ON b.login_id = a.login_id" + 
			"			WHERE a.login_id = ?1" + 
			"			UNION  " + 
			"			SELECT  " + 
			"			a.beneficiary_id,a.beneficiary_name,a.contact_info,b.login_id,b.user_name,b.role_id," + 
			"			NULL national_id, a.national_id_doc_name, " + 
			"			a.rep_email,a.rep_mobile_no,NULL comm_reg_doc_name,NULL auth_lettter_doc_name,  " + 
			"			a.leg_agree_doc_name,a.leg_auth_doc_name, NULL cr_no " + 
			"			FROM t_beneficiarydetails  a" + 
			"			INNER JOIN T_UserMaster b ON b.login_id = a.login_id" + 
			"			WHERE a.login_id = ?2 " + 
			"", nativeQuery=true)
	List<Object[]> findProfileInfoByUserId(String loginId, String loginId2);
	
	
	
	@Query(value="SELECT  b.role_id, b.role_desc " + 
			"FROM t_usermaster a " + 
			"INNER JOIN t_rolemaster b ON a.role_id= b.role_id " + 
			"WHERE a.login_id =  ?1 "  
			, nativeQuery=true)
	List<Object[]> findRoleByUserId(String loginId);
	
	
		
	@Transactional
	@Modifying
	@Query(value="Update T_UserMaster SET user_status =?1 WHERE login_id = ?2",nativeQuery=true)
	void setCCVerificationStatus(String rep_status, String user_Id);

	@Transactional
	@Modifying
	@Query(value="Update T_UserMaster SET user_status =?1 WHERE login_id = ?2",nativeQuery=true)
	void setBankVerificationStatus(String str, String uId);
	
	
	@Transactional
	@Modifying
	@Query(value="Update T_SupplierDetails SET supplier_status_flag =?1 WHERE login_id = ?2",nativeQuery=true)
	void setSupplierStatus(String status, String lid);
	
	@Transactional
	@Modifying
	@Query(value="Update T_UserMaster SET password =?1 WHERE login_id = ?2",nativeQuery=true)
	void updatePassword(String pass, String uId);

	
	@Transactional
	@Modifying
	
	@Query(value="INSERT INTO T_UserIbanMapping (login_id,iban_no,bank_code) VALUES (?1,?2,?3) ",nativeQuery=true)
	void saveSupplierIBAN(String loginId, String ibanNo, String bcode);

	TUsermaster findByLoginId(String loginId);
	
	@Transactional
	@Modifying
	@Query(value="Update T_UserMaster SET user_status =?1 WHERE login_id = ?2",nativeQuery=true)
	void setBeneficiaryStatus(String status, String lid);

	@Query(value="(SELECT  admin_id as id, admin_name as name, admin_email  as officialemail from  T_AdminRegistration where login_id=?1)", nativeQuery=true)
	List<Object[]> findOfficialEmailByAdminId(String loginId);


	@Transactional
	@Modifying
	@Query(value="Update T_UserMaster SET password =?1 WHERE login_id = ?2",nativeQuery=true)
	void changePasswordAfterRequest(String rep_status, String user_Id);
	
	
	
	
	/* JK  */
	

/*  please check */  
//	TUsermaster findByLoginId(String loginId);

	
	@Query(value="SELECT CASE WHEN (SELECT   count(request_id)  FROM T_SERVICEREQUEST     WHERE  login_id  =  ?1  AND request_id=   ?2) <1 THEN 0 ELSE 1 END AS isExists ",nativeQuery=true)
	int isRequestIdValid(String uid, String rid);
	

	
	
	/*   JK  */
	
}




