package sa.wthaq.JPA.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sa.wthaq.Entities.TLg;
import sa.wthaq.Entities.TUsermaster;

@Repository
public interface TLgRepository extends JpaRepository< TLg, Long>{

	@Modifying
	@Transactional
	@Query(value="INSERT INTO T_LG (lg_no,"
			+ "lg_type_id,"
			+ "terms_condition_type ,"
			+ "contract_value ,"
			+ "lg_value,"
			+ "currency_code  ,"
			+ "project_id     ,"
			+ "project_name   ,"
			+ "lg_issue_date  ,"
			+ "lg_validity_date,"
			+ "lg_doc_path    ,"
			+ "lg_doc_name    ,"
			+ "zakat_start_date  ,"
			+ "zakat_end_date ,"
			+ "byan_no        ,"
			+ "beneficiary_id ,"
			+ "supplier_id    ,"
			+ "request_id     ,"
			+ "active_status  ,"
			+ "lg_issued_by ,"
			+ "terms_condition_message ," 
			+ "role_id )"
			
+ "VALUES ( ?1 , ?2, ?3,"
+ " ?4, ?5,"
+ "				 ?6,?7,?8,"
+ "				 ?9,?10,?11,"
+ "				 ?12,?13,?14,"
+ "				 ?15,?16,?17,?18,"
+ "				 'ACTIVE',?19,?20,?21)", nativeQuery=true)
void saveLg(String lg_no , String lg_typ_id, String terms_condition_type,String contract_value
        ,String lg_value, String currency_code,
		 String project_id,String project_name,String lg_issue,
		 String lg_validity_date,String lg_doc_path,String lg_doc_name,
		 String zakat_start_date,String zakat_end_date,String byan_no,
		 String beneficiary_id, String supplier_id,int request_id,
		 String login_id,String termsNconditionsMessage,int roleId);
	
	
	@Query(value="SELECT tl.case_id FROM T_LG tl WHERE tl.request_id = ?1", nativeQuery=true)
	public int getCaseIdFromReq(int reqId);
	
	@Query(value="SELECT tl FROM TLg tl WHERE tl.requestId = ?1")
	public TLg getAllFromRequestId(String requestId);

}
