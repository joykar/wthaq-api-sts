package sa.wthaq.JPA.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import sa.wthaq.Entities.TBankmaster;

public interface BankMasterRepository extends JpaRepository<TBankmaster, String>{
	
	TBankmaster findByBankCode(String bankCode);
	
	List<TBankmaster> findAll();
	
	TBankmaster findByBankName(String bankName);

}
