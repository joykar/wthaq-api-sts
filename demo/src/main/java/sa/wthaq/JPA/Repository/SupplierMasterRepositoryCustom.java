package sa.wthaq.JPA.Repository;

import java.util.List;

import sa.wthaq.Entities.TSupplierdetail;

public interface SupplierMasterRepositoryCustom {

	TSupplierdetail findSupplierCRNWise(String crnNo);
	Boolean checkIfCRNOK(String crnNo);
	List<TSupplierdetail> findSuppliers();
}
