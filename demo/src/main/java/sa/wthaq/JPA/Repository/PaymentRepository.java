package sa.wthaq.JPA.Repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import sa.wthaq.Entities.TELGPaymentDetail;

 
 
public interface PaymentRepository  extends JpaRepository< TELGPaymentDetail, Long> {

	
	
	@Query(value="SELECT CASE WHEN (SELECT   count(payment_id)  FROM t_elgpaymentdetail WHERE  payment_id  =  ?1  AND signature=   ?2) <1 THEN 0 ELSE 1 END AS isExists ",nativeQuery=true)
	int fetchPaymentDetailsByPIDandHash(int paymentID, String signature);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value="UPDATE TELGPaymentDetail u SET u.esponseMessage=?1, u.payment_status=?2, u.responseCode=?3 WHERE u.payment_id =?4 and u.signature = ?5")
	int savePaymentDetailsByPIDandHash(String esponseMessage, String payment_status, String responseCode, int payment_id, String signature);
	
}
